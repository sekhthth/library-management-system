-- MySQL dump 10.13  Distrib 8.0.15, for Win64 (x86_64)
--
-- Host: localhost    Database: librarymanagement
-- ------------------------------------------------------
-- Server version	8.0.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `book` (
  `id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `borrowed` int(11) DEFAULT NULL,
  `cover_image` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `manufacturer` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `timePrint` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `ymanufacture` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8tbqyqnmcls0mij52knnk1ojr` (`category_id`),
  CONSTRAINT `FK_8tbqyqnmcls0mij52knnk1ojr` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES ('ACCOUNTING',0,'/resources/image/book/ACCOUNTING.jpg','HCMIU','Test 1',150000,10,'2','1999','BA'),('BA',0,'/resources/image/book/INTRO BA.jpg','HCMIU','Test 16',150000,10,'2','1999','BA'),('BC',0,'/resources/image/book/Business Communication.jpg','HCMIU','Test 4',150000,10,'2','1999','BA'),('BE',0,'/resources/image/book/Business ethics.jpg','HCMIU','Test 5',150000,10,'2','1999','BA'),('BIOCAL',0,'/resources/image/book/BIOCALCULUS.jpg','HCMIU','Biocalculus',150000,10,'2','1999','BT'),('BIOCHEM',0,'/resources/image/book/BIOCHEMISTRY.jpg','HCMIU','Biochemistry',150000,10,'2','1999','BT'),('BIOLOGY',0,'/resources/image/book/BIOLOGY.jpg','HCMIU','Biology',150000,10,'2','1999','BT'),('BRAND',0,'/resources/image/book/BRAND MANA.jpg','HCMIU','Test 2',150000,10,'2','1999','BA'),('BRM',0,'/resources/image/book/BRM.jpg','HCMIU','Test 3',150000,10,'2','1999','BA'),('CAL1',0,'/resources/image/book/Calculus1.jpg','HCMIU','Calculus 1',150000,10,'2','1999','MA'),('CAL2',0,'/resources/image/book/CAL 2.jpg','HCMIU','Calculus 2',150000,10,'2','1999','MA'),('CELLBIO',0,'/resources/image/book/CELL BIOLOGY.jpg','HCMIU','Essential cell biology',150000,10,'2','1999','BT'),('CF',0,'/resources/image/book/CORPORATE FINANCE.jpg','HCMIU','Test 7',150000,10,'2','1999','BA'),('CONSUMBEHAV',0,'/resources/image/book/CONSUMER BEHAVIOR.jpg','HCMIU','Test 6',150000,10,'2','1999','BA'),('CRITICAL',0,'/resources/image/book/CRITICAL THINKING.jpg','HCMIU','Critical Thinking',150000,10,'2','1999','PE'),('DLD',0,'/resources/image/book/DIGITAL LOGIC DESIGN.jpg','HCMIU','Degital Design Principles & Practices',150000,10,'2','1999','EE'),('EC1',0,'/resources/image/book/EC 1.jpg','HCMIU','Test 8',150000,10,'2','1999','BA'),('EC2',0,'/resources/image/book/EC 2.jpg','HCMIU','Test 9',150000,10,'2','1999','BA'),('ES',0,'/resources/image/book/ES.jpg','HCMIU',' ENVIROMENTAL SCIENCE ',150000,10,'2','1999','BT'),('ES2',0,'/resources/image/book/ES 2.jpg','HCMIU',' ENVIROMENTAL SCIENCE 2',150000,10,'2','1999','BT'),('FFPE',0,'/resources/image/book/Fundamentals of Food Process Engineering.jpg','HCMIU',' Fundamentals of Food Process Engineering',150000,10,'2','1999','BT'),('FM',0,'/resources/image/book/FM.jpg','HCMIU','Test 10',150000,10,'2','1999','BA'),('FRANCH',0,'/resources/image/book/FRANCHISING.jpg','HCMIU','Test 11',150000,10,'2','1999','BA'),('GENETICS',0,'/resources/image/book/GENETICS.jpg','HCMIU',' GENETICS',150000,10,'2','1999','BT'),('GEO',0,'/resources/image/book/GEO.jpg','HCMIU','The geography of the world economy',150000,10,'2','1999','PE'),('HRM',0,'/resources/image/book/HRM.jpg','HCMIU','Test 12',150000,10,'2','1999','BA'),('HUMANPHY',0,'/resources/image/book/HUMAN PHYSIOLOGY.jpg','HCMIU','Introduction to Human Physiology',150000,10,'2','1999','BT'),('IB',0,'/resources/image/book/International Business.jpg','HCMIU','Test 15',150000,10,'2','1999','BA'),('IE',0,'/resources/image/book/IE.jpg','HCMIU','Test 13',150000,10,'2','1999','BA'),('IFM',0,'/resources/image/book/IFM.jpg','HCMIU','Test 14',150000,10,'2','1999','BA'),('INTROFT',0,'/resources/image/book/Intro to Food Toxicology.jpg','HCMIU',' Intro to Food Toxicology',150000,10,'2','1999','BT'),('IS',0,'/resources/image/book/INTRO TO IS.jpg','HCMIU','Test 17',150000,10,'2','1999','BA'),('JAVA',0,'/resources/image/book/JAVA.jpg','HCMIU','Java How to program',150000,10,'2','1999','IT'),('LOG',0,'/resources/image/book/INTRO TO SUPPLY CHAIN MANA.jpg','HCMIU','An introduction to supply chain management',150000,10,'2','1999','ISE'),('MAC',0,'/resources/image/book/MAC LENIN.jpg','HCMIU','Nhung nguyen ly co ban cua chu nghia Mac - Lenin',150000,10,'2','1999','PE'),('MACRO2',0,'/resources/image/book/PRINCIPLE OF MACROECONOMICS.jpg','HCMIU','Test 24',150000,10,'2','1999','BA'),('MANA',0,'/resources/image/book/INTRO TO MANA.jpg','HCMIU','Test 18',150000,10,'2','1999','BA'),('MATH',0,'/resources/image/book/MATHEMATICS.jpg','HCMIU','Mathematics',150000,10,'2','1999','MA'),('MICRO',0,'/resources/image/book/MICROBIOLOGY.jpg','HCMIU','Essential Microbiology',150000,10,'2','1999','BT'),('MICRO1',0,'/resources/image/book/Principle of Economics.jpg','HCMIU','Test 23',150000,10,'2','1999','BA'),('MICRO3',0,'/resources/image/book/MICROBIOLOGY.jpg','HCMIU','Test 26',150000,10,'2','1999','BA'),('MIS',0,'/resources/image/book/MIS.jpg','HCMIU','Test 19',150000,10,'2','1999','BA'),('MKT',0,'/resources/image/book/Principle of Marketing.jpg','HCMIU','Test 25',150000,10,'2','1999','BA'),('NEGO',0,'/resources/image/book/NEGO.jpg','HCMIU','Test 20',150000,10,'2','1999','BA'),('OB',0,'/resources/image/book/OB.jpg','HCMIU','Test 21',150000,10,'2','1999','BA'),('PHYSICS',0,'/resources/image/book/PHYSICS.jpg','HCMIU','Fundamentals of Physics',150000,10,'2','1999','PH'),('PM',0,'/resources/image/book/PROJECT MANA.jpg','HCMIU','Test 27',150000,10,'2','1999','BA'),('POM',0,'/resources/image/book/POM.jpg','HCMIU','Test 22',150000,10,'2','1999','BA'),('PROMANA',0,'/resources/image/book/PROMANA.jpg','HCMIU','Test 28',150000,10,'2','1999','BA'),('PSYCHO',0,'/resources/image/book/PSYCHO.jpg','HCMIU','Test 29',150000,10,'2','1999','BA'),('QM',0,'/resources/image/book/QM.jpg','HCMIU','Test 30',150000,10,'2','1999','BA'),('QM2',0,'/resources/image/book/QM 2.jpg','HCMIU','Test 30',150000,10,'2','1999','BA'),('RISK',0,'/resources/image/book/RISK MANA.jpg','HCMIU','Test 31',150000,10,'2','1999','BA'),('SALE',0,'/resources/image/book/SALE MANA.jpg','HCMIU','Test 32',150000,10,'2','1999','BA'),('STAT',0,'/resources/image/book/STAT.jpg','HCMIU','Test 33',150000,10,'2','1999','BA'),('STRATEGIC',0,'/resources/image/book/STRATEGIC MANA.jpg','HCMIU','Test 34',150000,10,'2','1999','BA'),('WRITINGAE',0,'/resources/image/book/WRITING AE.jpg','HCMIU','Writing Academic English',150000,10,'2','1999','EN');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrow_log`
--

DROP TABLE IF EXISTS `borrow_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `borrow_log` (
  `id` int(11) NOT NULL,
  `borrow_date` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `return_date` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `person_id` varchar(12) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `staff_id` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bfo9hlnm3rgy56lsclvqimhg` (`person_id`),
  KEY `FK_svcfdbw1kd2gqryyag1da068c` (`staff_id`),
  CONSTRAINT `FK_bfo9hlnm3rgy56lsclvqimhg` FOREIGN KEY (`person_id`) REFERENCES `borrower` (`person_id`),
  CONSTRAINT `FK_svcfdbw1kd2gqryyag1da068c` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrow_log`
--

LOCK TABLES `borrow_log` WRITE;
/*!40000 ALTER TABLE `borrow_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `borrow_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrow_log_detail`
--

DROP TABLE IF EXISTS `borrow_log_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `borrow_log_detail` (
  `penalty` int(11) DEFAULT NULL,
  `reserved` int(11) DEFAULT NULL,
  `return_date` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `borrow_log_id` int(11) NOT NULL,
  `book_id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`book_id`,`borrow_log_id`),
  KEY `FK_3gycidr43x73jhj9a14lqg3eb` (`borrow_log_id`),
  CONSTRAINT `FK_3gycidr43x73jhj9a14lqg3eb` FOREIGN KEY (`borrow_log_id`) REFERENCES `borrow_log` (`id`),
  CONSTRAINT `FK_gh5otfdndwheryu1j7u1mhxcl` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrow_log_detail`
--

LOCK TABLES `borrow_log_detail` WRITE;
/*!40000 ALTER TABLE `borrow_log_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `borrow_log_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrower`
--

DROP TABLE IF EXISTS `borrower`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `borrower` (
  `person_id` varchar(12) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrower`
--

LOCK TABLES `borrower` WRITE;
/*!40000 ALTER TABLE `borrower` DISABLE KEYS */;
INSERT INTO `borrower` VALUES ('BABAIU17030'),('BABAIU17031'),('BABAIU17032'),('BABAIU17033'),('BABAIU17034'),('BABAIU17035'),('BABAIU17036'),('BABAIU17037'),('BABAIU17038'),('BABAIU17039'),('BTBTIU17030'),('BTBTIU17031'),('BTBTIU17032'),('BTBTIU17033'),('BTBTIU17034'),('BTBTIU17035'),('BTBTIU17036'),('BTBTIU17037'),('BTBTIU17038'),('BTBTIU17039'),('CECEIU17030'),('CECEIU17031'),('CECEIU17032'),('CECEIU17033'),('CECEIU17034'),('CECEIU17035'),('CECEIU17036'),('CECEIU17037'),('CECEIU17038'),('CECEIU17039'),('EEEEIU17030'),('EEEEIU17031'),('EEEEIU17032'),('EEEEIU17033'),('EEEEIU17034'),('EEEEIU17035'),('EEEEIU17036'),('EEEEIU17037'),('EEEEIU17038'),('EEEEIU17039'),('ITITIU17030'),('ITITIU17031'),('ITITIU17032'),('ITITIU17033'),('ITITIU17034'),('ITITIU17035'),('ITITIU17036'),('ITITIU17037'),('ITITIU17038'),('ITITIU17039'),('LELEIU00001'),('LELEIU00002'),('LELEIU00003'),('LELEIU00004'),('LELEIU00005'),('LELEIU00006'),('LELEIU00007'),('LELEIU00008'),('LELEIU00009'),('LELEIU00010'),('LELEIU00011'),('LELEIU00012'),('LELEIU00013'),('LELEIU00014'),('LELEIU00015'),('LELEIU00016'),('LELEIU00017'),('LELEIU00018'),('LELEIU00019'),('LELEIU00020'),('LELEIU00021'),('LELEIU00022'),('LELEIU00023'),('LELEIU00024'),('LELEIU00025'),('LELEIU00026'),('LELEIU00027'),('LELEIU00028'),('LELEIU00029'),('LELEIU00030'),('LELEIU00031'),('LELEIU00032'),('LELEIU00033'),('LELEIU00034'),('LELEIU00035'),('LELEIU00036'),('LELEIU00037'),('LELEIU00038'),('LELEIU00039'),('LELEIU00040'),('LELEIU00041'),('LELEIU00042'),('LELEIU00043'),('LELEIU00044'),('LELEIU00045'),('LELEIU00046'),('LELEIU00047'),('LELEIU00048'),('LELEIU00049'),('LELEIU00050'),('LELEIU00051'),('LELEIU00052'),('LELEIU00053'),('LELEIU00054'),('LELEIU00055'),('LELEIU00056'),('LELEIU00057'),('LELEIU00058'),('LELEIU00059'),('LELEIU00060'),('LELEIU00061'),('LELEIU00062'),('LELEIU00063'),('MAMAIU17030'),('MAMAIU17031'),('MAMAIU17032'),('MAMAIU17033'),('MAMAIU17034'),('MAMAIU17035'),('MAMAIU17036'),('MAMAIU17037'),('MAMAIU17038'),('MAMAIU17039'),('SESEIU17030'),('SESEIU17031'),('SESEIU17032'),('SESEIU17033'),('SESEIU17034'),('SESEIU17035'),('SESEIU17036'),('SESEIU17037'),('SESEIU17038'),('SESEIU17039');
/*!40000 ALTER TABLE `borrower` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `category` (
  `id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES ('BA','Business'),('BT','Bio-Technology”'),('EE','Electrical Engineering'),('EN','English'),('ISE','Industrial Engineering & Management'),('IT','Computer Science & Engineering'),('MA','Mathematics'),('PE','Political Economy'),('PH','Physics');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `department` (
  `id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES ('BA','School of Business'),('BT','School of Biotechnology'),('CE','Department of Civil Engineering'),('EE','School of Electrical Engineering'),('ISE','School of Industrial Engineering and Management'),('IT','School of Computer Science and Engineering'),('MA','Department of Mathematics');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lecturer`
--

DROP TABLE IF EXISTS `lecturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lecturer` (
  `id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `depertment_id` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1sat21k89t89ryyv0dt6xcrq9` (`depertment_id`),
  CONSTRAINT `FK_1sat21k89t89ryyv0dt6xcrq9` FOREIGN KEY (`depertment_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lecturer`
--

LOCK TABLES `lecturer` WRITE;
/*!40000 ALTER TABLE `lecturer` DISABLE KEYS */;
INSERT INTO `lecturer` VALUES ('LELEIU00001','/resources/image/profile/1.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 1','0964774964','BA'),('LELEIU00002','/resources/image/profile/2.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 2','0964774964','BA'),('LELEIU00003','/resources/image/profile/3.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 3','0964774964','BA'),('LELEIU00004','/resources/image/profile/4.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 4','0964774964','BA'),('LELEIU00005','/resources/image/profile/5.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 5','0964774964','BA'),('LELEIU00006','/resources/image/profile/6.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 6','0964774964','BA'),('LELEIU00007','/resources/image/profile/7.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 7','0964774964','BA'),('LELEIU00008','/resources/image/profile/8.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 8','0964774964','BA'),('LELEIU00009','/resources/image/profile/9.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BA 9','0964774964','BA'),('LELEIU00010','/resources/image/profile/10.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 1','0964774964','IT'),('LELEIU00011','/resources/image/profile/11.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 2','0964774964','IT'),('LELEIU00012','/resources/image/profile/12.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 3','0964774964','IT'),('LELEIU00013','/resources/image/profile/13.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 4','0964774964','IT'),('LELEIU00014','/resources/image/profile/14.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 5','0964774964','IT'),('LELEIU00015','/resources/image/profile/15.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 6','0964774964','IT'),('LELEIU00016','/resources/image/profile/16.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 7','0964774964','IT'),('LELEIU00017','/resources/image/profile/17.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 8','0964774964','IT'),('LELEIU00018','/resources/image/profile/18.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer IT 9','0964774964','IT'),('LELEIU00019','/resources/image/profile/19.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 1','0964774964','EE'),('LELEIU00020','/resources/image/profile/20.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 2','0964774964','EE'),('LELEIU00021','/resources/image/profile/21.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 3','0964774964','EE'),('LELEIU00022','/resources/image/profile/22.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 4','0964774964','EE'),('LELEIU00023','/resources/image/profile/23.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 5','0964774964','EE'),('LELEIU00024','/resources/image/profile/24.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 6','0964774964','EE'),('LELEIU00025','/resources/image/profile/25.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 7','0964774964','EE'),('LELEIU00026','/resources/image/profile/26.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 8','0964774964','EE'),('LELEIU00027','/resources/image/profile/27.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer EE 9','0964774964','EE'),('LELEIU00028','/resources/image/profile/28.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 1','0964774964','BT'),('LELEIU00029','/resources/image/profile/29.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 2','0964774964','BT'),('LELEIU00030','/resources/image/profile/30.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 3','0964774964','BT'),('LELEIU00031','/resources/image/profile/31.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 4','0964774964','BT'),('LELEIU00032','/resources/image/profile/32.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 5','0964774964','BT'),('LELEIU00033','/resources/image/profile/33.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 6','0964774964','BT'),('LELEIU00034','/resources/image/profile/34.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 7','0964774964','BT'),('LELEIU00035','/resources/image/profile/35.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 8','0964774964','BT'),('LELEIU00036','/resources/image/profile/36.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer BT 9','0964774964','BT'),('LELEIU00037','/resources/image/profile/37.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 1','0964774964','ISE'),('LELEIU00038','/resources/image/profile/38.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 2','0964774964','ISE'),('LELEIU00039','/resources/image/profile/39.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 3','0964774964','ISE'),('LELEIU00040','/resources/image/profile/40.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 4','0964774964','ISE'),('LELEIU00041','/resources/image/profile/41.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 5','0964774964','ISE'),('LELEIU00042','/resources/image/profile/42.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 6','0964774964','ISE'),('LELEIU00043','/resources/image/profile/43.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 7','0964774964','ISE'),('LELEIU00044','/resources/image/profile/44.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 8','0964774964','ISE'),('LELEIU00045','/resources/image/profile/45.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer ISE 9','0964774964','ISE'),('LELEIU00046','/resources/image/profile/46.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 1','0964774964','MA'),('LELEIU00047','/resources/image/profile/47.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 2','0964774964','MA'),('LELEIU00048','/resources/image/profile/48.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 3','0964774964','MA'),('LELEIU00049','/resources/image/profile/49.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 4','0964774964','MA'),('LELEIU00050','/resources/image/profile/50.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 5','0964774964','MA'),('LELEIU00051','/resources/image/profile/51.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 6','0964774964','MA'),('LELEIU00052','/resources/image/profile/52.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 7','0964774964','MA'),('LELEIU00053','/resources/image/profile/53.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 8','0964774964','MA'),('LELEIU00054','/resources/image/profile/54.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer MA 9','0964774964','MA'),('LELEIU00055','/resources/image/profile/55.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 1','0964774964','CE'),('LELEIU00056','/resources/image/profile/56.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 2','0964774964','CE'),('LELEIU00057','/resources/image/profile/57.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 3','0964774964','CE'),('LELEIU00058','/resources/image/profile/58.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 4','0964774964','CE'),('LELEIU00059','/resources/image/profile/59.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 5','0964774964','CE'),('LELEIU00060','/resources/image/profile/60.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 6','0964774964','CE'),('LELEIU00061','/resources/image/profile/61.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 7','0964774964','CE'),('LELEIU00062','/resources/image/profile/62.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 8','0964774964','CE'),('LELEIU00063','/resources/image/profile/63.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn Lecturer CE 9','0964774964','CE');
/*!40000 ALTER TABLE `lecturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other_percenatage_log`
--

DROP TABLE IF EXISTS `other_percenatage_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `other_percenatage_log` (
  `penalty_log_id` int(11) NOT NULL,
  `other_percentage_id` int(11) NOT NULL,
  PRIMARY KEY (`other_percentage_id`,`penalty_log_id`),
  KEY `FK_rdfkqyd3255snjcojjc4labmx` (`penalty_log_id`),
  CONSTRAINT `FK_i9ax50fxo1r28xxghs3k05imn` FOREIGN KEY (`other_percentage_id`) REFERENCES `other_percentage` (`id`),
  CONSTRAINT `FK_rdfkqyd3255snjcojjc4labmx` FOREIGN KEY (`penalty_log_id`) REFERENCES `penalty_log` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_percenatage_log`
--

LOCK TABLES `other_percenatage_log` WRITE;
/*!40000 ALTER TABLE `other_percenatage_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_percenatage_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other_percentage`
--

DROP TABLE IF EXISTS `other_percentage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `other_percentage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fine` int(11) DEFAULT NULL,
  `percentage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_percentage`
--

LOCK TABLES `other_percentage` WRITE;
/*!40000 ALTER TABLE `other_percentage` DISABLE KEYS */;
INSERT INTO `other_percentage` VALUES (1,20,20),(2,50,50),(3,80,80),(4,100,100);
/*!40000 ALTER TABLE `other_percentage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `penalty_log`
--

DROP TABLE IF EXISTS `penalty_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `penalty_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penalty` int(11) DEFAULT NULL,
  `start` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `book_id` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `borrow_log_id` int(11) DEFAULT NULL,
  `person_id` varchar(12) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9d40o6shry2o2gdb60wrtdpge` (`book_id`),
  KEY `FK_7cuwipp0arjidbh95tbe7lvkv` (`borrow_log_id`),
  KEY `FK_1s9hi9l0jsivbnxxcwkkllvaf` (`person_id`),
  CONSTRAINT `FK_1s9hi9l0jsivbnxxcwkkllvaf` FOREIGN KEY (`person_id`) REFERENCES `borrower` (`person_id`),
  CONSTRAINT `FK_7cuwipp0arjidbh95tbe7lvkv` FOREIGN KEY (`borrow_log_id`) REFERENCES `borrow_log` (`id`),
  CONSTRAINT `FK_9d40o6shry2o2gdb60wrtdpge` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `penalty_log`
--

LOCK TABLES `penalty_log` WRITE;
/*!40000 ALTER TABLE `penalty_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `penalty_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_date` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `staff_id` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gyhta3xlykqfbpb0ts9wb9w25` (`staff_id`),
  CONSTRAINT `FK_gyhta3xlykqfbpb0ts9wb9w25` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservate_log`
--

DROP TABLE IF EXISTS `reservate_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reservate_log` (
  `id` int(11) NOT NULL,
  `reservate_date` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `person_id` varchar(12) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `staff_id` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7ver07h28f70r3xcclhxhi7tv` (`person_id`),
  KEY `FK_r8m73o8ov4av0eydjll7ac0fp` (`staff_id`),
  CONSTRAINT `FK_7ver07h28f70r3xcclhxhi7tv` FOREIGN KEY (`person_id`) REFERENCES `borrower` (`person_id`),
  CONSTRAINT `FK_r8m73o8ov4av0eydjll7ac0fp` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservate_log`
--

LOCK TABLES `reservate_log` WRITE;
/*!40000 ALTER TABLE `reservate_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservate_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservate_log_detail`
--

DROP TABLE IF EXISTS `reservate_log_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `reservate_log_detail` (
  `due_date` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `reserved` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `reservate_log_id` int(11) NOT NULL,
  `book_id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`book_id`,`reservate_log_id`),
  KEY `FK_jgbglu2vkj7h03rfe9vjelh04` (`reservate_log_id`),
  CONSTRAINT `FK_19rh38jrnnr5x457jca91fkx8` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  CONSTRAINT `FK_jgbglu2vkj7h03rfe9vjelh04` FOREIGN KEY (`reservate_log_id`) REFERENCES `reservate_log` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservate_log_detail`
--

LOCK TABLES `reservate_log_detail` WRITE;
/*!40000 ALTER TABLE `reservate_log_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservate_log_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `staff` (
  `id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES ('ITITIU17014','/resources/image/profile/135.jpg','nltthanh@gmail.com','Nguyễn Lê Tuấn Thành','/jZELp8cQsI=','staff'),('ITITIU17038','/resources/image/profile/134.jpg','nlthanhititiu17038@gmail.com','Nguyễn Lâm Thành','/jZELp8cQsI=','manager'),('ITITIU17065','/resources/image/profile/136.jpg','pak@gmail.com','Phạm Anh Khoa','/jZELp8cQsI=','staff');
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `id` varchar(255) COLLATE utf8_vietnamese_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `depertment_id` varchar(255) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1wd4998vclhi84j8rb2ce44yc` (`depertment_id`),
  CONSTRAINT `FK_1wd4998vclhi84j8rb2ce44yc` FOREIGN KEY (`depertment_id`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES ('BABAIU17030','/resources/image/profile/64.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn A','0964774964','BA'),('BABAIU17031','/resources/image/profile/65.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn B','0964774964','BA'),('BABAIU17032','/resources/image/profile/66.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn C','0964774964','BA'),('BABAIU17033','/resources/image/profile/67.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn D','0964774964','BA'),('BABAIU17034','/resources/image/profile/68.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn E','0964774964','BA'),('BABAIU17035','/resources/image/profile/69.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn F','0964774964','BA'),('BABAIU17036','/resources/image/profile/70.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn G','0964774964','BA'),('BABAIU17037','/resources/image/profile/71.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn H','0964774964','BA'),('BABAIU17038','/resources/image/profile/72.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn I','0964774964','BA'),('BABAIU17039','/resources/image/profile/73.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn J','0964774964','BA'),('BTBTIU17030','/resources/image/profile/94.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn AAAA','0964774964','BT'),('BTBTIU17031','/resources/image/profile/95.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn BBBB','0964774964','BT'),('BTBTIU17032','/resources/image/profile/96.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn CCCC','0964774964','BT'),('BTBTIU17033','/resources/image/profile/97.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn DDDD','0964774964','BT'),('BTBTIU17034','/resources/image/profile/98.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn EEEE','0964774964','BT'),('BTBTIU17035','/resources/image/profile/99.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn FFFF','0964774964','BT'),('BTBTIU17036','/resources/image/profile/100.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn GGGG','0964774964','BT'),('BTBTIU17037','/resources/image/profile/101.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn HHHH','0964774964','BT'),('BTBTIU17038','/resources/image/profile/102.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn IIII','0964774964','BT'),('BTBTIU17039','/resources/image/profile/103.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn JJJJ','0964774964','BT'),('CECEIU17030','/resources/image/profile/124.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn AAAAAAA','0964774964','CE'),('CECEIU17031','/resources/image/profile/125.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn BBBBBBB','0964774964','CE'),('CECEIU17032','/resources/image/profile/126.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn CCCCCCC','0964774964','CE'),('CECEIU17033','/resources/image/profile/127.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn DDDDDDD','0964774964','CE'),('CECEIU17034','/resources/image/profile/128.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn EEEEEEE','0964774964','CE'),('CECEIU17035','/resources/image/profile/129.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn FFFFFFF','0964774964','CE'),('CECEIU17036','/resources/image/profile/130.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn GGGGGGG','0964774964','CE'),('CECEIU17037','/resources/image/profile/131.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn HHHHHHH','0964774964','CE'),('CECEIU17038','/resources/image/profile/132.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn IIIIIII','0964774964','CE'),('CECEIU17039','/resources/image/profile/133.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn JJJJJJJ','0964774964','CE'),('EEEEIU17030','/resources/image/profile/84.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn AAA','0964774964','EE'),('EEEEIU17031','/resources/image/profile/85.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn BBB','0964774964','EE'),('EEEEIU17032','/resources/image/profile/86.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn CCC','0964774964','EE'),('EEEEIU17033','/resources/image/profile/87.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn DDD','0964774964','EE'),('EEEEIU17034','/resources/image/profile/88.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn EEE','0964774964','EE'),('EEEEIU17035','/resources/image/profile/89.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn FFF','0964774964','EE'),('EEEEIU17036','/resources/image/profile/90.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn GGG','0964774964','EE'),('EEEEIU17037','/resources/image/profile/91.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn HHH','0964774964','EE'),('EEEEIU17038','/resources/image/profile/92.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn III','0964774964','EE'),('EEEEIU17039','/resources/image/profile/93.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn JJJ','0964774964','EE'),('ITITIU17030','/resources/image/profile/74.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn AA','0964774964','IT'),('ITITIU17031','/resources/image/profile/75.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn BB','0964774964','IT'),('ITITIU17032','/resources/image/profile/76.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn CC','0964774964','IT'),('ITITIU17033','/resources/image/profile/77.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn DF','0964774964','IT'),('ITITIU17034','/resources/image/profile/78.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn EE','0964774964','IT'),('ITITIU17035','/resources/image/profile/79.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn FF','0964774964','IT'),('ITITIU17036','/resources/image/profile/80.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn GG','0964774964','IT'),('ITITIU17037','/resources/image/profile/81.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn HH','0964774964','IT'),('ITITIU17038','/resources/image/profile/82.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn II','0964774964','IT'),('ITITIU17039','/resources/image/profile/83.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn JJ','0964774964','IT'),('MAMAIU17030','/resources/image/profile/114.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn AAAAAA','0964774964','MA'),('MAMAIU17031','/resources/image/profile/115.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn BBBBBB','0964774964','MA'),('MAMAIU17032','/resources/image/profile/116.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn CCCCCC','0964774964','MA'),('MAMAIU17033','/resources/image/profile/117.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn DDDDDD','0964774964','MA'),('MAMAIU17034','/resources/image/profile/118.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn EEEEEE','0964774964','MA'),('MAMAIU17035','/resources/image/profile/119.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn FFFFFF','0964774964','MA'),('MAMAIU17036','/resources/image/profile/120.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn GGGGGG','0964774964','MA'),('MAMAIU17037','/resources/image/profile/121.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn HHHHHH','0964774964','MA'),('MAMAIU17038','/resources/image/profile/122.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn IIIIII','0964774964','MA'),('MAMAIU17039','/resources/image/profile/123.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn JJJJJJ','0964774964','MA'),('SESEIU17030','/resources/image/profile/104.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn AAAAA','0964774964','ISE'),('SESEIU17031','/resources/image/profile/105.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn BBBBB','0964774964','ISE'),('SESEIU17032','/resources/image/profile/106.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn CCCCC','0964774964','ISE'),('SESEIU17033','/resources/image/profile/107.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn DDDDD','0964774964','ISE'),('SESEIU17034','/resources/image/profile/108.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn EEEEE','0964774964','ISE'),('SESEIU17035','/resources/image/profile/109.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn FFFFF','0964774964','ISE'),('SESEIU17036','/resources/image/profile/110.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn GGGGG','0964774964','ISE'),('SESEIU17037','/resources/image/profile/111.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn HHHHH','0964774964','ISE'),('SESEIU17038','/resources/image/profile/112.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn IIIII','0964774964','ISE'),('SESEIU17039','/resources/image/profile/113.jpg','nlthanhititiu17038@gmail.com','Nguyễn Văn JJJJJ','0964774964','ISE');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'librarymanagement'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-03  1:08:14
