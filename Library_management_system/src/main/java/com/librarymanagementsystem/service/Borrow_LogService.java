/**
 * 
 */
package com.librarymanagementsystem.service;

import static org.mockito.Mockito.never;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.BorrowTmp;
import com.librarymanagementsystem.entity.Borrow_Log;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class Borrow_LogService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Borrow_Log> borrow_logDao;
	
	public ArrayList<Borrow_Log> getAll() {
		return borrow_logDao.getAll();
	}
	
	public Borrow_Log getById(Borrow_Log t) {
		return borrow_logDao.getById(t);
	}
	
	public Borrow_Log add(Borrow_Log t) {
		return borrow_logDao.add(t);
	}
	
	public boolean update(Borrow_Log t) {
		return borrow_logDao.update(t);
	}
	
	public boolean delete(Borrow_Log t) {
		return borrow_logDao.delete(t);
	}
	
	public boolean deleteById(Borrow_Log t) {
		return borrow_logDao.deleteById(t);
	}
	
	public int isBorrowed(String person_id, String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select B.id.book.id.id from Borrow_Log as A, Borrow_Log_Detail as B where A.id.id = B.id.borrow_log.id.id"
				+ " and A.person_id.id.person_id = :person_id and B.id.book.id.id = :book_id and A.status = false";
		Query query = session.createQuery(hql);
		System.out.println(query);
		query.setParameter("person_id", person_id);
		query.setParameter("book_id", book_id);
		return query.list().size();
	}
	
	public int maxId() {
		return 1;
	}
	
	
	public int getLastestId() {
		Session session = sessionFactory.getCurrentSession();
		System.out.println("here");
		String hql = "select max(id.id) from Borrow_Log";
		Query query = session.createQuery(hql);
		System.out.println(query.list().get(0));
		return (query.list().get(0) == null ? 1 : Integer.parseInt(""+query.list().get(0))+1);
	}
	
	public int existARecordById(String id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Borrow_Log where id.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.list().size();
	}
	
	public Borrow_Log minReturnDateBorrowLog(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Borrow_Log as C where C.borrow_date = (select min(A.borrow_date) from Borrow_Log as A, "
				+ "Borrow_Log_Detail as B where A.id.id = B.id.borrow_log.id.id and B.id.book.id.id = :book_id and B.reserved = 0)";
//		String t = "select C.return_date, C.id from borrow_log as C where C.borrow_date = (select min(A.borrow_date) from borrow_log as A, \r\n" + 
//				"borrow_log_detail as B where A.id = B.borrow_log_id and B.book_id = \"CAL1\" and B.reserved = 0);";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		return (Borrow_Log) query.list().get(0);
	}
	
	public int getBorrowTmp(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select min(date_format(A.return_date, '%Y-%m-%d %H:%i:%S')), A.id.id from Borrow_Log as A, "
				+ "Borrow_Log_Detail as B where A.id.id =  B.id.borrow_log.id.id and B.id.book.id.id = :book_id and B.reserved = 0 and A.status = 0";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		Gson gson = new Gson();
		String tmp = gson.toJson(query.list().get(0));
		//gson.toJson(query.list().get(0)).replaceAll("\"", "");
		System.out.println(gson.toJson(query.list().get(0)).equalsIgnoreCase("[null,null]"));
		if (tmp.equalsIgnoreCase("[null,null]"))
			return 0;
		tmp = tmp.replaceAll("[\\[\\]\"]", "");
		System.out.println(tmp);
		System.out.println(tmp.substring(tmp.indexOf(",")+1));
		return Integer.parseInt(tmp.substring(tmp.indexOf(",")+1));	
	}
	
	public Borrow_Log getBorrowLog(int id) {
		Borrow_Log borrow_Log = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Borrow_Log as A where A.id.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		if (query.list().size() > 0)
			borrow_Log = (Borrow_Log) query.list().get(0);
		return borrow_Log;
	}
	
	public static void main(String[] args) {
		Gson gson = new Gson();
		String t = gson.toJson('['+"2020-1-3 17:00:00,2"+']');
		t = t.replaceAll("[\\[\\]\"]" + "", "");
		System.out.println(t.substring(t.indexOf(",")+1));
		System.out.println(t.substring(0, t.indexOf(",")));
		String m = "null,null";
		String mm = "null,null";
		System.out.println(!m.equalsIgnoreCase(mm));
	}
	
	public List<Borrow_Log> getListRemindReturn() {
		List<Borrow_Log> list = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "select * from Borrow_Log as A where date_sub(date_format(A.return_date, '%Y-%m-%d'), interval 7 day) <= "
				+ "date_format(current_date(), '%Y-%m-%d') and A.status = 0";
		Query query = session.createSQLQuery(hql).addEntity(Borrow_Log.class);
		System.out.println(query.list().size() + "size of list remind");
		if (query.list().size() > 0)
			list = (List<Borrow_Log>) query.list();
//		for (Borrow_Log b : list) {
//			System.out.println(b.getBorrow_date());
//		}
		return list;
	}
	
	public List<Borrow_Log> getListBorrowLog(String current_date, int status) {
		List<Borrow_Log> list = new ArrayList<Borrow_Log>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		if (status == 0)
			hql = "select A.* from Borrow_Log as A where day(A.borrow_date) "
				+ "= day(:current_date)";
		else
			hql = "select A.* from Borrow_Log as A, Borrow_Log_Detail as B where "
					+ "A.id = B.borrow_log_id and day(B.return_date) "
					+ "= day(:current_date)";
		Query query = session.createSQLQuery(hql).addEntity(Borrow_Log.class);
		query.setParameter("current_date", current_date);
//		if (status == 1)
//			query.setParameter("status", status==1?true:false);
		if (query.list().size() > 0)
			list = (List<Borrow_Log>) query.list();
		return list;
	}
}
