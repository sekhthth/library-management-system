/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Category;
import com.librarymanagementsystem.entity.Staff;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class CategoryService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Category> categoryDao;
	
	public ArrayList<Category> getAll() {
		return categoryDao.getAll();
	}
	
	public Category getById(Category t) {
		return categoryDao.getById(t);
	}
	
	public Category add(Category t) {
		return categoryDao.add(t);
	}
	
	public boolean add2(Category t) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.persist(t);
		} catch (Exception e) {
			session.close();
			return false;
		}
		return true;
	}
	
	public boolean update(Category t) {
		return categoryDao.update(t);
	}
	
	public boolean delete(Category t) {
		return categoryDao.delete(t);
	}
	
	public boolean deleteById(Category t) {
		return categoryDao.deleteById(t);
	}
	
	public List<Category> getAll2(){
		List<Category> list = new ArrayList<Category>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Category";
		Query query = session.createQuery(hql);
		if (query.list().size() >= 1)
			list = (List<Category>) query.list();
		return list;
	}
	
	public Category getACategoryById(String category_id) {
		Category category = new Category();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Category where id.id = :category_id";
		Query query = session.createQuery(hql);
		query.setParameter("category_id", category_id);
		if (query.list().size() >= 1)
			category = (Category) query.list().get(query.list().size()-1);
		return category;
	}
	
	public int delete2(String category_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Category where id.id = :category_id";
		Query query = session.createQuery(hql);
		query.setParameter("category_id", category_id);
//		System.out.println(query.executeUpdate());
		return query.executeUpdate();
	}

	/**
	 * @param keyword
	 * @param type
	 * @return
	 */
	public List<Category> searchCategoryByKeyWord(String keyword, int type) {
		List<Category> list = new ArrayList<Category>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		if (type == 1)
			hql = "from Category where id.id like :keyword";
		else 
			hql = "from Category where name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyword+"%");
		if (query.list().size() >= 1)
			list = query.list();
		return list;
	}
}
