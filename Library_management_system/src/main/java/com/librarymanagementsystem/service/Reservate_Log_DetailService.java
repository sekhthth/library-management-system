/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Reservate_Log_Detail;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class Reservate_Log_DetailService{
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Reservate_Log_Detail> reservate_Log_DetailDao;
	
	public ArrayList<Reservate_Log_Detail> getAll() {
		return reservate_Log_DetailDao.getAll();
	}
	
	public Reservate_Log_Detail getById(Reservate_Log_Detail t) {
		return reservate_Log_DetailDao.getById(t);
	}
	
	public Reservate_Log_Detail add(Reservate_Log_Detail t) {
		return reservate_Log_DetailDao.add(t);
	}
	
	public boolean update(Reservate_Log_Detail t) {
		return reservate_Log_DetailDao.update(t);
	}
	
	public boolean delete(Reservate_Log_Detail t) {
		return reservate_Log_DetailDao.delete(t);
	}
	
	public boolean deleteById(Reservate_Log_Detail t) {
		return reservate_Log_DetailDao.deleteById(t);
	}
	
	public Reservate_Log_Detail getByBorrowLogId(String reservate_log_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Reservate_Log_Detail where id.reservate_log.id.id = :reservate_log_id";
		Query query = session.createQuery(hql);
		query.setParameter("reservate_log_id", reservate_log_id);
		return (Reservate_Log_Detail) query.list().get(0);
	}
	
	public Reservate_Log_Detail getByIdAndBookId(String book_id, int reservate_log_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Reservate_Log_Detail as A where A.id.book.id.id = :book_id and A.id.reservate_log.id.id = :reservate_log_id";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		query.setParameter("reservate_log_id", reservate_log_id);
		return (Reservate_Log_Detail) query.list().get(0);
	}
	
	public int getWaitingAmount(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select count(B.id.reservate_log.id.id) from Reservate_Log as A, Reservate_Log_Detail as B where A.id.id = B.id.reservate_log.id.id and B.duration != :duration and B.id.book.id.id = :book_id and B.status = 2";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		query.setParameter("duration", "");
		return ((Long) query.list().get(0)).intValue();
	}
	
	public List<Reservate_Log_Detail> getListNotTaken() {
		List<Reservate_Log_Detail> list = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Reservate_Log_Detail as A where A.status = 2 and A.duration != :duration";
		Query query = session.createQuery(hql);
		query.setParameter("duration", "");
		if (query.list().size() > 0)
			list = query.list();
		return list;
	}
	
	public List<Book> getBookForTaking(int reservate_log_id) {
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book as A where A.id.id in (select B.id.book.id.id from Reservate_Log_Detail as B "
				+ "where B.id.reservate_log.id.id = :reservate_log_id and B.status = 2 and B.duration != :duration)";
		Query query = session.createQuery(hql);
		query.setParameter("reservate_log_id", reservate_log_id);
		query.setParameter("duration", "");
		if (query.list().size() > 0)
			list = (List<Book>) query.list();
		return list;
	}
	
	public List<Book> isTaken(int reservate_log_id) {
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "select A.* from reservate_log_detail as A where (A.status = 1 or A.status = 3) and A.reservate_log_id = :reservate_log_id "
				+ "having count(*) = (select count(*) from reservate_log_detail as B where B.reservate_log_id = :reservate_log_id)";
		Query query = session.createSQLQuery(hql).addEntity(Reservate_Log_Detail.class);
		query.setParameter("reservate_log_id", reservate_log_id);
		System.out.println(query.list().size() + "   test ne");
		if (query.list().size() > 0)
			list = (List<Book>) query.list();
		return list;
	}
	
	public int isReservated(String person_id, String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select B.id.book.id.id from Reservate_Log as A, Reservate_Log_Detail as B where A.id.id = B.id.reservate_log.id.id"
				+ " and A.person_id.id.person_id = :person_id and B.id.book.id.id = :book_id and B.status = 2";
		Query query = session.createQuery(hql);
		System.out.println(query);
		query.setParameter("person_id", person_id);
		query.setParameter("book_id", book_id);
		System.out.println(query.list() + "test laij");
		return query.list()==null?0:query.list().size();
	}
}
