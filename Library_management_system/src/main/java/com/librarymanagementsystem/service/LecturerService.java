/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.entity.Student;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class LecturerService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Lecturer> lecturerDao;
	
	public ArrayList<Lecturer> getAll() {
		return lecturerDao.getAll();
	}
	
	public Lecturer getById(Lecturer t) {
		return lecturerDao.getById(t);
	}
	
	public Lecturer add(Lecturer t) {
		return lecturerDao.add(t);
	}
	
	public boolean update(Lecturer t) {
		return lecturerDao.update(t);
	}
	
	public boolean delete(Lecturer t) {
		return lecturerDao.delete(t);
	}
	
	public boolean deleteById(Lecturer t) {
		return lecturerDao.deleteById(t);
	}
	
	public Lecturer getLecturer(String id) {
		Lecturer list = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Lecturer where id.id like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%" + id + "%");
		if (query.list().size() >= 1)
			list = (Lecturer) query.list().get(0);
		return list;
	}
	
	public List<Lecturer> searchLecturers(String id) {
		List<Lecturer> list = new ArrayList<Lecturer>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Lecturer where id.id like :keyword or name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%" + id + "%");
		if (query.list().size() >= 1)
			list = (List<Lecturer>) query.list();
		return list;
	}
}
