/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Borrow_Log;
import com.librarymanagementsystem.entity.Reservate_Log;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class Reservate_LogService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Reservate_Log> reservate_logDao;
	
	public ArrayList<Reservate_Log> getAll() {
		return reservate_logDao.getAll();
	}
	
	public Reservate_Log getById(Reservate_Log t) {
		return reservate_logDao.getById(t);
	}
	
	public Reservate_Log add(Reservate_Log t) {
		return reservate_logDao.add(t);
	}
	
	public boolean update(Reservate_Log t) {
		return reservate_logDao.update(t);
	}
	
	public boolean delete(Reservate_Log t) {
		return reservate_logDao.delete(t);
	}
	
	public boolean deleteById(Reservate_Log t) {
		return reservate_logDao.deleteById(t);
	}
	
	public int isReservated(String person_id, String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select B.id.book.id.id from Reservate_Log as A, Reservate_Log_Detail as B where A.id.id = B.id.reservate_log.id.id"
				+ " and A.person_id.id.person_id = :person_id and B.id.book.id.id = :book_id and B.status = 2";
		Query query = session.createQuery(hql);
		System.out.println(query);
		query.setParameter("person_id", person_id);
		query.setParameter("book_id", book_id);
		System.out.println(query.list() + "test laij");
		return query.list()==null?0:query.list().size();
	}
	
	public int getLastestId() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select max(id.id) from Reservate_Log";
		Query query = session.createQuery(hql);
		return (query.list().get(0) == null ? 1 : Integer.parseInt(""+query.list().get(0))+1);
	}
	
	public int existARecordById(String id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Reservate_Log where id.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.list().size();
	}
	
	public Reservate_Log minReturnDateBorrowLog(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Reservate_Log as C, Reservate_Log_Detail as D where D.due_date = (select min(A.due_date) from Reservate_Log as A, Reservate_Log_Detail as B where A.id.id =  B.id.reservate_log.id.id and B.id.book.id.id = :book_id and B.reserved = 0) and C.id.id = D.id.reservate_log.id.id";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		return (Reservate_Log) query.list().get(0);
	}
	
	public int getReservatedTmp(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select min(date_format(B.due_date, '%Y-%m-%d %H:%i:%S')), A.id.id from Reservate_Log as A, "
				+ "Reservate_Log_Detail as B where A.id.id =  B.id.reservate_log.id.id and B.id.book.id.id = :book_id and B.reserved = 0 and B.status = 2";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		Gson gson = new Gson();
		String tmp = gson.toJson(query.list().get(0));
		if (tmp.equalsIgnoreCase("[null,null]"))
			return 0;
		tmp = tmp.replaceAll("[\\[\\]\"]", "");
		System.out.println(tmp);
		System.out.println(tmp.substring(tmp.indexOf(",")+1));
		return Integer.parseInt(tmp.substring(tmp.indexOf(",")+1));	
	}
	
	public Reservate_Log getReservateLog(int id) {
		Reservate_Log reservate_Log = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Reservate_Log as A where A.id.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		if (query.list().size() > 0)
			reservate_Log = (Reservate_Log) query.list().get(0);
		return reservate_Log;
	}
	
	public int getAPersonForMail(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select min(date_format(A.reservate_date, '%Y-%m-%d %H:%i:%S')), A.id.id from Reservate_Log as A, "
				+ "Reservate_Log_Detail as B where A.id.id = B.id.reservate_log.id.id and B.id.book.id.id = :book_id and B.status = 2 and B.duration = :duration";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		query.setParameter("duration", "");
		Gson gson = new Gson();
		String tmp = gson.toJson(query.list().get(0));
		System.out.println(tmp + "   tim nguoi bao co sach r lai nhan");
		if (tmp.equalsIgnoreCase("[null,null]"))
			return 0;
		tmp = tmp.replaceAll("[\\[\\]\"]" + "", "");
		return Integer.parseInt(tmp.substring(tmp.indexOf(",")+1));	
	}
	
	public int getAPersonForMail2(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select min(date_format(A.reservate_date, '%Y-%m-%d %H:%i:%S')), A.id.id from Reservate_Log as A, "
				+ "Reservate_Log_Detail as B where A.id.id = B.id.reservate_log.id.id and B.id.book.id.id = :book_id and B.status = 2 and B.duration = :duration";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		query.setParameter("duration", "");
		Gson gson = new Gson();
		String tmp = gson.toJson(query.list().get(0));
		if (tmp.equalsIgnoreCase("[null,null]"))
			return 0;
		tmp = tmp.replaceAll("[\\[\\]\"]" + "", "");
		return Integer.parseInt(tmp.substring(tmp.indexOf(",")+1));	
	}
	
	public List<Reservate_Log> getListRervateLog(String current_date) {
		List<Reservate_Log> list = new ArrayList<Reservate_Log>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Reservate_Log as A where day(A.reservate_date) "
				+ "= day(:current_date)";
		Query query = session.createQuery(hql);
		query.setParameter("current_date", current_date);
		if (query.list().size() > 0)
			list = (List<Reservate_Log>) query.list();
		return list;
	}
	
	public List<Book> getBooksInReservateLog(int reservate_log_id) {
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book as B where B.id.id in (select A.id.book.id.id from Reservate_Log_Detail as A where "
				+ "A.id.reservate_log.id.id = :reservate_log_id)";
		Query query = session.createQuery(hql);
		query.setParameter("reservate_log_id", reservate_log_id);
		if (query.list().size() > 0)
			list = query.list();
		return list;
	}
}
