/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Student;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class StudentService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Student> studentDao;

	public ArrayList<Student> getAll() {
		return studentDao.getAll();
	}

	public Student getById(Student t) {
		return studentDao.getById(t);
	}

	public Student add(Student t) {
		return studentDao.add(t);
	}

	public boolean update(Student t) {
		return studentDao.update(t);
	}

	public boolean delete(Student t) {
		return studentDao.delete(t);
	}

	public boolean deleteById(Student t) {
		return studentDao.deleteById(t);
	}

	public List<Student> searchStudents(String id) {
		List<Student> list = new ArrayList<Student>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Student where id.id like :keyword or name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%" + id + "%");
		if (query.list().size() >= 1)
			list = (List<Student>) query.list();
		return list;
	}
	
	public Student getStudent(String id) {
		Student list = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Student where id.id like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%" + id + "%");
		if (query.list().size() >= 1)
			list = (Student) query.list().get(0);
		return list;
	}
}
