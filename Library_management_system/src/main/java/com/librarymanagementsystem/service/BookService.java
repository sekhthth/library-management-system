/**
 * 
 */
package com.librarymanagementsystem.service;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Borrow_Log;
import com.librarymanagementsystem.entity.Borrow_Log_Detail;
import com.librarymanagementsystem.entity.Category;
import com.librarymanagementsystem.entity.Department;
import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.entity.Report;
import com.librarymanagementsystem.entity.Reservate_Log;
import com.librarymanagementsystem.entity.Reservate_Log_Detail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.Student;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class BookService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Book> bookDao;
	
	public ArrayList<Book> getAll() {
		return bookDao.getAll();
	}
	
	public Book getById(Book t) {
		return bookDao.getById(t);
	}
	
	public Book add(Book t) {
		return bookDao.add(t);
	}
	
	public boolean add2(Book t) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.persist(t);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public boolean update(Book t) {
		return bookDao.update(t);
	}
	
	public boolean delete(Book t) {
		return bookDao.delete(t);
	}
	
	public boolean deleteById(Book t) {
		return bookDao.deleteById(t);
	}
	
	public List<Book> searchBooks(String id) {
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book where id.id like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%" + id + "%");
		if (query.list().size() >= 1)
			list = (List<Book>) query.list();
		return list;
	}
	
	public List<Book> getAll2(){
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book";
		Query query = session.createQuery(hql);
		if (query.list().size() >= 1)
			list = (List<Book>) query.list();
		return list;
	}
	
	public Book getABookById(String id) {
		Session session = sessionFactory.getCurrentSession();
		Book book = new Book();
		String hql = "from Book where id.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		if (query.list().size() >= 1)
			book = (Book) query.list().get(query.list().size()-1);
		return book;
	}
	
	public List<Book> booksByBorrowerId(String borrower_id, String return_date){
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "select * from IN (select book_id from Historical_Borrowing where borrower_id = :borrower_id and return_date = :return_date)";
		Query query = session.createQuery(hql);
		query.setParameter("borrower_id", borrower_id);
		query.setParameter("return_date", return_date);
		if (query.list().size() >= 1)
			list = (List<Book>) query.list();
		return list;
	}
	
	public void delete2(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Book where id.id = :book_id";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		query.executeUpdate();
	}
	
	public int isOutOfAvailable(String id) {
		Session session = sessionFactory.getCurrentSession();
		Book book = new Book();
		String hql = "from Book where id.id = :id and borrowed = FORMAT((quantity/2), 0)";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return query.list().size();
	}
	
	public List<Book> getListBookReturn(int borrow_log_id) {
		List<Book> list = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Book as B where B.id.id in (select D.id.book.id.id from Borrow_Log_Detail as D where D.id.borrow_log.id.id = :borrow_log_id)";
		Query query = session.createQuery(hql);
		query.setParameter("borrow_log_id", borrow_log_id);
		if (query.list().size() >= 1)
			list = (List<Book>) query.list();
		return (List<Book>) list;
	}
	
	public int getAllowedAmount(String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select cast(format(B.quantity/2), 0)-B.borrowed from Book as B where B.id.id = :book_id";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		return (int) query.list().get(0);
	}
	
	public List<Book> searchBookByKeyWord(String keyword, int type) {
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		if (type == 1)
			hql = "from Book where id.id like :keyword";
		else 
			hql = "from Book where name like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyword+"%");
		if (query.list().size() >= 1)
			list = query.list();
		return list;
	}
}
