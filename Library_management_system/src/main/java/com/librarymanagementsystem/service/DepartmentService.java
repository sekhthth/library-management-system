/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Department;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class DepartmentService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Department> departmentDao;
	
	public ArrayList<Department> getAll() {
		return departmentDao.getAll();
	}
	
	public Department getById(Department t) {
		return departmentDao.getById(t);
	}
	
	public Department add(Department t) {
		return departmentDao.add(t);
	}
	
	public boolean update(Department t) {
		return departmentDao.update(t);
	}
	
	public boolean delete(Department t) {
		return departmentDao.delete(t);
	}
	
	public boolean deleteById(Department t) {
		return departmentDao.deleteById(t);
	}
}
