/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Other_Percentage_Log;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class Other_Percentage_LogService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Other_Percentage_Log> other_Percentage_LogDao;
	
	public ArrayList<Other_Percentage_Log> getAll() {
		return other_Percentage_LogDao.getAll();
	}
	
	public Other_Percentage_Log getById(Other_Percentage_Log t) {
		return other_Percentage_LogDao.getById(t);
	}
	
	public Other_Percentage_Log add(Other_Percentage_Log t) {
		return other_Percentage_LogDao.add(t);
	}
	
	public boolean update(Other_Percentage_Log t) {
		return other_Percentage_LogDao.update(t);
	}
	
	public boolean delete(Other_Percentage_Log t) {
		return other_Percentage_LogDao.delete(t);
	}
	
	public boolean deleteById(Other_Percentage_Log t) {
		return other_Percentage_LogDao.deleteById(t);
	}
	public Other_Percentage_Log getByPenalty_Log_Id(int borrow_Log_id, String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Other_Percentage_Log as A where A.id.penalty_log.id = (select max(B.id) from Penalty_Log as B where B.borrow_log_id.id.id = :borrow_log_id"
					+ " and B.book_id.id.id = :book_id and B.penalty = 3)";
		Query query = session.createQuery(hql);
		query.setParameter("borrow_log_id", borrow_Log_id);
		query.setParameter("book_id", book_id);
		if (query.list().size() <= 0) return null;
		return (Other_Percentage_Log) query.list().get(0);
	}
	
	public Other_Percentage_Log getByPenalty_Log_Id2(int penalty_log_id, int percentage_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Other_Percentage_Log as A where A.id.penalty_log.id = :penalty_log_id "
				+ "and A.id.other_percentage.id = :percentage_id";
		Query query = session.createQuery(hql);
		query.setParameter("penalty_log_id", penalty_log_id);
		query.setParameter("percentage_id", percentage_id);
		System.out.println(query.toString() + " testhere");
		if (query.list().size() <= 0) return null;
		return (Other_Percentage_Log) query.list().get(0);
	}
}
