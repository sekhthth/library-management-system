/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Borrower;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class BorrowerService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Borrower> borrowerDao;
	
	public ArrayList<Borrower> getAll() {
		return borrowerDao.getAll();
	}
	
	public Borrower getById(Borrower t) {
		return borrowerDao.getById(t);
	}
	
	public Borrower add(Borrower t) {
		return borrowerDao.add(t);
	}
	
	public boolean update(Borrower t) {
		return borrowerDao.update(t);
	}
	
	public boolean delete(Borrower t) {
		return borrowerDao.delete(t);
	}
	
	public boolean deleteById(Borrower t) {
		return borrowerDao.deleteById(t);
	}
	
	public Borrower getById2(String person_id) {
		Borrower borrower = null;
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Borrower where id.person_id = :person_id";
		Query query = session.createQuery(hql);
		query.setParameter("person_id", person_id);
		if (query.list().size() > 0) 
			borrower = (Borrower) query.list().get(0);
		return borrower;
	}
 	
	public boolean add2(Borrower t) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.persist(t);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
}
