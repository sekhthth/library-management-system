/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Other_Percentage;
import com.librarymanagementsystem.entity.Other_Percentage_Log;
import com.librarymanagementsystem.entity.Penalty_Log;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class Penalty_LogService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Penalty_Log> penalty_logDao;

	public ArrayList<Penalty_Log> getAll() {
		return penalty_logDao.getAll();
	}

	public Penalty_Log getById(Penalty_Log t) {
		return penalty_logDao.getById(t);
	}

	public Penalty_Log add(Penalty_Log t) {
		return penalty_logDao.add(t);
	}

	public boolean update(Penalty_Log t) {
		return penalty_logDao.update(t);
	}

	public boolean delete(Penalty_Log t) {
		return penalty_logDao.delete(t);
	}

	public boolean deleteById(Penalty_Log t) {
		return penalty_logDao.deleteById(t);
	}

	public String getStart(String person_id) {
		Session session = sessionFactory.getCurrentSession();
		String tmp = "";
		String hql = "select max(date_format(A.start, '%Y-%m-%d %H:%i:%S')) from Penalty_Log as A where A.person_id = :person_id "
				+ "and date_add(date_format(A.start, '%Y-%m-%d'), interval 30 day) > date_format(current_date(), '%Y-%m-%d')";
		Query query = session.createSQLQuery(hql);
		query.setParameter("person_id", person_id);
		if (query.list().size() > 0) {
			if (query.list().get(0) != null)
				tmp = (String) query.list().get(0);
		}
		System.out.println(tmp + "get start" + query.list().size());
		return tmp;
	}

	public List<Penalty_Log> getListPenaltyLog(String person_id) {
		List<Penalty_Log> list = new ArrayList<Penalty_Log>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "select A.id from Penalty_Log as A where A.person_id = :person_id "
				+ "and date_add(date_format(A.start, '%Y-%m-%d'), interval 30 day) > date_format(current_date(), '%Y-%m-%d')";
		Query query = session.createSQLQuery(hql);
		query.setParameter("person_id", person_id);
		Gson gson = new Gson();
		String tmp = gson.toJson(query.list());
		System.out.println(tmp + " ne");
		if (query.list().size() > 0) {
			for (int i = 0; i < query.list().size(); i++) {
				list.add(getById2((int) query.list().get(i)));
			}
		}
		return list;
	}

	public Penalty_Log getById2(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Penalty_Log as A where A.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		if (query.list().size() <= 0) return null;
		return (Penalty_Log) query.list().get(0);
	}

	public Penalty_Log getLastesPenalty_Log() {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Penalty_Log as A where A.id = (select max(B.id) from Penalty_Log as B)";
		Query query = session.createQuery(hql);
		return (Penalty_Log) query.list().get(0);
	}

	public int getPenaltyIdForReturn(int borrow_log_id, String person_id, String book_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select A.id from Penalty_Log as A where A.borrow_log_id.id.id = :borrow_log_id and "
				+ "A.person_id.id.person_id = :person_id and "
				+ "A.book_id.id.id = :book_id";
		Query query = session.createQuery(hql);
		query.setParameter("borrow_log_id", borrow_log_id);
		query.setParameter("person_id", person_id);
		query.setParameter("book_id", book_id);
		if (query.list().size() > 0)
			return (int) query.list().get(0);
		return -1;
	}
}
