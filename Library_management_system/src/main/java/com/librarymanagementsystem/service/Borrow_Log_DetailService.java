/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Borrow_Log_Detail;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class Borrow_Log_DetailService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Borrow_Log_Detail> borrow_Log_DetailDao;

	public ArrayList<Borrow_Log_Detail> getAll() {
		return borrow_Log_DetailDao.getAll();
	}

	public Borrow_Log_Detail getById(Borrow_Log_Detail t) {
		return borrow_Log_DetailDao.getById(t);
	}

	public Borrow_Log_Detail add(Borrow_Log_Detail t) {
		return borrow_Log_DetailDao.add(t);
	}

	public boolean update(Borrow_Log_Detail t) {
		return borrow_Log_DetailDao.update(t);
	}

	public boolean delete(Borrow_Log_Detail t) {
		return borrow_Log_DetailDao.delete(t);
	}

	public boolean deleteById(Borrow_Log_Detail t) {
		return borrow_Log_DetailDao.deleteById(t);
	}

	public Borrow_Log_Detail getByIdAndBookId(String book_id, int borrow_log_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Borrow_Log_Detail as A where A.id.book.id.id = :book_id and A.id.borrow_log.id.id = :borrow_log_id";
		Query query = session.createQuery(hql);
		query.setParameter("book_id", book_id);
		query.setParameter("borrow_log_id", borrow_log_id);
		return (Borrow_Log_Detail) query.list().get(0);
	}

	public List<Borrow_Log_Detail> getBorrowLogDetails(int borrow_log_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Borrow_Log_Detail as A where A.id.borrow_log.id.id = :borrow_log_id";
		Query query = session.createQuery(hql);
		query.setParameter("borrow_log_id", borrow_log_id);
		return (List<Borrow_Log_Detail>) query.list();
	}

	public int numOfBooksOfARecord(int borrow_log_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select count(A.id.book.id.id) from Borrow_Log_Detail as A where A.id.borrow_log.id.id = :borrow_log_id";
		Query query = session.createQuery(hql);
		query.setParameter("borrow_log_id", borrow_log_id);
		return ((Long) query.list().get(0)).intValue();
	}

	public int numOfBooksReturned(int borrow_log_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "select count(A.id.book.id.id) from Borrow_Log_Detail as A where A.id.borrow_log.id.id = :borrow_log_id and A.penalty = :penalty";
		Query query = session.createQuery(hql);
		query.setParameter("borrow_log_id", borrow_log_id);
		query.setParameter("penalty", 4);
		return Integer.parseInt("" + query.list().get(0));
	}

	public List<Book> getBooksInBorrowLog(int borrow_log_id, int type) {
		List<Book> list = new ArrayList<Book>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		Query query;
		if (type == 0) {
			hql = "from Book as B where B.id.id in (select A.id.book.id.id from Borrow_Log_Detail as A where "
					+ "A.id.borrow_log.id.id = :borrow_log_id)";
			query = session.createQuery(hql);
		} else {
			hql = "from Book as B where B.id.id in (select A.id.book.id.id from Borrow_Log_Detail as A where "
					+ "A.id.borrow_log.id.id = :borrow_log_id and A.return_date != :return_date)";
			query = session.createQuery(hql);
			query.setParameter("return_date", "");
		}
		query.setParameter("borrow_log_id", borrow_log_id);
		if (query.list().size() > 0)
			list = query.list();
		return list;
	}

	public List<Borrow_Log_Detail> getBorrowedBooks(String person_id) {
		List<Borrow_Log_Detail> list = new ArrayList<Borrow_Log_Detail>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "select B.* from Borrow_Log as A, Borrow_Log_Detail as B where A.id = B.borrow_log_id and "
				+ "A.person_id = :person_id and B.penalty != 4";
		Query query = session.createSQLQuery(hql).addEntity(Borrow_Log_Detail.class);
		query.setParameter("person_id", person_id);
		if (query.list().size() > 0)
			list = query.list();
		return list;
	}
}
