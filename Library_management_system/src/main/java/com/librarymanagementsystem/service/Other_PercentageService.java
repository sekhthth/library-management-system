/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Other_Percentage;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class Other_PercentageService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Other_Percentage> other_PercentageDao;
	
	public ArrayList<Other_Percentage> getAll() {
		return other_PercentageDao.getAll();
	}
	
	public Other_Percentage getById(Other_Percentage t) {
		return other_PercentageDao.getById(t);
	}
	
	public Other_Percentage add(Other_Percentage t) {
		return other_PercentageDao.add(t);
	}
	
	public boolean update(Other_Percentage t) {
		return other_PercentageDao.update(t);
	}
	
	public boolean delete(Other_Percentage t) {
		return other_PercentageDao.delete(t);
	}
	
	public boolean deleteById(Other_Percentage t) {
		return other_PercentageDao.deleteById(t);
	}
	
	public List<Other_Percentage> getAll2() {
		List<Other_Percentage> list = new ArrayList<Other_Percentage>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Other_Percentage";
		Query query = session.createQuery(hql);
		if (query.list().size() > 0)
			list = query.list();
		return list;
	}
	
	public Other_Percentage getById2(int id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Other_Percentage where id = :id" ;
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		return (Other_Percentage) query.list().get(0);
	}
	
	public Other_Percentage getFromOtherPercentageLog(int penalty_log_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Other_Percentage as A where id in (select B.id.other_percentage.id from Other_Percentage_Log as B where "
				+ "B.id.penalty_log.id = :penalty_log_id)" ;
		Query query = session.createQuery(hql);
		query.setParameter("penalty_log_id", penalty_log_id);
		if (query.list().size() > 0) return (Other_Percentage) query.list().get(0);
		return null;
	}
}
