/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Category;
import com.librarymanagementsystem.entity.Staff;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class StaffService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Staff> staffDao;
	
	public ArrayList<Staff> getAll() {
		return staffDao.getAll();
	}
	
	public Staff getById(Staff t) {
		return staffDao.getById(t);
	}
	
	public Staff add(Staff t) {
		return staffDao.add(t);
	}
	
	public boolean update(Staff t) {
		return staffDao.update(t);
	}
	
	public boolean delete(Staff t) {
		return staffDao.delete(t);
	}
	
	public boolean deleteById(Staff t) {
		return staffDao.deleteById(t);
	}
	
	public boolean add2(Staff t) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.persist(t);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}
	
	public Staff getAStaffById(String id) {
		Session session = sessionFactory.getCurrentSession();
		Staff staff = new Staff();
		String hql = "from Staff where id.id = :id";
		Query query = session.createQuery(hql);
		query.setParameter("id", id);
		if (query.list().size() >= 1)
			staff = (Staff) query.list().get(query.list().size()-1);
		return staff;
	}
	
	public List<Staff> getAll2(){
		List<Staff> list = new ArrayList<Staff>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Staff";
		Query query = session.createQuery(hql);
		if (query.list().size() >= 1)
			list = (List<Staff>) query.list();
		return list;
	}
	
	public int delete2(String staff_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "delete from Staff where id.id = :staff_id";
		Query query = session.createQuery(hql);
		query.setParameter("staff_id", staff_id);
//		System.out.println(query.executeUpdate());
		return query.executeUpdate();
	}
	
	public Staff getManager() {
		Session session = sessionFactory.getCurrentSession();
		Staff staff = new Staff();
		String hql = "from Staff where role = :role";
		Query query = session.createQuery(hql);
		query.setParameter("role", "manager");
		if (query.list().size() >= 1)
			staff = (Staff) query.list().get(query.list().size()-1);
		return staff;
	}
	
	public List<Staff> searchStaffByKeyWord(String keyword, int type) {
		List<Staff> list = new ArrayList<Staff>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "";
		if (type == 1)
			hql = "from Staff where id.id like :keyword";
		else if (type == 2)
			hql = "from Staff where name like :keyword";
		else
			hql = "from Staff where email like :keyword";
		Query query = session.createQuery(hql);
		query.setParameter("keyword", "%"+keyword+"%");
		if (query.list().size() >= 1)
			list = query.list();
		return list;
	}
	
	public int checkEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Staff where email = :email";
		Query query = session.createQuery(hql);
		query.setParameter("email", email);
		if (query.list().size() > 0) return 1;
		return 0;
	}
}

