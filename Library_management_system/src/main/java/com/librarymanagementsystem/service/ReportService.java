/**
 * 
 */
package com.librarymanagementsystem.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.librarymanagementsystem.dao.Dao;
import com.librarymanagementsystem.entity.Report;

/**
 * @author Michael_IT
 *
 */
@Service
@Transactional
public class ReportService {
	@Autowired
	SessionFactory sessionFactory;
	@Autowired
	Dao<Report> reportDao;
	
	public ArrayList<Report> getAll() {
		return reportDao.getAll();
	}
	
	public Report getById(Report t) {
		return reportDao.getById(t);
	}
	
	public Report add(Report t) {
		return reportDao.add(t);
	}
	
	public boolean update(Report t) {
		return reportDao.update(t);
	}
	
	public boolean delete(Report t) {
		return reportDao.delete(t);
	}
	
	public boolean deleteById(Report t) {
		return reportDao.deleteById(t);
	}
	
	public List<Report> getAll2() {
		List<Report> list = new ArrayList<Report>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Report";
		Query query = session.createQuery(hql);
		if(query.list().size() > 0)
			list = query.list();
		return list;
	}
	
	public Report getReport(int report_id) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "from Report where id = :report_id";
		Query query = session.createQuery(hql);
		query.setParameter("report_id", report_id);
		return (Report) query.list().get(0);
	}
}
