/**
 * 
 */
package com.librarymanagementsystem.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.librarymanagementsystem.entity.Other_Percentage_Log;

/**
 * @author Michael_IT
 *
 */
@Repository
public class Other_Percentage_LogDao extends Dao<Other_Percentage_Log>{
	@Autowired
	SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<Other_Percentage_Log> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<Other_Percentage_Log>) session.createQuery("from Other_Percentage_Log").list();
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public Other_Percentage_Log getById(Other_Percentage_Log t) {
		Session session = sessionFactory.getCurrentSession();
		return (Other_Percentage_Log) session.get(Other_Percentage_Log.class, t.getId());
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public Other_Percentage_Log add(Other_Percentage_Log t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(Other_Percentage_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Other_Percentage_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != t) 
		{
			try {
				session.delete(t);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(Other_Percentage_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		Other_Percentage_Log lecturer = (Other_Percentage_Log) session.load(Other_Percentage_Log.class, t.getId());
		if(null != lecturer)
		{
			session.delete(lecturer);
		}
		return Boolean.FALSE;
	}
}
