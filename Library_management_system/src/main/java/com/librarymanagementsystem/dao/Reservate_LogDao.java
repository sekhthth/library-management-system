/**
 * 
 */
package com.librarymanagementsystem.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.librarymanagementsystem.entity.Reservate_Log;
import com.librarymanagementsystem.entity.Reservate_Log;

/**
 * @author Michael_IT
 *
 */
@Repository
public class Reservate_LogDao extends Dao<Reservate_Log> {
	@Autowired
	SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<Reservate_Log> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<Reservate_Log>) session.createQuery("from Reservate_Log").list();
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public Reservate_Log getById(Reservate_Log t) {
		Session session = sessionFactory.getCurrentSession();
		return (Reservate_Log) session.get(Reservate_Log.class, t.getId());
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public Reservate_Log add(Reservate_Log t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(Reservate_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Reservate_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != t) 
		{
			try {
				session.delete(t);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(Reservate_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		Reservate_Log historical_borrowing = (Reservate_Log) session.load(Reservate_Log.class, t.getId());
		if(null != historical_borrowing)
		{
			session.delete(historical_borrowing);
		}
		return Boolean.FALSE;
	}
}
