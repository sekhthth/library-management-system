/**
 * 
 */
package com.librarymanagementsystem.dao;

import java.util.ArrayList;

/**
 * @author Michael_IT
 *
 */
public abstract class Dao<T> {
	public abstract ArrayList<T> getAll();
	public abstract T getById(T t);
	public abstract T add(T t);
	public abstract boolean update(T t);
	public abstract boolean delete(T t);
	public abstract boolean deleteById(T t);
}
