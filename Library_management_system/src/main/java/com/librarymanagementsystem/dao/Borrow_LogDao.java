/**
 * 
 */
package com.librarymanagementsystem.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.librarymanagementsystem.entity.Borrow_Log;
import com.librarymanagementsystem.entity.Borrow_Log;

/**
 * @author Michael_IT
 *
 */
@Repository
public class Borrow_LogDao extends Dao<Borrow_Log> {
	@Autowired
	SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<Borrow_Log> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<Borrow_Log>) session.createQuery("from Borrow_Log").list();
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public Borrow_Log getById(Borrow_Log t) {
		Session session = sessionFactory.getCurrentSession();
		return (Borrow_Log) session.get(Borrow_Log.class, t.getId());
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public Borrow_Log add(Borrow_Log t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(Borrow_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Borrow_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != t) 
		{
			try {
				session.delete(t);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(Borrow_Log t) {
		Session session = this.sessionFactory.getCurrentSession();
		Borrow_Log historical_borrowing = (Borrow_Log) session.load(Borrow_Log.class, t.getId());
		if(null != historical_borrowing)
		{
			session.delete(historical_borrowing);
		}
		return Boolean.FALSE;
	}
}
