/**
 * 
 */
package com.librarymanagementsystem.dao;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.librarymanagementsystem.entity.Book;

/**
 * @author Michael_IT
 *
 */
@Repository
public class BookDao extends Dao<Book> {
	@Autowired
	SessionFactory sessionFactory;

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getAll()
	 */
	@Override
	public ArrayList<Book> getAll() {
		Session session = sessionFactory.getCurrentSession();
		return (ArrayList<Book>) session.createQuery("from Book").list();
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#getById(java.lang.Object)
	 */
	@Override
	public Book getById(Book t) {
		Session session = sessionFactory.getCurrentSession();
		return (Book) session.get(Book.class, t.getId());
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#add(java.lang.Object)
	 */
	@Override
	public Book add(Book t) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(t);
		return t;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#update(java.lang.Object)
	 */
	@Override
	public boolean update(Book t) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(t);
			return Boolean.TRUE;
		}
		catch(Exception e){
			return Boolean.FALSE;
		}
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#delete(java.lang.Object)
	 */
	@Override
	public boolean delete(Book t) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != t) 
		{
			try {
				session.delete(t);
				return Boolean.TRUE;
			}
			catch(Exception e)
			{
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	/* (non-Javadoc)
	 * @see com.librarymanagementsystem.dao.Dao#deleteById(java.lang.Object)
	 */
	@Override
	public boolean deleteById(Book t) {
		Session session = this.sessionFactory.getCurrentSession();
		Book book = (Book) session.load(Book.class, t.getId());
		if(null != book)
		{
			session.delete(book);
		}
		return Boolean.FALSE;
	}
}
