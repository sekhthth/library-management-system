/**
 * 
 */
package com.librarymanagementsystem.entity;

/**
 * @author Michael_IT
 *
 */
public class BorrowTmp {
	private String return_date;
	private String id;

	public BorrowTmp(String return_date, String id) {
		super();
		this.return_date = return_date;
		this.id = id;
	}

	public BorrowTmp() {
		super();
	}

	public String getReturn_date() {
		return return_date;
	}

	public void setReturn_date(String return_date) {
		this.return_date = return_date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
