/**
 * 
 */
package com.librarymanagementsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "report", catalog = "librarymanagement")
public class Report {
	private int id;
	private Staff staff_id;
	private String report_date;

	public Report(int id, Staff staff_id, String report_date) {
		super();
		this.id = id;
		this.staff_id = staff_id;
		this.report_date = report_date;
	}

	public Report() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name="staff_id", referencedColumnName="id")
	public Staff getStaff_id() {
		return staff_id;
	}

	public void setStaff_id(Staff staff_id) {
		this.staff_id = staff_id;
	}

	@Column(name="report_date")
	public String getReport_date() {
		return report_date;
	}

	public void setReport_date(String report_date) {
		this.report_date = report_date;
	}
}
