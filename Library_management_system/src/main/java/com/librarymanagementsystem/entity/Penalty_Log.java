/**
 * 
 */
package com.librarymanagementsystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "penalty_log", catalog = "librarymanagement")
public class Penalty_Log {
	private int id;
//	private String book_id;
//	private int borrow_log_id;
	private String start;
	private int penalty;
//	private String person_id;
	private Book book_id;
	private Borrow_Log borrow_log_id;
	private Borrower person_id;
	private Other_Percentage_Log other_Percentage_Log;
	
	public Penalty_Log(int id, String book_id, int borrow_log_id, String start, int penalty, String person_id) {
		super();
		this.id = id;
//		this.book_id = book_id;
//		this.borrow_log_id = borrow_log_id;
		this.start = start;
		this.penalty = penalty;
//		this.person_id = person_id;
	}

	public Penalty_Log() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

//	@Column(name="book_id")
//	public String getBook_id() {
//		return book_id;
//	}
//
//	public void setBook_id(String book_id) {
//		this.book_id = book_id;
//	}
//
//	@Column(name="borrow_log_id")
//	public int getBorrow_log_id() {
//		return borrow_log_id;
//	}
//
//	public void setBorrow_log_id(int borrow_log_id) {
//		this.borrow_log_id = borrow_log_id;
//	}

	@Column(name="start")
	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	@Column(name="penalty")
	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}

//	@Column(name="person_id")
//	public String getPerson_id() {
//		return person_id;
//	}
//
//	public void setPerson_id(String person_id) {
//		this.person_id = person_id;
//	}

	@ManyToOne
	@JoinColumn(name="book_id", referencedColumnName="id")
	public Book getBook_id() {
		return book_id;
	}

	public void setBook_id(Book book_id) {
		this.book_id = book_id;
	}

	@ManyToOne
	@JoinColumn(name="borrow_log_id", referencedColumnName="id")
	public Borrow_Log getBorrow_log_id() {
		return borrow_log_id;
	}

	public void setBorrow_log_id(Borrow_Log borrow_log_id) {
		this.borrow_log_id = borrow_log_id;
	}
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="person_id", referencedColumnName="person_id")
	public Borrower getPerson_id() {
		return person_id;
	}

	public void setPerson_id(Borrower person_id) {
		this.person_id = person_id;
	}

	@OneToOne(mappedBy="id.penalty_log", fetch=FetchType.LAZY, optional=false)
	@JsonIgnore
	public Other_Percentage_Log getOther_Percentage_Log() {
		return other_Percentage_Log;
	}

	public void setOther_Percentage_Log(Other_Percentage_Log other_Percentage_Log) {
		this.other_Percentage_Log = other_Percentage_Log;
	}
}
