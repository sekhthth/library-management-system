/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "reservate_log", catalog = "librarymanagement")
public class Reservate_Log {
	private Reservate_LogId id;
	private String reservate_date;
//	private String person_id;
	private Staff staff_id;
	private Set<Reservate_Log_Detail> reservate_Log_Details = new HashSet<>();
	private Borrower person_id;

	public Reservate_Log(Reservate_LogId id, String reservate_date, Staff staff_id) {
		super();
		this.id = id;
		this.reservate_date = reservate_date;
		this.staff_id = staff_id;
	}

	public Reservate_Log() {
		super();
	}

	@EmbeddedId
	public Reservate_LogId getId() {
		return id;
	}

	public void setId(Reservate_LogId id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name="staff_id", referencedColumnName="id")
	@JsonIgnore
	public Staff getStaff_id() {
		return staff_id;
	}

	public void setStaff_id(Staff staff_id) {
		this.staff_id = staff_id;
	}

	@Column(name="reservate_date")
	public String getReservate_date() {
		return reservate_date;
	}

	public void setReservate_date(String reservate_date) {
		this.reservate_date = reservate_date;
	}

//	@Column(name="person_id")
//	public String getPerson_id() {
//		return person_id;
//	}
//
//	public void setPerson_id(String person_id) {
//		this.person_id = person_id;
//	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="id.reservate_log")
	@JsonIgnore
	public Set<Reservate_Log_Detail> getReservate_Log_Details() {
		return reservate_Log_Details;
	}

	public void setReservate_Log_Details(Set<Reservate_Log_Detail> reservate_Log_Details) {
		this.reservate_Log_Details = reservate_Log_Details;
	}
	
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="person_id", referencedColumnName="person_id")
	public Borrower getPerson_id() {
		return person_id;
	}

	public void setPerson_id(Borrower person_id) {
		this.person_id = person_id;
	}
}
