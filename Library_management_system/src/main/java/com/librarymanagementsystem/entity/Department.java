/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "department", catalog = "librarymanagement")
public class Department {
	private DepartmentId id;
	private String name;
	private Set<Student> students;
	private Set<Lecturer> lecturers;

	public Department(DepartmentId id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Department() {
		super();
	}

	@EmbeddedId
	public DepartmentId getId() {
		return id;
	}

	public void setId(DepartmentId id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy = "department_id", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	@OneToMany(mappedBy = "department_id", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	public Set<Lecturer> getLecturers() {
		return lecturers;
	}

	public void setLecturers(Set<Lecturer> lecturers) {
		this.lecturers = lecturers;
	}
}
