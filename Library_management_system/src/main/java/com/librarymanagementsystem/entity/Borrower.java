/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "borrower", catalog = "librarymanagement")
public class Borrower {
	private BorrowerId id;
	private Set<Borrow_Log> borrow_Logs = new HashSet<>();
	private Set<Reservate_Log> reservate_Logs = new HashSet<Reservate_Log>();
	private Set<Penalty_Log> penalty_Logs = new HashSet<Penalty_Log>();

	public Borrower(BorrowerId id) {
		super();
		this.id = id;
	}

	public Borrower() {
		super();
	}

	@EmbeddedId
	public BorrowerId getId() {
		return id;
	}

	public void setId(BorrowerId id) {
		this.id = id;
	}

	@OneToMany(mappedBy = "person_id", fetch = FetchType.EAGER)
	public Set<Borrow_Log> getBorrow_Logs() {
		return borrow_Logs;
	}

	public void setBorrow_Logs(Set<Borrow_Log> borrow_Logs) {
		this.borrow_Logs = borrow_Logs;
	}

	@OneToMany(mappedBy = "person_id", fetch = FetchType.LAZY)
	@JsonIgnore
	public Set<Reservate_Log> getReservate_Logs() {
		return reservate_Logs;
	}

	public void setReservate_Logs(Set<Reservate_Log> reservate_Logs) {
		this.reservate_Logs = reservate_Logs;
	}

	@OneToMany(mappedBy = "person_id", fetch = FetchType.LAZY)
	@JsonIgnore
	public Set<Penalty_Log> getPenalty_Logs() {
		return penalty_Logs;
	}

	public void setPenalty_Logs(Set<Penalty_Log> penalty_Logs) {
		this.penalty_Logs = penalty_Logs;
	}
}
