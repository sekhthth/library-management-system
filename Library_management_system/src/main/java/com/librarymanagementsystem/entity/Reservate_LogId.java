/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class Reservate_LogId implements Serializable {
	private int id;

	public Reservate_LogId(int id) {
		super();
		this.id = id;
	}

	public Reservate_LogId() {
		super();
	}

	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservate_LogId other = (Reservate_LogId) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
