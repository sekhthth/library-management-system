/**
 * 
 */
package com.librarymanagementsystem.entity;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author Michael_IT
 *
 */
public class Myfile {
	private static final long serialVersionUID = 1L;
	private MultipartFile multipartFile;
	private String description;

	public MultipartFile getMultipartFile() {
		return multipartFile;
	}

	public void setMultipartFile(MultipartFile multipartFile) {
		this.multipartFile = multipartFile;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
