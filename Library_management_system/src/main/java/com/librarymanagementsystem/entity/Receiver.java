/**
 * 
 */
package com.librarymanagementsystem.entity;

/**
 * @author Michael_IT
 *
 */
public class Receiver {
	private String email;
	private String contentMail;
	private String contentTail;
	private String bookHtml;
	private String person_id;

	public Receiver(String email, String contentMail, String contentTail, String bookHtml, String person_id) {
		super();
		this.email = email;
		this.contentMail = contentMail;
		this.contentTail = contentTail;
		this.bookHtml = bookHtml;
		this.person_id = person_id;
	}

	public Receiver() {
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContentMail() {
		return contentMail;
	}

	public void setContentMail(String contentMail) {
		this.contentMail = contentMail;
	}

	public String getContentTail() {
		return contentTail;
	}

	public void setContentTail(String contentTail) {
		this.contentTail = contentTail;
	}

	public String getBookHtml() {
		return bookHtml;
	}

	public void setBookHtml(String bookHtml) {
		this.bookHtml = bookHtml;
	}
	
	public String getPerson_id() {
		return person_id;
	}

	public void setPerson_id(String person_id) {
		this.person_id = person_id;
	}
	
	public String getCompleteContentMail() {
		return this.contentMail+this.bookHtml+this.contentTail;
	}
	
	public void addBookHtmlToContentMail(String bookHtml) {
		this.bookHtml += bookHtml;
	}
}
