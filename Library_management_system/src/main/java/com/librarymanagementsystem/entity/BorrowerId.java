/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

import com.google.gson.annotations.Since;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class BorrowerId implements Serializable {
	private String person_id;

	public BorrowerId(String person_id) {
		super();
		this.person_id = person_id;
	}

	public BorrowerId() {
		super();
	}

	@Column(name="person_id", length=12)
	@Size(min=12, max=12,message="Person id must contain 12 character")
	public String getPerson_id() {
		return person_id;
	}

	public void setPerson_id(String person_id) {
		this.person_id = person_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((person_id == null) ? 0 : person_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BorrowerId other = (BorrowerId) obj;
		if (person_id == null) {
			if (other.person_id != null)
				return false;
		} else if (!person_id.equals(other.person_id))
			return false;
		return true;
	}
}
