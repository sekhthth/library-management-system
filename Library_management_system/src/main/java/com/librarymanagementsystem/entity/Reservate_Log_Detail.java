/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "reservate_log_detail", catalog = "librarymanagement")
public class Reservate_Log_Detail {
	private Reservate_Log_DetailIdTmp id;
	private String due_date;
	private String duration;
	private int reserved;
	private int status;

	public Reservate_Log_Detail(Reservate_Log_DetailIdTmp id, String due_date, String duration, int status) {
		super();
		this.id = id;
		this.due_date = due_date;
		this.duration = duration;
		this.status = status;
	}

	public Reservate_Log_Detail() {
		super();
	}

	@EmbeddedId
	public Reservate_Log_DetailIdTmp getId() {
		return id;
	}

	public void setId(Reservate_Log_DetailIdTmp id) {
		this.id = id;
	}

	@Column(name="due_date")
	public String getDue_date() {
		return due_date;
	}

	public void setDue_date(String due_date) {
		this.due_date = due_date;
	}

	@Column(name="duration")
	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	@Column(name="status")
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	@Column(name="reserved")
	public int getReserved() {
		return reserved;
	}

	public void setReserved(int reserved) {
		this.reserved = reserved;
	}
}
