/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.librarymanagementsystem.dao.StaffDao;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "staff", catalog = "librarymanagement")
public class Staff {
	private StaffId id;
	private String name;
	private String password;
	private String email;
	private String avatar;
	private String role;
	private Myfile myfile = new Myfile();
	private Set<Borrow_Log> borrow_Logs = new HashSet<>();
	private Set<Reservate_Log> reservate_Logs = new HashSet<>();
	private Set<Report> reports = new HashSet<>();

	public Staff(StaffId id, String name, String password, String email, String avatar) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.email = email;
		this.avatar = avatar;
	}

	public Staff() {
		super();
	}

	public Staff(StaffId id) {
		super();
		this.id = id;
	}

	@EmbeddedId
	public StaffId getId() {
		return id;
	}

	public void setId(StaffId id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "avatar")
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	@OneToMany(mappedBy = "staff_id")
	@JsonIgnore
	public Set<Borrow_Log> getBorrow_Logs() {
		return borrow_Logs;
	}

	public void setBorrow_Logs(Set<Borrow_Log> borrow_Logs) {
		this.borrow_Logs = borrow_Logs;
	}

	@OneToMany(mappedBy = "staff_id")
	@JsonIgnore
	public Set<Reservate_Log> getReservate_Logs() {
		return reservate_Logs;
	}

	public void setReservate_Logs(Set<Reservate_Log> reservate_Logs) {
		this.reservate_Logs = reservate_Logs;
	}

	@OneToMany(mappedBy = "staff_id")
	@JsonIgnore
	public Set<Report> getReports() {
		return reports;
	}

	public void setReports(Set<Report> reports) {
		this.reports = reports;
	}

	@Transient
	public Myfile getMyfile() {
		return myfile;
	}

	public void setMyfile(Myfile myfile) {
		this.myfile = myfile;
	}

	@Column(name = "role")
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
