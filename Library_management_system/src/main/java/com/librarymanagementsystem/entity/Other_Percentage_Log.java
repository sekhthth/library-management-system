/**
 * 
 */
package com.librarymanagementsystem.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "other_percenatage_log", catalog = "librarymanagement")
public class Other_Percentage_Log {
	private Other_Percentage_LogId id;

	public Other_Percentage_Log(Other_Percentage_LogId id) {
		super();
		this.id = id;
	}

	public Other_Percentage_Log() {
		super();
	}

	@EmbeddedId
	public Other_Percentage_LogId getId() {
		return id;
	}

	public void setId(Other_Percentage_LogId id) {
		this.id = id;
	}

}
