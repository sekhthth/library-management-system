/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "borrow_log", catalog = "librarymanagement")
public class Borrow_Log {
	private Borrow_LogId id;
//	private String borrower_id;
	private String borrow_date;
	private String return_date;
	private boolean status;
	private Staff staff_id;
//	private int penalty;
	private Set<Borrow_Log_Detail> borrow_Log_Details = new HashSet<>();
	private Set<Penalty_Log> penalty_Logs = new HashSet<>();
	private Borrower person_id;

	public Borrow_Log(Borrow_LogId id, String borrow_date, Staff staff_id, int penalty) {
		super();
		this.id = id;
		this.borrow_date = borrow_date;
		this.staff_id = staff_id;
//		this.penalty = penalty;
	}

	public Borrow_Log() {
		super();
	}

	@EmbeddedId
	public Borrow_LogId getId() {
		return id;
	}

	public void setId(Borrow_LogId id) {
		this.id = id;
	}

	@Column(name="borrow_date")
	public String getBorrow_date() {
		return borrow_date;
	}

	public void setBorrow_date(String borrow_date) {
		this.borrow_date = borrow_date;
	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="staff_id")
	public Staff getStaff_id() {
		return staff_id;
	}

	public void setStaff_id(Staff staff_id) {
		this.staff_id = staff_id;
	}

//	@Column(name="penalty")
//	public int getPenalty() {
//		return penalty;
//	}
//
//	public void setPenalty(int penalty) {
//		this.penalty = penalty;
//	}

	@Column(name="status", columnDefinition="TINYINT(1)")
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

//	@Column(name="borrower_id")
//	public String getBorrower_id() {
//		return borrower_id;
//	}
//
//	public void setBorrower_id(String borrower_id) {
//		this.borrower_id = borrower_id;
//	}

	@Column(name="return_date")
	public String getReturn_date() {
		return return_date;
	}

	public void setReturn_date(String return_date) {
		this.return_date = return_date;
	}
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="id.borrow_log")
	@JsonIgnore
	public Set<Borrow_Log_Detail> getBorrow_Log_Details() {
		return borrow_Log_Details;
	}

	public void setBorrow_Log_Details(Set<Borrow_Log_Detail> borrow_Log_Details) {
		this.borrow_Log_Details = borrow_Log_Details;
	}

	@OneToMany(fetch=FetchType.LAZY, mappedBy="borrow_log_id")
	@JsonIgnore
	public Set<Penalty_Log> getPenalty_Logs() {
		return penalty_Logs;
	}

	public void setPenalty_Logs(Set<Penalty_Log> penalty_Logs) {
		this.penalty_Logs = penalty_Logs;
	}

	@ManyToOne()
	@JsonIgnore
	@JoinColumn(name="person_id")
	public Borrower getPerson_id() {
		return person_id;
	}

	public void setPerson_id(Borrower person_id) {
		this.person_id = person_id;
	}
}
