/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class Reservate_Log_DetailIdTmp implements Serializable {
	private static final long serialVersionUID = 1L;
	private Book book;
	private Reservate_Log reservate_log;

	public Reservate_Log_DetailIdTmp(Book book, Reservate_Log reservate_log) {
		super();
		this.book = book;
		this.reservate_log = reservate_log;
	}

	public Reservate_Log_DetailIdTmp() {
		super();
	}

	@ManyToOne
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@ManyToOne
	public Reservate_Log getReservate_log() {
		return reservate_log;
	}

	public void setReservate_log(Reservate_Log reservate_log) {
		this.reservate_log = reservate_log;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((reservate_log == null) ? 0 : reservate_log.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservate_Log_DetailIdTmp other = (Reservate_Log_DetailIdTmp) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (reservate_log == null) {
			if (other.reservate_log != null)
				return false;
		} else if (!reservate_log.equals(other.reservate_log))
			return false;
		return true;
	}
}
