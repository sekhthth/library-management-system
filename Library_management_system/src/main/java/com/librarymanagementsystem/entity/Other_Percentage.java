/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "other_percentage", catalog = "librarymanagement")
public class Other_Percentage {
	private int id;
	private int percentage;
	private int fine;
	private Set<Penalty_Log> penalty_Logs = new HashSet<Penalty_Log>();
	private Set<Other_Percentage_Log> other_Percentage_Logs = new HashSet<Other_Percentage_Log>();

	public Other_Percentage(int id, int percentage) {
		super();
		this.id = id;
		this.percentage = percentage;
	}

	public Other_Percentage() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "percentage")
	public int getPercentage() {
		return percentage;
	}

	public void setPercentage(int percentage) {
		this.percentage = percentage;
	}

	@OneToMany(mappedBy="id.other_percentage")
	@JsonIgnore
	public Set<Other_Percentage_Log> getOther_Percentage_Logs() {
		return other_Percentage_Logs;
	}

	public void setOther_Percentage_Logs(Set<Other_Percentage_Log> other_Percentage_Logs) {
		this.other_Percentage_Logs = other_Percentage_Logs;
	}

	@Column(name="fine")
	public int getFine() {
		return fine;
	}

	public void setFine(int fine) {
		this.fine = fine;
	}
}
