/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class Other_Percentage_LogId implements Serializable{
	private Penalty_Log penalty_log;
	private Other_Percentage other_percentage;

	public Other_Percentage_LogId(Penalty_Log penalty_log, Other_Percentage other_percentage) {
		super();
		this.penalty_log = penalty_log;
		this.other_percentage = other_percentage;
	}

	public Other_Percentage_LogId() {
		super();
	}

	@OneToOne(fetch=FetchType.LAZY)
	public Penalty_Log getPenalty_log() {
		return penalty_log;
	}

	public void setPenalty_log(Penalty_Log penalty_log) {
		this.penalty_log = penalty_log;
	}

	@ManyToOne()
	@JsonIgnore
	public Other_Percentage getOther_percentage() {
		return other_percentage;
	}

	public void setOther_percentage(Other_Percentage other_percentage) {
		this.other_percentage = other_percentage;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((other_percentage == null) ? 0 : other_percentage.hashCode());
		result = prime * result + ((penalty_log == null) ? 0 : penalty_log.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Other_Percentage_LogId other = (Other_Percentage_LogId) obj;
		if (other_percentage == null) {
			if (other.other_percentage != null)
				return false;
		} else if (!other_percentage.equals(other.other_percentage))
			return false;
		if (penalty_log == null) {
			if (other.penalty_log != null)
				return false;
		} else if (!penalty_log.equals(other.penalty_log))
			return false;
		return true;
	}
}
