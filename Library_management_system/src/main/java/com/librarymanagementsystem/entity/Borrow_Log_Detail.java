/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "borrow_log_detail", catalog = "librarymanagement")
public class Borrow_Log_Detail {
	private Borrow_Log_DetailIdTmp id;
	private int reserved;
	private int penalty;
	private String return_date;
//	private boolean status;
	

	public Borrow_Log_Detail(Borrow_Log_DetailIdTmp id) {
		super();
		this.id = id;
	}

	public Borrow_Log_Detail() {
		super();
	}

	@EmbeddedId
	public Borrow_Log_DetailIdTmp getId() {
		return id;
	}

	public void setId(Borrow_Log_DetailIdTmp id) {
		this.id = id;
	}

	@Column(name="reserved")
	public int getReserved() {
		return reserved;
	}

	public void setReserved(int reserved) {
		this.reserved = reserved;
	}

	@Column(name="penalty")
	public int getPenalty() {
		return penalty;
	}

	public void setPenalty(int penalty) {
		this.penalty = penalty;
	}
	
	@Column(name="return_date")
	public String getReturn_date() {
		return return_date;
	}

	public void setReturn_date(String return_date) {
		this.return_date = return_date;
	}
	
//	@Column(name="status", columnDefinition="TINYINT(1)")
//	public boolean isStatus() {
//		return status;
//	}
//
//	public void setStatus(boolean status) {
//		this.status = status;
//	}
}
