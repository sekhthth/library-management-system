/**
 * 
 */
package com.librarymanagementsystem.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "lecturer", catalog = "librarymanagement")
public class Lecturer {
	private LecturerId id;
	private String name;
	private String phone;
	private String email;
	private String avatar;
	private Department department_id;

	public Lecturer(LecturerId id, String name, String phone, String email, String dob,String avatar) {
		super();
		this.id = id;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.avatar = avatar;
	}

	public Lecturer() {
		super();
	}

	@EmbeddedId
	public LecturerId getId() {
		return id;
	}
	
	public void setId(LecturerId id) {
		this.id = id;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="phone")
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name="email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

//	@Column(name="dob")
//	public String getDob() {
//		return dob;
//	}
//
//	public void setDob(String dob) {
//		this.dob = dob;
//	}

//	@Column(name="sex")
//	public int getSex() {
//		return sex;
//	}
//
//	public void setSex(int sex) {
//		this.sex = sex;
//	}

	@Column(name="avatar")
	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
//	@Column(name="room")
//	public String getRoom() {
//		return room;
//	}
//
//	public void setRoom(String room) {
//		this.room = room;
//	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name="depertment_id", referencedColumnName="id")
	public Department getDepartment_id() {
		return department_id;
	}

	public void setDepartment_id(Department department_id) {
		this.department_id = department_id;
	}
}
