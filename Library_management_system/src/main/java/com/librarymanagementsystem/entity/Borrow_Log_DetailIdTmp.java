/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

/**
 * @author Michael_IT
 *
 */
@Embeddable
public class Borrow_Log_DetailIdTmp implements Serializable {
	private Book book;
	private Borrow_Log borrow_log;

	public Borrow_Log_DetailIdTmp(Book book, Borrow_Log borrow_log) {
		super();
		this.book = book;
		this.borrow_log = borrow_log;
	}

	public Borrow_Log_DetailIdTmp() {
		super();
	}

	@ManyToOne
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@ManyToOne
	public Borrow_Log getBorrow_log() {
		return borrow_log;
	}

	public void setBorrow_log(Borrow_Log borrow_log) {
		this.borrow_log = borrow_log;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((borrow_log == null) ? 0 : borrow_log.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Borrow_Log_DetailIdTmp other = (Borrow_Log_DetailIdTmp) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (borrow_log == null) {
			if (other.borrow_log != null)
				return false;
		} else if (!borrow_log.equals(other.borrow_log))
			return false;
		return true;
	}
}
