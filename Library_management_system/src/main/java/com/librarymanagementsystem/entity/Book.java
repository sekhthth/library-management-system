/**
 * 
 */
package com.librarymanagementsystem.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Michael_IT
 *
 */
@Entity
@Table(name = "book", catalog = "librarymanagement")
public class Book {
	private BookId id;
	private String name;
	private String cover_image;
	private String timePrint;
	private String ymanufacture;
	private String manufacturer;
	private int quantity;
	private int borrowed;
//	private int reservated;
	private Category category_id;
	private Myfile myfile = new Myfile();
	private Set<Borrow_Log_Detail> borrow_Log_Details = new HashSet<>();
	private Set<Reservate_Log_Detail> reservate_Log_Details = new HashSet<>();
	private Set<Penalty_Log> penalty_Logs = new HashSet<>();
	private int price;

	public Book(BookId id, String name, String cover_image) {
		super();
		this.id = id;
		this.name = name;
		this.cover_image = cover_image;
	}

	public Book() {
		super();
	}

	@EmbeddedId
	public BookId getId() {
		return id;
	}

	public void setId(BookId id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "cover_image")
	public String getCover_image() {
		return cover_image;
	}

	public void setCover_image(String cover_image) {
		this.cover_image = cover_image;
	}

	@Column(name = "borrowed")
	public int getBorrowed() {
		return borrowed;
	}

	public void setBorrowed(int borrowed) {
		this.borrowed = borrowed;
	}

	@Column(name = "timePrint")
	public String getTimePrint() {
		return timePrint;
	}

	public void setTimePrint(String timePrint) {
		this.timePrint = timePrint;
	}

	@Column(name = "manufacturer")
	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	@Column(name="ymanufacture")
	public String getYmanufacture() {
		return ymanufacture;
	}

	public void setYmanufacture(String ymanufacture) {
		this.ymanufacture = ymanufacture;
	}

	@Column(name="quantity")
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="price")
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Transient
	public Myfile getMyfile() {
		return myfile;
	}

	public void setMyfile(Myfile myfile) {
		this.myfile = myfile;
	}

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	public Category getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Category category_id) {
		this.category_id = category_id;
	}

	@OneToMany(mappedBy="id.book")
	@JsonIgnore
	public Set<Borrow_Log_Detail> getBorrow_Log_Details() {
		return borrow_Log_Details;
	}

	public void setBorrow_Log_Details(Set<Borrow_Log_Detail> borrow_Log_Details) {
		this.borrow_Log_Details = borrow_Log_Details;
	}

	@OneToMany( mappedBy="id.book")
	@JsonIgnore
	public Set<Reservate_Log_Detail> getReservate_Log_Details() {
		return reservate_Log_Details;
	}

	public void setReservate_Log_Details(Set<Reservate_Log_Detail> reservate_Log_Details) {
		this.reservate_Log_Details = reservate_Log_Details;
	}

	@OneToMany(mappedBy="book_id")
	@JsonIgnore
	public Set<Penalty_Log> getPenalty_Logs() {
		return penalty_Logs;
	}

	public void setPenalty_Logs(Set<Penalty_Log> penalty_Logs) {
		this.penalty_Logs = penalty_Logs;
	}
	
	
	
	

//	@Column(name="reserved")
//	public int getReservated() {
//		return reservated;
//	}
//
//	public void setReservated(int reservated) {
//		this.reservated = reservated;
//	}
}
