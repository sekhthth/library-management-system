package com.librarymanagementsystem.config;

import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Category;
import com.librarymanagementsystem.entity.Department;
import com.librarymanagementsystem.entity.Borrow_Log;
import com.librarymanagementsystem.entity.Borrow_Log_Detail;
import com.librarymanagementsystem.entity.Borrower;
import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.entity.Penalty_Log;
import com.librarymanagementsystem.entity.Report;
import com.librarymanagementsystem.entity.Reservate_Log;
import com.librarymanagementsystem.entity.Reservate_Log_Detail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.Student;

@EnableTransactionManagement
@Configuration
public class DatabaseConfig extends WebMvcConfigurerAdapter {
	@Bean
	public LocalSessionFactoryBean sessionFactory(BasicDataSource dataSource) {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource);
		sessionFactory.setPackagesToScan(new String[] { "com.librarymanagementsystem.entity" });
		sessionFactory.setAnnotatedClasses(Category.class, Book.class, Department.class, 
											Student.class, Lecturer.class, Staff.class, 
											Report.class, Reservate_Log.class, 
											Borrow_Log.class, Reservate_Log_Detail.class, 
											Borrow_Log_Detail.class, Penalty_Log.class,
											Borrower.class);
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		properties.put("hibernate.show_sql", true);
		properties.put("hibernate.format_sql", true);
		properties.put("hibernate.hbm2ddl.auto", "update");
		return properties;
	}

	@Bean(name = "dataSource")
	public BasicDataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/librarymanagement");
		dataSource.setUsername("root");
		dataSource.setPassword("nlthanh");
		dataSource.setInitialSize(10);
		return dataSource;
	}

	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory s) {
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(s);
		return txManager;
	}
}
