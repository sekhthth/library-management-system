/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Borrow_Log;
import com.librarymanagementsystem.entity.Borrow_Log_Detail;
import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.entity.Other_Percentage;
import com.librarymanagementsystem.entity.Other_Percentage_Log;
import com.librarymanagementsystem.entity.Other_Percentage_LogId;
import com.librarymanagementsystem.entity.Penalty_Log;
import com.librarymanagementsystem.entity.Receiver;
import com.librarymanagementsystem.entity.Reservate_Log;
import com.librarymanagementsystem.entity.Reservate_Log_Detail;
import com.librarymanagementsystem.entity.SendMail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.Student;
import com.librarymanagementsystem.service.BookService;
import com.librarymanagementsystem.service.Borrow_LogService;
import com.librarymanagementsystem.service.Borrow_Log_DetailService;
import com.librarymanagementsystem.service.BorrowerService;
import com.librarymanagementsystem.service.LecturerService;
import com.librarymanagementsystem.service.Other_PercentageService;
import com.librarymanagementsystem.service.Other_Percentage_LogService;
import com.librarymanagementsystem.service.Penalty_LogService;
import com.librarymanagementsystem.service.Reservate_LogService;
import com.librarymanagementsystem.service.Reservate_Log_DetailService;
import com.librarymanagementsystem.service.StaffService;
import com.librarymanagementsystem.service.StudentService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value = "/returnController")
public class ReturnController {
	@Autowired
	private Borrow_LogService borrow_LogService;
	@Autowired
	private Borrow_Log_DetailService borrow_Log_DetailService;
	@Autowired
	private BookService bookService;
	@Autowired
	private Reservate_Log_DetailService reservate_Log_DetailService;
	@Autowired
	private Reservate_LogService reservate_LogService;
	@Autowired
	private StudentService studentService;
	@Autowired
	private LecturerService lecturerService;
	@Autowired
	private StaffService staffService;
	@Autowired
	private Penalty_LogService penalty_LogService;
	@Autowired
	private BorrowerService borrowerService;
	@Autowired
	private Other_PercentageService other_PercentageService;
	@Autowired
	private Other_Percentage_LogService other_Percentage_LogService;

	private String content = "<!-- <!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "	<title></title>\r\n"
			+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"2.css\">\r\n" + "</head>\r\n" + "<body>\r\n"
			+ "	<div class=\"test\">\r\n" + "		<button class=\"btn\">Test</button>\r\n"
			+ "		<div class=\"menu\" id=\"nav\">\r\n" + "			<a href=\"\">1</a>\r\n"
			+ "			<a href=\"\">1</a>\r\n" + "		</div>\r\n" + "	</div>\r\n" + "</body>\r\n" + "</html> -->\r\n"
			+ "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "<head>\r\n" + "	<meta charset=\"UTF-8\">\r\n"
			+ "	<title>IU Library</title>\r\n" + "	<style type=\"text/css\">\r\n" + "		* {\r\n"
			+ "			margin: 0;\r\n" + "			padding: 0;\r\n"
			+ "			font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\r\n" + "		}\r\n"
			+ "		html, body {\r\n" + "			width: 100%;\r\n" + "			height: 100%;\r\n"
			+ "			overflow: hidden;\r\n" + "		}\r\n" + "	</style>\r\n" + "</head>\r\n"
			+ "<body style=\"\">\r\n"
			+ "	<div class=\"container\" style=\"box-sizing: border-box;width: 100%;color: #2c3e50;\r\n"
			+ "    background: linear-gradient(#3498db, #1aa3ff99);\">\r\n"
			+ "		<div style=\"max-width: 800px; margin: auto;min-width: 600px;width: 80%;\">\r\n"
			+ "			<h1 class=\"title\" style=\"text-align: center;margin: 10px 0;padding: 0px;\">International University Library - HCMIU</h1>\r\n"
			+ "			<h3 class=\"text\" style=\"color: aqua;text-align: center;\">The books that you reserved before are available now.</h3>\r\n"
			+ "			<div class=\"warning\" style=\"color: firebrick;margin: 10px 0;text-align: center;\">\r\n"
			+ "				<h3>You have 3 days afer this mail sent for coming IU Library in order to take these books. After 3 days if you do not take these books, your reservation will be remove.</h3>\r\n"
			+ "				<h3 style=\"text-transform: uppercase;margin: 5px 0;\"><span>FROM : </span><span>#current_date</span><span>-TO : </span><span>#due_date</span></h3>\r\n"
			+ "			</div>\r\n"
			+ "			<div class=\"reservation_detail_container\" style=\"width: 60%;margin: auto;border: 1px solid;padding: 10px 10px;\">\r\n"
			+ "				<h2 style=\"text-align: center;margin-bottom: 10px;\">Reservation detail</h2>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation id : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_id</h4>\r\n" + "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation person id : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_person_id</h4>\r\n"
			+ "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation person name : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_person_name</h4>\r\n"
			+ "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation date : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_date</h4>\r\n" + "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Staff id : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#staff_id</h4>\r\n" + "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">staff name : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#staff_name</h4>\r\n" + "				</div>\r\n"
			+ "			</div>\r\n"
			+ "			<div class=\"books_container\" style=\"width: 60%;margin: auto;padding: 10px 10px;border: 1px solid;margin-top: 10px;\">\r\n"
			+ "				<h2 style=\"text-align: center;margin-bottom: 10px;\">Available book(s)</h2>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
//			+ "						<p style=\"font-weight: 500;\"><span>#book_order. </span></p>\r\n"
//			+ "						<p  style=\"font-weight: 500;\"><span>#book_name - </span><span>#book_id</span></p>\r\n"
			+ "					</div>\r\n" + "					\r\n";
	private String contentTail = "</div>\r\n" + "		</div>\r\n"
			+ "		<div class=\"contact\" style=\"width: 100%;border-top: 1px solid;margin-top: 10px;\">\r\n"
			+ "				<div style=\"width: max-content;margin: 0 auto;padding: 5px 0;font-weight: 600;\">\r\n"
			+ "					<p>Cotact us for more detail : </p>\r\n"
			+ "					<p>Email : hcmiu@edu.vn</p>\r\n" + "					<p>Phone : 0xxxxxxxxxxx</p>\r\n"
			+ "					<p>Fax : xxxxxxxxxxxxxx.</p>\r\n" + "				</div>\r\n"
			+ "			</div>	\r\n" + "	</div>\r\n" + "</body>\r\n" + "</html>";
	private String contentTmp = content;
	private int initialContentLength = content.length();
	private int test = 1;
	private String email = "";
	private ArrayList<Receiver> receivers = new ArrayList<Receiver>();
	private Receiver receiver = null;
	
	class object {
		private String text;

		public object(String text) {
			super();
			this.text = text;
		}

		public object() {
			super();
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

	}

	
	@RequestMapping(value = "/return", method = RequestMethod.GET)
	@ResponseBody
	public object returnBook(@RequestParam("data") String data, @RequestParam("borrow_log_id") String borrow_log_id,
			@RequestParam("end") int end, @RequestParam("current_date") String current_date,
			@RequestParam("person_id") String person_id) {
		System.out.println(person_id + "test return");
		String bookHtml = "<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
				+ "						<p style=\"font-weight: 500;\"><span>#book_order. </span></p>\r\n"
				+ "						<p  style=\"font-weight: 500;\"><span>#book_name - </span><span>#book_id</span></p>\r\n"
				+ "					</div>\r\n";
		System.out.println("end : " + end);
		object o = new object();
		Book book = new Book();
		book = bookService.getABookById(data.split(",")[0]);
		Borrow_Log_Detail borrow_Log_Detail = new Borrow_Log_Detail();
		borrow_Log_Detail = borrow_Log_DetailService.getByIdAndBookId(data.split(",")[0],
				Integer.parseInt(borrow_log_id));
		borrow_Log_Detail.setPenalty(Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))));
		if (borrow_Log_Detail.getReturn_date().equalsIgnoreCase(""))
			borrow_Log_Detail.setReturn_date("");
		Penalty_Log penalty_Log = null;
		//borrow_log_id
		System.out.println(borrow_log_id + " borrow_log_id");
		//person_id
		System.out.println(person_id + " person_id");
		//book_id
		System.out.println(data.split(",")[0] + " book_id");
		int penalty_log_id = penalty_LogService.getPenaltyIdForReturn(Integer.parseInt(borrow_log_id), person_id, data.split(",")[0]);
		//penalty_log_id
		System.out.println(penalty_log_id + " penalty_log_id");
		Other_Percentage other_Percentage = null;
		Other_Percentage_Log other_Percentage_Log = null;
		if (penalty_log_id != -1) {
			penalty_Log = penalty_LogService.getById2(penalty_log_id);
			other_Percentage = other_PercentageService.getFromOtherPercentageLog(penalty_log_id);
			if (other_Percentage != null) {
				//other_percentage_id
				System.out.println(other_Percentage.getId() + " other_percentage_id");
				other_Percentage_Log = other_Percentage_LogService.getByPenalty_Log_Id2(penalty_log_id, other_Percentage.getId());
			}
		}
		
		if (Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) == 4
				&& Integer.parseInt(data.substring(data.indexOf(",") + 1, data.indexOf("-"))) != 1) {
			
			borrow_Log_Detail.setReturn_date(formatDate(current_date));
			System.out.println("end4 : " + end);
			Reservate_Log_Detail reservate_Log_Detail = new Reservate_Log_Detail();
			Reservate_Log reservate_Log = new Reservate_Log();
			Student student = null;
			Lecturer lecturer = null;
			book.setBorrowed(book.getBorrowed() - 1);
			int tmp = reservate_LogService.getAPersonForMail(data.split(",")[0]);
			System.out.println(tmp + " Người được gửi mail có sách");
			if (tmp != 0) {
				System.out.println("end0 : " + end);
				contentTmp = content;
				reservate_Log_Detail = reservate_Log_DetailService.getByIdAndBookId(data.split(",")[0], tmp);
				reservate_Log = reservate_LogService.getReservateLog(tmp);
				if (reservate_Log.getPerson_id().getId().getPerson_id().substring(0, 2).equalsIgnoreCase("LE")) {
					lecturer = lecturerService.getLecturer(reservate_Log.getPerson_id().getId().getPerson_id());
					contentTmp = contentTmp.replace("#reservation_person_id", lecturer.getId().getId());
					contentTmp = contentTmp.replace("#reservation_person_name", lecturer.getName());
					email = lecturer.getEmail();
				} else {
					student = studentService.getStudent(reservate_Log.getPerson_id().getId().getPerson_id());
					contentTmp = contentTmp.replace("#reservation_person_id", student.getId().getId());
					contentTmp = contentTmp.replace("#reservation_person_name", student.getName());
					email = student.getEmail();
				}
				
//				System.out.println(reservate_Log_Detail.getReserved());

				Date d = new Date();
				contentTmp = contentTmp.replace("#current_date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d));
				reservate_Log_Detail.setDuration(getDuration(d));
				bookHtml = bookHtml.replace("#book_order", (test++) + "");
				bookHtml = bookHtml.replace("#book_name", book.getName());
				bookHtml = bookHtml.replace("#book_id", book.getId().getId());
				contentTmp = contentTmp.replace("#reservation_id",
						reservate_Log_Detail.getId().getReservate_log().getId().getId() + "");
				contentTmp = contentTmp.replace("#reservation_date", reservate_Log.getReservate_date());
				contentTmp = contentTmp.replace("#staff_id", reservate_Log.getStaff_id().getId().getId());
				contentTmp = contentTmp.replace("#staff_name", reservate_Log.getStaff_id().getName());
				contentTmp = contentTmp.replace("#due_date", reservate_Log_Detail.getDuration());
				receiver = new Receiver(email, contentTmp, contentTail, bookHtml, reservate_Log.getPerson_id().getId().getPerson_id());
				receivers.add(receiver);
				contentTmp += bookHtml;

				reservate_Log_DetailService.update(reservate_Log_Detail);//update
			}
			bookService.update(book);//update
		} else {

			if ((Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) == 1
					&& Integer.parseInt(data.substring(data.indexOf(",") + 1, data.indexOf("-"))) != 1)
					|| (Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) == 2
							&& Integer.parseInt(data.substring(data.indexOf(",") + 1, data.indexOf("-"))) != 1)
					|| (Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) == 3
							&& Integer.parseInt(data.substring(data.indexOf(",") + 1, data.indexOf("-"))) != 1)) {
				if (Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) == 1 || 
						Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) == 2) {
					penalty_Log = new Penalty_Log();
					penalty_Log.setPerson_id(borrowerService.getById2(person_id));
					penalty_Log.setBorrow_log_id(borrow_LogService.getBorrowLog(Integer.parseInt(borrow_log_id)));
					penalty_Log.setBook_id(bookService.getABookById(data.split(",")[0]));

					String tmp = penalty_LogService.getStart(person_id);
					System.out.println(tmp + " book_id" + " test laij");
					System.out.println(data.split(",")[0] + " book_id" + " test laij");
					if (!tmp.equalsIgnoreCase(""))
						penalty_Log.setStart(addOneMonth(tmp));
					else
						penalty_Log.setStart(formatDate(current_date));

					penalty_Log.setPenalty(Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))));
					penalty_LogService.add(penalty_Log);//update
				}
			}

			if (Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) == 3) {
				if (other_Percentage_Log != null) {
					System.out.println(other_Percentage_Log.getId().getOther_percentage().getId() + " other_percentage_id_2 " + Integer.parseInt(data.substring(data.lastIndexOf(",") + 1)));
					if (Integer.parseInt(data.substring(data.lastIndexOf(",") + 1)) != other_Percentage_Log.getId().getOther_percentage().getId()) {
						other_Percentage_LogService.delete(other_Percentage_Log);
						other_Percentage_Log.getId().getOther_percentage().setId(Integer.parseInt(data.substring(data.lastIndexOf(",") + 1)));
						other_Percentage_LogService.add(other_Percentage_Log);
					}
				} else {
					penalty_Log = new Penalty_Log();
					penalty_Log.setPerson_id(borrowerService.getById2(person_id));
					penalty_Log.setBorrow_log_id(borrow_LogService.getBorrowLog(Integer.parseInt(borrow_log_id)));
					penalty_Log.setBook_id(bookService.getABookById(data.split(",")[0]));

					String tmp = penalty_LogService.getStart(person_id);
					if (!tmp.equalsIgnoreCase(""))
						penalty_Log.setStart(addOneMonth(tmp));
					else
						penalty_Log.setStart(formatDate(current_date));

					penalty_Log.setPenalty(Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))));
					penalty_LogService.add(penalty_Log);//update
					other_Percentage_Log = new Other_Percentage_Log(new Other_Percentage_LogId(
							penalty_LogService.getLastesPenalty_Log(),
							other_PercentageService.getById2(Integer.parseInt(data.substring(data.lastIndexOf(",") + 1)))));
					other_Percentage_LogService.add(other_Percentage_Log);//update
				}
			}
			borrow_Log_Detail.setReturn_date(borrow_Log_Detail.getReturn_date());
		}

		// CAL1,1-4

		borrow_Log_DetailService.update(borrow_Log_Detail);//update
		o.setText(getPenalty(data.substring(data.indexOf("-") + 1, data.lastIndexOf(",")), book.getName(),
				Integer.parseInt(data.substring(data.lastIndexOf(",") + 1)), book));
		
		if (end == 1) {
			System.out.println("end1 : " + end);
			contentTmp += contentTail;
//			SendMail.send(contentTmp, email);
			contentTmp = content;
			ArrayList<Receiver> receivers2 = new ArrayList<Receiver>();
			int index;
			for(Receiver r : receivers) {
				System.out.println(r.getBookHtml());
				index = isExist(receivers2, r.getPerson_id());
				System.out.println(index);
				if (index >= 0) {
					receivers2.get(index).addBookHtmlToContentMail(r.getBookHtml());
					System.out.println("---");
					System.out.println(receivers2.get(index).getBookHtml());
					System.out.println("---");
				} else {
					receivers2.add(r);
				}
			}
			System.out.println("lenght : " + receivers2.size());
			for(Receiver r : receivers2) {
				SendMail.send(r.getCompleteContentMail(), r.getEmail());
			}
			
			receivers = new ArrayList<Receiver>();
			receiver = null;
		}
		return o;
	}
	
	@Async
	public void sendMailToReceivers(ArrayList<Receiver> list) {
		for(Receiver r : list) {
			System.out.println(r.getCompleteContentMail());
			SendMail.send(r.getCompleteContentMail(), r.getEmail());
		}
	}
	
	public int isExist(ArrayList<Receiver> list, String person_id) {
		for(Receiver r : list) {
			if (person_id.equalsIgnoreCase(r.getPerson_id()))
				return list.indexOf(r);
		}
		return -1;
	}

	@RequestMapping(value = "/checknumbook", method = RequestMethod.GET)
	@ResponseBody
	public object checkNumBook(@RequestParam("borrow_log_id") String borrow_log_id) {
		object o = new object();
		Borrow_Log borrow_Log = new Borrow_Log();
		if (borrow_Log_DetailService.numOfBooksOfARecord(Integer.parseInt(borrow_log_id)) == borrow_Log_DetailService
				.numOfBooksReturned(Integer.parseInt(borrow_log_id))) {
			borrow_Log = borrow_LogService.getBorrowLog(Integer.parseInt(borrow_log_id));
			borrow_Log.setStatus(true);
			borrow_LogService.update(borrow_Log);//update
			o.setText("Finish return!!!");
		} else {
			o.setText("Unfish return!!!");
		}
		return o;
	}

	public String getPenalty(String penalty, String book_name, int brokenlevel, Book book) {
		String penal = "";
		switch (Integer.parseInt(penalty)) {
		case 1:
			penal = book_name + "#Status : Uncompleted - Late - You are not allowed to borrow for 1 month.";
			break;
		case 2:
			penal = book_name + "#Status : Uncompleted - Miss - Pay money.";
			break;
		case 3:
			penal = book_name + "#Status : Uncompleted - Other - Pay money ("
					+ (other_PercentageService.getById2(brokenlevel).getFine() / 100.0) * book.getPrice()
					+ " VND = " + other_PercentageService.getById2(brokenlevel).getPercentage() + "% of "
					+ book.getPrice() + ".";
			break;
		case 4:
			penal = book_name + "#Status : Completed - Normal - No penalty.";
			break;
		case 5:
			penal = book_name + "#Status : Uncompleted - None - No penalty.";
			break;
		default:
			break;
		}
		return penal;
	}

	public String getDuration(Date current_date) {
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		current_date.setDate(current_date.getDate() + 3);
		return sFormat.format(current_date);
	}

	public String addOneMonth(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
			d.setDate(d.getDate() + 30);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}
	
	public String formatDate(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}

	public static void main(String[] args) {
		String data = "CAL1,1-4,1";
		System.out.println(data.split(",")[0] + " Book_id");
		System.out
				.println(Integer.parseInt(data.substring(data.indexOf(",") + 1, data.indexOf("-"))) + " Check return");
		System.out.println(Integer.parseInt(data.substring(data.indexOf("-") + 1, data.lastIndexOf(","))) + " Penalty");
		System.out.println(Integer.parseInt(data.substring(data.lastIndexOf(",") + 1)) + " Broken level");
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse("2020-01-01 14:01:21");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(sFormat.format(d));
	}
}
