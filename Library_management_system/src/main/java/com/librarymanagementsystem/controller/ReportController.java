/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.librarymanagementsystem.entity.Report;
import com.librarymanagementsystem.service.Other_PercentageService;
import com.librarymanagementsystem.service.ReportService;
import com.librarymanagementsystem.service.StaffService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value="/reportController")
public class ReportController {
	@Autowired
	private ReportService reportService;
	@Autowired
	private Other_PercentageService other_PercentageService;
	@RequestMapping(value = "/reports", method = RequestMethod.GET)
	public String display(ModelMap modelMap, @ModelAttribute("report") Report report) {
		List<Report> reports = new ArrayList<Report>();
		reports = reportService.getAll2();
		modelMap.addAttribute("reports", reports);
		modelMap.addAttribute("num_table", reports.size() / 20);
		modelMap.addAttribute("percentages", other_PercentageService.getAll2());
		return "report";
	}
	
	@RequestMapping(value="/report", method=RequestMethod.GET)
	@ResponseBody
	public Report getReport(@RequestParam("report_id") int report_id) {
		return reportService.getReport(report_id);
	}
}
