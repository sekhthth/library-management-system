/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.util.Random;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionAttributeStore;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.librarymanagementsystem.entity.SendMail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.StaffId;
import com.librarymanagementsystem.entity.TripleDES;
import com.librarymanagementsystem.service.StaffService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value = "/loginController")
@SessionAttributes({ "user" })
public class LoginController {
	@Autowired
	private StaffService staffService;

	private String newPasscode = "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "<head>\r\n"
			+ "    <meta charset=\"UTF-8\">\r\n"
			+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n"
			+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n" + "    <title>Document</title>\r\n"
			+ "    <style>\r\n" + "        * {\r\n" + "            margin: 0;\r\n" + "            padding: 0;\r\n"
			+ "            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\r\n" + "        }\r\n"
			+ "        html,\r\n" + "        body {\r\n" + "            width: 100%;\r\n"
			+ "            height: 100%;\r\n" + "            overflow: hidden;\r\n" + "        }\r\n"
			+ "        h2.big_title {\r\n" + "            text-transform: uppercase;\r\n"
			+ "            letter-spacing: 3px;\r\n" + "            font-size: 35px;\r\n" + "        }\r\n" + "\r\n"
			+ "        .small_title {\r\n" + "            letter-spacing: 2px;\r\n" + "            font-size: 20px;\r\n"
			+ "        }\r\n" + "    </style>\r\n" + "</head>\r\n" + "<body>\r\n"
			+ "    <div style=\"background: linear-gradient(#3498db, #1aa3ff99);\">\r\n"
			+ "        <h1 class=\"title\" style=\"text-align: center;padding: 10px o;\">International University Library - HCMIU</h1>\r\n"
			+ "        <div class=\"newAccount\" style=\"text-align: center;\">\r\n"
			+ "            <h2>#information</h2>\r\n" + "        </div>\r\n"
			+ "        <div class=\"contact\" style=\"width: 100%;border-top: 1px solid;margin-top: 10px;\">\r\n"
			+ "            <div style=\"width: max-content;margin: 0 auto;padding: 5px 0;font-weight: 600;\">\r\n"
			+ "                <p>Cotact us for more detail : </p>\r\n"
			+ "                <p>Email : hcmiu@edu.vn</p>\r\n" + "                <p>Phone : 0xxxxxxxxxxx</p>\r\n"
			+ "                <p>Fax : xxxxxxxxxxxxxx.</p>\r\n" + "            </div>\r\n" + "        </div>\r\n"
			+ "    </div>\r\n" + "</body>\r\n" + "</html>";
	private String newPasscodeTmp = "";
	private int sendMailType = 1;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView display() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public String login(@RequestParam("id") String id, @RequestParam("password") String password, Model model) {
		Staff staff = new Staff();
		staff.setId(new StaffId(id));
		staff = staffService.getById(staff);
		if (staff != null) {
			if (TripleDES.Decrypt(staff.getPassword(), "123456").equals(password)) {
				model.addAttribute("user", staff);
				return "Success-" + staff.getRole();
			} else {
				return "Your password is incorrect!!!";
			}
		} else {
			return "This account does not exist!!!";
		}
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	@ResponseBody
	public void logout(SessionStatus sessionStatus, WebRequest request) {
		sessionStatus.setComplete();
		request.removeAttribute("user", WebRequest.SCOPE_SESSION);
	}

	@RequestMapping(value = "/passcode", method = RequestMethod.GET)
	@ResponseBody
	public int getPasscode(@RequestParam("email") String email) {
		Random random = new Random();
		int passcode = random.nextInt(1000000 - 100000) + 100000;
		String infor = "This is the passcode for creating new password : <span style=\"color: red;\">#passcode</span>";
		infor = infor.replace("#passcode", passcode + "");
		newPasscodeTmp = newPasscode;
		newPasscodeTmp = newPasscodeTmp.replace("#information", infor);
		return passcode;
	}

	@Async
	@RequestMapping(value = "/sendpasscode", method = RequestMethod.POST)
	@ResponseBody
	public void sendPasscode(@RequestParam("email") String email) {
		// sendMailType == 1 => send passcode
		// sendMailType == 2 => send create new pass success
		SendMail.send(newPasscodeTmp, email);
		newPasscodeTmp = newPasscode;
	}

	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	@ResponseBody
	public void resetPassword(@RequestParam("person_id") String person_id, @RequestParam("password") String password) {
		Staff staff = staffService.getAStaffById(person_id);
		staff.setPassword(TripleDES.Encrypt(password, "123456"));
		String infor = "You changed your password successfully.<span style=\"display: block;\">This is your new password : <span style=\"color: red;\">#password</span></span>";
		infor = infor.replace("#password", password);
		newPasscodeTmp = newPasscode;
		newPasscodeTmp = newPasscodeTmp.replace("#information", infor);
		staffService.update(staff);
	}

	public static void main(String[] args) {
		System.out.println(TripleDES.Decrypt("t3n1SOtHmwlIMsUWlpvaVg==", "123456"));
	}
}
