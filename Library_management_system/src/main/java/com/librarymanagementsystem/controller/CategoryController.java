/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Category;
import com.librarymanagementsystem.entity.CategoryId;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.TripleDES;
import com.librarymanagementsystem.service.CategoryService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value="/categoryController")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;
	
	private int status = -1;
	
	@RequestMapping(value="/categories", method=RequestMethod.GET)
	public String display(ModelMap modelMap, @ModelAttribute("category") Category category) {
		List<Category> categories = categoryService.getAll2();
		modelMap.addAttribute("categories", categories);
		modelMap.addAttribute("categories", categories);
		modelMap.addAttribute("num_table", categories.size() / 20);
		modelMap.addAttribute("status", status);
		return "category";
	}
	
	@RequestMapping(value="/category", method=RequestMethod.GET)
	@ResponseBody
	private Category getACategory(@RequestParam("category_id") String category_id) {
		return categoryService.getACategoryById(category_id);
	}
	
	@RequestMapping(value="/category", method=RequestMethod.POST)
	public String addCategory(@ModelAttribute("category") Category category, ModelMap map) {
		System.out.println(category.getId().getId());
		System.out.println(category.getName());
		try {
			System.out.println(categoryService.add2(category));
			status = 1;
		} catch (Exception e) {
			status = 2;
		}
		return "redirect:/categoryController/categories";
	}
	
	@RequestMapping(value="/category", method=RequestMethod.DELETE)
	@ResponseBody
	public int  deleteCategory(@RequestParam("category_id") String category_id) {
		return categoryService.delete2(category_id);
	}
	
	@RequestMapping(value="ucategory", method=RequestMethod.POST)
	public String updateCategory(@ModelAttribute("category") Category category, @RequestParam("type") String category_id) {
		CategoryId id = new CategoryId();
		id.setId(category_id);
		category.setId(id);
		boolean t = categoryService.update(category);
		if (t) status = 11;
		else status = 22;
		return "redirect:/categoryController/categories";
	}
	
	@RequestMapping(value = "/resetstatus", method=RequestMethod.GET)
	@ResponseBody
	public void resetStatus() {
		status = -1;
	}
	
	@RequestMapping(value = "/searchcate", method=RequestMethod.GET)
	@ResponseBody
	public List<Category> getCategorys(@RequestParam("keyword") String keyword,
								 @RequestParam("type") int type) {
		List<Category> list = categoryService.searchCategoryByKeyWord(keyword, type);
		return list;
	}
}
