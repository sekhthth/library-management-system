/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.librarymanagementsystem.entity.Penalty_Log;
import com.librarymanagementsystem.service.BookService;
import com.librarymanagementsystem.service.Borrow_LogService;
import com.librarymanagementsystem.service.Penalty_LogService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value="/penaltyController")
public class PenaltyController {
	@Autowired
	private Penalty_LogService penalty_LogService;
	@Autowired
	private Borrow_LogService borrow_LogService;
	@Autowired
	private BookService bookService;
	
	@RequestMapping(value="/ispunished", method=RequestMethod.GET)
	@ResponseBody
	public List<Penalty_Log> isPunished(@RequestParam("person_id") String person_id) {
		List<Penalty_Log> list = new ArrayList<Penalty_Log>();
		list = penalty_LogService.getListPenaltyLog(person_id);
		return list;
	}
	
	@RequestMapping(value="/punish", method=RequestMethod.POST)
	@ResponseBody
	public void punish(@RequestParam("person_id") String person_id,
					   @RequestParam("book_id") String book_id,
					   @RequestParam("borrow_log_id") int borrow_log_id,
					   @RequestParam("penalty") int penalty,
					   @RequestParam("start") String start) {
		Penalty_Log penalty_Log = new Penalty_Log();
		penalty_Log.setId(1);
//		penalty_Log.setPerson_id(person_id);update
		penalty_Log.setBorrow_log_id(borrow_LogService.getBorrowLog(borrow_log_id));
		penalty_Log.setBook_id(bookService.getABookById(book_id));		
		String tmp = penalty_LogService.getStart(person_id);
		if (!tmp.equalsIgnoreCase(""))
			penalty_Log.setStart(addOneMonth(tmp));
		else
			penalty_Log.setStart(start);
		penalty_LogService.add(penalty_Log);
	}
	
	public String addOneMonth(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
			d.setDate(d.getDate() + 30);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}
}
