/**
 * 
 */
package com.librarymanagementsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.service.LecturerService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value="/lecturerController")
public class LecturerController {
	@Autowired
	private LecturerService lecturerService;
	
	@RequestMapping(value="/lecturer", method=RequestMethod.GET)
	@ResponseBody
	public Lecturer getLecturer(@RequestParam("id") String id) {
		return lecturerService.getLecturer(id);
	}
}
