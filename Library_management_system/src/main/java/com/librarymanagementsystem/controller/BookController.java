/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.BookId;
import com.librarymanagementsystem.entity.Category;
import com.librarymanagementsystem.entity.Myfile;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.TripleDES;
import com.librarymanagementsystem.service.BookService;
import com.librarymanagementsystem.service.CategoryService;
import com.librarymanagementsystem.service.Reservate_Log_DetailService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value = "/bookController")
public class BookController {

	private static final String UPLOAD_DIRECTORY = "/resources/image";
	@Autowired
	ServletContext context;
	@Autowired
	private BookService bookServices;
	@Autowired
	private CategoryService categoryServices;
	@Autowired
	private Reservate_Log_DetailService reservate_Log_DetailService;
	private int status = -1;

	@RequestMapping(value = "/books", method = RequestMethod.GET)
	public String display(ModelMap modelMap, @ModelAttribute("category") Category category,
			@ModelAttribute("book") Book book) {
		List<Book> books = bookServices.getAll2();
		modelMap.addAttribute("books", books);
		modelMap.addAttribute("categories", categoryServices.getAll2());
		modelMap.addAttribute("num_table", books.size() / 20);
		modelMap.addAttribute("status", status);
		return "book";
	}

	@RequestMapping(value = "/book", method = RequestMethod.GET)
	@ResponseBody
	public Book getABook(@RequestParam("book_id") String book_id) {
		Book book = new Book();
		book = bookServices.getABookById(book_id);
		if (book.getId() != null)
			book.getMyfile().setDescription(book.getCategory_id().getId().getId());
		return book;
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView addBook(@ModelAttribute("book") Book book, HttpSession session, @RequestParam("type") String type)
			throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/bookController/books");
		try {
			MultipartFile multipartFile = book.getMyfile().getMultipartFile();
			String fileName = multipartFile.getOriginalFilename();
			File file2 = new File(context.getRealPath("/resources/image"), fileName);
			multipartFile.transferTo(file2);
//		      System.out.println(context.getRealPath("/resources/image"));
//		      System.out.println(book.getMyfile().getMultipartFile().getOriginalFilename());
			book.setCover_image("/resources/image/" + fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			System.out.println(bookServices.add2(book));
			status = 1;
		} catch (Exception e) {
			status = 2;
		}
		System.out.println(book.getName());
		System.out.println(book.getCover_image());
		System.out.println(book.getQuantity());
		System.out.println(book.getTimePrint());
		System.out.println(book.getYmanufacture());
		System.out.println(book.getManufacturer());
		System.out.println(book.getCategory_id().getId().getId());

		return modelAndView;
	}

	@RequestMapping(value = "/book", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteBook(@RequestParam("book_id") String book_id) {
		bookServices.delete2(book_id);
	}

	@RequestMapping(value = "/ubook", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView updateBook(@ModelAttribute("book") Book book, @RequestParam("type") String book_id) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/bookController/books");
		try {
		      MultipartFile multipartFile = book.getMyfile().getMultipartFile();
		      String fileName = multipartFile.getOriginalFilename();
		      if (!fileName.equalsIgnoreCase("")) {
		    	  System.out.println("here 1");
		    	  File file2 = new File(context.getRealPath("/resources/image"), fileName);
			      multipartFile.transferTo(file2);
			      System.out.println(context.getRealPath("/resources/image"));
			      System.out.println(book.getMyfile().getMultipartFile().getOriginalFilename());
			      book.setCover_image("/resources/image/" + fileName);
		      } else {
		    	  System.out.println("here 2");
		    	  book.setCover_image(bookServices.getABookById(book_id).getCover_image());
		      }
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		BookId id = new BookId();
		id.setId(book_id);
		book.setId(id);
		System.out.println(book.getId().getId());
		System.out.println(book.getName() + "name");
		System.out.println(book.getCover_image() + "image");
		System.out.println(book.getQuantity() + "quantity");
		System.out.println(book.getTimePrint() + "time");
		System.out.println(book.getYmanufacture() + "year");
		System.out.println(book.getManufacturer() + "man");
		System.out.println(book.getCategory_id().getId().getId() + "cate");
		System.out.println(book.getMyfile().getDescription() + "file");
		boolean t = bookServices.update(book);
		if (t) status = 11;
		else status = 22;
		return modelAndView;
	}
	
	@RequestMapping(value="/outof", method=RequestMethod.GET)
	@ResponseBody
	public int checkOutOfAvilableList(@RequestParam("book_id") String book_id) {
		Book book = bookServices.getABookById(book_id);
		int allowedAmount = (book.getQuantity()/2)-book.getBorrowed();
		int waitingAmount = reservate_Log_DetailService.getWaitingAmount(book_id);
		System.out.println(allowedAmount + "-" + waitingAmount);
		if (bookServices.isOutOfAvailable(book_id) == 1 || (waitingAmount == allowedAmount))
			return 1;
		return 0;
	}
	
	@RequestMapping(value = "/resetstatus", method=RequestMethod.GET)
	@ResponseBody
	public void resetStatus() {
		status = -1;
	}
	
	@RequestMapping(value = "/searchbook", method=RequestMethod.GET)
	@ResponseBody
	public List<Book> getBooks(@RequestParam("keyword") String keyword,
								 @RequestParam("type") int type) {
		List<Book> list = bookServices.searchBookByKeyWord(keyword, type);
		for (Book b : list) 
			b.getMyfile().setDescription(b.getCategory_id().getName());
		return list;
	}
}
