/**
 * 
 */
package com.librarymanagementsystem.controller;

import static org.mockito.Matchers.startsWith;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.Category;
import com.librarymanagementsystem.entity.Borrow_Log_Detail;
import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.entity.Other_Percentage;
import com.librarymanagementsystem.entity.Report;
import com.librarymanagementsystem.entity.SendMail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.Student;
import com.librarymanagementsystem.entity.TripleDES;
import com.librarymanagementsystem.service.BookService;
import com.librarymanagementsystem.service.Borrow_LogService;
import com.librarymanagementsystem.service.Borrow_Log_DetailService;
import com.librarymanagementsystem.service.LecturerService;
import com.librarymanagementsystem.service.Other_PercentageService;
import com.librarymanagementsystem.service.Penalty_LogService;
import com.librarymanagementsystem.service.ReportService;
import com.librarymanagementsystem.service.StaffService;
import com.librarymanagementsystem.service.StudentService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value = "/homeController")
@SessionAttributes({ "user" })
public class HomeController {
	@Autowired
	private StudentService studentService;
	@Autowired
	private LecturerService lecturerService;
	@Autowired
	private BookService bookService;
	@Autowired
	private Borrow_LogService borrow_LogService;
	@Autowired
	private Borrow_Log_DetailService borrow_Log_DetailService;
	@Autowired
	private StaffService staffService;
	@Autowired
	private ReportService reportService;
	@Autowired
	private Other_PercentageService other_PercentageService;
	@Autowired
	private Penalty_LogService penalty_LogService;
	
	private String currentPass = "";

	@RequestMapping(value = "/display", method = RequestMethod.GET)
	public ModelAndView display(HttpServletRequest request, @ModelAttribute("staff") Staff staff) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("home");
		modelAndView.addObject("percentages", other_PercentageService.getAll());
		staff = (Staff) request.getSession().getAttribute("user");
		if (staff != null) {
			System.out.println(staff.getPassword() );
			System.out.println(currentPass);
			if (!staff.getPassword().equalsIgnoreCase(currentPass)) {
				staff.setPassword(TripleDES.Decrypt(staff.getPassword(), "123456"));
				currentPass = staff.getPassword();
			}
			modelAndView.addObject("staff", staff);
		}
		return modelAndView;
	}

	@RequestMapping(value = "/student", method = RequestMethod.POST)
	@ResponseBody
	public List<Object> searchStudent(@RequestParam("id") String id) {
		List<Object> list = new ArrayList<Object>();
		List<Object> objs = new ArrayList<Object>();
		List<Student> students = studentService.searchStudents(id);
		List<Lecturer> leturers = lecturerService.searchLecturers(id);
		objs.add(students);
		objs.add(leturers);
		List<String> departments = new ArrayList<String>();
//		for (Object s : objs) {
//			if (objs.indexOf(s) < students.size())
//				departments.add(students.get(objs.indexOf(s)).getDepartment_id().getName());
//			else
//				departments.add(leturers.get(objs.indexOf(s)).getDepartment_id().getName());
//		}

		for (int i = 0; i < students.size() + leturers.size(); i++) {
			if (i < students.size())
				departments.add(students.get(i).getDepartment_id().getName());
			else
				departments.add(leturers.get(i - students.size()).getDepartment_id().getName());
		}
		list.add(objs);
		list.add(departments);
		return list;
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	@ResponseBody
	public List<Object> searchBook(@RequestParam("id") String id) {
		List<Object> list = new ArrayList<Object>();
		List<Book> books = bookService.searchBooks(id);
		List<Category> categorires = new ArrayList<Category>();
		for (Book b : books) {
			categorires.add(b.getCategory_id());
		}
		list.add(books);
		list.add(categorires);
		return list;
	}

	@Async
	@RequestMapping(value = "/sendreport", method = RequestMethod.POST)
	@ResponseBody
	public void sendReport(@RequestParam("content") String content, @RequestParam("staff_id") String staff_id,
			@RequestParam("report_date") String report_date) {
		String style = "        * {\r\n" + "            margin: 0;\r\n" + "            padding: 0;\r\n"
				+ "            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\r\n" + "        }\r\n"
				+ "\r\n" + "        .reportContainer {\r\n" + "            min-width: 900px;\r\n"
				+ "            text-align: center;\r\n" + "            background-color: #ecf0f1;\r\n"
				+ "            padding: 10px 10px;\r\n" + "            border-radius: 10px;\r\n" + "        }\r\n"
				+ "\r\n" + "        h2.big_title,\r\n" + "        .small_title,\r\n" + "        .report_date {\r\n"
				+ "            text-align: center;\r\n" + "        }\r\n" + "\r\n" + "        .borrow,\r\n"
				+ "        .reservate,\r\n" + "        .return {\r\n" + "            text-align: center;\r\n"
				+ "        }\r\n" + "\r\n" + "        .borrow table,\r\n" + "        .reservate table,\r\n"
				+ "        .return table,\r\n" + "        .moreContainer table {\r\n" + "            width: 100%;\r\n"
				+ "            text-align: center;\r\n" + "        }\r\n" + "\r\n" + "        .borrow table th,\r\n"
				+ "        .reservate table th,\r\n" + "        .return table th,\r\n"
				+ "        .moreContainer table th {\r\n" + "            background-color: #95a5a6;\r\n"
				+ "        }\r\n" + "\r\n" + "        .small_title {\r\n" + "            letter-spacing: 2px;\r\n"
				+ "            font-size: 20px;\r\n" + "        }\r\n" + "\r\n" + "        h2.big_title {\r\n"
				+ "            text-transform: uppercase;\r\n" + "            letter-spacing: 3px;\r\n"
				+ "            font-size: 35px;\r\n" + "        }\r\n" + "\r\n" + "        h4.report_date {\r\n"
				+ "            font-size: 25px;\r\n" + "            border-bottom: 1px solid;\r\n"
				+ "            padding: 5px 0;\r\n" + "        }\r\n" + "\r\n" + "        .reservate {\r\n"
				+ "            margin: 20px 0;\r\n" + "        }\r\n" + "\r\n" + "        .borrow table tr,\r\n"
				+ "        .reservate table tr,\r\n" + "        .return table tr,\r\n"
				+ "        .moreContainer table tr {\r\n" + "            height: 30px;\r\n" + "        }\r\n" + "\r\n"
				+ "        button {\r\n" + "            width: 100%;\r\n" + "            color: #2c3e50;\r\n"
				+ "            background-color: #27ae60;\r\n" + "            transition: 0.4s;\r\n"
				+ "            display: block;\r\n" + "            cursor: pointer;\r\n"
				+ "            height: 30px;\r\n" + "            line-height: 30px;\r\n"
				+ "            font-size: 20px;\r\n" + "            font-weight: 700;\r\n" + "        }\r\n" + "\r\n"
				+ "        button:hover {\r\n" + "            background-color: #f1c40f;\r\n"
				+ "            color: #ecf0f1;\r\n" + "        }\r\n";
		content = content.replace("#style_content", style);
		Staff manager = staffService.getManager();
		content = content.replace("#id", manager.getId().getId());
		content = content.replace("#password", TripleDES.Decrypt(manager.getPassword(), "123456"));
		System.out.println(content);
		SendMail.send(content, manager.getEmail());
		Report report = new Report();
		Staff staff = new Staff();
		staff = staffService.getAStaffById(staff_id);
		report.setReport_date(formatDate(report_date));
		report.setStaff_id(staff);
		reportService.add(report);
	}

	@RequestMapping(value = "/full", method = RequestMethod.GET)
	@ResponseBody
	public List<Borrow_Log_Detail> getBorrowedBooks(@RequestParam("person_id") String person_id) {
		return borrow_Log_DetailService.getBorrowedBooks(person_id);
	}
	
	@RequestMapping(value="/otherpercentage", method=RequestMethod.GET)
	@ResponseBody
	public Other_Percentage getOtherPercentage(int penalty_log_id){
		return other_PercentageService.getFromOtherPercentageLog(penalty_log_id);
	}
	
	@RequestMapping(value="/otherpercentagereturn", method=RequestMethod.GET)
	@ResponseBody
	public Other_Percentage getOtherPercentageReturn(@RequestParam("borrow_log_id") int borrow_log_id,
													 @RequestParam("person_id") String person_id,
													 @RequestParam("book_id") String book_id){
		return other_PercentageService.getFromOtherPercentageLog(penalty_LogService.getPenaltyIdForReturn(borrow_log_id, person_id, book_id));
	}
	
	public String formatDate(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}
}
