/**
 * 
 */
package com.librarymanagementsystem.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.librarymanagementsystem.entity.Student;
import com.librarymanagementsystem.service.StudentService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value="/studentController")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value="/student", method=RequestMethod.GET)
	@ResponseBody
	public Student getStudent(@RequestParam("id") String id) {
		return studentService.getStudent(id);
	}
}
