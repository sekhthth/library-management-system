/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.BookId;
import com.librarymanagementsystem.entity.Borrow_Log;
import com.librarymanagementsystem.entity.Borrow_LogId;
import com.librarymanagementsystem.entity.Borrow_Log_Detail;
import com.librarymanagementsystem.entity.Borrow_Log_DetailIdTmp;
import com.librarymanagementsystem.entity.Borrower;
import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.entity.Other_Percentage_Log;
import com.librarymanagementsystem.entity.Penalty_Log;
import com.librarymanagementsystem.entity.SendMail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.StaffId;
import com.librarymanagementsystem.entity.Student;
import com.librarymanagementsystem.service.BookService;
import com.librarymanagementsystem.service.Borrow_LogService;
import com.librarymanagementsystem.service.Borrow_Log_DetailService;
import com.librarymanagementsystem.service.BorrowerService;
import com.librarymanagementsystem.service.LecturerService;
import com.librarymanagementsystem.service.Other_Percentage_LogService;
import com.librarymanagementsystem.service.Penalty_LogService;
import com.librarymanagementsystem.service.StaffService;
import com.librarymanagementsystem.service.StudentService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value = "/borrowController")
public class BorrowController {
	@Autowired
	private Borrow_LogService borrow_LogService;
	@Autowired
	private Borrow_Log_DetailService borrow_Log_DetailService;
	@Autowired
	private BookService bookService;
	@Autowired
	private StaffService staffService;
	@Autowired
	private StudentService studenService;
	@Autowired
	private LecturerService lecturerService;
	@Autowired
	private BorrowerService borrowerService;
	@Autowired
	private Other_Percentage_LogService other_Percentage_LogService;
	@Autowired
	private Penalty_LogService penalty_LogService;

	private String returnContentMail = "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "<head>\r\n"
			+ "    <meta charset=\"UTF-8\">\r\n"
			+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n"
			+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n" + "    <title>Document</title>\r\n"
			+ "    <style type=\"text/css\">\r\n" + "		* {\r\n" + "			margin: 0;\r\n"
			+ "			padding: 0;\r\n"
			+ "			font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\r\n" + "		}\r\n"
			+ "		html, body {\r\n" + "			width: 100%;\r\n" + "			height: 100%;\r\n"
			+ "			overflow: hidden;\r\n" + "		}\r\n" + "	</style>\r\n" + "</head>\r\n" + "\r\n"
			+ "<body>\r\n"
			+ "    <div class=\"container\" style=\"box-sizing: border-box;width: 100%;color: #2c3e50;\r\n"
			+ "    background: linear-gradient(#3498db, #1aa3ff99);\">\r\n"
			+ "        <div style=\"max-width: 800px; margin: auto;min-width: 600px;width: 80%;\">\r\n"
			+ "            <h1 class=\"title\" style=\"text-align: center;margin: 10px 0;padding: 0px;\">International University Library -\r\n"
			+ "                HCMIU</h1>\r\n"
			+ "            <h3 class=\"text\" style=\"color: aqua;text-align: center;\">Notice : #daysleft #behind</h3>\r\n"
			+ "            <div class=\"warning\" style=\"color: firebrick;margin: 10px 0;text-align: center;\">\r\n"
			+ "                <h3 style=\"text-transform: uppercase;margin: 5px 0;\"><span>FROM :#current_date</h3>\r\n"
			+ "                    <h3 style=\"text-transform: uppercase;margin: 5px 0;\"><span>TO :#due_date</h3>\r\n"
			+ "            </div>\r\n" + "            <div class=\"reservation_detail_container\"\r\n"
			+ "                style=\"width: 60%;margin: auto;border: 1px solid;padding: 10px 10px;\">\r\n"
			+ "                <h2 style=\"text-align: center;margin-bottom: 10px;\">Borrow detail</h2>\r\n"
			+ "                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">Borrow id : </h4>\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">#borrow_id</h4>\r\n" + "                </div>\r\n"
			+ "                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">borrow person id : </h4>\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">#borrow_person_id</h4>\r\n"
			+ "                </div>\r\n"
			+ "                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">borrow person name : </h4>\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">#borrow_person_name</h4>\r\n"
			+ "                </div>\r\n"
			+ "                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">borrow date : </h4>\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">#borrow_date</h4>\r\n" + "                </div>\r\n"
			+ "                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">Staff id : </h4>\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">#staff_id</h4>\r\n" + "                </div>\r\n"
			+ "                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">staff name : </h4>\r\n"
			+ "                    <h4 style=\"font-weight: 500;\">#staff_name</h4>\r\n" + "                </div>\r\n"
			+ "            </div>\r\n" + "            <div class=\"books_container\"\r\n"
			+ "                style=\"width: 60%;margin: auto;padding: 10px 10px;border: 1px solid;margin-top: 10px;\">\r\n"
			+ "                <h2 style=\"text-align: center;margin-bottom: 10px;\">Borrowed book(s)</h2>";
	private String returnContentMailBook = " <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "                    <p style=\"font-weight: 500;\"><span>#book_order. </span></p>\r\n"
			+ "                    <p style=\"font-weight: 500;\"><span>#book_name - </span><span>#book_id</span></p>\r\n"
			+ "                </div>";
	private String returnContentMailTail = "</div>\r\n" + "        </div>\r\n"
			+ "        <div class=\"contact\" style=\"width: 100%;border-top: 1px solid;margin-top: 10px;\">\r\n"
			+ "            <div style=\"width: max-content;margin: 0 auto;padding: 5px 0;font-weight: 600;\">\r\n"
			+ "                <p>Cotact us for more detail : </p>\r\n"
			+ "                <p>Email : hcmiu@edu.vn</p>\r\n" + "                <p>Phone : 0xxxxxxxxxxx</p>\r\n"
			+ "                <p>Fax : xxxxxxxxxxxxxx.</p>\r\n" + "            </div>\r\n" + "        </div>\r\n"
			+ "    </div>";
	private String returnContentMailTmp = returnContentMail;

	@RequestMapping(value = "/borrowed", method = RequestMethod.GET)
	@ResponseBody
	public int isBorrowed(@RequestParam("person_id") String person_id, @RequestParam("book_id") String book_id) {
		System.out.println(borrow_LogService.getLastestId());
		return borrow_LogService.isBorrowed(person_id, book_id);
	}

	@RequestMapping(value = "/currentid", method = RequestMethod.GET)
	@ResponseBody
	public int currentId() {
		return borrow_LogService.getLastestId();
	}

	@RequestMapping(value = "/borrow", method = RequestMethod.POST)
	@ResponseBody
	public void borrow(@RequestParam("person_id") String person_id, @RequestParam("book_id") String book_id,
			@RequestParam("borrow_date") String borrow_date, @RequestParam("return_date") String return_date,
			@RequestParam("staff_id") String staff_id, @RequestParam("current_id") int current_id) {
		System.out.println(person_id);
		System.out.println(book_id);
		System.out.println(borrow_date);
		System.out.println(return_date);

		Borrow_Log borrow_log = new Borrow_Log();
		Borrow_LogId borrow_LogId = new Borrow_LogId();
		Borrow_Log_Detail borrow_log_detail = new Borrow_Log_Detail();
		Borrow_Log_DetailIdTmp borrow_Log_DetailId = new Borrow_Log_DetailIdTmp();
		Book book = new Book();
		int tmp = borrow_LogService.getLastestId();
		System.out.println(tmp);
		System.out.println(current_id);

		if (current_id == tmp) {
			borrow_LogId.setId(current_id);
			borrow_log.setId(borrow_LogId);
			borrow_log.setPerson_id(borrowerService.getById2(person_id));
			borrow_log.setBorrow_date(formatDate(borrow_date));
			borrow_log.setReturn_date(formatDate(return_date));
			borrow_log.setStaff_id(new Staff(new StaffId(staff_id)));
			borrow_log.setStatus(false);
			borrow_LogService.add(borrow_log);
			borrow_Log_DetailId.setBorrow_log(borrow_LogService.getBorrowLog(borrow_LogId.getId()));
		} else {
			borrow_Log_DetailId.setBorrow_log(borrow_LogService.getBorrowLog(current_id));
		}

		borrow_Log_DetailId.setBook(bookService.getABookById(book_id));
		borrow_log_detail.setId(borrow_Log_DetailId);
		borrow_log_detail.setReserved(0);
		borrow_log_detail.setPenalty(5);
		borrow_log_detail.setReturn_date("");
		borrow_Log_DetailService.add(borrow_log_detail);
		book = bookService.getABookById(book_id);
		book.setBorrowed(book.getBorrowed() + 1);
		bookService.update(book);
	}

	@RequestMapping(value = "/borrowlog", method = RequestMethod.GET)
	@ResponseBody
	public List<Object> getBorrow_Log_Record(@RequestParam("borrow_log_id") String borrow_log_id) {
		System.out.println(borrow_log_id);
		List<Object> list = new ArrayList<Object>();
		Borrow_Log borrow_Log = borrow_LogService.getBorrowLog(Integer.parseInt(borrow_log_id));
		Staff staff = staffService.getAStaffById(borrow_Log.getStaff_id().getId().getId());
		Student student;
		Lecturer lecturer;
		list.add(borrow_Log);
		if (!borrow_Log.getPerson_id().getId().getPerson_id().substring(0, 2).equals("LE")) {
			student = studenService.getStudent(borrow_Log.getPerson_id().getId().getPerson_id());
			list.add(student);
		} else {
			lecturer = lecturerService.getLecturer(borrow_Log.getPerson_id().getId().getPerson_id());
			list.add(lecturer);
		}
		list.add(staff);
		return list;
	}

	@RequestMapping(value = "/borrowlogdetails", method = RequestMethod.GET)
	@ResponseBody
	public List<Book> getBorrow_Log_Details(@RequestParam("borrow_log_id") String borrow_log_id) {
		List<Book> list = bookService.getListBookReturn(Integer.parseInt(borrow_log_id));
		List<Borrow_Log_Detail> borrows = borrow_Log_DetailService.getBorrowLogDetails(Integer.parseInt(borrow_log_id));
		int num = 0;
		Other_Percentage_Log other_Percentage_Log = new Other_Percentage_Log();
		for (Book b : list) {
			b.setYmanufacture(b.getYmanufacture() + "," + b.getCategory_id().getName() + "-"
					+ borrows.get(list.indexOf(b)).getPenalty());
			// đánh dấu rằng 1 quyển đã trả r
			if (borrows.get(list.indexOf(b)).getPenalty() == 4)
				b.setId(new BookId(b.getId().getId() + "," + "1"));
			else
				b.setId(new BookId(b.getId().getId() + "," + "0"));
		}
		return list;
	}

	@Async
	@RequestMapping(value = "/remindreturn", method = RequestMethod.POST)
	@ResponseBody
	public void remindReturn() {
		Book book = null;
		String emailAddress = "";
		Student student = null;
		Lecturer lecturer = null;
		List<Borrow_Log> list = borrow_LogService.getListRemindReturn();
		List<Borrow_Log_Detail> list2 = null;
		Date date = null;
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Penalty_Log penalty_Log = null;
		Borrow_Log_Detail borrow_Log_Detail = null;
		String returnContentMailBookTmp = "";
		boolean isLate = false;
		int totalMonth = 0;
		System.out.println(list);
		if (list != null) {
			for (Borrow_Log b : list) {

				if (b.getPerson_id().getId().getPerson_id().substring(0, 2).equalsIgnoreCase("LE")) {
					lecturer = lecturerService.getLecturer(b.getPerson_id().getId().getPerson_id());
					emailAddress = lecturer.getEmail();
					returnContentMailTmp = returnContentMailTmp.replace("#borrow_person_name", lecturer.getName());
				} else {
					student = studenService.getStudent(b.getPerson_id().getId().getPerson_id());
					emailAddress = student.getEmail();
					returnContentMailTmp = returnContentMailTmp.replace("#borrow_person_name", student.getName());
				}
				try {
					date = sFormat.parse(b.getReturn_date());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				returnContentMailTmp = returnContentMailTmp.replace("#due_date", b.getReturn_date());
				returnContentMailTmp = returnContentMailTmp.replace("#current_date", b.getBorrow_date());
				returnContentMailTmp = returnContentMailTmp.replace("#borrow_id", b.getId().getId() + "");
				returnContentMailTmp = returnContentMailTmp.replace("#borrow_person_id",
						b.getPerson_id().getId().getPerson_id());
				returnContentMailTmp = returnContentMailTmp.replace("#borrow_date", b.getBorrow_date());
				returnContentMailTmp = returnContentMailTmp.replace("#staff_id", b.getStaff_id().getId().getId());
				returnContentMailTmp = returnContentMailTmp.replace("#staff_name", b.getStaff_id().getName());
				if (differentDays(date) <= 0) {
					penalty_Log = new Penalty_Log();
					penalty_Log.setPerson_id(borrowerService.getById2(b.getPerson_id().getId().getPerson_id()));
					penalty_Log.setBorrow_log_id(borrow_LogService.getBorrowLog(b.getId().getId()));

					String tmp = penalty_LogService.getStart(b.getPerson_id().getId().getPerson_id());
					if (!tmp.equalsIgnoreCase("")) {
						penalty_Log.setStart(addOneMonth(tmp));
					} else
						penalty_Log.setStart(sFormat.format(new Date()));

					penalty_Log.setPenalty(1);

					isLate = true;
				} else {
					returnContentMailTmp = returnContentMailTmp.replace("#daysleft", differentDays(date) + "");
					returnContentMailTmp = returnContentMailTmp.replace("#behind",
							" days left till return date.\nDO NOT BE LATE.");
				}

				list2 = borrow_Log_DetailService.getBorrowLogDetails(b.getId().getId());

				for (Borrow_Log_Detail bb : list2) {
					if (bb.getPenalty() == 5 || bb.getPenalty() == 1) {
						returnContentMailBookTmp = returnContentMailBook;
						book = bookService.getABookById(bb.getId().getBook().getId().getId());
						returnContentMailBookTmp = returnContentMailBookTmp.replace("#book_order",
								(list2.indexOf(bb) + 1) + "");
						returnContentMailBookTmp = returnContentMailBookTmp.replace("#book_name", book.getName());
						returnContentMailBookTmp = returnContentMailBookTmp.replace("#book_id", book.getId().getId());

						returnContentMailTmp += returnContentMailBookTmp;

						if (isLate) {
							penalty_Log.setBook_id(book);
							if (penalty_LogService.getById2(penalty_LogService.getPenaltyIdForReturn(b.getId().getId(),
									b.getPerson_id().getId().getPerson_id(), bb.getId().getBook().getId().getId())) == null) {
								penalty_LogService.add(penalty_Log);// update
							}
							borrow_Log_Detail = borrow_Log_DetailService
									.getByIdAndBookId(bb.getId().getBook().getId().getId(), b.getId().getId());
							borrow_Log_Detail.setPenalty(1);
							borrow_Log_DetailService.update(borrow_Log_Detail);
							if (list2.indexOf(bb) == list2.size() - 1) {
								returnContentMailTmp = returnContentMailTmp.replace("#daysleft", "");
								returnContentMailTmp = returnContentMailTmp.replace("#behind",
										"You must return all borrowed books as soon as possiple.\nNow, you will be punished that you ARE NOT ALLOWED to borrow any book after "
												+ addOneMonth(penalty_LogService
														.getStart(b.getPerson_id().getId().getPerson_id()))
												+ ".");
								isLate = false;
							}
						}
					}

				}
				// Guửi cho từng thằng
				if (returnContentMailTmp.length() > returnContentMail.length()) {
					returnContentMailTmp += returnContentMailTail;
					SendMail.send(returnContentMailTmp, emailAddress);
				}
				//
				returnContentMailTmp = returnContentMail;
			}
		}
	}

	public int differentDays(Date date) {
		Date current_date = new Date();
		long diff = date.getTime() - current_date.getTime();

		int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
		return diffDays;
	}

	public String addOneMonth(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
			d.setDate(d.getDate() + 30);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}
	
	public String formatDate(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}

	@RequestMapping(value = "/recordswithoutreturn", method = RequestMethod.GET)
	@ResponseBody
	public List<Object> getListBorrowLog(@RequestParam("current_date") String current_date,
			@RequestParam("status") int status) {
		List<Object> list = new ArrayList<Object>();
		List<Borrow_Log> borrow_Logs = new ArrayList<Borrow_Log>();
		List<Borrower> borrowers = new ArrayList<Borrower>();
		List<Staff> staffs = new ArrayList<Staff>();
		if (status == 0)
			borrow_Logs = borrow_LogService.getListBorrowLog(formatDate(current_date), status);
		else
			borrow_Logs = borrow_LogService.getListBorrowLog(formatDate(current_date), status);
		for (Borrow_Log b : borrow_Logs) {
			borrowers.add(borrowerService.getById2(b.getPerson_id().getId().getPerson_id()));
			staffs.add(staffService.getAStaffById(b.getStaff_id().getId().getId()));
		}
		list.add(borrow_Logs);
		System.out.println(borrow_Logs.size());
		list.add(borrowers);
		System.out.println(borrowers.size());
		list.add(staffs);
		System.out.println(staffs.size());
		return list;
	}

	@RequestMapping(value = "/booksinborrowlog", method = RequestMethod.GET)
	@ResponseBody
	public List<Book> getBooksInBorrowLog(@RequestParam("borrow_log_id") int borrow_log_id,
			@RequestParam("type") int type) {
		List<Book> list = new ArrayList<Book>();
		list = borrow_Log_DetailService.getBooksInBorrowLog(borrow_log_id, type);
		for (Book b : list)
			b.setId(new BookId(b.getId().getId() + "-" + b.getCategory_id().getName()));
		return list;
	}

	@RequestMapping(value = "/borrowlogdetail", method = RequestMethod.GET)
	@ResponseBody
	public Borrow_Log_Detail getABorrowLogDetail(@RequestParam("borrow_log_id") int borrow_log_id,
			@RequestParam("book_id") String book_id) {
		return borrow_Log_DetailService.getByIdAndBookId(book_id, borrow_log_id);
	}

	@RequestMapping(value = "/getborrowlog", method = RequestMethod.GET)
	@ResponseBody
	public Borrow_Log getABorrowLogDetail(@RequestParam("borrow_log_id") int borrow_log_id) {
		return borrow_LogService.getBorrowLog(borrow_log_id);
	}
}
