/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.librarymanagementsystem.entity.Other_Percentage;
import com.librarymanagementsystem.entity.SendMail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.StaffId;
import com.librarymanagementsystem.entity.TripleDES;
import com.librarymanagementsystem.service.Other_PercentageService;
import com.librarymanagementsystem.service.StaffService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value = "/staffController")
public class StaffController {
	@Autowired
	ServletContext context;
	@Autowired
	private StaffService staffService;
	@Autowired
	private Other_PercentageService other_PercentageService;
	private String newAccount = "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "\r\n" + "<head>\r\n"
			+ "    <meta charset=\"UTF-8\">\r\n"
			+ "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n"
			+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n" + "    <title>Document</title>\r\n"
			+ "    <style>\r\n" + "        * {\r\n" + "            margin: 0;\r\n" + "            padding: 0;\r\n"
			+ "            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\r\n" + "        }\r\n"
			+ "        html,\r\n" + "        body {\r\n" + "            width: 100%;\r\n"
			+ "            height: 100%;\r\n" + "            overflow: hidden;\r\n" + "        }\r\n"
			+ "        h2.big_title {\r\n" + "            text-transform: uppercase;\r\n"
			+ "            letter-spacing: 3px;\r\n" + "            font-size: 35px;\r\n" + "        }\r\n" + "\r\n"
			+ "        .small_title {\r\n" + "            letter-spacing: 2px;\r\n" + "            font-size: 20px;\r\n"
			+ "        }\r\n" + "    </style>\r\n" + "</head>\r\n" + "<body>\r\n"
			+ "    <div style=\"background: linear-gradient(#3498db, #1aa3ff99);\">\r\n"
			+ " <h1 class=\"title\" style=\"text-align: center;padding: 10px 0;\">International University Library - HCMIU</h1>"
			+ "        <div class=\"newAccount\" style=\"text-align: center;\">\r\n"
			+ "            <h2 class=\"big_title\">You are now a #role of IU library.</h2>\r\n"
			+ "            <div class=\"account\" style=\"width: 300px;margin: auto;border: 5px solid grey;padding: 5px;\">\r\n"
			+ "                <h3 class=\"small_title\">This is your account.</h3 s>\r\n"
			+ "                <h4>Staff ID : #staff_id</h4>\r\n"
			+ "                <h4>Staff Password : #staff_password #hint</h4>\r\n" + "            </div>\r\n"
			+ "           #recommended" + "        </div>\r\n"
			+ "        <div class=\"contact\" style=\"width: 100%;border-top: 1px solid;margin-top: 10px;\">\r\n"
			+ "            <div style=\"width: max-content;margin: 0 auto;padding: 5px 0;font-weight: 600;\">\r\n"
			+ "                <p>Cotact us for more detail : </p>\r\n"
			+ "                <p>Email : hcmiu@edu.vn</p>\r\n" + "                <p>Phone : 0xxxxxxxxxxx</p>\r\n"
			+ "                <p>Fax : xxxxxxxxxxxxxx.</p>\r\n" + "            </div>\r\n" + "        </div>\r\n"
			+ "    </div>\r\n" + "</body>\r\n" + "</html>";
	private int status = -1;

	@RequestMapping(value = "/staffs", method = RequestMethod.GET)
	public String display(ModelMap modelMap, @ModelAttribute("staff") Staff staff) {
		List<Staff> staffs = staffService.getAll2();
		for (Staff s : staffs)
			s.setPassword(TripleDES.Decrypt(s.getPassword(), "123456"));
		modelMap.addAttribute("staffs", staffs);
		modelMap.addAttribute("num_table", staffs.size() / 20);
		modelMap.addAttribute("status", status);
		modelMap.addAttribute("percentages", other_PercentageService.getAll2());
		return "staff";
	}

	@Async
	@RequestMapping(value = "/staff", method = RequestMethod.POST)
	public String addStaff(@ModelAttribute("staff") Staff staff, ModelMap map, HttpSession session) throws Exception {
		System.out.println(staff.getId().getId());
		System.out.println(staff.getName());
		System.out.println(staff.getRole());
		System.out.println(staff.getPassword());
		Staff staffTmp = new Staff();
		try {
			MultipartFile multipartFile = staff.getMyfile().getMultipartFile();
			String fileName = multipartFile.getOriginalFilename();
//			File file2 = new File(context.getRealPath("/resources/image"), fileName);
			if (!fileName.equalsIgnoreCase("")) {
				System.out.println("here 1");
				File file2 = new File(context.getRealPath("/resources/image"), fileName);
				multipartFile.transferTo(file2);
				System.out.println(context.getRealPath("/resources/image"));
				System.out.println(staff.getMyfile().getMultipartFile().getOriginalFilename());
				staff.setAvatar("/resources/image/" + fileName);
			} else {
				System.out.println("here 2");
				staff.setAvatar("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		staff.setPassword(TripleDES.Encrypt(staff.getEmail().substring(0, 8), "123456"));
		try {
			System.out.println(staffService.add2(staff));
			status = 1;
			notifyNewAccount(staff, 1);
		} catch (Exception e) {
			status = 2;
		}
		return "redirect:/staffController/staffs";
	}

	public void notifyNewAccount(Staff staff, int type) {
		// Type 1 = add
		// Type 2 = switch role
		String newAccountTmp = newAccount;
		if (type == 1) {
			newAccountTmp = newAccountTmp.replace("#role", "librarian");
			newAccountTmp = newAccountTmp.replace("#recommend",
					"<h1 style=\"color: red;\">You are recommended to change your password at the first login time.</h1>");
			newAccountTmp = newAccountTmp.replace("#staff_password", staff.getEmail().substring(0, 8));
			newAccountTmp = newAccountTmp.replace("#hint", "(The first 8 letters of your email)");
		} else {
			newAccountTmp = newAccountTmp.replace("#role", "manager");
			newAccountTmp = newAccountTmp.replace("#recommended", "");
			newAccountTmp = newAccountTmp.replace("#staff_password", TripleDES.Decrypt(staff.getPassword(), "123456"));
			newAccountTmp = newAccountTmp.replace("#hint", "");
		}
		newAccountTmp = newAccountTmp.replace("#staff_id", staff.getId().getId());
		SendMail.send(newAccountTmp, staff.getEmail());
	}

	@RequestMapping(value = "/staff", method = RequestMethod.GET)
	@ResponseBody
	public Staff getABook(@RequestParam("staff_id") String staff_id) {
		Staff staff = new Staff();
		staff = staffService.getAStaffById(staff_id);
		if (staff.getId() != null)
			staff.setPassword(TripleDES.Decrypt(staff.getPassword(), "123456"));
		return staff;
	}

	@RequestMapping(value = "/staff", method = RequestMethod.DELETE)
	@ResponseBody
	public int deleteCategory(@RequestParam("staff_id") String staff_id) {
		return staffService.delete2(staff_id);
	}

	@RequestMapping(value = "/staffcheckemail", method = RequestMethod.GET)
	@ResponseBody
	public int checkEmail(@RequestParam("email") String email) {
		return staffService.checkEmail(email);
	}

	@RequestMapping(value = "/ustaff", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView updateBook(@ModelAttribute("staff") Staff staff, @RequestParam("type") String staff_id)
			throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("redirect:/homeController/display");
		System.out.println(staff.getName());
		Staff staffTmp = new Staff();
		StaffId id = new StaffId();
		try {
			MultipartFile multipartFile = staff.getMyfile().getMultipartFile();
			String fileName = multipartFile.getOriginalFilename();
			if (!fileName.equalsIgnoreCase("")) {
				File file2 = new File(context.getRealPath("/resources/image"), fileName);
				multipartFile.transferTo(file2);
				System.out.println(context.getRealPath("/resources/image"));
				System.out.println(staff.getMyfile().getMultipartFile().getOriginalFilename());
				staff.setAvatar("/resources/image/" + fileName);
			} else {
				if (!staff_id.equals("")) {
					modelAndView.setViewName("redirect:/staffController/staffs");
					id.setId(staff_id);
					staff.setId(id);
					staffTmp = staffService.getAStaffById(staff_id);
					staff.setAvatar(staffService.getAStaffById(staff_id).getAvatar());
					staff.setRole(staffService.getAStaffById(staff_id).getRole());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		staff.setPassword(TripleDES.Encrypt(staff.getPassword(), "123456"));
		boolean t = staffService.update(staff);
		if (t) {
			status = 11;
		} else
			status = 22;
		return modelAndView;
	}

	@RequestMapping(value = "/resetstatus", method = RequestMethod.GET)
	@ResponseBody
	public void resetStatus() {
		status = -1;
	}

	@RequestMapping(value = "/searchstaff", method = RequestMethod.GET)
	@ResponseBody
	public List<Staff> getStaffs(@RequestParam("keyword") String keyword, @RequestParam("type") int type) {
		List<Staff> list = staffService.searchStaffByKeyWord(keyword, type);
		for (Staff s : list)
			s.setPassword(TripleDES.Decrypt(s.getPassword(), "123456"));
		return list;
	}

	@Async
	@RequestMapping(value = "/setmanager", method = RequestMethod.POST)
	@ResponseBody
	public void setManager(@RequestParam("staff_id_current") String staff_id_current,
			@RequestParam("staff_id_next") String staff_id_next, HttpServletRequest request) {
		System.out.println(staff_id_current);
		System.out.println(staff_id_next);
		Staff currentManger = staffService.getAStaffById(staff_id_current);
		Staff nextManager = staffService.getAStaffById(staff_id_next);
		nextManager.setRole("manager");
		currentManger.setRole("staff");
		staffService.update(currentManger);
		staffService.update(nextManager);
		notifyNewAccount(nextManager, 2);
	}

	@RequestMapping(value = "/broken", method = RequestMethod.POST)
	@ResponseBody
	public void addBrokenLevel(@RequestParam("broken_level") int broken_level,
			@RequestParam("broken_fine") int broken_fine) {
		Other_Percentage other_Percentage = new Other_Percentage();
		other_Percentage.setPercentage(broken_level);
		other_Percentage.setFine(broken_fine);
		other_PercentageService.add(other_Percentage);
	}
	
	@RequestMapping(value = "/broken", method = RequestMethod.DELETE)
	@ResponseBody
	public void removeBrokenLevel(@RequestParam("broken_id") int broken_id) {
		Other_Percentage other_Percentage = other_PercentageService.getById2(broken_id);
		other_PercentageService.delete(other_Percentage);
	}
	
	@RequestMapping(value = "/ubroken", method = RequestMethod.POST)
	@ResponseBody
	public void updateBrokenLevel(@RequestParam("broken_level") int broken_level,
			@RequestParam("broken_fine") int broken_fine,
			@RequestParam("broken_id") int broken_id) {
		Other_Percentage other_Percentage = other_PercentageService.getById2(broken_id);
		other_Percentage.setPercentage(broken_level);
		other_Percentage.setFine(broken_fine);
		other_PercentageService.update(other_Percentage);
	}
	
	@RequestMapping(value = "/broken", method = RequestMethod.GET)
	@ResponseBody
	public Other_Percentage getBrokenLevel(@RequestParam("broken_id") int broken_id) {
		Other_Percentage other_Percentage = other_PercentageService.getById2(broken_id);
		return other_Percentage;
	}
}
