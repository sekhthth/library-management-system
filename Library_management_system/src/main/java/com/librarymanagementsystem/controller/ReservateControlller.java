/**
 * 
 */
package com.librarymanagementsystem.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.crypto.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.librarymanagementsystem.entity.Book;
import com.librarymanagementsystem.entity.BookId;
import com.librarymanagementsystem.entity.Borrow_Log;
import com.librarymanagementsystem.entity.Borrow_Log_Detail;
import com.librarymanagementsystem.entity.Borrower;
import com.librarymanagementsystem.entity.Lecturer;
import com.librarymanagementsystem.entity.Receiver;
import com.librarymanagementsystem.entity.Reservate_Log;
import com.librarymanagementsystem.entity.Reservate_LogId;
import com.librarymanagementsystem.entity.Reservate_Log_Detail;
import com.librarymanagementsystem.entity.Reservate_Log_DetailIdTmp;
import com.librarymanagementsystem.entity.SendMail;
import com.librarymanagementsystem.entity.Staff;
import com.librarymanagementsystem.entity.StaffId;
import com.librarymanagementsystem.entity.Student;
import com.librarymanagementsystem.service.BookService;
import com.librarymanagementsystem.service.Borrow_LogService;
import com.librarymanagementsystem.service.Borrow_Log_DetailService;
import com.librarymanagementsystem.service.BorrowerService;
import com.librarymanagementsystem.service.LecturerService;
import com.librarymanagementsystem.service.Reservate_LogService;
import com.librarymanagementsystem.service.Reservate_Log_DetailService;
import com.librarymanagementsystem.service.StaffService;
import com.librarymanagementsystem.service.StudentService;

/**
 * @author Michael_IT
 *
 */
@Controller
@RequestMapping(value="/reservateController")
public class ReservateControlller {
	@Autowired
	private Reservate_LogService reservate_LogService;
	@Autowired
	private Reservate_Log_DetailService reservate_Log_DetailService;
	@Autowired
	private BookService bookService;
	@Autowired
	private Borrow_LogService borrow_LogService;
	@Autowired
	private Borrow_Log_DetailService borrow_Log_DetailService;
	@Autowired
	private LecturerService lecturerService;
	@Autowired
	private StudentService studentService;
	@Autowired
	private StaffService staffService;
	@Autowired
	private BorrowerService borrowerService;
	
	private String expiredContentMail = "<div class=\"container\" style=\"box-sizing: border-box;width: 100%;color: #2c3e50;\r\n" + 
			"    background: linear-gradient(#3498db, #1aa3ff99);\">\r\n" + 
			"        <div style=\"max-width: 800px; margin: auto;min-width: 600px;width: 80%;\">\r\n" + 
			"            <h1 class=\"title\" style=\"text-align: center;margin: 10px 0;padding: 0px;\">International University Library -\r\n" + 
			"                HCMIU</h1>\r\n" + 
			"            <h3 class=\"text\" style=\"color: aqua;text-align: center;\">Your reservation record expired.</h3>\r\n" + 
			"            <div class=\"warning\" style=\"color: firebrick;margin: 10px 0;text-align: center;\">\r\n" + 
			"                <h3 style=\"text-transform: uppercase;margin: 5px 0;\"><span>FROM :#current_date</h3>\r\n" + 
			"                    <h3 style=\"text-transform: uppercase;margin: 5px 0;\"><span>TO :#due_date</h3>\r\n" + 
			"            </div>\r\n" + 
			"            <div class=\"reservation_detail_container\"\r\n" + 
			"                style=\"width: 60%;margin: auto;border: 1px solid;padding: 10px 10px;\">\r\n" + 
			"                <h2 style=\"text-align: center;margin-bottom: 10px;\">Reservation detail</h2>\r\n" + 
			"                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">Reservation id : </h4>\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">#reservation_id</h4>\r\n" + 
			"                </div>\r\n" + 
			"                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">Reservation person id : </h4>\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">#reservation_person_id</h4>\r\n" + 
			"                </div>\r\n" + 
			"                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">Reservation person name : </h4>\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">#reservation_person_name</h4>\r\n" + 
			"                </div>\r\n" + 
			"                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">Reservation date : </h4>\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">#reservation_date</h4>\r\n" + 
			"                </div>\r\n" + 
			"                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">Staff id : </h4>\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">#staff_id</h4>\r\n" + 
			"                </div>\r\n" + 
			"                <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">staff name : </h4>\r\n" + 
			"                    <h4 style=\"font-weight: 500;\">#staff_name</h4>\r\n" + 
			"                </div>\r\n" + 
			"            </div>\r\n" + 
			"            <div class=\"books_container\"\r\n" + 
			"                style=\"width: 60%;margin: auto;padding: 10px 10px;border: 1px solid;margin-top: 10px;\">\r\n" + 
			"                <h2 style=\"text-align: center;margin-bottom: 10px;\">Reserved book(s)</h2>";
	
	private String expriedContentMailBook = " <div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n" + 
			"                    <p style=\"font-weight: 500;\"><span>#book_order. </span></p>\r\n" + 
			"                    <p style=\"font-weight: 500;\"><span>#book_name - </span><span>#book_id</span></p>\r\n" + 
			"                </div>";
	private String expriedContentMailBookTmp = expriedContentMailBook;
	private String expriedContentMailTail = "</div>\r\n" + 
			"        </div>\r\n" + 
			"        <div class=\"contact\" style=\"width: 100%;border-top: 1px solid;margin-top: 10px;\">\r\n" + 
			"            <div style=\"width: max-content;margin: 0 auto;padding: 5px 0;font-weight: 600;\">\r\n" + 
			"                <p>Cotact us for more detail : </p>\r\n" + 
			"                <p>Email : hcmiu@edu.vn</p>\r\n" + 
			"                <p>Phone : 0xxxxxxxxxxx</p>\r\n" + 
			"                <p>Fax : xxxxxxxxxxxxxx.</p>\r\n" + 
			"            </div>\r\n" + 
			"        </div>\r\n" + 
			"    </div>";
	private String expiredContentMailTmp = expiredContentMail;
	private String content = "<!-- <!DOCTYPE html>\r\n" + "<html>\r\n" + "<head>\r\n" + "	<title></title>\r\n"
			+ "	<link rel=\"stylesheet\" type=\"text/css\" href=\"2.css\">\r\n" + "</head>\r\n" + "<body>\r\n"
			+ "	<div class=\"test\">\r\n" + "		<button class=\"btn\">Test</button>\r\n"
			+ "		<div class=\"menu\" id=\"nav\">\r\n" + "			<a href=\"\">1</a>\r\n"
			+ "			<a href=\"\">1</a>\r\n" + "		</div>\r\n" + "	</div>\r\n" + "</body>\r\n" + "</html> -->\r\n"
			+ "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "<head>\r\n" + "	<meta charset=\"UTF-8\">\r\n"
			+ "	<title>IU Library</title>\r\n" + "	<style type=\"text/css\">\r\n" + "		* {\r\n"
			+ "			margin: 0;\r\n" + "			padding: 0;\r\n"
			+ "			font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;\r\n" + "		}\r\n"
			+ "		html, body {\r\n" + "			width: 100%;\r\n" + "			height: 100%;\r\n"
			+ "			overflow: hidden;\r\n" + "		}\r\n" + "	</style>\r\n" + "</head>\r\n"
			+ "<body style=\"\">\r\n"
			+ "	<div class=\"container\" style=\"box-sizing: border-box;width: 100%;color: #2c3e50;\r\n"
			+ "    background: linear-gradient(#3498db, #1aa3ff99);\">\r\n"
			+ "		<div style=\"max-width: 800px; margin: auto;min-width: 600px;width: 80%;\">\r\n"
			+ "			<h1 class=\"title\" style=\"text-align: center;margin: 10px 0;padding: 0px;\">International University Library - HCMIU</h1>\r\n"
			+ "			<h3 class=\"text\" style=\"color: aqua;text-align: center;\">The books that you reserved before are available now.</h3>\r\n"
			+ "			<div class=\"warning\" style=\"color: firebrick;margin: 10px 0;text-align: center;\">\r\n"
			+ "				<h3>You have 3 days afer this mail sent for coming IU Library in order to take these books. After 3 days if you do not take these books, your reservation will be remove.</h3>\r\n"
			+ "				<h3 style=\"text-transform: uppercase;margin: 5px 0;\"><span>FROM : </span><span>#current_date</span><span>-TO : </span><span>#due_date</span></h3>\r\n"
			+ "			</div>\r\n"
			+ "			<div class=\"reservation_detail_container\" style=\"width: 60%;margin: auto;border: 1px solid;padding: 10px 10px;\">\r\n"
			+ "				<h2 style=\"text-align: center;margin-bottom: 10px;\">Reservation detail</h2>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation id : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_id</h4>\r\n" + "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation person id : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_person_id</h4>\r\n"
			+ "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation person name : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_person_name</h4>\r\n"
			+ "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Reservation date : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#reservation_date</h4>\r\n" + "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">Staff id : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#staff_id</h4>\r\n" + "				</div>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
			+ "					<h4 style=\"font-weight: 500;\">staff name : </h4>\r\n"
			+ "					<h4 style=\"font-weight: 500;\">#staff_name</h4>\r\n" + "				</div>\r\n"
			+ "			</div>\r\n"
			+ "			<div class=\"books_container\" style=\"width: 60%;margin: auto;padding: 10px 10px;border: 1px solid;margin-top: 10px;\">\r\n"
			+ "				<h2 style=\"text-align: center;margin-bottom: 10px;\">Available book(s)</h2>\r\n"
			+ "				<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
//			+ "						<p style=\"font-weight: 500;\"><span>#book_order. </span></p>\r\n"
//			+ "						<p  style=\"font-weight: 500;\"><span>#book_name - </span><span>#book_id</span></p>\r\n"
			+ "					</div>\r\n" + "					\r\n";
	private String contentTail = "</div>\r\n" + "		</div>\r\n"
			+ "		<div class=\"contact\" style=\"width: 100%;border-top: 1px solid;margin-top: 10px;\">\r\n"
			+ "				<div style=\"width: max-content;margin: 0 auto;padding: 5px 0;font-weight: 600;\">\r\n"
			+ "					<p>Cotact us for more detail : </p>\r\n"
			+ "					<p>Email : hcmiu@edu.vn</p>\r\n" + "					<p>Phone : 0xxxxxxxxxxx</p>\r\n"
			+ "					<p>Fax : xxxxxxxxxxxxxx.</p>\r\n" + "				</div>\r\n"
			+ "			</div>	\r\n" + "	</div>\r\n" + "</body>\r\n" + "</html>";
	private String contentTmp = content;
	private int initialContentLength = content.length();	
	private int test = 1;
	private String email = "";
	private ArrayList<Receiver> receivers = new ArrayList<Receiver>();
	private Receiver receiver;
	
	private String due_date = "";
	
	@RequestMapping(value="/reservated", method=RequestMethod.GET)
	@ResponseBody
	public int isReservated(@RequestParam("person_id") String person_id, @RequestParam("book_id") String book_id) {
		System.out.println(person_id + "---" + book_id);
		return reservate_LogService.isReservated(person_id, book_id);
	}
	
	@RequestMapping(value="/currentid", method=RequestMethod.GET)
	@ResponseBody
	public int currentId() {
		return reservate_LogService.getLastestId();
	}
	
	@RequestMapping(value="/reservate", method=RequestMethod.POST)
	@ResponseBody
	public void borrow(@RequestParam("person_id") String person_id, 
			@RequestParam("book_id") String book_id, 
			@RequestParam("reservate_date") String reservate_date, 
			@RequestParam("staff_id") String staff_id,
			@RequestParam("current_id") int current_id) {
		System.out.println(person_id);
		System.out.println(book_id);
		System.out.println(reservate_date);
		Borrow_Log borrow_Log;
		Borrow_Log_Detail borrow_Log_Detail;
		Reservate_Log reservate_Log1;
		Reservate_Log_Detail reservate_Log_Detail1;
		
		Reservate_Log reservate_Log = new Reservate_Log();
		Reservate_LogId reservate_LogId = new Reservate_LogId();
		Reservate_Log_Detail reservate_Log_Detail = new Reservate_Log_Detail();
		Reservate_Log_DetailIdTmp reservate_Log_DetailId = new Reservate_Log_DetailIdTmp();
		int tmp = reservate_LogService.getLastestId();
		
		if (current_id == tmp) {
			reservate_LogId.setId(current_id);
			reservate_Log.setId(reservate_LogId);
			reservate_Log.setPerson_id(borrowerService.getById2(person_id));
			reservate_Log.setReservate_date(formatDate(reservate_date));
			reservate_Log.setStaff_id(new Staff(new StaffId(staff_id)));
			reservate_LogService.add(reservate_Log);
			reservate_Log_DetailId.setReservate_log(reservate_LogService.getReservateLog(reservate_LogId.getId()));
		} else {
			reservate_Log_DetailId.setReservate_log(reservate_LogService.getReservateLog(current_id));
		}
		
		reservate_Log_DetailId.setBook(bookService.getABookById(book_id));
		reservate_Log_Detail.setId(reservate_Log_DetailId);
		
		borrow_Log = borrow_LogService.getBorrowLog(borrow_LogService.getBorrowTmp(book_id));
//		System.out.println(borrow_Log.getId().getId());
		if (borrow_Log != null) {
			reservate_Log_Detail.setDue_date(borrow_Log.getReturn_date());
			borrow_Log_Detail = borrow_Log_DetailService.getByIdAndBookId(book_id, borrow_Log.getId().getId());
			borrow_Log_Detail.setReserved(1);
			borrow_Log_DetailService.update(borrow_Log_Detail);
		} else {
			System.out.println("No va day nua");
			reservate_Log1 = reservate_LogService.getReservateLog(reservate_LogService.getReservatedTmp(book_id));
			reservate_Log_Detail1 = reservate_Log_DetailService.getByIdAndBookId(book_id, reservate_Log1.getId().getId());
			reservate_Log_Detail1.setReserved(1);
			reservate_Log_Detail.setDue_date(addOneMonth(reservate_Log_Detail1.getDue_date()));
			reservate_Log_DetailService.update(reservate_Log_Detail1);
		}
//		
		reservate_Log_Detail.setStatus(2);
		reservate_Log_Detail.setDuration("");
		due_date = reservate_Log_Detail.getDue_date();
		reservate_Log_DetailService.add(reservate_Log_Detail);
//		System.out.println(borrow_LogService.getBorrowTmp(book_id));
	}
	
	@RequestMapping(value="/getduedate", method=RequestMethod.GET)
	@ResponseBody
	public Reservate_Log_Detail getDuedate(@RequestParam("book_id") String book_id,
							 @RequestParam("borrow_log_id") int borrow_log_id) {
		return reservate_Log_DetailService.getByIdAndBookId(book_id, borrow_log_id);
	}
	
	@Async
	@RequestMapping(value="/nottaken", method=RequestMethod.POST)
	@ResponseBody
	public void notTaken() {
		List<Reservate_Log_Detail> list;
		list = reservate_Log_DetailService.getListNotTaken();
		Date date = null;
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Book book = null;
		Student student = null;
		Lecturer lecturer = null;
		Reservate_Log reservate_Log = null;
		String mailContent = "";
		String emailAddress = "";
		String bookHtml1 = "<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
				+ "						<p style=\"font-weight: 500;\"><span>#book_order. </span></p>\r\n"
				+ "						<p  style=\"font-weight: 500;\"><span>#book_name - </span><span>#book_id</span></p>\r\n"
				+ "					</div>\r\n";
		String bookHtml = "";
		Reservate_Log reservate_Log2 = null;
		Reservate_Log_Detail reservate_Log_Detail2 = null;
		if (list != null) {
			for (Reservate_Log_Detail r : list) {
				try {
					date = sFormat.parse(r.getDuration());
					
					if (differentDays(date) <= 0) {
						System.out.println(r.getId().getReservate_log().getId().getId() + "Bi tre " + differentDays(date));
						contentTmp = content;
						reservate_Log = reservate_LogService.getReservateLog(r.getId().getReservate_log().getId().getId());
						book = bookService.getABookById(r.getId().getBook().getId().getId());
						if (reservate_Log.getPerson_id().getId().getPerson_id().substring(0, 2).equalsIgnoreCase("LE")) {
							lecturer = lecturerService.getLecturer(reservate_Log.getPerson_id().getId().getPerson_id());
							emailAddress = lecturer.getEmail();
							expiredContentMailTmp = expiredContentMailTmp.replace("#reservation_person_name", lecturer.getName());
							
							contentTmp = contentTmp.replace("#reservation_person_id", lecturer.getId().getId());
							contentTmp = contentTmp.replace("#reservation_person_name", lecturer.getName());
							email = lecturer.getEmail();
						} else {
							student = studentService.getStudent(reservate_Log.getPerson_id().getId().getPerson_id());
							emailAddress = student.getEmail();
							expiredContentMailTmp = expiredContentMailTmp.replace("#reservation_person_name", student.getName());
							
							contentTmp = contentTmp.replace("#reservation_person_id", student.getId().getId());
							contentTmp = contentTmp.replace("#reservation_person_name", student.getName());
							email = student.getEmail();
						}
						
						expiredContentMailTmp = expiredContentMailTmp.replace("#due_date", sFormat.format(date));
						date.setDate(date.getDate()-3);
						expiredContentMailTmp = expiredContentMailTmp.replace("#current_date", sFormat.format(date));
						expiredContentMailTmp = expiredContentMailTmp.replace("#reservation_id", reservate_Log.getId().getId()+"");
						expiredContentMailTmp = expiredContentMailTmp.replace("#reservation_person_id", reservate_Log.getPerson_id().getId().getPerson_id());
						expiredContentMailTmp = expiredContentMailTmp.replace("#reservation_date", reservate_Log.getReservate_date());
						expiredContentMailTmp = expiredContentMailTmp.replace("#staff_id", reservate_Log.getStaff_id().getId().getId());
						expiredContentMailTmp = expiredContentMailTmp.replace("#staff_name", reservate_Log.getStaff_id().getName());
					
						expriedContentMailBookTmp = expriedContentMailBookTmp.replace("#book_order", (list.indexOf(r)+1)+"");
						expriedContentMailBookTmp = expriedContentMailBookTmp.replace("#book_name", book.getName());
						expriedContentMailBookTmp = expriedContentMailBookTmp.replace("#book_id", book.getId().getId());
						
						expiredContentMailTmp += expriedContentMailBook;
						expriedContentMailBookTmp = expriedContentMailBook;
						
						r.setStatus(3);//Hủy
						reservate_Log_DetailService.update(r);
						int tmp = reservate_LogService.getAPersonForMail(book.getId().getId());
						if (tmp != 0) {
							reservate_Log2 = reservate_LogService.getReservateLog(tmp);
							reservate_Log_Detail2 = reservate_Log_DetailService.getByIdAndBookId(book.getId().getId(), tmp);
							Date d = new Date();
							contentTmp = contentTmp.replace("#current_date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d));
							reservate_Log_Detail2.setDuration(getDuration(d));
							bookHtml = bookHtml1;
							bookHtml = bookHtml.replace("#book_order", (test++) + "");
							bookHtml = bookHtml.replace("#book_name", book.getName());
							bookHtml = bookHtml.replace("#book_id", book.getId().getId());
							contentTmp = contentTmp.replace("#reservation_id",
									reservate_Log_Detail2.getId().getReservate_log().getId().getId() + "");
							contentTmp = contentTmp.replace("#reservation_date", reservate_Log2.getReservate_date());
							contentTmp = contentTmp.replace("#staff_id", reservate_Log2.getStaff_id().getId().getId());
							contentTmp = contentTmp.replace("#staff_name", reservate_Log2.getStaff_id().getName());
							contentTmp = contentTmp.replace("#due_date", reservate_Log_Detail2.getDuration());
							receiver = new Receiver(email, contentTmp, contentTail, bookHtml, reservate_Log2.getPerson_id().getId().getPerson_id());
							receivers.add(receiver);
							
							reservate_Log_DetailService.update(reservate_Log_Detail2);//update
						}
					} else
						System.out.println("Khoong co tre" + differentDays(date));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			ArrayList<Receiver> receivers2 = new ArrayList<Receiver>();
			int index;
			for(Receiver r : receivers) {
				System.out.println(r.getBookHtml());
				index = isExist(receivers2, r.getPerson_id());
				System.out.println(index);
				if (index >= 0) {
					receivers2.get(index).addBookHtmlToContentMail(r.getBookHtml());
					System.out.println("---");
					System.out.println(receivers2.get(index).getBookHtml());
					System.out.println("---");
				} else {
					receivers2.add(r);
				}
			}
			System.out.println("lenght : " + receivers2.size());
			for(Receiver r : receivers2) {
				SendMail.send(r.getCompleteContentMail(), r.getEmail());
			}
			
			receivers = new ArrayList<Receiver>();
			receiver = null;
		}
		if (expiredContentMailTmp.length() > expiredContentMail.length()) {
			expiredContentMailTmp += expriedContentMailTail;
			SendMail.send(expiredContentMailTmp, emailAddress);
			expiredContentMailTmp = expiredContentMail;
		}
	}
	
//	@RequestMapping(value="/reservatelog", method=RequestMethod.GET)
//	@ResponseBody
//	public Reservate_Log getReservate_Log(@RequestParam("reservate_log_id") int reservate_log_id) {
//		return reservate_LogService.getReservateLog(reservate_log_id);
//	}
	
	@RequestMapping(value="/reservatelog", method=RequestMethod.GET)
	@ResponseBody
	public List<Object> getReservate_Log(@RequestParam("reservate_log_id") int reservate_log_id) {
		List<Object> list = new ArrayList<Object>();
		Reservate_Log reservate_Log = reservate_LogService.getReservateLog(reservate_log_id);
		list.add(reservate_Log);
		if (reservate_Log != null)
			list.add(borrowerService.getById2(reservate_Log.getPerson_id().getId().getPerson_id()));
		else list.add(new Borrower());
		if (reservate_Log != null)
			list.add(staffService.getAStaffById(reservate_Log.getStaff_id().getId().getId()));
		else list.add(new Staff());
		return list;
	}
	
	@RequestMapping(value="/bookfortaking", method=RequestMethod.GET)
	@ResponseBody
	public List<Book> getBooksForTaking(@RequestParam("reservate_log_id") int reservate_log_id) {
		List<Book> list = reservate_Log_DetailService.getBookForTaking(reservate_log_id);
		for (Book b : list)
			b.setYmanufacture(b.getYmanufacture()+"-"+b.getCategory_id().getName());
		return list;
	}
	
	@RequestMapping(value="/taken", method=RequestMethod.GET)
	@ResponseBody
	public List<Book> isTaken(@RequestParam("reservate_log_id") int reservate_log_id) {
		List<Book> list = reservate_Log_DetailService.isTaken(reservate_log_id);
//		for (Book b : list)
//			b.setYmanufacture(b.getYmanufacture()+"-"+b.getCategory_id().getName());
		return list;
	}
	
	@RequestMapping(value="/take", method=RequestMethod.POST)
	@ResponseBody
	public void take(@RequestParam("reservate_log_id") int reservate_log_id, @RequestParam("book_id") String book_id) {
		Reservate_Log_Detail reservate_Log_Detail = reservate_Log_DetailService.getByIdAndBookId(book_id, reservate_log_id);
		reservate_Log_Detail.setStatus(1);
		reservate_Log_DetailService.update(reservate_Log_Detail);
	}
	
	@Async
	@RequestMapping(value="/nottake", method=RequestMethod.POST)
	@ResponseBody
	public void notTake(@RequestParam("reservate_log_id") int reservate_log_id, 
						@RequestParam("book_id") String book_id, 
						@RequestParam("end") int end) {
		Reservate_Log_Detail reservate_Log_Detail = reservate_Log_DetailService.getByIdAndBookId(book_id, reservate_log_id);
		reservate_Log_Detail.setStatus(3);
		reservate_Log_DetailService.update(reservate_Log_Detail);
		
		Book book = new Book();
		book = bookService.getABookById(book_id);
		int tmp = reservate_LogService.getAPersonForMail(book_id);
		Reservate_Log reservate_Log = null;
		Lecturer lecturer = null;
		Student student = null;
		String bookHtml = "<div style=\"display: flex;justify-content: space-between;flex-wrap: wrap;\">\r\n"
				+ "						<p style=\"font-weight: 500;\"><span>#book_order. </span></p>\r\n"
				+ "						<p  style=\"font-weight: 500;\"><span>#book_name - </span><span>#book_id</span></p>\r\n"
				+ "					</div>\r\n";
		System.out.println(tmp + " Người được gửi mail có sách");
		if (tmp != 0) {
			System.out.println("end0 : " + end);
			contentTmp = content;
			reservate_Log_Detail = reservate_Log_DetailService.getByIdAndBookId(book_id, tmp);
			reservate_Log = reservate_LogService.getReservateLog(tmp);
			if (reservate_Log.getPerson_id().getId().getPerson_id().substring(0, 2).equalsIgnoreCase("LE")) {
				lecturer = lecturerService.getLecturer(reservate_Log.getPerson_id().getId().getPerson_id());
				contentTmp = contentTmp.replace("#reservation_person_id", lecturer.getId().getId());
				contentTmp = contentTmp.replace("#reservation_person_name", lecturer.getName());
				email = lecturer.getEmail();
			} else {
				student = studentService.getStudent(reservate_Log.getPerson_id().getId().getPerson_id());
				contentTmp = contentTmp.replace("#reservation_person_id", student.getId().getId());
				contentTmp = contentTmp.replace("#reservation_person_name", student.getName());
				email = student.getEmail();
			}
//			System.out.println(reservate_Log_Detail.getReserved());

			Date d = new Date();
			contentTmp = contentTmp.replace("#current_date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d));
			reservate_Log_Detail.setDuration(getDuration(d));
			bookHtml = bookHtml.replace("#book_order", (test++) + "");
			bookHtml = bookHtml.replace("#book_name", book.getName());
			bookHtml = bookHtml.replace("#book_id", book.getId().getId());
			contentTmp = contentTmp.replace("#reservation_id",
					reservate_Log_Detail.getId().getReservate_log().getId().getId() + "");
			contentTmp = contentTmp.replace("#reservation_date", reservate_Log.getReservate_date());
			contentTmp = contentTmp.replace("#staff_id", reservate_Log.getStaff_id().getId().getId());
			contentTmp = contentTmp.replace("#staff_name", reservate_Log.getStaff_id().getName());
			contentTmp = contentTmp.replace("#due_date", reservate_Log_Detail.getDuration());
			receiver = new Receiver(email, contentTmp, contentTail, bookHtml, reservate_Log.getPerson_id().getId().getPerson_id());
			receivers.add(receiver);
			contentTmp += bookHtml;

			reservate_Log_DetailService.update(reservate_Log_Detail);//update
		}
		
		if (end == 1) {
			System.out.println("end1 : " + end);
			contentTmp += contentTail;
//			SendMail.send(contentTmp, email);
			contentTmp = content;
			ArrayList<Receiver> receivers2 = new ArrayList<Receiver>();
			int index;
			for(Receiver r : receivers) {
				System.out.println(r.getBookHtml());
				index = isExist(receivers2, r.getPerson_id());
				System.out.println(index);
				if (index >= 0) {
					receivers2.get(index).addBookHtmlToContentMail(r.getBookHtml());
					System.out.println("---");
					System.out.println(receivers2.get(index).getBookHtml());
					System.out.println("---");
				} else {
					receivers2.add(r);
				}
			}
			System.out.println("lenght : " + receivers2.size());
			for(Receiver r : receivers2) {
				SendMail.send(r.getCompleteContentMail(), r.getEmail());
			}
			
			receivers = new ArrayList<Receiver>();
			receiver = null;
		}
	}
	
	public int isExist(ArrayList<Receiver> list, String person_id) {
		for(Receiver r : list) {
			if (person_id.equalsIgnoreCase(r.getPerson_id()))
				return list.indexOf(r);
		}
		return -1;
	}
	
	public String getDuration(Date current_date) {
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		current_date.setDate(current_date.getDate() + 3);
		return sFormat.format(current_date);
	}
	
//	@RequestMapping(value="/recordswithoutreturn", method=RequestMethod.GET)
//	@ResponseBody
//	public List<Reservate_Log> getListReservateLog(@RequestParam("current_date") String current_date) {
//		List<Reservate_Log> list = new ArrayList<Reservate_Log>();
//		list = reservate_LogService.getListRervateLog(current_date);
//		return list;
//	}
	
	@RequestMapping(value="/recordswithoutreturn", method=RequestMethod.GET)
	@ResponseBody
	public List<Object> getListReservateLog(@RequestParam("current_date") String current_date) {
		List<Object> list = new ArrayList<Object>();
		List<Reservate_Log> reservate_Logs = new ArrayList<Reservate_Log>();
		List<Borrower> borrowers = new ArrayList<Borrower>();
		List<Staff> staffs = new ArrayList<Staff>();
		reservate_Logs = reservate_LogService.getListRervateLog(current_date);
		for (Reservate_Log b : reservate_Logs) {
			borrowers.add(borrowerService.getById2(b.getPerson_id().getId().getPerson_id()));
			staffs.add(staffService.getAStaffById(b.getStaff_id().getId().getId()));
		}
		list.add(reservate_Logs);
		System.out.println(reservate_Logs.size());
		list.add(borrowers);
		System.out.println(borrowers.size());
		list.add(staffs);
		System.out.println(staffs.size());
		return list;
	}
	
	@RequestMapping(value="/booksinresevatelog", method=RequestMethod.GET)
	@ResponseBody
	public List<Book> getBooksInReservateLog(@RequestParam("reservate_log_id") int reservate_log_id) {
		List<Book> list = new ArrayList<Book>();
		list = reservate_LogService.getBooksInReservateLog(reservate_log_id);
		for (Book b : list)
			b.setId(new BookId(b.getId().getId()+"-"+b.getCategory_id().getName()));
		return list;
	}
	
	public int differentDays(Date date) {
		Date current_date = new Date();
		long diff = date.getTime() - current_date.getTime();
		 
		int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
		return diffDays;
	}
	
	public String get3DaysAgo(Date date) {
		return "";
	}
	
	public String addOneMonth(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
			d.setDate(d.getDate() + 30);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}
	
	public String formatDate(String date) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			d = sFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sFormat.format(d);
	}
	
	public static void main(String[] args) {
		Date d = new Date();
		SimpleDateFormat sFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		try {
//			d = sFormat.parse("2020-1-3 17:00:00");
//			d.setDate(d.getDate() + 30);
//			System.out.println(sFormat.format(d));
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		try {
////			d = sFormat.parse(sFormat.format(d));
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		System.out.println(sFormat.format(d));
		d.setDate(d.getDate()+3);
		System.out.println(sFormat.format(d));
		Date d1 = new Date();
		Date d2 = new Date();
		d2.setDate(d2.getDate()+1);
		d2.setMinutes(1);
		System.out.println(d1);
		System.out.println(d2);
		long diff = d1.getTime() - d2.getTime();
		 
		int diffDays = (int) (diff / (24 * 60 * 60 * 1000));
		System.out.println("difference between days: " + diffDays);
		
	}
}
