<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Report|Manager</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/report.css"/>'>
	<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/book.css"/>'>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/fontawesome/all.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/report.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/nav.js"></c:url>"></script>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/fontawesome/all.min.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.min.js"></c:url>"></script>
<link href="https://fonts.googleapis.com/css?family=Monda&display=swap"
	rel="stylesheet">
</head>
<body>
	<c:if test="${user != null}">
		<c:if test="${user.role == 'manager'}">
			<nav class="">
				<span class="closeNavBtn">X</span>
				<ul>
					<li><a href="../staffController/staffs">Staff</a></li>
					<li><a class="active">Report</a></li>
					<li><button class="logoutBtn">Logout</button></li>
					<li><button class="brokenBtn">Broken level</button></li>
				</ul>
			</nav>
			<div class="toggle">
				<p>Menu</p>
			</div>
			<div class="black"></div>
			<div class="black2"></div>
			<h1 class="title">Manage Report</h1>

			<div class="content">
				<div class="books_container">
					<c:set var="num_book" value="0"></c:set>
					<c:set var="num_page" value="0"></c:set>
					<c:if test="${fn:length(reports) <= 20}">
						<table class="books">
							<tr>
								<th>ID</th>
								<th>Report date</th>
								<th>Staff id</th>
								<th>Staff name</th>
								<th>Action</th>
							</tr>
							<c:if test="${fn:length(reports) > 0}">
								<c:forEach items="${reports}" var="book" begin="0"
									end="${fn:length(reports)-1}">
									<tr id="${reports[num_book].id}">
										<td>${reports[num_book].id}</td>
										<td>${reports[num_book].report_date}</td>
										<td>${reports[num_book].staff_id.name}</td>
										<td>${reports[num_book].staff_id.id.id}</td>
										<td>
											<button value="${reports[num_book].id}" data-table="0">
												<i class="fas fa-info"></i>
											</button>
										</td>
									</tr>
									<c:set var="num_book" value="${num_book+1}"></c:set>
								</c:forEach>
							</c:if>
						</table>
						<div class="pages">
							<span class="page_active">1</span>
						</div>
					</c:if>
					<c:if test="${fn:length(reports) > 20}">
						<c:forEach begin="0" end="${num_table}" var="table">
							<table class="books">
								<tr>
									<th>ID</th>
									<th>Report date</th>
									<th>Staff</th>
									<th>Action</th>
								</tr>
								<c:forEach items="${reports}" var="book" begin="0" end="19">
									<c:if test="${reports[num_book]!=null}">
										<tr id="${reports[num_book].id}">
											<td>${reports[num_book].id}</td>
											<td>${reports[num_book].report_date}</td>
											<td>${reports[num_book].staff_id.id.id}</td>
											<td>${reports[num_book].staff_id.name}</td>
											<td>
												<button value="${reports[num_book].id}"
													data-table="${table}">
													<i class="fas fa-info"></i>
												</button>
											</td>
										</tr>
									</c:if>
									<c:set var="num_book" value="${num_book+1}"></c:set>
								</c:forEach>
							</table>
							<c:set var="num_page" value="${num_page+1}"></c:set>
						</c:forEach>
					</c:if>
					<span class="previous"><i class="fas fa-chevron-left"></i></span> <span
						class="next"><i class="fas fa-chevron-right"></i></span>
					<div class="pages">
						<c:forEach var="page" begin="1" end="${num_page}">
							<span>${page}</span>
						</c:forEach>
					</div>
				</div>
				<div class="edit-form"></div>
				<iframe name="hidden" style="width: 0; height: 0"></iframe>
			</div>

			<div class="reportContainer">
				<h2 class="big_title">Report</h2>
				<h4 class="report_date">
					Report date : <span></span>
				</h4>
				<div class="borrow">
					<h5 class="small_title">Borrow Record(s)</h5>
					<table>
						<!-- <tr>
                    <th>No.</th>
                    <th>Borrower id</th>
                    <th>Borrower name</th>
                    <th>Borrow date</th>
                    <th>Return date</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>#borrow_id</td>
                    <td>#borrower_id</td>
                    <td>#borrower_name</td>
                    <td>#borrow_date</td>
                    <td>#return_date</td>
                    <td><button id="#borrow_id">More</button></td>
                </tr> -->
					</table>
				</div>
				<div class="reservate">
					<h5 class="small_title">Reservate Record(s)</h5>
					<table>
						<!-- <tr>
                    <th>No.</th>
                    <th>Reservation person id</th>
                    <th>Reservation person name</th>
                    <th>Reservation date</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>#reservation_id</td>
                    <td>#reservation_person_id</td>
                    <td>#reservation_person_name</td>
                    <td>#reservation_date</td>
                    <td><button id="#reservation_id">More</button></td>
                </tr> -->
					</table>
				</div>
				<div class="return">
					<h5 class="small_title">Return Record(s)</h5>
					<table>
						<!-- <tr>
                    <th>No.</th>
                    <th>Borrower id</th>
                    <th>Borrower name</th>
                    <th>Borrow date</th>
                    <th>Return date</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>#borrow_id</td>
                    <td>#borrower_id</td>
                    <td>#borrower_name</td>
                    <td>#borrow_date</td>
                    <td>#return_date</td>
                    <td><button id="#borrow_id">More</button></td>
                </tr> -->
					</table>
				</div>
			</div>

			<div class="moreContainer">
				<h2 class="big_title">Borrowed Book(s)</h2>
				<table>
				</table>
			</div>
			
			<div class="brokenContainer">
				<h1>Broken level</h1>
				<c:forEach items="${percentages}" var="percentage">
				<div class="broken">
					<div class="brokenInfo">
						<p class="brokenId">${percentage.id}</p>
						<div>
							<label for="brokenLevel${percentage.id}">Broken level</label> <input type="text"
								class="brokenLevel" value="${percentage.percentage}" id="brokenLevel${percentage.id}">
						</div>
						<div>
							<label for="brokenFine${percentage.id}">Broken Fine</label> <input type="text"
								class="brokenLevel" value="${percentage.fine}" id="brokenFine${percentage.id}">
						</div>
					</div>
					<div class="brokenButton">
						<button class="removeBrokenBtn" value="${percentage.id}">
							<i class="far fa-trash-alt"></i>
						</button>
						<button class="modifyBrokenBtn" value="${percentage.id}">
							<i class="far fa-edit"></i>
						</button>
						<button class="undoBtn" value="${percentage.id}">
							<i class="fas fa-undo"></i>
						</button>
					</div>
				</div>
				</c:forEach>
				<div class="addBrokenContainer">
					<div>
						<label for="brokenLevelAdd" class="brokenLevelAdd">Broken level</label> <input
							type="text" class="brokenLevel" value=""
							placeholder="Enter broken level" id="brokenLevelAdd">
					</div>
					<div>
						<label for="brokenFineAdd" class="brokenFineAdd">Broken fine</label> <input type="text"
							class="brokenLevel" value="" placeholder="Enter broken fine"
							id="brokenFineAdd">
					</div>
					<button class="addBrokenBtn">Add</button>
				</div>
			</div>
		</c:if>
		<c:if test="${user.role != 'manager'}">
			<div class="background">
				<img alt="" src="/Library_management_system/resources/image/iu.png">
			</div>
			<div class="warningAccess warning">
				<h2 class="big_title">You are not allowed to access to this
					page</h2>
				<h3>
					You will be redirected to the home page after <span>#time</span>s.
				</h3>
				<h3>
					Or you can click on <a href="../homeController/display">this</a> in
					order to immediately redirect to the home page.
				</h3>
			</div>
		</c:if>
	</c:if>

	<c:if test="${user == null}">
	<div class="background">
				<img alt="" src="/Library_management_system/resources/image/iu.png">
			</div>
		<div class="warning">
			<h2 class="big_title">You must login first.</h2>
			<h3>
				You will be redirected to the login page after <span>#time</span>s.
			</h3>
			<h3>
				Or you can click on <a href="../loginController/login">this</a> in
				order to immediately redirect to the login page.
			</h3>
		</div>
	</c:if>
</body>
</html>