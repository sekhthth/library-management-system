<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/book.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/staff.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/nav.js"></c:url>"></script>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/fontawesome/all.min.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.min.js"></c:url>"></script>
<link href="https://fonts.googleapis.com/css?family=Monda&display=swap"
	rel="stylesheet">
</head>
<body>
	<c:if test="${user != null}">
		<c:if test="${user.role == 'manager'}">
			<nav class="">
				<span class="closeNavBtn">X</span>
				<ul>
					<li><a class="active">Staff</a></li>
					<li><a href="../reportController/reports">Report</a></li>
					<li><button class="logoutBtn">Logout</button></li>
					<li><button class="brokenBtn">Broken level</button></li>
				</ul>
			</nav>
			<div class="toggle">
				<p>Menu</p>
			</div>
			<div class="black"></div>
			<h1 class="title">Manage Staff</h1>
			<div class="searchArea">
				<span>Search</span>
				<div class="rigth">
					<div class="rigth1">
						<div>
							<label for="id">ID</label> <input type="radio" value="ID"
								name="type" id="id" checked="checked">
						</div>
						<div>
							<label for="name">Name</label> <input type="radio" value="name"
								name="type" id="name">
						</div>
						<div>
							<label for="email">Email</label> <input type="radio"
								value="email" name="type" id="email">
						</div>
					</div>
					<div class="rigth2">
						<input type="text" placeholder="Search">
					</div>
				</div>
			</div>
			<div class="button_add_container">
				<button class="button_add" type="button">
					<i class="far fa-plus-square"></i>
				</button>
			</div>
			<div class="content">
				<div class="books_container">
					<c:set var="num_book" value="0"></c:set>
					<c:set var="num_page" value="0"></c:set>
					<c:if test="${fn:length(staffs) <= 20}">
						<table class="books">
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Password</th>
								<th>Role</th>
								<th>Avatar</th>
								<th>Action</th>
							</tr>
							<c:forEach items="${staffs}" var="book" begin="0"
								end="${fn:length(staffs)-1}">
								<tr id="${staffs[num_book].id.id}">
									<td>${staffs[num_book].id.id}</td>
									<td>${staffs[num_book].name}</td>
									<td>${staffs[num_book].email}</td>
									<td>${staffs[num_book].password}</td>
									<td>${staffs[num_book].role}</td>
									<td><img
										src="<c:url value='${staffs[num_book].avatar}'></c:url>">
									</td>
									<td><c:if test="${staffs[num_book].role == 'staff'}">
											<button value="${staffs[num_book].id.id}"
												data-table="${table}" class="removeBtn" title="Remove staff">
												<i class="far fa-trash-alt"></i>
											</button>
										</c:if>
										<button value="${staffs[num_book].id.id}"
											data-table="${table}" class="modifyBtn" title="Modify staff">
											<i class="far fa-edit"></i>
										</button> <c:if test="${staffs[num_book].role == 'staff'}">
											<button value="${staffs[num_book].id.id},${user.id.id}"
												data-table="${table}" title="Set as a manager"
												class="setManagerBtn">
												<i class="fas fa-exchange-alt"></i>
											</button>
										</c:if> <c:if test="${staffs[num_book].role != 'staff'}">
											<button value="${staffs[num_book].id.id}"
												data-table="${table}" title="Set as a manager"
												class="setManagerBtn" hidden="hidden">
												<i class="fas fa-exchange-alt"></i>
											</button>
										</c:if></td>
								</tr>
								<c:set var="num_book" value="${num_book+1}"></c:set>
							</c:forEach>
						</table>
						<div class="pages">
							<span class="page_active">1</span>
						</div>
					</c:if>
					<c:if test="${fn:length(staffs) > 20}">
						<c:forEach begin="0" end="${num_table}" var="table">
							<table class="books">
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Email</th>
									<th>Password</th>
									<th>Role</th>
									<th>Avatar</th>
									<th>Action</th>
								</tr>
								<c:forEach items="${staffs}" var="book" begin="0" end="19">
									<c:if test="${staffs[num_book]!=null}">
										<tr id="${staffs[num_book].id.id}">
											<td>${staffs[num_book].id.id}</td>
											<td>${staffs[num_book].name}</td>
											<td>${staffs[num_book].email}</td>
											<td>${staffs[num_book].password}</td>
											<td>${staffs[num_book].role}</td>
											<td><img
												src="<c:url value='${staffs[num_book].avatar}'></c:url>"></td>
											<td>
												<button value="${staffs[num_book].id.id}"
													data-table="${table}" class="removeBtn"
													title="Remove staff">
													<i class="far fa-trash-alt"></i>
												</button>
												<button value="${staffs[num_book].id.id}"
													data-table="${table}" class="modifyBtn"
													title="Modify staff">
													<i class="far fa-edit"></i>
												</button> <c:if test="${staffs[num_book].role == 'staff'}">
													<button value="${staffs[num_book].id.id},${user.id.id}"
														data-table="${table}" title="Set as a manager"
														class="setManagerBtn">
														<i class="fas fa-exchange-alt"></i>
													</button>
												</c:if> <c:if test="${staffs[num_book].role != 'staff'}">
													<button value="${staffs[num_book].id.id}"
														data-table="${table}" title="Set as a manager"
														class="setManagerBtn" hidden="hidden">
														<i class="fas fa-exchange-alt"></i>
													</button>
												</c:if>
											</td>
										</tr>
									</c:if>
									<c:set var="num_book" value="${num_book+1}"></c:set>
								</c:forEach>
							</table>
							<c:set var="num_page" value="${num_page+1}"></c:set>
						</c:forEach>
					</c:if>
					<span class="previous"><i class="fas fa-chevron-left"></i></span> <span
						class="next"><i class="fas fa-chevron-right"></i></span>
					<div class="pages">
						<c:forEach var="page" begin="1" end="${num_page}">
							<span>${page}</span>
						</c:forEach>
					</div>
				</div>
				<div class="edit-form"></div>
				<iframe name="hidden" style="width: 0; height: 0"></iframe>
				<!--  addform-->
			</div>
			<div class="searchContainer">
				<div class="books_container">
					<!-- <span class="previous"><i class="fas fa-chevron-left"></i></span> <span
						class="next"><i class="fas fa-chevron-right"></i></span> -->
					<%-- <table class="books">
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Email</th>
							<th>Password</th>
							<th>Avatar</th>
							<th>Action</th>
						</tr>
						#records
					</table> --%>

					<!-- <div class="pages">
						#pages
					</div> -->
				</div>
			</div>
			<c:if test="${status == 1}">
				<div class="addSuccessful">
					<h2 class="big_title">Successful staff adding</h2>
				</div>
			</c:if>
			<c:if test="${status == 2}">
				<div class="addSuccessful">
					<h2 class="big_title">Failure staff adding</h2>
				</div>
			</c:if>
			<c:if test="${status == 11}">
				<div class="addSuccessful">
					<h2 class="big_title">Successful staff updating</h2>
				</div>
			</c:if>
			<c:if test="${status == 22}">
				<div class="addSuccessful">
					<h2 class="big_title">Failure staff updating</h2>
				</div>
			</c:if>
			<div class="setManangerSuccessfull">
				<h2 class="big_title">
					You now are not an manager, you will be redirected to login page
					after <span></span><span>s</span>
				</h2>
				<h3>
					Or you can click on <a href="../loginController/login">this</a> in
					order to immediately redirect to the login page.
				</h3>
			</div>

			<div class="brokenContainer">
				<h1>Broken level</h1>
				<c:forEach items="${percentages}" var="percentage">
					<div class="broken">
						<div class="brokenInfo">
							<p class="brokenId">${percentage.id}</p>
							<div>
								<label for="brokenLevel${percentage.id}">Broken level</label> <input
									type="text" class="brokenLevel"
									value="${percentage.percentage}"
									id="brokenLevel${percentage.id}">
							</div>
							<div>
								<label for="brokenFine${percentage.id}">Broken Fine</label> <input
									type="text" class="brokenLevel" value="${percentage.fine}"
									id="brokenFine${percentage.id}">
							</div>
						</div>
						<div class="brokenButton">
							<button class="removeBrokenBtn" value="${percentage.id}">
								<i class="far fa-trash-alt"></i>
							</button>
							<button class="modifyBrokenBtn" value="${percentage.id}">
								<i class="far fa-edit"></i>
							</button>
							<button class="undoBtn" value="${percentage.id}">
								<i class="fas fa-undo"></i>
							</button>
						</div>
					</div>
				</c:forEach>
				<div class="addBrokenContainer">
					<div>
						<label for="brokenLevelAdd" class="brokenLevelAdd">Broken
							level</label> <input type="text" class="brokenLevel" value=""
							placeholder="Enter broken level" id="brokenLevelAdd">
					</div>
					<div>
						<label for="brokenFineAdd" class="brokenFineAdd">Broken
							fine</label> <input type="text" class="brokenLevel" value=""
							placeholder="Enter broken fine" id="brokenFineAdd">
					</div>
					<button class="addBrokenBtn">Add</button>
				</div>
			</div>

		</c:if>
		<c:if test="${user.role != 'manager'}">
			<div class="background">
				<img alt="" src="/Library_management_system/resources/image/iu.png">
			</div>
			${user.role != 'manager'}
			<div class="warningAccess warning">
				<h2 class="big_title">You are not allowed to access to this
					page</h2>
				<h3>
					You will be redirected to the home page after <span>#time</span>s.
				</h3>
				<h3>
					Or you can click on <a href="../homeController/display">this</a> in
					order to immediately redirect to the home page.
				</h3>
			</div>
		</c:if>
	</c:if>

	<c:if test="${user == null}">
		<div class="background">
			<img alt="" src="/Library_management_system/resources/image/iu.png">
		</div>
		<div class="warning">
			<h2 class="big_title">You must login first.</h2>
			<h3>
				You will be redirected to the login page after <span>#time</span>s.
			</h3>
			<h3>
				Or you can click on <a href="../loginController/login">this</a> in
				order to immediately redirect to the login page.
			</h3>
		</div>
	</c:if>
	<div class="add_form">
		<form:form method="POST" modelAttribute="staff"
			enctype="multipart/form-data" action="./staff" accept-charset="utf-8">
			<h2>
				<span>Add a new staff</span> <i class="far fa-times-circle"></i>
			</h2>
			<div class="id_container">
				<form:label path="id.id" for="id">Staff id</form:label>
				<form:input path="id.id" name="id" class="form_input"
					placeholder="Enter staff id..."></form:input>
			</div>
			<div class="name_container">
				<form:label path="name" for="name">Staff name</form:label>
				<form:input path="name" name="name" class="form_input"
					placeholder="Enter staff name..." accept-charset="utf-8"></form:input>
			</div>
			<div class="email_container">
				<form:label path="email" for="email">Staff email</form:label>
				<form:input path="email" name="email" class="form_input"
					placeholder="Enter staff email..."></form:input>
			</div>
			<div class="password_container">
				<form:label path="password" for="password">Staff password</form:label>
				<form:input path="password" name="password" class="form_input"
					placeholder="Will be generated after email is enter..."
					readonly="true"></form:input>
			</div>
			<div class="password_container">
				<form:label path="role" for="role">Role</form:label>
				<form:input path="role" name="role" class="form_input"
					placeholder="" value="staff" readonly="true"></form:input>
			</div>
			<div class="cover_image_container">
				<label for="">Avatar</label> <img alt=""
					style="display: none; width: 100px; float: right;"
					src='<c:url value=""></c:url>'>
				<form:input path="myfile.multipartFile" type="file" />
				<div style="content: ''; clear: both;"></div>
			</div>
			<!-- <button name="type" class="btnHidden" hidden="hidden"></button> -->
			<div>
				<button type="submit" onclick="" data-table="" value="" name="type"
					class="btnSub">Add</button>
				<p class="notify"></p>
			</div>
		</form:form>
	</div>
</body>
</html>