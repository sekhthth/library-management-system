<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/home.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/home.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/nav.js"></c:url>"></script>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/fontawesome/all.min.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.min.js"></c:url>"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body onload="">
	<div class="background">
		<img alt="" src="/Library_management_system/resources/image/iu.png">
	</div>
	<c:if test="${user != null && user.role == 'staff'}">
		<div class="feature">
			<button type="button" class="featureBtn featureBtnActive">Borrow</button>
			<button type="button" class="featureBtn">Return</button>
			<button type="button" class="featureBtn">Take</button>
			<button type="button" class="featureBtn">Reset</button>
		</div>
		<!-- <div class="background">
			<img alt="" src="/Library_management_system/resources/image/iu.png">
		</div> -->
		<nav>
			<span class="closeNavBtn">X</span>
			<ul>
				<li><a class="active">Home</a></li>
				<li><a href="../categoryController/categories">Category</a></li>
				<li><a href="../bookController/books">Book</a></li>
				<li><button class="makeReportBtn">Make report</button></li>
				<li><button class="profileBtn">Profile</button></li>
				<li><button class="logoutBtn">Logout</button></li>
			</ul>
		</nav>
		<div class="toggle">
			<p>Menu</p>
		</div>
		<div class="black"></div>
		<div class="black2"></div>
		<div class="content">
			<!-- <div class="feature">
			<button type="button" class="featureBtn">Return</button>
			<button type="button" class="featureBtn">Reset</button>
			<button type="button" class="featureBtn">Take</button>
		</div> -->
			<div class="studentBookInfoContainer">
				<div class="staffInfo">
					<div class="studentResult" id="${user.id.id}">
						<div class="studentResultLeft">
							<h3 class="studentResultLeftId">${user.id.id}</h3>
							<h3 class="studentResultLeftName">${user.name}</h3>
							<h3 class="studentResultLeftEmail">${user.email}</h3>
						</div>
						<div class="studentResultRight">
							<img alt="" src='<c:url value="${user.avatar}"></c:url>'>
						</div>
					</div>
				</div>
				<div class="studentInfo">
					<div class="student">
						<label>Student ID</label> <input type="text"
							placeholder="Enter student id" class="studentId" id="studentId">
					</div>
					<div class="studentResultsContainer">
						<div class="studentResults">
							<%-- <div class="studentResult">
							<div class="studentResultLeft">
								<h3 class="studentResultLeftId">#id</h3>
								<h3 class="studentResultLeftName">#name</h3>
								<h3 class="studentResultLeftEmail">#mail</h3>
								<h3 class="studentResultLeftPhone">#phone</h3>
								<h3 class="studentResultLeftSex">#Sex</h3>
								<h3 class="studentResultLeftDob">#dob</h3>
							</div>
							<div class="studentResultRight">
								<img alt=""
									src='<c:url value="/resources/image/test.jpg"></c:url>'>
							</div>
						</div> --%>
						</div>
					</div>
				</div>
				<div class="bookInfo">
					<div class="book">
						<label>Book ID</label> <input type="text"
							placeholder="Enter book id" class="bookId" id="bookId">
					</div>
					<div class="book_status"></div>
					<div class="bookResultsContainer">
						<div class="bookResults">
							<%-- <div class="bookResult">
							<div class="bookResultLeft">
								<h3 class="bookResultLeftId"></h3>
								<h3 class="bookResultLeftName"></h3>
								<h3 class="bokkResultTimePrint"></h3>
								<h3 class="bokkResultYManufacture"></h3>
								<h3 class="bokkResultManufacturer"></h3>
								<h3 class="bokkResultCategory"></h3>
								<h3 class="bokkResultIsBorrowed"></h3>
							</div>
							<div class="bookResultRight">
								<img alt=""
									src="<c:url value="/resources/image/test.jpg"></c:url>">
							</div>
						</div> --%>
						</div>
					</div>
				</div>
				<div class="returnInfo returnFeature">
					<div class="book">
						<label>Borrow card ID</label> <input
							placeholder="Enter borrow card id" class="bookId" id="bookId"
							type="text" data-toggle="tooltip" data-placement="top"
							title="Tooltip on top">
					</div>
					<div class="search">
						<button type="button">Search</button>
					</div>
				</div>
				<div class="takeInfo returnFeature">
					<div class="book">
						<label>Reservation card ID</label> <input
							placeholder="Enter reservation card id" class="bookId"
							id="bookId" type="text" data-toggle="tooltip"
							data-placement="top" title="Tooltip on top">
					</div>
					<div class="search">
						<button type="button">Search</button>
					</div>
				</div>
				<div class="studentBorrow"></div>
				<div class="listBooksContainer">
					<h2>List book</h2>
					<div class="listBooks">
						<%-- <div class="bookResult">
						<div class="bookResultLeft">
							<h3 class="bookResultLeftId">ITITIU17038</h3>
							<h3 class="bookResultLeftName">Calculus 3</h3>
						</div>
						<div class="bookResultRight">
							<img alt=""
								src="<c:url value="/resources/image/test.jpg"></c:url>">
							<div class="penalties">
								<div>
									<label for="penalty#order">1. Normal</label> <input name="penalty"
										value="1" type="radio">
								</div>
								<div>
									<label for="penalty#order">2. Late</label> <input name="penalty" value="1"
										type="radio">
								</div>
								<div>
									<label for="penalty#order">3. Miss</label> <input name="penalty" value="3"
										type="radio">
								</div>
								<div>
									<label for="penalty#order">4. Other</label> <input name="penalty"
										value="4" type="radio">
								</div>
							</div>
						</div>
					</div> --%>
						<%-- <div class="bookResult">
						<div class="bookResultLeft">
							<h3 class="bookResultLeftId">ITITIU17038</h3>
							<h3 class="bookResultLeftName">Calculus 3</h3>
						</div>
						<div class="bookResultRight">
							<img alt=""
								src="<c:url value="/resources/image/test.jpg"></c:url>">
						</div>
					</div> --%>
					</div>
					<div class="generatingOrReturning">
						<button class="featureBtn checkOutBtn" onclick="">Check
							out</button>
						<button class="featureBtn checkInBtn returnFeature">Check
							in</button>
						<button class="featureBtn reservateBtn returnFeature">Reservate</button>
						<button class="featureBtn takeBtn returnFeature">Take</button>
					</div>
				</div>
			</div>
		</div>

		<div class="reportContainer">
			<h2 class="big_title">Report</h2>
			<h4 class="report_date">
				Report date : <span></span>
			</h4>
			<div class="borrow">
				<h5 class="small_title">Borrow Record(s)</h5>
				<table>
					<!-- <tr>
                    <th>No.</th>
                    <th>Borrower id</th>
                    <th>Borrower name</th>
                    <th>Borrow date</th>
                    <th>Return date</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>#borrow_id</td>
                    <td>#borrower_id</td>
                    <td>#borrower_name</td>
                    <td>#borrow_date</td>
                    <td>#return_date</td>
                    <td><button id="#borrow_id">More</button></td>
                </tr> -->
				</table>
			</div>
			<div class="reservate">
				<h5 class="small_title">Reservate Record(s)</h5>
				<table>
					<!-- <tr>
                    <th>No.</th>
                    <th>Reservation person id</th>
                    <th>Reservation person name</th>
                    <th>Reservation date</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>#reservation_id</td>
                    <td>#reservation_person_id</td>
                    <td>#reservation_person_name</td>
                    <td>#reservation_date</td>
                    <td><button id="#reservation_id">More</button></td>
                </tr> -->
				</table>
			</div>
			<div class="return">
				<h5 class="small_title">Return Record(s)</h5>
				<table>
					<!-- <tr>
                    <th>No.</th>
                    <th>Borrower id</th>
                    <th>Borrower name</th>
                    <th>Borrow date</th>
                    <th>Return date</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>#borrow_id</td>
                    <td>#borrower_id</td>
                    <td>#borrower_name</td>
                    <td>#borrow_date</td>
                    <td>#return_date</td>
                    <td><button id="#borrow_id">More</button></td>
                </tr> -->
				</table>
			</div>
			<button class="sendReport">Send To Manager</button>
		</div>

		<div class="moreContainer">
			<h2 class="big_title">Borrowed Book(s)</h2>
			<table>
			</table>
		</div>

		<div class="returnCard">
			<h2 class="big_title">Return Card</h2>
			<div class="borrower">
				<!-- <div>
				<h4>Borrower id :</h4>
				<h4>#borrower_id</h4>
			</div>
			<div>
				<h4>Borrower name :</h4>
				<h4>#borrower_name</h4>
			</div> -->
			</div>
			<div class="borrow_log">
				<!-- <div>
				<h4>Borrow id :</h4>
				<h4>#borrow_id</h4>
			</div>
			<div>
				<h4>Borrow date :</h4>
				<h4>#borrow_date</h4>
			</div>
			<div>
				<h4>Return date :</h4>
				<h4>#return_date</h4>
			</div> -->
			</div>
			<div class="staff">
				<!-- <div>
				<h4>Staff id :</h4>
				<h4>#staff_id</h4>
			</div>
			<div>
				<h4>Staff name :</h4>
				<h4>#staff_name</h4>
			</div> -->
			</div>
			<div class="books">
				<!-- <h2 class="small_title">List book</h2>
			<p>#book</p> -->
			</div>
			<h1 class="small_title status"></h1>
		</div>

		<div class="borrowCard">
			<h2 class="big_title">Borrow Card</h2>
			<div class="borrower"></div>
			<div class="borrow_log"></div>
			<div class="staff"></div>
			<div class="books"></div>
		</div>

		<div class="reservateCard">
			<h2 class="big_title">Reservation Card</h2>
			<div class="borrower">
				<!-- <div>
				<h4>Reservation person id :</h4>
				<h4>#borrower_id</h4>
			</div>
			<div>
				<h4>Reservation person name :</h4>
				<h4>#borrower_name</h4>
			</div> -->
			</div>
			<div class="borrow_log">
				<!-- <div>
				<h4>Reservation id :</h4>
				<h4>#borrow_id</h4>
			</div>
			<div>
				<h4>Reservation date :</h4>
				<h4>#borrow_date</h4>
			</div> -->
			</div>
			<div class="staff">
				<!-- <div>
				<h4>Staff id :</h4>
				<h4>#staff_id</h4>
			</div>
			<div>
				<h4>Staff name :</h4>
				<h4>#staff_name</h4>
			</div> -->
			</div>
			<div class="books">
				<h2 class="small_title">List book</h2>
				<p>#book</p>
			</div>
		</div>

		<div class="punishmentCard">
			<h2 class="big_title">You are being punished.</h2>
			<div class="penalties">
				<h5 class="small_title">Penalties</h5>
				<!-- <div class="penalty_record">
               
            </div> -->
			</div>
		</div>

		<div class="otherContainer">
			<h2 class="big_title">Broken Level</h2>
			<button value="" hidden="hidden" id="percentageof"></button>
			<c:forEach items="${percentages}" var="percentage" varStatus="index">
				<div class="percentage${percentage.id}">
					<label for="percentage${percentage.id}">${percentage.percentage}% - <span class="fineOther">${percentage.fine}</span>% of <span class="priceOther"></span>VND = <span class="totalOther"></span>VND</label>
					<input type="radio" name="percentage"
						id="percentage${percentage.id}" value="${percentage.id}">
				</div>
			</c:forEach>
		</div>
		<div class="staffProfile">
			<h2 class="big_title">Staff Profile</h2>
			<form:form method="POST" modelAttribute="staff"
				enctype="multipart/form-data" action="../staffController/ustaff"
				accept-charset="utf-8">
				<div class="id_container parentContainer">
					<form:label path="id.id" for="id">Staff id</form:label>
					<form:input path="id.id" name="id" class="form_input"
						placeholder="Enter staff id..." readonly="true"></form:input>
				</div>
				<div class="name_container parentContainer">
					<form:label path="name" for="name">Staff name</form:label>
					<div>
						<form:input path="name" name="name" class="form_input"
							placeholder="Enter staff name..." accept-charset="utf-8"></form:input>
						<button class="nameResetBtn profileResetBtn" type="button">
							<i class="fas fa-undo"></i>
						</button>
					</div>
				</div>
				<div class="email_container parentContainer">
					<form:label path="email" for="email">Staff email</form:label>
					<div>
						<form:input path="email" name="email" class="form_input"
							placeholder="Enter staff email..."></form:input>
						<button class="emailResetBtn profileResetBtn" type="button">
							<i class="fas fa-undo"></i>
						</button>
					</div>
				</div>
				<div class="password_container parentContainer">
					<form:label path="password" for="password">Staff password<i class="fas fa-eye-slash seePassBtn"></i></form:label>
					<div>
						<form:input path="password" name="password" class="form_input"
							placeholder="Will be generated after email is enter..." type="password"></form:input>
						<button class="passwordResetBtn profileResetBtn" type="button">
							<i class="fas fa-undo"></i>
						</button>
					</div>
				</div>
				<div class="password_container parentContainer">
					<form:label path="role" for="role">Role</form:label>
					<form:input path="role" name="role" class="form_input"
						placeholder="" value="staff" readonly="true"></form:input>
				</div>
				<div class="cover_image_container parentContainer">
					<label for="">Avatar</label>
					<div>
						<form:input path="myfile.multipartFile" type="file" />
						<button class="imgResetBtn profileResetBtn" type="button">
							<i class="fas fa-undo"></i>
						</button>
					</div>
				</div>
				<div class="previewImage parentContainer">
					<img alt="" style="width: 100px; float: right;"
						src='<c:url value=""></c:url>'>
					<form:input path="avatar" hidden="true" id="avatarHidden" />
				</div>
				<div class="parentContainer">
					<button type="submit" onclick="" data-table="" value="" name="type"
						class="btnSub">UPDATE</button>
					<p class="notify"></p>
				</div>
			</form:form>
		</div>
	</c:if>
	<c:if test="${user == null || user.role != 'staff'}">
		<div class="warning">
			<h2 class="big_title">You must login first.</h2>
			<h3>
				You will be redirected to the login page after <span>#time</span>s.
			</h3>
			<h3>
				Or you can click on <a href="../loginController/login">this</a> in
				order to immediately redirect to the login page.
			</h3>
		</div>
	</c:if>
</body>
</html>