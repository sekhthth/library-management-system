<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/login.css"></c:url>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/login.js"></c:url>"></script>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/fontawesome/all.min.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.min.js"></c:url>"></script>
</head>
<body style="position: reletive;">
	<div class="black2"></div>
	<div class="background">
		<img alt="" src="/Library_management_system/resources/image/iu.png">
	</div>
	<div style="position: absolute; z-index: 2;">
		<h1>IU Library Management System</h1>
		<div class="main">
			<h2>LOGIN</h2>
			<div>
				<label for="id">Your ID : </label><input type="text" name="id"
					class="id" placeholder="Your Satff/Manager ID">
			</div>
			<div>
				<label for="password">Your password : </label><input type="password"
					class="password" name="password" placeholder="Your password">
			</div>
			<button type="button" class="loginBtn">LOGIN</button>
			<a href="#">Forgot password?</a>
			<p>${notify}</p>
		</div>
	</div>
	<div class="forgetPassContainer">
		<h2 class="big_title">Reset password <span class="clearBtn">Clear</span> </h2>
		<div class="searchAccount divParent">
			<input type="text" value=""
				placeholder="Input your mail to find your account">
			<button class="searchBtn">Find</button>
		</div>
		<div class="account divParent">
		</div>
		<button class="sendPassCodeBtn">Send passcode</button>
		<div class="verifyPasscode divParent">
			<p>
				<span style="display: block;">We sent a passcode(contain 6 digits) to you.</span>
				<span style="display: block;">Please check your mail.</span>
				You have <span id="minute"></span><span>:</span><span id="second"></span><span>s</span> to enter passcode.
			</p>
			<div class="checkPasscode">
				<input type="text" value=""
					placeholder="Enter received passcode here (contain 6 digits)">
				<button class="checkPasscodeBtn">Check</button>
			</div>
		</div>
		<div class="createNewPassword divParent">
			<p class="passwordP">Enter new password</p>
			<input type="password" value="" placeholder="New password">
			<p class="passwordP">Confirm new password</p>
			<input type="password" value="" placeholder="Confirm new password">
			<button class="changePassword">Change</button>
		</div>
	</div>
	    <div class="resetPasswordSuccessfully">
        <h1>RESET PASSWORD SUCCCESSFULLY.</h1>
        <h2>We sent you a mail. Please check it.</h2>
    </div>
	<!-- position:absolute;z-index:1;width:100%;height:100% -->
</body>
</html>