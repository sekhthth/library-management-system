<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/book.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/book.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/nav.js"></c:url>"></script>
<link rel="stylesheet" type="text/css"
	href='<c:url value="/resources/css/fontawesome/all.min.css"/>'>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.js"></c:url>"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery/jquery-3.4.1.min.js"></c:url>"></script>
<link href="https://fonts.googleapis.com/css?family=Monda&display=swap"
	rel="stylesheet">
</head>
<body>
	<c:if test="${user != null && user.role == 'staff'}">
		<nav class="">
			<span class="closeNavBtn">X</span>
			<ul>
				<li><a href="../homeController/display">Home</a></li>
				<li><a href="../categoryController/categories">Category</a></li>
				<li><a class="active">Book</a></li>
				<li><button class="logoutBtn">Logout</button></li>
			</ul>
		</nav>
		<div class="toggle">
			<p>Menu</p>
		</div>
		<div class="black"></div>
		<h1 class="title">Manage Book</h1>
		<div class="searchArea">
			<span>Search</span>
			<div class="rigth">
				<div class="rigth1">
					<div>
						<label for="id">ID</label> <input type="radio" value="ID"
							name="type" id="id" checked="checked">
					</div>
					<div>
						<label for="name">Name</label> <input type="radio" value="name"
							name="type" id="name">
					</div>
				</div>
				<div class="rigth2">
					<input type="text" placeholder="Search">
				</div>
			</div>
		</div>
		<div class="button_add_container">
			<button class="button_add" type="button">
				<i class="far fa-plus-square"></i>
			</button>
		</div>
		<div class="content">
			<div class="books_container">
				<c:set var="num_book" value="0"></c:set>
				<c:set var="num_page" value="0"></c:set>
				<c:if test="${fn:length(books) <= 20}">
					<table class="books">
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Category</th>
							<th>Borrowed</th>
							<th>Quantity</th>
							<th>Edition</th>
							<th>Year manufacture</th>
							<th>Manufacturer</th>
							<th>Price</th>
							<th>Cover image</th>
							<th>Action</th>
						</tr>
						<c:forEach items="${books}" var="book" begin="0"
							end="${fn:length(books)-1}">
							<tr id="${books[num_book].id.id}">
								<td>${books[num_book].id.id}</td>
								<td>${books[num_book].name}</td>
								<td>${books[num_book].category_id.name}</td>
								<td>${books[num_book].borrowed}</td>
								<td>${books[num_book].quantity}</td>
								<td>${books[num_book].timePrint}</td>
								<td>${books[num_book].ymanufacture}</td>
								<td>${books[num_book].manufacturer}</td>
								<td>${books[num_book].price}</td>
								<td><img
									src="<c:url value='${books[num_book].cover_image}'></c:url>"></td>
								<td>
									<button value="${books[num_book].id.id}" data-table="${table}"
										class="removeBtn" title="Remove book" class="removeBtn">
										<i class="far fa-trash-alt"></i>
									</button>
									<button value="${books[num_book].id.id}" data-table="${table}"
										class="modifyBtn" title="Modify book" class="removeBtn">
										<i class="far fa-edit"></i>
									</button>
								</td>
							</tr>
							<c:set var="num_book" value="${num_book+1}"></c:set>
						</c:forEach>
					</table>
					<div class="pages">
						<span class="page_active">1</span>
					</div>
				</c:if>
				<c:if test="${fn:length(books) > 20}">
					<c:forEach begin="0" end="${num_table}" var="table">
						<table class="books">
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Category</th>
								<th>Borrowed</th>
								<th>Quantity</th>
								<th>Edition</th>
								<th>Year manufacture</th>
								<th>Manufacturer</th>
								<th>Price</th>
								<th>Cover image</th>
								<th>Action</th>
							</tr>
							<c:forEach items="${books}" var="book" begin="0" end="19">
								<c:if test="${books[num_book]!=null}">
									<tr id="${books[num_book].id.id}">
										<td>${books[num_book].id.id}</td>
										<td>${books[num_book].name}</td>
										<td>${books[num_book].category_id.name}</td>
										<td>${books[num_book].borrowed}</td>
										<td>${books[num_book].quantity}</td>
										<td>${books[num_book].timePrint}</td>
										<td>${books[num_book].ymanufacture}</td>
										<td>${books[num_book].manufacturer}</td>
										<td>${books[num_book].price}</td>
										<td><img
											src="<c:url value='${books[num_book].cover_image}'></c:url>"></td>
										<td>
											<button value="${books[num_book].id.id}"
												data-table="${table}" class="removeBtn" title="Remove book">
												<i class="far fa-trash-alt"></i>
											</button>
											<button value="${books[num_book].id.id}"
												data-table="${table}" class="modifyBtn" title="Modify book">
												<i class="far fa-edit"></i>
											</button>
										</td>
									</tr>
								</c:if>
								<c:set var="num_book" value="${num_book+1}"></c:set>
							</c:forEach>
						</table>
						<c:set var="num_page" value="${num_page+1}"></c:set>
					</c:forEach>
				</c:if>
				<span class="previous"><i class="fas fa-chevron-left"></i></span> <span
					class="next"><i class="fas fa-chevron-right"></i></span>
				<div class="pages">
					<c:forEach var="page" begin="1" end="${num_page}">
						<span>${page}</span>
					</c:forEach>
				</div>
			</div>
			<div class="edit-form"></div>
			<iframe name="hidden" style="width: 0; height: 0"></iframe>
		</div>
		<div class="searchContainer">
			<div class="books_container">
				<!-- <span class="previous"><i class="fas fa-chevron-left"></i></span> <span
						class="next"><i class="fas fa-chevron-right"></i></span> -->
				<!-- 					<table class="books">
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Category</th>
							<th>Borrowed</th>
							<th>Quantity</th>
							<th>Time print</th>
							<th>Year manufacture</th>
							<th>Manufacturer</th>
							<th>Cover image</th>
							<th>Action</th>
						</tr>
						#records
					</table> -->
				<!-- <div class="pages">
						#pages
					</div> -->
			</div>
		</div>
		<c:if test="${status == 1}">
			<div class="addSuccessful">
				<h2 class="big_title">Successful category adding</h2>
			</div>
		</c:if>
		<c:if test="${status == 2}">
			<div class="addSuccessful">
				<h2 class="big_title">Failure category adding</h2>
			</div>
		</c:if>
		<c:if test="${status == 11}">
			<div class="addSuccessful">
				<h2 class="big_title">Successful category updating</h2>
			</div>
		</c:if>
		<c:if test="${status == 22}">
			<div class="addSuccessful">
				<h2 class="big_title">Failure category updating</h2>
			</div>
		</c:if>
	</c:if>
	<c:if test="${user == null || user.role != 'staff'}">
		<div class="background">
			<img alt="" src="/Library_management_system/resources/image/iu.png">
		</div>
		<div class="warning">
			<h2 class="big_title">You must login first.</h2>
			<h3>
				You will be redirected to the login page after <span>#time</span>s.
			</h3>
			<h3>
				Or you can click on <a href="../loginController/login">this</a> in
				order to immediately redirect to the login page.
			</h3>
		</div>
	</c:if>
	<div class="add_form">
		<form:form method="POST" modelAttribute="book"
			enctype="multipart/form-data" action="./book">
			<h2>
				<span>Add a new book</span> <i class="far fa-times-circle"></i>
			</h2>
			<div class="id_container">
				<form:label path="id.id" for="id">Book id</form:label>
				<form:input path="id.id" name="id" class="form_input"
					placeholder="Enter book id..."></form:input>
			</div>
			<div class="name_container">
				<form:label path="name" for="name">Book name</form:label>
				<form:input path="name" name="name" class="form_input"
					placeholder="Enter book name..."></form:input>
			</div>
			<div class="category_container">
				<form:label path="category_id.id.id" for="category">Book Category</form:label>
				<form:select path="category_id.id.id" cssClass="categories">
					<form:options items="${categories}" itemValue="id.id"
						itemLabel="name" path="category_id.id.id" />
				</form:select>
			</div>
			<div class="quantity_container">
				<form:label path="quantity" for="quantiy">Book quantity</form:label>
				<form:input path="quantity" name="quantity" class="form_input"
					placeholder="Enter book quantity..." type="number" min="1" max="10"></form:input>
			</div>
			<div class="edition_container">
				<form:label path="timePrint" for="timePrint">Book edition</form:label>
				<form:input path="timePrint" name="timePrint" class="form_input"
					placeholder="Enter book quantity..." type="number" min="1"
					max="1000"></form:input>
			</div>
			<div class="ymanufacture_container">
				<form:label path="ymanufacture" for="ymanufacture">Book year-manufacture</form:label>
				<form:input path="ymanufacture" name="ymanufacture"
					class="form_input" placeholder="Enter book year-manufacture..."
					type="number" min="1800" max="2019"></form:input>
			</div>
			<div class="manufacturer_container">
				<form:label path="manufacturer" for="manufacturer">Book manufacturer</form:label>
				<form:input path="manufacturer" name="manufacturer"
					class="form_input" placeholder="Enter book manufacturer..."></form:input>
			</div>
			<div class="manufacturer_container">
				<form:label path="price" for="price">Book price</form:label>
				<form:input path="price" name="price"
					class="form_input" placeholder="Enter book price..." type="number" min="0" max="1000000000"></form:input>
			</div>
			<div class="cover_image_container">
				<label for="">Book cover image</label> <img alt=""
					style="display: none; width: 100px; float: right;"
					src='<c:url value=""></c:url>'>
				<form:input path="myfile.multipartFile" type="file" />
				<div style="content: ''; clear: both;"></div>
			</div>
			<div>
				<button type="submit" onclick="" data-table="" value="" name="type"
					class="btnSub">Add</button>
				<p class="notify"></p>
			</div>
		</form:form>
	</div>
</body>
</html>