window.addEventListener("DOMContentLoaded", function() {
	// Pagination
	setTimeout(function() {
		var biggestHeight = 0;
		$(".books").each(function() {
			if ($(this).height() > biggestHeight) {
				biggestHeight = $(this).height() + $("div.pages").height();
			}
		});
		$(".books_container").height(biggestHeight + $("div.pages").height());
	}, 0);

	var tables = $(".content table");
	tables[0].classList.add("slide_active");
	for (let i = 1; i < tables.length; i++) {
		tables[i].classList.add("moveRight");
	}
	//
	
	//
	var indexTmp;
	
	$("form input").each(function(index) {
		$(this).keyup(function() {
			if ($(this).val().trim().length > 0 && index == indexTmp)
				$(".notify").text("");
		});
	});
	var t = 0;
	var con = false;
	var error = false;
	$("form").submit(function(e) {
		if (t == 0) {
			if ($(".btnSub").text() == "Add") {
				console.log(1);
				if ($($("form input")[0]).val().trim().length == 0) {
					$(".notify").text("Book ID cannot be empty!");
					$("form input")[0].focus();
					indexTmp = 0;
					e.preventDefault();
					return;
				} else {
					$.ajax({
						url: "./book?book_id=" +$($("form input")[0]).val().trim(),
						type: "GET",
						data: "",
						contentType: "application/json;charset=utf-8",
						success: function(data) {
							console.log(data);
							if (data.id != null) {
								indexTmp = 0;
								error = true;
							}
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					if (error) {
						$(".notify").text("Book ID already existed!");
						$("form input")[0].focus();
						e.preventDefault();
						return;
					} else {
						if ($($("form input")[1]).val().trim().length == 0) {
							$(".notify").text("Book name cannot be empty!");
							$("form input")[1].focus();
							indexTmp = 1;
							e.preventDefault();
							return;
						} else {
							if ($($("select")[0]).val() == null) {
								$(".notify").text("Book category must be selected!");
//								$("form input")[1].focus();
//								indexTmp = 1;
								e.preventDefault();
								return;
							} else {
								if ($($("form input")[2]).val().trim().length == 0) {
									$(".notify").text("Book quantity cannot be empty!");
									$("form input")[2].focus();
									indexTmp = 2;
									e.preventDefault();
									return;
								} else {
									if ($($("form input")[3]).val().trim().length == 0) {
										$(".notify").text("Book edition cannot be empty!");
										$("form input")[3].focus();
										indexTmp = 3;
										e.preventDefault();
										return;
									} else {
										if ($($("form input")[4]).val().trim().length == 0) {
											$(".notify").text("Book year manufacture cannot be empty!");
											$("form input")[4].focus();
											indexTmp = 4;
											e.preventDefault();
											return;
										} else {
											if ($($("form input")[5]).val().trim().length == 0) {
												$(".notify").text("Book manufacturer cannot be empty!");
												$("form input")[5].focus();
												indexTmp = 5;
												e.preventDefault();
												return;
											} else {
												if ($($("form input")[6]).val().trim().length == 0) {
													$(".notify").text("Book price cannot be empty!");
													indexTmp = 6;
													e.preventDefault();
													return;
												} else {
													if ($($("form input")[7]).val().length == 0) {
														$(".notify").text("Book cover image must be interted!");
														var ava = confirm("Upload later?");
														if (!ava)  {
															indexTmp = 4;
															e.preventDefault();
															return;
														} else {
															t = 1;
															$("form").submit();
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				console.log(2);
				if ($($("form input")[1]).val().trim().length == 0) {
					$(".notify").text("Book name cannot be empty!");
					$("form input")[1].focus();
					indexTmp = 1;
					e.preventDefault();
					return;
				} else {
					if ($($("select")[0]).val() == null) {
						$(".notify").text("Book category must be selected!");
//						$("form input")[1].focus();
//						indexTmp = 1;
						e.preventDefault();
						return;
					} else {
						if ($($("form input")[2]).val().trim().length == 0) {
							$(".notify").text("Book quantity cannot be empty!");
							$("form input")[2].focus();
							indexTmp = 2;
							e.preventDefault();
							return;
						} else {
							if ($($("form input")[3]).val().trim().length == 0) {
								$(".notify").text("Book edition cannot be empty!");
								$("form input")[3].focus();
								indexTmp = 3;
								e.preventDefault();
								return;
							} else {
								if ($($("form input")[4]).val().trim().length == 0) {
									$(".notify").text("Book year manufacture cannot be empty!");
									$("form input")[4].focus();
									indexTmp = 4;
									e.preventDefault();
									return;
								} else {
									if ($($("form input")[5]).val().trim().length == 0) {
										$(".notify").text("Book manufacturer cannot be empty!");
										$("form input")[5].focus();
										indexTmp = 5;
										e.preventDefault();
										return;
									} else {
										if ($($("form input")[6]).val().trim().length == 0) {
											$(".notify").text("Book price cannot be empty!");
											indexTmp = 6;
											e.preventDefault();
											return;
										} else {
											if ($($("form input")[7]).val().length == 0) {
												$(".notify").text("Book cover image must be interted!");
												var con = confirm("Do you want to keep the current cover image?");
												if (con) {
													t = 1;
													$("form").submit();
												}else {
													e.preventDefault();
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	});
	//

	// Add button clicked
	$(".button_add_container .button_add").click(function() {
		$(".add_form").addClass("appear");
		$(".black").addClass("blackAppear");
		//Change file input tag to img tag when remove record
		$(".add_form form .cover_image_container img").attr("src","");
		$(".add_form form .cover_image_container img").css("display", "inline-block");
		$(".add_form form .cover_image_container input").css("display", "inline-block");
		//
	});
	$(".black").click(function() {
		if ($(".add_form form .id_container input").attr("disabled") == "disabled"){
			$(".add_form form input").removeAttr("disabled");
			$(".add_form form select").removeAttr("disabled");
			
			//Change type of button to button instead of adding
			$(".add_form form button:first-child").text("Add");
			$(".add_form form button:first-child").attr("type", "submit");
			$(".add_form form button:first-child").val("");
			//
			
			//Change img tag to input tag in cover_image_container
			$(".add_form form .cover_image_container img").attr("src", "");
			$(".add_form form .cover_image_container img").css("display", "none");
			$(".add_form form .cover_image_container input").css("display", "block");
			//
			
			//Open warning window
			$(".add_form form h2 span").text("Add a new book").css("color", "");
			//
		}
		
		//Set all input tags and select tags
		$(".add_form form input").val("");
		$(".add_form form select").val("MAMA");
		//
		
		$(".add_form").removeClass("appear");
		$(this).removeClass("blackAppear");
	});
	//
	
	//Close add form button clicked
	$(".add_form form h2 i").click(function() {
		if ($(".add_form form .id_container input").attr("disabled") == "disabled"){
			$(".add_form form input").removeAttr("disabled");
			$(".add_form form select").removeAttr("disabled");
			
			//Change type of button to button instead of adding
			$(".add_form form button:first-child").text("Add");
			$(".add_form form button:first-child").attr("type", "submit");
			$(".add_form form button:first-child").val("");
			//
			
			//Change img tag to input tag in cover_image_container
			$(".add_form form .cover_image_container img").attr("src", "");
			$(".add_form form .cover_image_container img").css("display", "none");
			$(".add_form form .cover_image_container input").css("display", "block");
			//
			
			//Open warning window
			$(".add_form form h2 span").text("Add a new book").css("color", "");
			//
		}
		
		//Set all input tags and select tags
		$(".add_form form input").val("");
		$(".add_form form select").val("MAMA");
		//
		
		$(".add_form").removeClass("appear");
		$(".black").removeClass("blackAppear");
	});
	//

	// Slide show
	// Next button clicked
	$("span.next").click(function() {
		if ($(".books.slide_active").next().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))+1].classList.add("page_active");
			$(".books.slide_active").addClass("moveLeft");
			$(".books.slide_active.moveLeft").next().addClass("slide_active");
			$(".books.slide_active.moveLeft").next().removeClass("moveRight");
			$(".books.slide_active.moveLeft").removeClass("slide_active");
			
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		}
	});
	//
	// Previous button clicked
	$("span.previous").click(function() {
		if ($(".books.slide_active").prev().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))-1].classList.add("page_active");
			$(".books.slide_active").addClass("moveRight");
			$(".books.slide_active.moveRight").prev().addClass("slide_active");
			$(".books.slide_active.moveRight").prev().removeClass("moveLeft");
			$(".books.slide_active.moveRight").removeClass("slide_active");
			
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		}
	});
	//
	
	//Page number clicked
	var pages = $(".pages span");
	pages[0].classList.add("page_active");
	pages.each(function() {
		$(this).click(function() {
			var tmp = $(this);
			if (parseInt($(this).text()) <= parseInt($(".pages .page_active").text())){
				$(pages.get().reverse()).each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.add("moveRight");
				});
			} else {
				pages.each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.add("moveLeft");
				});
			} 
			
			$(this).addClass("page_active");
			tables[parseInt($(this).text())-1].classList.add("slide_active");
			tables[$(this).text()-1].classList.remove("moveLeft");
			tables[$(this).text()-1].classList.remove("moveRight");
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		});
	});
	//
	//
	
	//Delete book
	var deleteBtns = $(".books tr td button.removeBtn");
	deleteBtns.each(function() {
		$(this).click(function() {
			var deleteBtn = $(this);
			$.ajax({
				url: "../bookController/book?book_id=" + $(this).val(),
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function (data) {
					//Change type of button to button instead of adding
					$(".add_form form button:first-child").text("Remove?");
					$(".add_form form button:first-child").attr("type", "button");
					$(".add_form form button:first-child").val(data.id.id);
					$(".add_form form button:first-child").data("table", $(deleteBtn).data("table"));
					//
					
					//Disable input tag in add form
					$(".add_form form input").attr("disabled", "disabled");
					$(".add_form form select").attr("disabled", "disabled");
//					console.log(data);
					//
					
					//Set the values of the book will be deleted
					$(".add_form form .id_container input").val(data.id.id);
					$(".add_form form .name_container input").val(data.name);
					$(".add_form form .category_container select").val(data.myfile.description);
					$(".add_form form .quantity_container input").val(data.quantity);
					$(".add_form form .edition_container input").val(data.timePrint);
					$(".add_form form .ymanufacture_container input").val(data.ymanufacture);
					$($(".add_form form .manufacturer_container input")[0]).val(data.manufacturer);
					$($(".add_form form .manufacturer_container input")[1]).val(data.price);
					
					//Change file input tag to img tag when remove record
					$(".add_form form .cover_image_container img:nth-child(2)").attr("src","/Library_management_system" + data.cover_image);
					$(".add_form form .cover_image_container img:nth-child(2)").css("display", "inline-block");
					$(".add_form form .cover_image_container input").css("display", "none");
					//
					
					//
					
					//Open warning window
					$(".add_form form h2 span").text("Waring").css("color", "red");
					$(".add_form").addClass("appear");
					$(".black").addClass("blackAppear");
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		});
	});
	
	//Remove? or Save? button clicked
	$(".add_form form button:button").click(function() {
		var book;
		if ($(this).text() == "Remove?"){
			//Remove this record in the table
//			console.log(".books tbody tr#" + $(this).val());
//			console.log($(this).data("table"));
//			$(".books tbody")[$(this).data("table")].removeChild($(".books tbody tr#" + $(this).val())[0]);
			//
			
			//Remove this record in the database
			$.ajax({
				url: "./book?book_id=" + $(this).val(),
				type: "DELETE",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function(data) {
					alert("The book removed successfull!!!");
					
					$(".add_form").removeClass("appear");
					$(".black").removeClass("blackAppear");
					
					$(".add_form form input").removeAttr("disabled");
					$(".add_form form select").removeAttr("disabled");
					
					//Change img tag to input tag in cover_image_container
					$(".add_form form .cover_image_container img").attr("src", "");
					$(".add_form form .cover_image_container img").css("display", "none");
					$(".add_form form .cover_image_container input").css("display", "block");
					//
					
					//Open warning window
					$(".add_form form h2 span").text("Add a new book").css("color", "");
					//
					
					//Set all input tags and select tags
					$(".add_form form input").val("");
					$(".add_form form select").val("MAMA");
					//
					
					setTimeout(function(){
						//Change type of button to button instead of adding
						$(".add_form form button:first-child").text("Add");
						$(".add_form form button:first-child").attr("type", "submit");
						$(".add_form form button:first-child").val("");
						//
					}, 500);
					location.href="./books";
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
			//
		}else {
			
		}
	});
	//
	
	//Edit button clicked
	var editBtns = $(".books tr td button:last-child");
	editBtns.each(function() {
		$(this).click(function(){
			var editBtn = $(this);
			var book = {
					"id" : {"id" : 1},
					"name" : 1,
					"cover_image" : 1,
					"timePrint" : 1,
					"ymanufacture" : 1,
					"manufacturer" : 1,
					"quantity" : 1,
					"borrowed" : 1,
					"category_id" : {"id" : {"id" : 1, "name" : 1}},
					"myfile" : {"description" : 1, "multipartFile" : null}
			};
			
			console.log(book);
			
			$.ajax({
				url: "../bookController/book?book_id=" + $(this).val(),
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function (data) {
					//Change type of button to button instead of adding
					$(".add_form form button:first-child").text("Save?");
//					$(".add_form form button:first-child").attr("type", "button");
					$(".add_form form button:first-child").val(data.id.id);
					$(".add_form form button:first-child").data("table", $(editBtn).data("table"));
					//
					
					//Disable input tag in add form
					$(".add_form form .id_container input").attr("disabled", "disabled");
//					$(".add_form form select").attr("disabled", "disabled");
//					console.log(data);
					//
					
					//Set the values of the book will be deleted
					$(".add_form form .id_container input").val(data.id.id);
					$(".add_form form .name_container input").val(data.name);
					$(".add_form form .category_container select").val(data.myfile.description);
					$(".add_form form .quantity_container input").val(data.quantity);
					$(".add_form form .edition_container input").val(data.timePrint);
					$(".add_form form .ymanufacture_container input").val(data.ymanufacture);
					$($(".add_form form .manufacturer_container input")[0]).val(data.manufacturer);
					$($(".add_form form .manufacturer_container input")[1]).val(data.price);
					
					//Change file input tag to img tag when remove record
					$(".add_form form .cover_image_container img").attr("src","/Library_management_system" + data.cover_image);
					$(".add_form form .cover_image_container img").css("display", "inline-block");
					//
					
					//
					
					//Open warning window
					$(".add_form form h2 span").text("Modify the book").css("color", "");
					$(".add_form").addClass("appear");
					$(".black").addClass("blackAppear");
					//
					
					//Change action of form input
					$(".add_form form").attr("action", "./ubook");
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		});
	});
	//
	
	//Preview image
	var input = document.querySelector('input[type=file]');

	input.addEventListener('change', updateImageDisplay);
	
	function updateImageDisplay() {

		  var curFiles = input.files;
		  if(curFiles.length === 0) {
		  } else {
		    for(var i = 0; i < curFiles.length; i++) {
		      if(validFileType(curFiles[i])) {
		        var image = $(".add_form form .cover_image_container img");
		        console.log(window.URL.createObjectURL(curFiles[i]));
		        image.attr("src",window.URL.createObjectURL(curFiles[i]));
		      } else {
		      }
		    }
		  }
		}
	
	var fileTypes = [
		  'image/jpeg',
		  'image/pjpeg',
		  'image/png'
		]

		function validFileType(file) {
		  for(var i = 0; i < fileTypes.length; i++) {
		    if(file.type === fileTypes[i]) {
		      return true;
		    }
		  }

		  return false;
		}
	
	function returnFileSize(number) {
		  if(number < 1024) {
		    return number + 'bytes';
		  } else if(number >= 1024 && number < 1048576) {
		    return (number/1024).toFixed(1) + 'KB';
		  } else if(number >= 1048576) {
		    return (number/1048576).toFixed(1) + 'MB';
		  }
		}
	//
	
	//
	if ($(".addSuccessful")[0] != undefined) {
		$(".addSuccessful").addClass("warningAppear");
		$(".black").addClass("blackAppear");
		$.ajax({
			url : "../bookController/resetstatus",
			type : "GET",
			data : "",
			contentType : "application/json",
			success : function(data) {
				console.log("Reset status successfully");
			}
		});
	}
	//
});