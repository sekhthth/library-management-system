window.addEventListener("DOMContentLoaded", function() {
	// Nav
	if (document.querySelector(".toggle") != null)
		document.querySelector(".toggle").addEventListener("click", function() {
			document.querySelector("nav").classList.add("navAppear");
			document.querySelector(".black").classList.add("blackAppear");
			this.classList.add("toggleMove");
		});
	if (document.querySelector(".closeNavBtn") != null)
		document.querySelector(".closeNavBtn").addEventListener(
				"click",
				function() {
					document.querySelector("nav").classList.remove("navAppear");
					console.log(document.querySelector(".add_form"));
					if (document.querySelector(".add_form") == null)
						document.querySelector(".black").classList
								.remove("blackAppear");
	
					document.querySelector(".toggle").classList
							.remove("toggleMove");
		});
	if (document.querySelector(".black") != null)
		document.querySelector(".black").addEventListener("click", function() {
			if ($(".setManangerSuccessfullAppear")[0] != undefined) return;
			document.querySelector("nav").classList.remove("navAppear");
			document.querySelector(".black").classList.remove("blackAppear");
			document.querySelector(".toggle").classList.remove("toggleMove");
			
			if ($(".reportContainer")[0] != undefined) {
				$(".reportContainer").removeClass("reportContainerAppear");
			}
			
			if ($(".returnCard")[0] != undefined) {
				$(".returnCard").removeClass("reportContainerAppear");
			}
			
			if ($(".borrowCard")[0] != undefined) {
				$(".borrowCard").removeClass("reportContainerAppear");
			}
			
			if ($(".reservateCard")[0] != undefined) {
				$(".reservateCard").removeClass("reportContainerAppear");
			}
			
			if ($(".punishmentCard")[0] != undefined) {
				$(".punishmentCard").removeClass("reportContainerAppear");
				$(".punishmentCard .penalty_record").remove();
			}
			
			if ($(".addSuccessful")[0] != undefined) {
				$(".addSuccessful").removeClass("warningAppear");
			}
			
			if ($(".otherContainer")[0] != undefined) {
				$(".otherContainer").removeClass("reportContainerAppear");
				$(".otherContainer button#percentageof").val("");
				$(".otherContainer input").each(function(){$(this).prop("checked", false);})
			}
			
			if ($(".staffProfile")[0] != undefined) {
				$(".staffProfile").removeClass("reportContainerAppear");
			}
		});
	//

	// Logout
	if (document.querySelector(".logoutBtn") != null)
		document.querySelector(".logoutBtn").addEventListener("click", function() {
			$.ajax({
				url : "../loginController/logout",
				type : "POST",
				data : "",
				contentType : "application/json",
				success : function(data) {
					location.href = "../loginController/login";
				}
			});
		});
	//
	
	//Black 2 of more report
	if ($(".black2")[0] != undefined) {
		$(".black2").click(function() {
			$(this).removeClass("black2Appear");
			$(".moreContainer").removeClass("reportContainerAppear");
		});
	}
	//
	
	//Warning staff
	if ($(".warning")[0] != undefined) {
		$(".warning").addClass("warningAppear");
		var time = 5;
		$(".warning span").text(time);
		var timmer = setInterval(function() {
			$(".warning span").text(--time);
			if (parseInt($(".warning span").text()) == 0) {
				clearInterval(timmer);
				if ($(".warning").hasClass("warningAccess"))
					location.href = "../homeController/display";
				else {
					location.href = "../loginController/login";
				}
					
			}
		}, 1000);
	}
	//The end warning
	var staffTmp;
	
	//Search
	var mainContent = "";
	var firstTime = true;
	var name = "content";
	$(".searchArea .rigth .rigth2 input").focusin(function(){
		if (firstTime) {
			mainContent = $(".content .books_container").html();
			firstTime = false;
		}
//		$(".content .books_container").html("");
//		$(".content").css({"display":"none"});
	});
//	$(".searchArea .rigth .rigth2 input").focusout(function(){
//		$(".content").css({"display":"block"});
//	});
	$(".searchArea .rigth .rigth2 input").keyup(function() {
		var keyword = $(".searchArea .rigth .rigth2 input").val().trim();
		if (keyword.length == 0) {
			$(".searchArea .rigth .rigth2 input").val("");
			$(".content .books_container").html(mainContent);
			name = "content";
			$(".content").css({"display":"block"});
			$(".searchContainer").css({"display":"none"});
			slide_show();
		}
		else {
			$(".content .books_container").html("");
			$(".content").css({"display":"none"});
			var searchContainer = $(".searchContainer .books_container");
			var nextandpre = "<span class=\"previous\"><i class=\"fas fa-chevron-left\"></i></span> <span\r\n" + 
			"						class=\"next\"><i class=\"fas fa-chevron-right\"></i></span>";
			var table = "";
			var tr = "";
			var pages = "<div class=\"pages\">\r\n" + 
			"						#pages\r\n" + 
			"					</div>";
			var page = "<span class=\"#active\">#number</span>";
			
			var tableTmp = "";
			var trTmp = "";
			var pagesTmp = "";
			var pageTmp = "";
			
			var strTmp = "";
			var numTable = 1;
			var strTmpPage = "";
			var strTmpTable= "";
			//Search staff
			if ($(".searchArea .rigth .rigth1 div:nth-child(3) input[value=email]").length > 0) {
				searchContainer.html("");
				$(".searchContainer").css({"display":"block"});
				//1 by id
				//2 by name
				//3 by email
				table = "<table class=\"books\">\r\n" + 
				"						<tr>\r\n" + 
				"							<th>ID</th>\r\n" + 
				"							<th>Name</th>\r\n" + 
				"							<th>Email</th>\r\n" + 
				"							<th>Password</th>\r\n" + 
				"                           <th>Role</th>\r\n" + 
				"							<th>Avatar</th>\r\n" + 
				"							<th>Action</th>\r\n" + 
				"						</tr>\r\n" + 
				"						#records\r\n" + 
				"					</table>";
				tr = "<tr id=\"#trstaff_id\">\r\n" + 
				"							<td>#staff_id</td>\r\n" + 
				"							<td>#staff_name</td>\r\n" + 
				"							<td>#staff_email</td>\r\n" + 
				"							<td>#staff_password</td>\r\n" + 
				"                           <td>#staff_role</td>\r\n" + 
				"							<td><img\r\n" + 
				"								src=\"/Library_management_system/#staff_avatar\"></td>\r\n" + 
				"							<td>\r\n" + 
				"								<button value=\"#removestaff_id\" data-table=\"0\" class=\"removeBtn\" title=\"Remove staff\">\r\n" + 
				"									<i class=\"far fa-trash-alt\"></i>\r\n" + 
				"								</button>\r\n" + 
				"								<button value=\"#modifystaff_id\" data-table=\"0\" class=\"modifyBtn\" title=\"Modify staff\">\r\n" + 
				"									<i class=\"far fa-edit\"></i>\r\n" + 
				"								</button>\r\n" + 
				"								#btnSetAsManager"+				
				"							</td>\r\n" + 
				"						</tr>";
				var setManagerBtn = "<button value=\"#setManager_id\"\r\n" + 
				"												data-table=\"${table}\" title=\"Set as a manager\"\r\n" + 
				"												class=\"setManagerBtn\" #hidden>\r\n" + 
				"												<i class=\"fas fa-exchange-alt\"></i>\r\n" + 
				"											</button>";
				var setManagerBtnTmp = "";
				var url = "../staffController/searchstaff?keyword=" + keyword + "&type=";
				if ($(".searchArea .rigth .rigth1 div:nth-child(1) input").prop("checked")) {
					url += 1;
				} else if ($(".searchArea .rigth .rigth1 div:nth-child(2) input").prop("checked")) {
					url += 2;
				} else {
					url += 3;
				}
				setTimeout(function(){},800);
				
				$.ajax({
					url: url,
					type: "GET",
					data: "",
					contentType : "application/json;charset=utf-8",
					success: function(data){
						console.log(data);
						if (data.length > 0 && data.length <= 20) {
							console.log("here")
							pageTmp = page;
							pageTmp = pageTmp.replace("#number", 1);
							pagesTmp = pages;
							pagesTmp = pagesTmp.replace("#pages", pageTmp);
							
							for (var i = 0; i < data.length; i++) {
								if (data[i].role != "staff") {
									staffTmp = data[i];
									break;
								}
							}
							
							for (var i = 0; i < data.length; i++) {
								trTmp = tr;
								trTmp= trTmp.replace("#trstaff_id", data[i].id.id);
								trTmp= trTmp.replace("#staff_id", data[i].id.id);
								trTmp= trTmp.replace("#staff_name", data[i].name);
								trTmp= trTmp.replace("#staff_email", data[i].email);
								trTmp= trTmp.replace("#staff_password", data[i].password);
								trTmp= trTmp.replace("#staff_role", data[i].role);
								trTmp= trTmp.replace("#staff_avatar", data[i].avatar);
								trTmp= trTmp.replace("#removestaff_id", data[i].id.id);
								trTmp= trTmp.replace("#modifystaff_id", data[i].id.id);
								var setManagerBtnTmp = setManagerBtn;
								if (data[i].role == "staff") {
									setManagerBtnTmp = setManagerBtnTmp.replace("#hidden", "");
								} else {
									setManagerBtnTmp = setManagerBtnTmp.replace("#hidden", "hidden=\"hidden\"");
								}
								setManagerBtnTmp = setManagerBtnTmp.replace("#setManager_id", data[i].id.id+","+staffTmp.id.id);
								trTmp= trTmp.replace("#btnSetAsManager", setManagerBtnTmp);
								strTmp += trTmp;
							}
							tableTmp = table;
							tableTmp = tableTmp.replace("#records", strTmp);
							searchContainer.html(nextandpre+tableTmp+pagesTmp);
						}
						
						if (data.length > 20) {
							for (var i = 0; i < data.length; i++) {
								if (data[i].role != "staff") {
									staffTmp = data[i];
									return
								}
							}
							for (var i = 0; i < data.length; i++) {
								trTmp = tr;
								trTmp= trTmp.replace("#trstaff_id", data[i].id.id);
								trTmp= trTmp.replace("#staff_id", data[i].id.id);
								trTmp= trTmp.replace("#staff_name", data[i].name);
								trTmp= trTmp.replace("#staff_email", data[i].email);
								trTmp= trTmp.replace("#staff_password", data[i].password);
								trTmp= trTmp.replace("#staff_role", data[i].role);
								trTmp= trTmp.replace("#staff_avatar", data[i].avatar);
								trTmp= trTmp.replace("#removestaff_id", data[i].id.id);
								trTmp= trTmp.replace("#modifystaff_id", data[i].id.id);
								var setManagerBtnTmp = setManagerBtn;
								if (data[i].role == "staff") {
									setManagerBtnTmp = setManagerBtnTmp.replace("#hidden", "");
								} else {
									setManagerBtnTmp = setManagerBtnTmp.replace("#hidden", "hidden=\"hidden\"");
								}
								setManagerBtnTmp = setManagerBtnTmp.replace("#setManager_id", data[i].id.id+","+staffTmp.id.id);
								trTmp= trTmp.replace("#btnSetAsManager", setManagerBtnTmp);
								strTmp += trTmp;
								if ((i + 1) % 20 == 0) {
									tableTmp = table;
									pageTmp = page;
									if (numTable == 1) {
										tableTmp = tableTmp.replace("#active", "slide_active");
										pageTmp = pageTmp.replace("#active", "page_active");
									} else {
										tableTmp = tableTmp.replace("#active", "moveRight");
									}
									pageTmp = pageTmp.replace("#number", numTable++);
									strTmpPage += pageTmp;
									tableTmp = tableTmp.replace("#records", strTmp);
									strTmpTable += tableTmp;
									strTmp = "";
								}
							}
							
							for (var i = numTable*19; i < data.length; i++) {
								trTmp = tr;
								trTmp= trTmp.replace("#trstaff_id", data[i].id.id);
								trTmp= trTmp.replace("#staff_id", data[i].id.id);
								trTmp= trTmp.replace("#staff_name", data[i].name);
								trTmp= trTmp.replace("#staff_email", data[i].email);
								trTmp= trTmp.replace("#staff_password", data[i].password);
								trTmp= trTmp.replace("#staff_role", data[i].role);
								trTmp= trTmp.replace("#staff_avatar", data[i].avatar);
								trTmp= trTmp.replace("#removestaff_id", data[i].id.id);
								trTmp= trTmp.replace("#modifystaff_id", data[i].id.id);
								var setManagerBtnTmp = setManagerBtn;
								if (data[i].role == "staff") {
									setManagerBtnTmp = setManagerBtnTmp.replace("#hidden", "");
								} else {
									setManagerBtnTmp = setManagerBtnTmp.replace("#hidden", "hidden=\"hidden\"");
								}
								setManagerBtnTmp = setManagerBtnTmp.replace("#setManager_id", data[i].id.id+","+staffTmp.id.id);
								trTmp= trTmp.replace("#btnSetAsManager", setManagerBtnTmp);
								strTmp += trTmp;
							}
							tableTmp = table;
							tableTmp = tableTmp.replace("#active", "moveRight");
							tableTmp = tableTmp.replace("#records", strTmp);
							strTmpTable += tableTmp;
							pageTmp = page;
							pageTmp = pageTmp.replace("#number", numTable);
							strTmpPage += pageTmp;
							
							pagesTmp = pages;
							pagesTmp = pagesTmp.replace("#pages", strTmpPage);
//							tableTmp = tableTmp.replace("#records", strTmpTable);
							
							searchContainer.html(nextandpre+strTmpTable+pagesTmp);
						}
						
						//Edit staff
						var editBtns = $(".books tr td button.modifyBtn");
						editBtns.each(function() {
							$(this).click(function(){
								var editBtn = $(this);
								$.ajax({
									url: "../staffController/staff?staff_id=" + $(this).val(),
									type: "GET",
									data: "",
									contentType: "application/json;charset=utf-8",
									success: function (data) {
										console.log(data);
										//Change type of button to button instead of adding
										$(".add_form form button:first-child").text("Save?");
//										$(".add_form form button:first-child").attr("type", "button");
										$(".add_form form button:first-child").val(data.id.id);
										$(".add_form form button:first-child").data("table", $(editBtn).data("table"));
										//
										
										//Disable input tag in add form
										$(".add_form form .id_container input").attr("disabled", "disabled");
//										$(".add_form form select").attr("disabled", "disabled");
//										console.log(data);
										//
										
										//Set the values of the book will be deleted
										$(".add_form form .id_container input").val(data.id.id);
										$(".add_form form .name_container input").val(data.name);
										$(".add_form form .email_container input").val(data.email);
//										$(".add_form form .password_container input").val(data.password);
										$($(".add_form form .password_container input")[0]).attr("readonly", "true");
										$($(".add_form form .password_container input")[0]).val(data.password);
										$($(".add_form form .password_container input")[1]).attr("readonly", "true");
										$($(".add_form form .password_container input")[1]).val(data.role);
										//
										
										//Change file input tag to img tag when remove record
										$(".add_form form .cover_image_container img").attr("src","/Library_management_system" + data.avatar);
										$(".add_form form .cover_image_container img").css("display", "inline-block");
										//
										
										//
										
										//Open warning window
										$(".add_form form h2 span").text("Modify the staff").css("color", "");
										$(".add_form").addClass("appear");
										$(".black").addClass("blackAppear");
										//
										
										//Change action of form input
										$(".add_form form").attr("action", "./ustaff");
										//
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
							});
						});
						//The end edit staff
						
						//Remove staff
						var deleteBtns = $(".books tr td button.removeBtn");
						deleteBtns.each(function() {
							$(this).click(function() {
								var deleteBtn = $(this);
								$.ajax({
									url: "../staffController/staff?staff_id=" + $(this).val(),
									type: "GET",
									data: "",
									contentType: "application/json;charset=utf-8",
									success: function (data) {
										console.log(data);
										//Change type of button to button instead of adding
										$(".add_form form button:first-child").text("Remove?");
										$(".add_form form button:first-child").attr("type", "button");
										$(".add_form form button:first-child").val(data.id.id);
										$(".add_form form button:first-child").data("table", $(deleteBtn).data("table"));
										console.log($(deleteBtn).data("table"));
										console.log($(".add_form form button:first-child").data("table"));
										//
										
										//Disable input tag in add form
										$(".add_form form input").attr("disabled", "disabled");
//										console.log(data);
										//
										
										//Set the values of the book will be deleted
										$(".add_form form .id_container input").val(data.id.id);
										$(".add_form form .name_container input").val(data.name);
										$(".add_form form .email_container input").val(data.email);
//										$(".add_form form .password_container input").val(data.password);
										$($(".add_form form .password_container input")[0]).attr("readonly", "true");
										$($(".add_form form .password_container input")[0]).val(data.password);
										$($(".add_form form .password_container input")[1]).attr("readonly", "true");
										$($(".add_form form .password_container input")[1]).val(data.role);
										//
										
										//Change file input tag to img tag when remove record
										$(".add_form form .cover_image_container img:nth-child(2)").attr("src","/Library_management_system" + data.avatar);
										$(".add_form form .cover_image_container img:nth-child(2)").css("display", "inline-block");
										$(".add_form form .cover_image_container input").css("display", "none");
										//
										
										//Open warning window
										$(".add_form form h2 span").text("Waring").css("color", "red");
										$(".add_form").addClass("appear");
										$(".black").addClass("blackAppear");
										//
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
							});
						});
						//The end remove staff
					},
					async: false,
					error: function (e) {
						console.log(e);
					}	
				});
				
				//Set as a manager
				$(".books tr td button.setManagerBtn").each(function(index) {
					$(this).click(function() {
						console.log($(this).val());
						$.ajax({
							url : "../staffController/setmanager?staff_id_next=" + $(this).val().split(",")[0] + 
									"&staff_id_current=" + $(this).val().split(",")[1],
							type : "POST",
							data : "",
							contentType : "application/json",
							success : function(data) {
								$(".black").addClass("blackAppear");
								$(".setManangerSuccessfull").css({"display":"block"});
								$(".setManangerSuccessfull").addClass("setManangerSuccessfullAppear");
								$(".setManangerSuccessfull span").css({"color":"red"});
//								if ($(".warning")[0] != undefined) {
									var time = 5;
									$(".setManangerSuccessfull span:nth-child(1)").text(time);
									var timmer = setInterval(function() {
										$(".setManangerSuccessfull span:nth-child(1)").text(--time);
										if (parseInt($(".setManangerSuccessfull span:nth-child(1)").text()) == 0) {
											clearInterval(timmer);
											location.href = "../loginController/login";
										}
									}, 1000);
//								}
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
					});
				});
				//The end set as a manager
			}
			//The end search staff
			
			//Search book
			if ($(".searchArea .rigth .rigth1 div:nth-child(3) input[value=email]").length == 0
				&& $("#cate_hidden")[0] == undefined) {
				searchContainer.html("");
				$(".searchContainer").css({"display":"block"});
				table = "<table class=\"books #active\">\r\n" + 
				"						<tr>\r\n" + 
				"							<th>ID</th>\r\n" + 
				"							<th>Name</th>\r\n" + 
				"							<th>Category</th>\r\n" + 
				"							<th>Borrowed</th>\r\n" + 
				"							<th>Quantity</th>\r\n" + 
				"							<th>Time print</th>\r\n" + 
				"							<th>Year manufacture</th>\r\n" + 
				"							<th>Manufacturer</th>\r\n" + 
				"							<th>Price</th>\r\n" + 
				"							<th>Cover image</th>\r\n" + 
				"							<th>Action</th>\r\n" + 
				"						</tr>\r\n" + 
				"						#records\r\n" + 
				"					</table>";
				tr = "<tr id=\"#trbook_id\">\r\n" + 
				"<td>#book_id</td>\r\n" + 
				"<td>#book_name</td>\r\n" + 
				"<td>#book_cate</td>\r\n" + 
				"<td>#book_borrowed</td>\r\n" + 
				"<td>#book_quantity</td>\r\n" + 
				"<td>#book_timePrint</td>\r\n" + 
				"<td>#book_ymanufacture</td>\r\n" + 
				"<td>#book_manufacturer</td>\r\n" + 
				"<td>#book_price</td>\r\n" +
				"<td><img\r\n" + 
				"src=\"/Library_management_system#book_img\"></td>\r\n" + 
				"<td>\r\n" + 
				"<button value=\"#removebook_id\"\r\n" + 
				"data-table=\"${table}\" class=\"removeBtn\" title=\"Remove book\">\r\n" + 
				"<i class=\"far fa-trash-alt\"></i>\r\n" + 
				"</button>\r\n" + 
				"<button value=\"#modifybook_id\"\r\n" + 
				"data-table=\"${table}\" class=\"modifyBtn\" title=\"Modify book\">\r\n" + 
				"<i class=\"far fa-edit\"></i>\r\n" + 
				"</button>\r\n" + 
				"</td>\r\n" + 
				"</tr>";
				var url = "../bookController/searchbook?keyword=" + keyword + "&type=";
				if ($(".searchArea .rigth .rigth1 div:nth-child(1) input").prop("checked")) {
					url += 1;
				} else
					url += 2;
				
				setTimeout(function(){},800);
				
				$.ajax({
					url: url,
					type: "GET",
					data: "",
					contentType : "application/json;charset=utf-8",
					success: function(data){
						console.log(data);
						if (data.length > 0 && data.length <= 20) {
							pageTmp = page;
							pageTmp = pageTmp.replace("#number", 1);
							pagesTmp = pages;
							pagesTmp = pagesTmp.replace("#pages", pageTmp);
							
							for (var i = 0; i < data.length; i++) {
								trTmp = tr;
								trTmp = trTmp.replace("#trbook_id", data[i].id.id);
								trTmp = trTmp.replace("#book_id", data[i].id.id);
								trTmp = trTmp.replace("#book_name", data[i].name);
								trTmp = trTmp.replace("#book_cate", data[i].myfile.description);
								trTmp = trTmp.replace("#book_borrowed", data[i].borrowed);
								trTmp = trTmp.replace("#book_quantity", data[i].quantity);
								trTmp = trTmp.replace("#book_timePrint", data[i].timePrint);
								trTmp = trTmp.replace("#book_ymanufacture", data[i].ymanufacture);
								trTmp = trTmp.replace("#book_manufacturer", data[i].manufacturer);
								trTmp = trTmp.replace("#book_price", data[i].price);
								trTmp = trTmp.replace("#book_img", data[i].cover_image);
								trTmp = trTmp.replace("#removebook_id", data[i].id.id);
								trTmp = trTmp.replace("#modifybook_id", data[i].id.id);
								strTmp += trTmp;
							}
							tableTmp = table;
							tableTmp = tableTmp.replace("#records", strTmp);
							searchContainer.html(nextandpre+tableTmp+pagesTmp);

						}
						
						if (data.length > 20) {
							for (var i = 0; i < data.length; i++) {
								trTmp = tr;
								trTmp = trTmp.replace("#trbook_id", data[i].id.id);
								trTmp = trTmp.replace("#book_id", data[i].id.id);
								trTmp = trTmp.replace("#book_name", data[i].name);
								trTmp = trTmp.replace("#book_cate", data[i].myfile.description);
								trTmp = trTmp.replace("#book_borrowed", data[i].borrowed);
								trTmp = trTmp.replace("#book_quantity", data[i].quantity);
								trTmp = trTmp.replace("#book_timePrint", data[i].timePrint);
								trTmp = trTmp.replace("#book_ymanufacture", data[i].ymanufacture);
								trTmp = trTmp.replace("#book_manufacturer", data[i].manufacturer);
								trTmp = trTmp.replace("#book_price", data[i].price);
								trTmp = trTmp.replace("#book_img", data[i].cover_image);
								trTmp = trTmp.replace("#removebook_id", data[i].id.id);
								trTmp = trTmp.replace("#modifybook_id", data[i].id.id);
								strTmp += trTmp;
								if ((i + 1) % 20 == 0) {
									tableTmp = table;
									pageTmp = page;
									if (numTable == 1) {
										tableTmp = tableTmp.replace("#active", "slide_active");
										pageTmp = pageTmp.replace("#active", "page_active");
									} else {
										tableTmp = tableTmp.replace("#active", "moveRight");
									}
									pageTmp = pageTmp.replace("#number", numTable++);
									strTmpPage += pageTmp;
									tableTmp = tableTmp.replace("#records", strTmp);
									strTmpTable += tableTmp;
									strTmp = "";
								}
							}
							
							for (var i = numTable*19; i < data.length; i++) {
								trTmp = tr;
								trTmp = trTmp.replace("#trbook_id", data[i].id.id);
								trTmp = trTmp.replace("#book_id", data[i].id.id);
								trTmp = trTmp.replace("#book_name", data[i].name);
								trTmp = trTmp.replace("#book_cate", data[i].myfile.description);
								trTmp = trTmp.replace("#book_borrowed", data[i].borrowed);
								trTmp = trTmp.replace("#book_quantity", data[i].quantity);
								trTmp = trTmp.replace("#book_timePrint", data[i].timePrint);
								trTmp = trTmp.replace("#book_ymanufacture", data[i].ymanufacture);
								trTmp = trTmp.replace("#book_manufacturer", data[i].manufacturer);
								trTmp = trTmp.replace("#book_price", data[i].price);
								trTmp = trTmp.replace("#book_img", data[i].cover_image);
								trTmp = trTmp.replace("#removebook_id", data[i].id.id);
								trTmp = trTmp.replace("#modifybook_id", data[i].id.id);
								strTmp += trTmp;
							}
							tableTmp = table;
							tableTmp = tableTmp.replace("#active", "moveRight");
							tableTmp = tableTmp.replace("#records", strTmp);
							strTmpTable += tableTmp;
							pageTmp = page;
							pageTmp = pageTmp.replace("#number", numTable);
							strTmpPage += pageTmp;
							
							pagesTmp = pages;
							pagesTmp = pagesTmp.replace("#pages", strTmpPage);
//							tableTmp = tableTmp.replace("#records", strTmpTable);
							
							searchContainer.html(nextandpre+strTmpTable+pagesTmp);

						}
						
						console.log(pagesTmp);
//						searchContainer.html(nextandpre+tableTmp+pagesTmp);
						
						//Edit button clicked
						var editBtns = $(".books tr td button.modifyBtn");
						editBtns.each(function() {
							$(this).click(function(){
								var editBtn = $(this);
								$.ajax({
									url: "../bookController/book?book_id=" + $(this).val(),
									type: "GET",
									data: "",
									contentType: "application/json;charset=utf-8",
									success: function (data) {
										//Change type of button to button instead of adding
										$(".add_form form button:first-child").text("Save?");
//										$(".add_form form button:first-child").attr("type", "button");
										$(".add_form form button:first-child").val(data.id.id);
										$(".add_form form button:first-child").data("table", $(editBtn).data("table"));
										//
										
										//Disable input tag in add form
										$(".add_form form .id_container input").attr("disabled", "disabled");
//										$(".add_form form select").attr("disabled", "disabled");
//										console.log(data);
										//
										
										//Set the values of the book will be deleted
										$(".add_form form .id_container input").val(data.id.id);
										$(".add_form form .name_container input").val(data.name);
										$(".add_form form .category_container select").val(data.myfile.description);
										$(".add_form form .quantity_container input").val(data.quantity);
										$(".add_form form .edition_container input").val(data.timePrint);
										$(".add_form form .ymanufacture_container input").val(data.ymanufacture);
										$($(".add_form form .manufacturer_container input")[0]).val(data.manufacturer);
										$($(".add_form form .manufacturer_container input")[1]).val(data.price);
										
										//Change file input tag to img tag when remove record
										$(".add_form form .cover_image_container img").attr("src","/Library_management_system" + data.cover_image);
										$(".add_form form .cover_image_container img").css("display", "inline-block");
										//
										
										//
										
										//Open warning window
										$(".add_form form h2 span").text("Modify the book").css("color", "");
										$(".add_form").addClass("appear");
										$(".black").addClass("blackAppear");
										//
										
										//Change action of form input
										$(".add_form form").attr("action", "./ubook");
										//
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
							});
						});
						//The end modify book
						
						//Delete book
						var deleteBtns = $(".books tr td button.removeBtn");
						deleteBtns.each(function() {
							$(this).click(function() {
								var deleteBtn = $(this);
								$.ajax({
									url: "../bookController/book?book_id=" + $(this).val(),
									type: "GET",
									data: "",
									contentType: "application/json;charset=utf-8",
									success: function (data) {
										//Change type of button to button instead of adding
										$(".add_form form button:first-child").text("Remove?");
										$(".add_form form button:first-child").attr("type", "button");
										$(".add_form form button:first-child").val(data.id.id);
										$(".add_form form button:first-child").data("table", $(deleteBtn).data("table"));
										//
										
										//Disable input tag in add form
										$(".add_form form input").attr("disabled", "disabled");
										$(".add_form form select").attr("disabled", "disabled");
//										console.log(data);
										//
										
										//Set the values of the book will be deleted
										$(".add_form form .id_container input").val(data.id.id);
										$(".add_form form .name_container input").val(data.name);
										$(".add_form form .category_container select").val(data.myfile.description);
										$(".add_form form .quantity_container input").val(data.quantity);
										$(".add_form form .edition_container input").val(data.timePrint);
										$(".add_form form .ymanufacture_container input").val(data.ymanufacture);
										$($(".add_form form .manufacturer_container input")[0]).val(data.manufacturer);
										$($(".add_form form .manufacturer_container input")[1]).val(data.price);
										
										//Change file input tag to img tag when remove record
										$(".add_form form .cover_image_container img:nth-child(2)").attr("src","/Library_management_system" + data.cover_image);
										$(".add_form form .cover_image_container img:nth-child(2)").css("display", "inline-block");
										$(".add_form form .cover_image_container input").css("display", "none");
										//
										
										//
										
										//Open warning window
										$(".add_form form h2 span").text("Waring").css("color", "red");
										$(".add_form").addClass("appear");
										$(".black").addClass("blackAppear");
										//
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
							});
						});
						//The end remove book
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
			//The end search book
			
			//Search category
			if ($("#cate_hidden")[0] != undefined) {
				searchContainer.html("");
				$(".searchContainer").css({"display":"block"});
				table = 	"<table class=\"books\">\r\n" + 
				"						<tr>\r\n" + 
				"							<th>ID</th>\r\n" + 
				"							<th>Name</th>\r\n" + 
				"							<th>Action</th>\r\n" + 
				"						</tr>\r\n" + 
				"						#records\r\n" + 
				"					</table>";
				tr = "<tr id=\"#trcate_id\">\r\n" + 
				"										<td>#cate_id</td>\r\n" + 
				"										<td>#cate_name</td>\r\n" + 
				"										<td>\r\n" + 
				"											<button value=\"#removecate_id\"\r\n" + 
				"												data-table=\"#table_order\" class=\"removeBtn\" title=\"Remove category\">\r\n" + 
				"												<i class=\"far fa-trash-alt\"></i>\r\n" + 
				"											</button>\r\n" + 
				"											<button value=\"#modifycate_id\"\r\n" + 
				"												data-table=\"#table_order\" class=\"modifyBtn\" title=\"Remove category\">\r\n" + 
				"												<i class=\"far fa-edit\"></i>\r\n" + 
				"											</button>\r\n" + 
				"										</td>\r\n" + 
				"									</tr>";
				
				var url = "../categoryController/searchcate?keyword=" + keyword + "&type=";
				if (document.querySelectorAll(".searchArea .rigth .rigth1 div")[0].querySelector("input[name=type]").checked) {
					url += 1;
				} else
					url += 2;
				console.log(url);
				setTimeout(function(){},800);
				
				$.ajax({
					url: url,
					type: "GET",
					data: "",
					contentType : "application/json;charset=utf-8",
					success: function(data){
						console.log(data);
						if (data.length > 0 && data.length <= 20) {
							pageTmp = page;
							pageTmp = pageTmp.replace("#number", 1);
							pagesTmp = pages;
							pagesTmp = pagesTmp.replace("#pages", pageTmp);
							
							for (var i = 0; i < data.length; i++) {
								trTmp = tr;
								trTmp = trTmp.replace("#trcate_id", data[i].id.id);
								trTmp = trTmp.replace("#cate_id", data[i].id.id);
								trTmp = trTmp.replace("#cate_name", data[i].name);
								trTmp = trTmp.replace("#removecate_id", data[i].id.id);
								trTmp = trTmp.replace("#modifycate_id", data[i].id.id);
								trTmp = trTmp.replace("#table_order", 0);
								strTmp += trTmp;
							}
							tableTmp = table;
							tableTmp = tableTmp.replace("#records", strTmp);
							searchContainer.html(nextandpre+tableTmp+pagesTmp);

						}
						
						if (data.length > 20) {
							for (var i = 0; i < data.length; i++) {
								trTmp = tr;
								trTmp = trTmp.replace("#trcate_id", data[i].id.id);
								trTmp = trTmp.replace("#cate_id", data[i].id.id);
								trTmp = trTmp.replace("#cate_name", data[i].name);
								trTmp = trTmp.replace("#removecate_id", data[i].id.id);
								trTmp = trTmp.replace("#modifycate_id", data[i].id.id);
								trTmp = trTmp.replace("#table_order", 0);
								strTmp += trTmp;
								if ((i + 1) % 20 == 0) {
									tableTmp = table;
									pageTmp = page;
									if (numTable == 1) {
										tableTmp = tableTmp.replace("#active", "slide_active");
										pageTmp = pageTmp.replace("#active", "page_active");
									} else {
										tableTmp = tableTmp.replace("#active", "moveRight");
									}
									pageTmp = pageTmp.replace("#number", numTable++);
									strTmpPage += pageTmp;
									tableTmp = tableTmp.replace("#records", strTmp);
									strTmpTable += tableTmp;
									strTmp = "";
								}
							}
							
							for (var i = numTable*19; i < data.length; i++) {
								trTmp = tr;
								trTmp = trTmp.replace("#trcate_id", data[i].id.id);
								trTmp = trTmp.replace("#cate_id", data[i].id.id);
								trTmp = trTmp.replace("#cate_name", data[i].name);
								trTmp = trTmp.replace("#removecate_id", data[i].id.id);
								trTmp = trTmp.replace("#modifycate_id", data[i].id.id);
								trTmp = trTmp.replace("#table_order", 0);
								strTmp += trTmp;
							}
							tableTmp = table;
							tableTmp = tableTmp.replace("#active", "moveRight");
							tableTmp = tableTmp.replace("#records", strTmp);
							strTmpTable += tableTmp;
							pageTmp = page;
							pageTmp = pageTmp.replace("#number", numTable);
							strTmpPage += pageTmp;
							
							pagesTmp = pages;
							pagesTmp = pagesTmp.replace("#pages", strTmpPage);
//							tableTmp = tableTmp.replace("#records", strTmpTable);
							
							searchContainer.html(nextandpre+strTmpTable+pagesTmp);

						}
						
						//Edit button clicked
						var editBtns = $(".books tr td button.modifyBtn");
						editBtns.each(function() {
							$(this).click(function(){
								var editBtn = $(this);
								$.ajax({
									url: "./category?category_id=" + $(this).val(),
									type: "GET",
									data: "",
									contentType: "application/json;charset=utf-8",
									success: function (data) {
										//Change type of button to button instead of adding
										$(".add_form form button:first-child").text("Save?");
										$(".add_form form button:first-child").val(data.id.id);
										$(".add_form form button:first-child").data("table", $(editBtn).data("table"));
										//
										
										//Disable input tag in add form
										$(".add_form form .id_container input").attr("disabled", "disabled");
										//
										
										//Set the values of the book will be deleted
										$(".add_form form .id_container input").val(data.id.id);
										$(".add_form form .name_container input").val(data.name);
										//
										
										//Open warning window
										$(".add_form form h2 span").text("Modify the book").css("color", "");
										$(".add_form").addClass("appear");
										$(".black").addClass("blackAppear");
										//
										
										//Change action of form input
										$(".add_form form").attr("action", "./ucategory");
										//
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
							});
						});
						//The end modify category
						
						//Delete category
						var deleteBtns = $(".books tr td button.removeBtn");
						deleteBtns.each(function() {
							$(this).click(function() {
								var deleteBtn = $(this);
								$.ajax({
									url: "../categoryController/category?category_id=" + $(this).val(),
									type: "GET",
									data: "",
									contentType: "application/json;charset=utf-8",
									success: function (data) {
										//Change type of button to button instead of adding
										$(".add_form form button:first-child").text("Remove?");
										$(".add_form form button:first-child").attr("type", "button");
										$(".add_form form button:first-child").val(data.id.id);
										$(".add_form form button:first-child").data("table", $(deleteBtn).data("table"));
										console.log($(deleteBtn).data("table"));
										console.log($(".add_form form button:first-child").data("table"));
										//
										
										//Disable input tag in add form
										$(".add_form form input").attr("disabled", "disabled");
//										console.log(data);
										//
										
										//Set the values of the book will be deleted
										$(".add_form form .id_container input").val(data.id.id);
										$(".add_form form .name_container input").val(data.name);
										//
										
										//Open warning window
										$(".add_form form h2 span").text("Waring").css("color", "red");
										$(".add_form").addClass("appear");
										$(".black").addClass("blackAppear");
										//
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
							});
						});
						//The end remove category
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
			//The end search category
			name = "searchContainer";
			slide_show();
		}
	});
	//The end of search
	
	function slide_show() {
		// Slide show
		// Next button clicked
		var tables = $(`.${name} table`);
		var pagesObj = $(`.${name} .pages span`);
		$("span.next").click(function() {
			if ($(".books.slide_active").next().prop("tagName") == "TABLE") {
				pagesObj[tables.index($(".books.slide_active"))].classList.remove("page_active");
				pagesObj[tables.index($(".books.slide_active"))+1].classList.add("page_active");
				$(".books.slide_active").addClass("moveLeft");
				$(".books.slide_active.moveLeft").next().addClass("slide_active");
				$(".books.slide_active.moveLeft").next().removeClass("moveRight");
				$(".books.slide_active.moveLeft").removeClass("slide_active");
				
				if ($(".content .books_container").html() != "")
					$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
				else
					$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
			}
		});
		//
		// Previous button clicked
		$("span.previous").click(function() {
			if ($(".books.slide_active").prev().prop("tagName") == "TABLE") {
				pagesObj[tables.index($(".books.slide_active"))].classList.remove("page_active");
				pagesObj[tables.index($(".books.slide_active"))-1].classList.add("page_active");
				$(".books.slide_active").addClass("moveRight");
				$(".books.slide_active.moveRight").prev().addClass("slide_active");
				$(".books.slide_active.moveRight").prev().removeClass("moveLeft");
				$(".books.slide_active.moveRight").removeClass("slide_active");
				
				if ($(".content .books_container").html() != "")
					$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
				else
					$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
			}
		});
		//
		
		pagesObj.each(function() {
			$(this).click(function() {
				var tmp = $(this);
				if (parseInt($(this).text()) <= parseInt($(`.${name} .pages .page_active`).text())){
					$(pagesObj.get().reverse()).each(function() {
						console.log($(this));
						if ($(this).is(tmp)) return false;
						$(this).removeClass("page_active");
						tables[parseInt($(this).text())-1].classList.remove("slide_active");
						tables[parseInt($(this).text())-1].classList.remove("moveRight");
						tables[parseInt($(this).text())-1].classList.remove("moveLeft");
						tables[parseInt($(this).text())-1].classList.add("moveRight");
					});
				} else {
					pagesObj.each(function() {
						console.log($(this));
						if ($(this).is(tmp)) return false;
						$(this).removeClass("page_active");
						tables[parseInt($(this).text())-1].classList.remove("slide_active");
						tables[parseInt($(this).text())-1].classList.remove("moveLeft");
						tables[parseInt($(this).text())-1].classList.remove("moveRight");
						tables[parseInt($(this).text())-1].classList.add("moveLeft");
					});
				} 
				
				$(this).addClass("page_active");
				tables[parseInt($(this).text())-1].classList.add("slide_active");
				tables[$(this).text()-1].classList.remove("moveLeft");
				tables[$(this).text()-1].classList.remove("moveRight");
				if ($(".content .books_container").html() != "")
					$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
				else
					$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
			});
		});
		//The end slide show	
	}
});