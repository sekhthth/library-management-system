window.addEventListener('DOMContentLoaded', (event) => {
	
	document.querySelector(".password").addEventListener("focus", function () {
		hiddNotificationWhenLogin();
	});
	
	document.querySelector(".password").addEventListener("keydown", function () {
		if (document.querySelector(".id").value.trim().length <= 0) {
			document.querySelector(".id").value = "";
		}
	});
	
	document.querySelector(".loginBtn").addEventListener("click", function () {
		if (document.querySelector(".id").value.trim().length <= 0) {
			document.querySelector(".main p").style.opacity = "1";
			document.querySelector(".main p").style.visibility = "visible";
			document.querySelector(".main p").textContent = "ID required";
		} else {
			if (document.querySelector(".id").value.trim().length > 11 || document.querySelector(".id").value.trim().length < 11) {
				document.querySelector(".main p").style.opacity = "1";
				document.querySelector(".main p").style.visibility = "visible";
				document.querySelector(".main p").textContent = "ID must contains exactly 12 characters";
			} else {
				$.ajax({
					url: "./login?id=" + document.querySelector(".id").value + "&password=" + document.querySelector(".password").value,
					type: "POST",
					data: "",
					dataType: "text",
					contentType: "text/plain; charset=utf-8",
					success: function (data) {
						console.log(data.split("-")[0]);
						if (data.split("-")[0] == "Success") {
							if (data.split("-")[1] == "staff")
								location.href = "../homeController/display";
							else
								location.href = "../staffController/staffs";
						} else {
							document.querySelector(".main p").style.opacity = "1";
							document.querySelector(".main p").style.visibility = "visible";
							document.querySelector(".main p").textContent = data;
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
		}
		document.querySelector(".id").addEventListener("focus", function () {
			hiddNotificationWhenLogin();
		});
	});
	function hiddNotificationWhenLogin() {
		document.querySelector(".main p").style.opacity = "0";
		document.querySelector(".main p").style.visibility = "hidden";
	}
	
	$(".main a").click(function() {
		$(".main").css({"display":"none"});
		$(".black2").addClass("black2Appear");
		$(".forgetPassContainer").addClass("forgetPassContainerAppear");
	});
	
	$(".black2").click(function() {
		$(".main").css({"display":"block"});
		$(".black2").removeClass("black2Appear");
		if ($(".resetPasswordSuccessfully.test")[0] != undefined){
			 reset(); 
		} else {
			$(".forgetPassContainer").removeClass("forgetPassContainerAppear");
			$(".resetPasswordSuccessfully").removeClass("test");
		}
	});
	
	//nlthanhititiu17038@gmail.com
	var mail = "";
	var accountFound;
	var account = "<div class=\"accountAvatar\">\r\n" + 
	"				<img src=\"/Library_management_system#img\" alt=\"avatar\">\r\n" + 
	"			</div>\r\n" + 
	"			<div class=\"accountId\">\r\n" + 
	"				<p>#account_id</p>\r\n" + 
	"			</div>\r\n" + 
	"			<div class=\"accountName\">\r\n" + 
	"				<p>#account_name</p>\r\n" + 
	"			</div>";
	$(".searchAccount input").keydown(function() {
		$(".searchAccount").removeClass("invalidMail");
		$(".searchAccount").removeClass("userDoesNotExist");
		if ($(this).val().trim().length == 0) {
			$(this).val("");
			mail = "";
		}
	});
	
	$(".searchBtn").click(function() {
		var mailPattern = /^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,7}(\.[a-z]{2,5}){1,3}$/;
		mail = $(".searchAccount input").val();
		$(".account").html("");
		$(".account").removeClass("accountAppear");
		$(".account").css({"display":"none"});
		if (mail.length != 0) {
			if (mailPattern.test(mail)) {
				$.ajax({
					url: "../staffController/searchstaff?keyword="+mail+
						"&type=3",
					type: "GET",
					data: "",
					contentTpe:"application/json;charset=utf-8",
					success: function(data) {
//						console.log(data);
						if (data.length > 0) {
							accountFound = data[0];
							var accountTmp = account;
							accountTmp = accountTmp.replace("#img", data[0].avatar);
							accountTmp = accountTmp.replace("#account_id", data[0].id.id);
							accountTmp = accountTmp.replace("#account_name", data[0].name);
							$(".account").html(accountTmp);
							$(".account").css({"display":"block"});
							setTimeout(function(){$(".account").addClass("accountAppear");},100);
							setTimeout(function(){$(".account").addClass("accountAppear");},100);
						} else {
							$(".searchAccount").addClass("userDoesNotExist");
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			} else {
				$(".searchAccount").addClass("invalidMail");
			}
		}
	});
	
	var isReseting = false;
	var passcode;
	var timmer;
	$(".sendPassCodeBtn").click(function() {
		
		if ($(".account").children().length > 0) {
			if (!isReseting) {
				$(".verifyPasscode").css({"display":"block"});
				setTimeout(function(){$(".verifyPasscode").addClass("accountAppear");},100);
				
				//Get passcode
				$.ajax({
					url: "../loginController/passcode?email=" + mail,
					type: "GET",
					data: "",
					contentType: "text/plain;charset=utf-8",
					success: function(data) {
						passcode = data;
						console.log(data);
						$.ajax({
							url: "../loginController/sendpasscode?email=" + mail,
							type: "POST",
							data: "",
							contentType: "application/json;charset=utf-8",
							success: function(data) {
								isReseting = true;
								var minute = $("#minute");
								var second = $("#second");
								var minuteMax = 9;
								var secondMax = 59;
								minuteMaxTmp = minuteMax;
								secondMaxTmp = secondMax;
								$("#minute").text(minuteMaxTmp);
								$("#second").text(secondMaxTmp--);
								timmer = setInterval(function(){
									$("#minute").text(minuteMaxTmp);
									$("#second").text(secondMaxTmp--);
									if (secondMaxTmp < 0) {
										secondMaxTmp = secondMax;
										minuteMaxTmp--;
										if (minuteMaxTmp < 0) {
											clearInterval(timmer);
											$(".verifyPasscode").removeClass("accountAppear");
											setTimeout(function(){$(".verifyPasscode").css({"display":"none"});},400);
											passcode = null;
											isReseting = false;
										}
									}
								},1000);
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				//The end of get passcode
			}
		} else {
			
		}
	});
	
	$(".checkPasscode input").keydown(function() {
		$(".verifyPasscode").removeClass("incorrectPasscode");
		$(".verifyPasscode").removeClass("incorrectFormatPasscode");
		$(".verifyPasscode").removeClass("emptyPasscode");
		if ($(this).val().trim().length == 0) {
			$(this).val("");
		}
	});
	
	$(".checkPasscodeBtn").click(function() {
		if (!isNaN($(".checkPasscode input").val().trim())) {
			console.log($(".checkPasscode input").val().trim());
			if ($(".checkPasscode input").val().trim().length > 0) {
				if (parseInt($(".checkPasscode input").val().trim()) == passcode) {
					clearInterval(timmer);
					$(".createNewPassword").css({"display":"block"});
					setTimeout(function(){$(".createNewPassword").addClass("accountAppear");},100);
				} else {
					$(".verifyPasscode").addClass("incorrectPasscode");
				}
			} else {
				$(".verifyPasscode").addClass("emptyPasscode");
			}
		} else {
			$(".verifyPasscode").addClass("incorrectFormatPasscode");
		}
	});
	
	$($(".createNewPassword input")[0]).keydown(function() {
		$(".createNewPassword").removeClass("emptyPassword");
		$($(".createNewPassword p")[0]).removeClass("newPassword");
		$($(".createNewPassword p")[1]).removeClass("confirmPassword");
		$($(".createNewPassword p")[1]).removeClass("doesNotMatchPassword");
		if ($(this).val().trim().length == 0) {
			$(this).val("");
		}
	});
	
	$($(".createNewPassword input")[1]).keydown(function() {
		$(".createNewPassword").removeClass("emptyPassword");
		$($(".createNewPassword p")[0]).removeClass("newPassword");
		$($(".createNewPassword p")[1]).removeClass("confirmPassword");
		$($(".createNewPassword p")[1]).removeClass("doesNotMatchPassword");
		if ($(this).val().trim().length == 0) {
			$(this).val("");
		}
	});
	
	$(".changePassword").click(function() {
		if ($($(".createNewPassword input")[0]).val().trim().length > 0) {
			if ($($(".createNewPassword input")[1]).val().trim().length > 0) {
				if ($($(".createNewPassword input")[1]).val().trim() == $($(".createNewPassword input")[0]).val().trim()) {
					$.ajax({
						url: "../loginController/resetpassword?person_id=" + accountFound.id.id +
							 "&password=" + $($(".createNewPassword input")[0]).val().trim(),
						type: "POST",
						data: "",
						contentType: "application/json;charset=utf-8",
						success: function(data) {
							$.ajax({
								url: "../loginController/sendpasscode?email=" + mail,
								type: "POST",
								data: "",
								contentType: "application/json;charset=utf-8",
								success: function(data) {
									$(".resetPasswordSuccessfully").addClass("test");
									$(".forgetPassContainer").removeClass("forgetPassContainerAppear");
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
				} else {
					$(".createNewPassword").addClass("emptyPassword");
					$($(".createNewPassword p")[1]).addClass("doesNotMatchPassword");
				}
			} else {
				$(".createNewPassword").addClass("emptyPassword");
				$($(".createNewPassword p")[1]).addClass("confirmPassword");
			}
		} else {
			$(".createNewPassword").addClass("emptyPassword");
			$($(".createNewPassword p")[0]).addClass("newPassword");
		}
	});
	
	$("span.clearBtn").click(function() {
		reset();
	});
	
	function reset() {
		$(".main").css({"display":"block"});
		$(".black2").removeClass("black2Appear");
		
		$(".account").html("");
		$(".account").removeClass("accountAppear");
		$(".account").css({"display":"none"});
		
		$(".searchAccount").removeClass("invalidMail");
		$(".searchAccount").removeClass("userDoesNotExist");
		$(".searchAccount input").val("");
		
		$(".verifyPasscode").removeClass("accountAppear");
		$(".verifyPasscode").css({"display":"none"});
		$(".verifyPasscode").removeClass("incorrectPasscode");
		$(".verifyPasscode").removeClass("incorrectFormatPasscode");
		$(".verifyPasscode").removeClass("emptyPasscode");
		$(".checkPasscode input").val("");
		
		$(".createNewPassword").css({"display":"none"});
		$(".createNewPassword").removeClass("accountAppear");
		$(".createNewPassword").removeClass("emptyPassword");
		$($(".createNewPassword p")[0]).removeClass("newPassword");
		$($(".createNewPassword p")[1]).removeClass("confirmPassword");
		$($(".createNewPassword p")[1]).removeClass("doesNotMatchPassword");
		$($(".createNewPassword input")[0]).val("");
		$($(".createNewPassword input")[1]).val("")
		
		$(".forgetPassContainer").removeClass("forgetPassContainerAppear");
		$(".resetPasswordSuccessfully").removeClass("test");
		
		$("#minute").text("");
		$("#second").text("");
		
		isReseting = false;
		clearInterval(timmer);
		passcode = null;
	}
});