window.addEventListener("DOMContentLoaded", function() {
	// Pagination
	setTimeout(function() {
		var biggestHeight = 0;
		$(".books").each(function() {
			if ($(this).height() > biggestHeight) {
				biggestHeight = $(this).height() + $("div.pages").height();
			}
		});
		$(".books_container").height(biggestHeight + $("div.pages").height());
	}, 0);

	var tables = $(".content table");
	tables[0].classList.add("slide_active");
	for (let i = 1; i < tables.length; i++) {
		tables[i].classList.add("moveRight");
	}
	//
	
	// Slide show
	// Next button clicked
	$("span.next").click(function() {
		if ($(".books.slide_active").next().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))+1].classList.add("page_active");
			$(".books.slide_active").addClass("moveLeft");
			$(".books.slide_active.moveLeft").next().addClass("slide_active");
			$(".books.slide_active.moveLeft").next().removeClass("moveRight");
			$(".books.slide_active.moveLeft").removeClass("slide_active");
			
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		}
	});
	//
	// Previous button clicked
	$("span.previous").click(function() {
		if ($(".books.slide_active").prev().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))-1].classList.add("page_active");
			$(".books.slide_active").addClass("moveRight");
			$(".books.slide_active.moveRight").prev().addClass("slide_active");
			$(".books.slide_active.moveRight").prev().removeClass("moveLeft");
			$(".books.slide_active.moveRight").removeClass("slide_active");
			
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		}
	});
	//
	
	//Page number clicked
	var pages = $(".pages span");
	pages[0].classList.add("page_active");
	pages.each(function() {
		$(this).click(function() {
			var tmp = $(this);
			if (parseInt($(this).text()) <= parseInt($(".pages .page_active").text())){
				$(pages.get().reverse()).each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.add("moveRight");
					
				});
			} else {
				pages.each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.add("moveLeft");
				});
			} 
			
			$(this).addClass("page_active");
			tables[parseInt($(this).text())-1].classList.add("slide_active");
			tables[$(this).text()-1].classList.remove("moveLeft");
			tables[$(this).text()-1].classList.remove("moveRight");
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		});
	});
	//
	
	//Preview image
	var input = document.querySelector('input[type=file]');

	input.addEventListener('change', updateImageDisplay);
	
	function updateImageDisplay() {
		if (indexTmp == 4) $(".notify").text("");
		  var curFiles = input.files;
		  if(curFiles.length === 0) {
		  } else {
		    for(var i = 0; i < curFiles.length; i++) {
		      if(validFileType(curFiles[i])) {
		        var image = $(".add_form form .cover_image_container img");
		        console.log(window.URL.createObjectURL(curFiles[i]));
		        image.attr("src",window.URL.createObjectURL(curFiles[i]));
		      } else {
		      }
		    }
		  }
		}
	
	var fileTypes = [
		  'image/jpeg',
		  'image/pjpeg',
		  'image/png'
		]

		function validFileType(file) {
		  for(var i = 0; i < fileTypes.length; i++) {
		    if(file.type === fileTypes[i]) {
		      return true;
		    }
		  }

		  return false;
		}
	
	function returnFileSize(number) {
		  if(number < 1024) {
		    return number + 'bytes';
		  } else if(number >= 1024 && number < 1048576) {
		    return (number/1024).toFixed(1) + 'KB';
		  } else if(number >= 1048576) {
		    return (number/1048576).toFixed(1) + 'MB';
		  }
		}
	//
	
	var mailPattern = /^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,7}(\.[a-z]{2,5}){1,3}$/;
	var phonePattern = /^086|096[\d]{7}$/;
	
	//
	var indexTmp;
	$("#role").val("staff");
	$("form input").each(function(index) {
		$(this).keyup(function() {
			if (index == 2) {
				if ($(this).val().trim().length >= 8)
					$($("form input")[3]).val($(this).val().trim().substring(0, 8));
				else $($("form input")[3]).val("");
			}
			if ($(this).val().trim().length > 0 && index == indexTmp){
				$(".notify").text("");
				error = false;
			}
		});
	});
	var t = 0;
	var error = false;
	$("form").submit(function(e) {
		if (t == 0) {
			if ($(".btnSub").text() == "Add") {
				console.log(1);
				
				if ($($("form input")[0]).val().trim().length == 0) {
					$(".notify").text("Staff ID cannot be empty!");
					$("form input")[0].focus();
					indexTmp = 0;
					e.preventDefault();
					return;
				} else {
					$.ajax({
						url: "./staff?staff_id=" +$($("form input")[0]).val().trim(),
						type: "GET",
						data: "",
						contentType: "application/json;charset=utf-8",
						success: function(data) {
							console.log(data);
							if (data.id != null) {
								indexTmp = 0;
								error = true;
							}
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					if (error) {
						$(".notify").text("Staff ID already existed!");
						$("form input")[0].focus();
						e.preventDefault();
						return;
					} else {
						if ($($("form input")[1]).val().trim().length == 0) {
							$(".notify").text("Staff name cannot be empty!");
							$("form input")[1].focus();
							indexTmp = 1;
							e.preventDefault();
							return;
						} else {
							if ($($("form input")[2]).val().trim().length == 0) {
								$(".notify").text("Staff email cannot be empty!");
								$("form input")[2].focus();
								indexTmp = 2;
								e.preventDefault();
								return;
							} else {
								if (!mailPattern.test($($("form input")[2]).val().trim())) {
									$(".notify").text("Staff email is invalid!");
									$("form input")[2].focus();
									indexTmp = 2;
									e.preventDefault();
									return;
								} else {
									$.ajax({
										url: "./staffcheckemail?email=" +$($("form input")[2]).val().trim(),
										type: "GET",
										data: "",
										contentType: "text/plain;charset=utf-8",
										success: function(data) {
											if (data == 1) {
												error = true;
												indexTmp = 2;
											}
										},
										async: false,
										error: function (e) {
											console.log(e);
										}
									});
									if (error) {
										$(".notify").text("This email is already used!!!");
										$("form input")[2].focus();
										e.preventDefault();
										return;;
									} else {
//										if ($($("form input")[3]).val().trim().length == 0) {
//											$(".notify").text("Staff password cannot be empty!");
//											$("form input")[3].focus();
//											indexTmp = 3;
//											e.preventDefault();
//											return;
//										} else {
											$($("form input")[3]).val($($("form input")[2]).val().trim().substring(0, 9));
												if ($($("form input")[5]).val().length == 0) {
												$(".notify").text("Staff avatar must be interted!");
												var ava = confirm("Upload later?");
												if (!ava)  {
													indexTmp = 4;
													e.preventDefault();
													return;
												} else {
													t = 1;
													$("form").submit();
												}
											}
//										}
									}
								}
							}
						}
					}
				}
			} else {
				console.log(2);
				if ($($("form input")[1]).val().trim().length == 0) {
					$(".notify").text("Staff name cannot be empty!");
					$("form input")[1].focus();
					indexTmp = 1;
					e.preventDefault();
					return;
				} else {
					if ($($("form input")[2]).val().trim().length == 0) {
						$(".notify").text("Staff email cannot be empty!");
						$("form input")[2].focus();
						indexTmp = 2;
						e.preventDefault();
						return;
					} else {
						if (!mailPattern.test($($("form input")[2]).val().trim())) {
							$(".notify").text("Staff email is invalid!");
							$("form input")[2].focus();
							indexTmp = 2;
							e.preventDefault();
							return;
						} else {
							$.ajax({
								url: "./staffcheckemail?email=" +$($("form input")[2]).val().trim(),
								type: "GET",
								data: "",
								contentType: "text/plain;charset=utf-8",
								success: function(data) {
									if (data == 1) {
										$.ajax({
											url: "./staff?staff_id=" +$($("form input")[0]).val().trim(),
											type: "GET",
											data: "",
											contentType: "application/json;charset=utf-8",
											success: function(data) {
												if (data.email == $($("form input")[2]).val().trim()) {
													error = false;
												} else {
													error = true;
													indexTmp = 2;
												}
											},
											async: false,
											error: function (e) {
												console.log(e);
											}
										});
									}
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							if (error) {
								$(".notify").text("This email is already used!!!");
								$("form input")[2].focus();
								e.preventDefault();
								return;;
							} else {
//								if ($($("form input")[3]).val().trim().length == 0) {
//									$(".notify").text("Staff password cannot be empty!");
//									$("form input")[3].focus();
//									indexTmp = 3;
//									e.preventDefault();
//									return;
//								} else {
									$($("form input")[3]).val($($("form input")[2]).val().trim().substring(0, 9));
										if ($($("form input")[5]).val().length == 0) {
										$(".notify").text("Staff avatar must be interted!");
										indexTmp = 4;
										var con = confirm("Do you want to keep the current avatar?");
										if (con) {
											t = 1;
											$("form").submit();
										}else {
											e.preventDefault();
										}
									}
//								}
							}
						}
					}
				}
			}
		}
	});
	//
	
	// Add button clicked
	$(".button_add_container .button_add").click(function() {
		$(".add_form").addClass("appear");
		$(".black").addClass("blackAppear");
		
		//Change action of form input
		$(".add_form form").attr("action", "./staff");
		//
		
		//Change file input tag to img tag when remove record
		$(".add_form form .cover_image_container img").attr("src","");
		$(".add_form form .cover_image_container img").css("display", "inline-block");
		
		$(".add_form form .cover_image_container input").css("display", "inline-block");
		//
	});
	$(".black").click(function() {
		if ($(".setManangerSuccessfullAppear")[0] != undefined) return;
		if ($(".add_form form .id_container input").attr("disabled") == "disabled"){
			$(".add_form form input").removeAttr("disabled");
			
			//Change type of button to button instead of adding
			$(".add_form form button:first-child").text("Add");
			$(".add_form form button:first-child").attr("type", "submit");
			$(".add_form form button:first-child").val("");
			//
			
			//Change img tag to input tag in cover_image_container
			$(".add_form form .cover_image_container img").attr("src", "");
			$(".add_form form .cover_image_container img").css("display", "none");
			$(".add_form form .cover_image_container input").css("display", "block");
			//
			
			//Open warning window
			$(".add_form form h2 span").text("Add a new staff").css("color", "");
			//
		}
		
		//Set all input tags and select tags
		$(".add_form form input").val("");
		//
		
		$(".brokenContainer").removeClass("appear");
		$(".brokenContainer .broken button.undoBtn").each(function() {
			var broken_id = $(this).val();
			$.ajax({
				url: "../staffController/broken?broken_id=" + $(this).val() ,
					type: "GET",
					data: "",
					contentType: "application/json;charset=utf-8",
					success: function(data) {
//						console.log(data);
						$("#brokenLevel"+ broken_id).val(data.percentage);
						$("#brokenFine" + broken_id).val(data.fine);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
			});
		});
		$(".add_form").removeClass("appear");
		$(this).removeClass("blackAppear");
	});
	//
	
	//Close add form button clicked
	$(".add_form form h2 i").click(function() {
		if ($(".add_form form .id_container input").attr("disabled") == "disabled"){
			$(".add_form form input").removeAttr("disabled");
			
			//Change type of button to button instead of adding
			$(".add_form form button:first-child").text("Add");
			$(".add_form form button:first-child").attr("type", "submit");
			$(".add_form form button:first-child").val("");
			//
			
			//Change img tag to input tag in cover_image_container
			$(".add_form form .cover_image_container img").attr("src", "");
			$(".add_form form .cover_image_container img").css("display", "none");
			$(".add_form form .cover_image_container input").css("display", "block");
			//
			
			//Open warning window
			$(".add_form form h2 span").text("Add a new staff").css("color", "");
			//
		}
		
		//Set all input tags and select tags
		$(".add_form form input").val("");
		//
		
		$(".add_form").removeClass("appear");
		$(".black").removeClass("blackAppear");
	});
	//
	
	//Delete staff
	var deleteBtns = $(".books tr td button.removeBtn");
	deleteBtns.each(function() {
		$(this).click(function() {
			var deleteBtn = $(this);
			$.ajax({
				url: "../staffController/staff?staff_id=" + $(this).val(),
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function (data) {
					console.log(data);
					//Change type of button to button instead of adding
					$(".add_form form button:first-child").text("Remove?");
					$(".add_form form button:first-child").attr("type", "button");
					$(".add_form form button:first-child").val(data.id.id);
					$(".add_form form button:first-child").data("table", $(deleteBtn).data("table"));
					console.log($(deleteBtn).data("table"));
					console.log($(".add_form form button:first-child").data("table"));
					//
					
					//Disable input tag in add form
					$(".add_form form input").attr("disabled", "disabled");
//					console.log(data);
					//
					
					//Set the values of the book will be deleted
					$(".add_form form .id_container input").val(data.id.id);
					$(".add_form form .name_container input").val(data.name);
					$(".add_form form .email_container input").val(data.email);
					$($(".add_form form .password_container input")[0]).attr("readonly", "true");
					$($(".add_form form .password_container input")[0]).val(data.password);
					$($(".add_form form .password_container input")[1]).attr("readonly", "true");
					$($(".add_form form .password_container input")[1]).val(data.role);
					//
					
					//Change file input tag to img tag when remove record
					$(".add_form form .cover_image_container img:nth-child(2)").attr("src","/Library_management_system" + data.avatar);
					$(".add_form form .cover_image_container img:nth-child(2)").css("display", "inline-block");
					$(".add_form form .cover_image_container input").css("display", "none");
					//
					
					//Open warning window
					$(".add_form form h2 span").text("Waring").css("color", "red");
					$(".add_form").addClass("appear");
					$(".black").addClass("blackAppear");
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		});
	});
	//
	
	//Remove? or Save? button clicked
	$(".add_form form button:button").click(function() {
		var remove;
		if ($(this).text() == "Remove?"){
			remove = $(this);
			//Remove this record in the database
			$.ajax({
				url: "./staff?staff_id=" + $(this).val(),
				type: "DELETE",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function(data) {
					if (data == 1){
						//Remove this record in the table
//						console.log(".books tbody tr#" + $(this).val());
//						console.log($(this).data("table"));
						$(".books tbody")[remove.data("table")].removeChild($(".books tbody tr#" + remove.val())[0]);
						//
						alert("The staff removed successfull!!!");
					}
					else
						alert("Get some troubles during removing!!!\nPlease, try later!!!");
					
					$(".add_form").removeClass("appear");
					$(".black").removeClass("blackAppear");
					
					$(".add_form form input").removeAttr("disabled");
					
					//Open warning window
					$(".add_form form h2 span").text("Add a new staff").css("color", "");
					//
					
					//Set all input tags and select tags
					$(".add_form form input").val("");
					//
					
					setTimeout(function(){
						//Change type of button to button instead of adding
						$(".add_form form button:first-child").text("Add");
						$(".add_form form button:first-child").attr("type", "submit");
						$(".add_form form button:first-child").val("");
						//
					}, 500);
					location.href="./staffs";
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
			//
		}
	});
	//
	
	//Edit button clicked
	var editBtns = $(".books tr td button.modifyBtn");
	editBtns.each(function() {
		$(this).click(function(){
			editStaff($(this));
		});
	});
	//
	
	function editStaff(button) {
			var editBtn = button;
			$.ajax({
				url: "../staffController/staff?staff_id=" + button.val(),
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function (data) {
					console.log(data);
					//Change type of button to button instead of adding
					$(".add_form form button:first-child").text("Save?");
//					$(".add_form form button:first-child").attr("type", "button");
					$(".add_form form button:first-child").val(data.id.id);
					$(".add_form form button:first-child").data("table", $(editBtn).data("table"));
					//
					
					//Disable input tag in add form
					$(".add_form form .id_container input").attr("disabled", "disabled");
//					$(".add_form form select").attr("disabled", "disabled");
//					console.log(data);
					//
					
					//Set the values of the book will be deleted
					$(".add_form form .id_container input").val(data.id.id);
					$(".add_form form .name_container input").val(data.name);
					$(".add_form form .email_container input").val(data.email);
					$($(".add_form form .password_container input")[0]).attr("readonly", "true");
					$($(".add_form form .password_container input")[0]).val(data.password);
					$($(".add_form form .password_container input")[1]).attr("readonly", "true");
					$($(".add_form form .password_container input")[1]).val(data.role);
					//
					
					//Change file input tag to img tag when remove record
					$(".add_form form .cover_image_container img").attr("src","/Library_management_system" + data.avatar);
					$(".add_form form .cover_image_container img").css("display", "inline-block");
					//
					
					//
					
					//Open warning window
					$(".add_form form h2 span").text("Modify the staff").css("color", "");
					$(".add_form").addClass("appear");
					$(".black").addClass("blackAppear");
					//
					
					//Change action of form input
					$(".add_form form").attr("action", "./ustaff");
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
	}
	
	//Set as a manager
	$(".books tr td button.setManagerBtn").each(function(index) {
		$(this).click(function() {
			console.log($(this).val());
			var isSwitch = confirm("Do you really want to switch role?");
			if (isSwitch) {
				$.ajax({
					url : "../staffController/setmanager?staff_id_next=" + $(this).val().split(",")[0] + 
							"&staff_id_current=" + $(this).val().split(",")[1],
					type : "POST",
					data : "",
					contentType : "application/json",
					success : function(data) {
						$(".black").addClass("blackAppear");
						$(".setManangerSuccessfull").css({"display":"block"});
						$(".setManangerSuccessfull").addClass("setManangerSuccessfullAppear");
						$(".setManangerSuccessfull span").css({"color":"red"});
//						if ($(".warning")[0] != undefined) {
							var time = 5;
							$(".setManangerSuccessfull span:nth-child(1)").text(time);
							var timmer = setInterval(function() {
								$(".setManangerSuccessfull span:nth-child(1)").text(--time);
								if (parseInt($(".setManangerSuccessfull span:nth-child(1)").text()) == 0) {
									clearInterval(timmer);
									location.href = "../loginController/login";
								}
							}, 1000);
//						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			} else {
				
			}
		});
	});
	//The end set as a manager
	
	//
	if ($(".addSuccessful")[0] != undefined) {
		$(".addSuccessful").addClass("warningAppear");
		$(".black").addClass("blackAppear");
		$.ajax({
			url : "../staffController/resetstatus",
			type : "GET",
			data : "",
			contentType : "application/json",
			success : function(data) {
				console.log("Reset status successfully");
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
	}
	//
	
	//Broken level
	$(".brokenBtn").click(function() {
		$(".brokenContainer").addClass("appear");
		$(".black").addClass("blackAppear");
		$("nav").removeClass("navAppear");
		$(".toggle").removeClass("toggleMove");
	});
	
	$("#brokenLevelAdd").keydown(function() {
		$(".addBrokenContainer label.brokenLevelAdd").removeClass("emptyBrokenLevel");
		$(".addBrokenContainer label.brokenLevelAdd").removeClass("invalidBrokenLevel");
		$(".addBrokenContainer label.brokenLevelAdd").removeClass("amountBrokenLevel");
		if ($("#brokenFineAdd").val().trim().length == 0)
			$("#brokenFineAdd").val("");
	});
	
	$("#brokenFineAdd").keydown(function() {
		$(".addBrokenContainer label.brokenFineAdd").removeClass("emptyBrokenLevel");
		$(".addBrokenContainer label.brokenFineAdd").removeClass("invalidBrokenLevel");
		$(".addBrokenContainer label.brokenFineAdd").removeClass("amountBrokenLevel");
		if ($("#brokenFineAdd").val().trim().length == 0)
			$("#brokenFineAdd").val("");
	});
	
	$(".addBrokenBtn").click(function() {
		if ($("#brokenLevelAdd").val().trim().length > 0) {
			if (!isNaN($("#brokenLevelAdd").val())) {
				if (parseInt($("#brokenLevelAdd").val()) > 0 && parseInt($("#brokenLevelAdd").val()) <= 100) {
					if ($("#brokenFineAdd").val().trim().length > 0) {
						if (!isNaN($("#brokenFineAdd").val())) {
							if (parseInt($("#brokenFineAdd").val()) > 0 && parseInt($("#brokenFineAdd").val()) <= 100) {
								$.ajax({
									url: "../staffController/broken?broken_level=" + $("#brokenLevelAdd").val().trim() +
										"&broken_fine=" + $("#brokenFineAdd").val().trim(),
										type: "POST",
										data: "",
										contentType: "application/json;charset=utf-8",
										success: function(data) {
											$("button.addBrokenBtn").addClass("addSuccessfull");
											setTimeout(function(){location.href="../staffController/staffs";},1000);
										},
										async: false,
										error: function (e) {
											console.log(e);
										}
								});
							} else {
								$(".addBrokenContainer label.brokenFineAdd").addClass("amountBrokenLevel");
							}
						} else {
							$(".addBrokenContainer label.brokenFineAdd").addClass("invalidBrokenLevel");
						}
					} else {
						$(".addBrokenContainer label.brokenFineAdd").addClass("emptyBrokenLevel");
					}
				} else {
					$(".addBrokenContainer label.brokenLevelAdd").addClass("amountBrokenLevel");
				}
			} else {
				$(".addBrokenContainer label.brokenLevelAdd").addClass("invalidBrokenLevel");
			}
		} else {
			$(".addBrokenContainer label.brokenLevelAdd").addClass("emptyBrokenLevel");
		}
	});
	var check;
	$(".removeBrokenBtn").click(function() {
		check = confirm("Do you really want to delete this one?");
		if (check) {
			$.ajax({
				url: "../staffController/broken?broken_id=" + $(this).val(),
					type: "DELETE",
					data: "",
					contentType: "application/json;charset=utf-8",
					success: function(data) {
						$("button.addBrokenBtn").addClass("removeSuccessfull");
						setTimeout(function(){location.href="../staffController/staffs";},1000);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
			});
		}
	});
	
	$(".modifyBrokenBtn").click(function() {
		check = confirm("Do you really want to modify this one?");
		if (check) {
			$.ajax({
				url: "../staffController/ubroken?broken_id=" + $(this).val() + 
				"&broken_level=" + $("#brokenLevel"+$(this).val()).val().trim() +
				"&broken_fine=" + $("#brokenFine" + $(this).val()).val().trim(),
					type: "POST",
					data: "",
					contentType: "application/json;charset=utf-8",
					success: function(data) {
						$("button.addBrokenBtn").addClass("updateSuccessfull");
						setTimeout(function(){location.href="../staffController/staffs";},1000);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
			});
		}
	});
	
	$(".undoBtn").click(function() {
		var borken_id = $(this).val();
		$.ajax({
			url: "../staffController/broken?broken_id=" + $(this).val() ,
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function(data) {
					console.log(data);
					$("#brokenLevel"+ borken_id).val(data.percentage);
					$("#brokenFine" + borken_id).val(data.fine);
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
		});
	});
	//The end of broken level
});