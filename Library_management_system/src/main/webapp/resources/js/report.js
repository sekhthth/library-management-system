window.addEventListener("DOMContentLoaded", function() {
	// Pagination
	setTimeout(function() {
		var biggestHeight = 0;
		$(".books").each(function() {
			if ($(this).height() > biggestHeight) {
				biggestHeight = $(this).height() + $("div.pages").height();
			}
		});
		$(".books_container").height(biggestHeight + $("div.pages").height());
	}, 0);

	var tables = $(".content table");
	tables[0].classList.add("slide_active");
	for (let i = 1; i < tables.length; i++) {
		tables[i].classList.add("moveRight");
	}
	//
	
	//Black
	$(".black").click(function() {
		$(".brokenContainer").removeClass("appear");
		$(".brokenContainer .broken button.undoBtn").each(function() {
			var broken_id = $(this).val();
			$.ajax({
				url: "../staffController/broken?broken_id=" + $(this).val() ,
					type: "GET",
					data: "",
					contentType: "application/json;charset=utf-8",
					success: function(data) {
						console.log(data);
						$("#brokenLevel"+ broken_id).val(data.percentage);
						$("#brokenFine" + broken_id).val(data.fine);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
			});
		});
		if ($(".add_form form .id_container input").attr("disabled") == "disabled"){
			$(".add_form form input").removeAttr("disabled");
			$(".add_form form select").removeAttr("disabled");
			
			//Change type of button to button instead of adding
			$(".add_form form button:first-child").text("Add");
			$(".add_form form button:first-child").attr("type", "submit");
			$(".add_form form button:first-child").val("");
			//
			
			//Change img tag to input tag in cover_image_container
			$(".add_form form .cover_image_container img").attr("src", "");
			$(".add_form form .cover_image_container img").css("display", "none");
			$(".add_form form .cover_image_container input").css("display", "block");
			//
			
			//Open warning window
			$(".add_form form h2 span").text("Add a new book").css("color", "");
			//
		}
		
		//Set all input tags and select tags
		$(".add_form form input").val("");
		$(".add_form form select").val("MAMA");
		//
		
		$(".add_form").removeClass("appear");
		$(this).removeClass("blackAppear");
	});
	//
	
	
	// Slide show
	// Next button clicked
	$("span.next").click(function() {
		if ($(".books.slide_active").next().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))+1].classList.add("page_active");
			$(".books.slide_active").addClass("moveLeft");
			$(".books.slide_active.moveLeft").next().addClass("slide_active");
			$(".books.slide_active.moveLeft").next().removeClass("moveRight");
			$(".books.slide_active.moveLeft").removeClass("slide_active");
		}
	});
	//
	// Previous button clicked
	$("span.previous").click(function() {
		if ($(".books.slide_active").prev().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))-1].classList.add("page_active");
			$(".books.slide_active").addClass("moveRight");
			$(".books.slide_active.moveRight").prev().addClass("slide_active");
			$(".books.slide_active.moveRight").prev().removeClass("moveLeft");
			$(".books.slide_active.moveRight").removeClass("slide_active");
		}
	});
	//
	
	//Page number clicked
	var pages = $(".pages span");
	pages[0].classList.add("page_active");
	pages.each(function() {
		$(this).click(function() {
			var tmp = $(this);
			if (parseInt($(this).text()) <= parseInt($(".pages .page_active").text())){
				$(pages.get().reverse()).each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.add("moveRight");
					
				});
			} else {
				pages.each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.add("moveLeft");
				});
			} 
			
			$(this).addClass("page_active");
			tables[parseInt($(this).text())-1].classList.add("slide_active");
			tables[$(this).text()-1].classList.remove("moveLeft");
			tables[$(this).text()-1].classList.remove("moveRight");
		});
	});
	//
	//
	
	//Info button clicked
	$("table.books button:first-child").each(function() {
		$(this).click(function() {
			console.log($(this).val());
			//Make report area
			var thTableBorrowAndReturn = "<tr>\r\n" + 
			"                    <th>No.</th>\r\n" + 
			"                    <th>Borrower id</th>\r\n" + 
			"                    <th>Borrower name</th>\r\n" + 
			"                    <th>Borrow date</th>\r\n" + 
			"                    <th>Return date</th>\r\n" + 
			"                    <th id=\"removed\">Action</th>\r\n" + 
			"                </tr>";
			var thTableReservate = "<tr>\r\n" + 
			"                    <th>No.</th>\r\n" + 
			"                    <th>Reservation person id</th>\r\n" + 
			"                    <th>Reservation person name</th>\r\n" + 
			"                    <th>Reservation date</th>\r\n" + 
			"                    <th id=\"removed\">Action</th>\r\n" + 
			"                </tr>";
			var tdTableBorrowAndReturn = "<tr>\r\n" + 
			"                    <td>#borrow_id</td>\r\n" + 
			"                    <td>#borrower_id</td>\r\n" + 
			"                    <td>#borrower_name</td>\r\n" + 
			"                    <td>#borrow_date</td>\r\n" + 
			"                    <td>#return_date</td>\r\n" + 
			"                    <td id=\"removed\"><button id=\"#borrow_id\">More</button></td>\r\n" + 
			"                </tr>";
			var tdTableBorrowAndReturnTmp;
			var tdTableReservate = "<tr>\r\n" + 
			"                    <td>#reservation_id</td>\r\n" + 
			"                    <td>#reservation_person_id</td>\r\n" + 
			"                    <td>#reservation_person_name</td>\r\n" + 
			"                    <td>#reservation_date</td>\r\n" + 
			"                    <td id=\"removed\"><button id=\"#reservation_id\">More</button></td>\r\n" + 
			"                </tr>";
			var tdTableReservateTmp;
			var thTableMore = "<tr>\r\n" + 
			"                <th>No.</th>\r\n" + 
			"                <th>Book id</th>\r\n" + 
			"                <th>Book name</th>\r\n" + 
			"                <th>Category</th>\r\n" + 
			"                <th>Edition</th>\r\n" + 
			"                <th>Year manufacture</th>\r\n" + 
			"                <th>Manufacturer</th>\r\n" + 
			"                <th>Cover image</th>\r\n" + 
			"            </tr>";
			var tdTableMore = "<tr>\r\n" + 
			"                <td>#id</td>\r\n" + 
			"                <td>#book_id</td>\r\n" + 
			"                <td>#book_name</td>\r\n" + 
			"                <td>#book_category</td>\r\n" + 
			"                <td>#book_edition</td>\r\n" + 
			"                <td>#book_yearmanufacture</td>\r\n" + 
			"                <td>#book_manufacturer</td>\r\n" + 
			"                <td><img alt=\"\" src='/Library_management_system/#img'></td>\r\n" + 
			"            </tr>";
			var tdTableMoreTmp;
			
			var tableBorrow;
			var tableReservate;
			var tableReturn;
			var tableBorrowList;
			var tableReservateList;
			var tableReturnList;
			var report_date_report;
			//Make report clicked
//			$(".makeReportBtn").click(function() {
				$(".reportContainer").addClass("reportContainerAppear");
				$(".black").addClass("blackAppear");
				$("nav").removeClass("navAppear");
				$(".toggle").removeClass("toggleMove");
				
				tableBorrow = $(".borrow table");
				tableReservate = $(".reservate table");
				tableReturn = $(".return table");
				console.log(tableBorrow);
				console.log(tableReservate);
				console.log(tableReturn);
				
				//Get current date
				var current_date;
				$.ajax({
					url: "./report?report_id="+$(this).val(),
					type: "GET",
					data: "",
					contentType: "application/json;charset=utf-8",
					success: function(data) {
						current_date = data.report_date;
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				//
				
				$(".report_date span").text(current_date);
				//The end of get current date
				
				//Add record into borrow table
				$.ajax({
					url: "../borrowController/recordswithoutreturn?current_date=" + current_date + "&status=0",
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						//data is list of borrow records without return
						tableBorrowList = data;
						console.log(tableBorrowList);
						
						var contentHtml = "";
						var borrower;
						var urlTmp;
						for (var i = 0; i < tableBorrowList[0].length; i++){
							
							if (tableBorrowList[1][i].id.person_id.substring(0, 2) == "LE")
								urlTmp = "../lecturerController/lecturer?id=" + tableBorrowList[1][i].id.person_id;
							else
								urlTmp = "../studentController/student?id=" + tableBorrowList[1][i].id.person_id;
							
							$.ajax({
								url: urlTmp,
								type: "GET",
								data: "",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
									borrower = data;
//									console.log(borrower);
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							
							tdTableBorrowAndReturnTmp = tdTableBorrowAndReturn;
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrow_id", tableBorrowList[0][i].id.id);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrower_id", tableBorrowList[1][i].id.person_id);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrower_name", borrower.name);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrow_date", tableBorrowList[0][i].borrow_date);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#return_date", tableBorrowList[0][i].return_date);
							contentHtml += tdTableBorrowAndReturnTmp;
						}
						
						$(".borrow table").html(thTableBorrowAndReturn + contentHtml);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				//
				
				//Add record into reservate table
				$.ajax({
					url: "../reservateController/recordswithoutreturn?current_date=" + current_date,
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						//data is list of reservation records
						tableReservateList = data;
						console.log(tableReservateList);
						
						var contentHtml = "";
						var borrower;
						var urlTmp;
						for (var i = 0; i < tableReservateList[0].length; i++){
							
							if (tableReservateList[1][i].id.person_id.substring(0, 2) == "LE")
								urlTmp = "../lecturerController/lecturer?id=" + tableReservateList[1][i].id.person_id;
							else
								urlTmp = "../studentController/student?id=" + tableReservateList[1][i].id.person_id;
							
							$.ajax({
								url: urlTmp,
								type: "GET",
								data: "",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
									borrower = data;
//									console.log(borrower);
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							
							tdTableReservateTmp = tdTableReservate;
							tdTableReservateTmp = 
								tdTableReservateTmp.replace("#reservation_id", tableReservateList[0][i].id.id);
							tdTableReservateTmp = 
								tdTableReservateTmp.replace("#reservation_person_id", tableReservateList[1][i].id.person_id);
							tdTableReservateTmp = 
								tdTableReservateTmp.replace("#reservation_person_name", borrower.name);
							tdTableReservateTmp = 
								tdTableReservateTmp.replace("#reservation_date", tableReservateList[0][i].reservate_date);
							contentHtml += tdTableReservateTmp;
						}
						
						$(".reservate table").html(thTableReservate + contentHtml);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				//
				
				//Add record into return table
				$.ajax({
					url: "../borrowController/recordswithoutreturn?current_date=" + current_date + "&status=1",
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						//data is list of borrow records within return
						tableReturnList = data;
						console.log(tableReturnList);
						
						var contentHtml = "";
						var borrower;
						var urlTmp;
						for (var i = 0; i < tableReturnList[0].length; i++){
							
							if (tableReturnList[1][i].id.person_id.substring(0, 2) == "LE")
								urlTmp = "../lecturerController/lecturer?id=" + tableReturnList[1][i].id.person_id;
							else
								urlTmp = "../studentController/student?id=" + tableReturnList[1][i].id.person_id;
							
							$.ajax({
								url: urlTmp,
								type: "GET",
								data: "",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
									borrower = data;
//									console.log(borrower);
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							
							tdTableBorrowAndReturnTmp = tdTableBorrowAndReturn;
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrow_id", tableReturnList[0][i].id.id);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrower_id", tableReturnList[1][i].id.person_id);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrower_name", borrower.name);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#borrow_date", tableReturnList[0][i].borrow_date);
							tdTableBorrowAndReturnTmp = 
								tdTableBorrowAndReturnTmp.replace("#return_date", tableReturnList[0][i].return_date);
							contentHtml += tdTableBorrowAndReturnTmp;
						}
						
						$(".return table").html(thTableBorrowAndReturn + contentHtml);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				//
				
				//Get more button in table borrow
				if ($(".borrow table button").length > 0) {
					console.log($(".borrow table button"));
					$(".borrow table button").each(function(index) {
						$(this).click(function() {
							console.log("Button more : "+index);
							$(".black2").addClass("black2Appear");
							$(".moreContainer").addClass("reportContainerAppear");
							
							//Get books in this borrow record
							$.ajax({
								url: "../borrowController/booksinborrowlog?borrow_log_id=" + tableBorrowList[0][index].id.id
								+"&type=0",
								type: "GET",
								data: "",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
									console.log(data);
									var contentHtml = "";
									
									for (var i = 0; i < data.length; i++){
										tdTableMoreTmp = tdTableMore;
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#id", i);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_id", data[i].id.id.split("-")[0]);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_name", data[i].name);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_category", data[i].id.id.split("-")[1]);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_edition", data[i].timePrint);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_yearmanufacture", data[i].ymanufacture);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_manufacturer", data[i].manufacturer);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#img", data[i].cover_image);
										contentHtml += tdTableMoreTmp;
									}
									
									$(".moreContainer table").html(thTableMore + contentHtml);
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							//
						});
					});
				}
				//
				
				//Get more button in table reservate
				if ($(".reservate table button").length > 0) {
					console.log($(".reservate table button"));
					$(".reservate table button").each(function(index) {
						$(this).click(function() {
							console.log("Button more : "+index);
							$(".black2").addClass("black2Appear");
							$(".moreContainer").addClass("reportContainerAppear");
							
							//Get books in this borrow record
							$.ajax({
								url: "../reservateController/booksinresevatelog?reservate_log_id=" + tableReservateList[0][index].id.id,
								type: "GET",
								data: "",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
									console.log(data);
									var contentHtml = "";
									
									for (var i = 0; i < data.length; i++){
										tdTableMoreTmp = tdTableMore;
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#id", i);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_id", data[i].id.id.split("-")[0]);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_name", data[i].name);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_category", data[i].id.id.split("-")[1]);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_edition", data[i].timePrint);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_yearmanufacture", data[i].ymanufacture);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_manufacturer", data[i].manufacturer);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#img", data[i].cover_image);
										contentHtml += tdTableMoreTmp;
									}
									
									$(".moreContainer table").html(thTableMore + contentHtml);
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							//
						});
					});
				}
				//
				
				//Get more button in table return
				if ($(".return table button").length > 0) {
					console.log($(".return table button"));
					$(".return table button").each(function(index) {
						$(this).click(function() {
							console.log("Button more : "+index);
							$(".black2").addClass("black2Appear");
							$(".moreContainer").addClass("reportContainerAppear");
							
							//Get books in this borrow record
							$.ajax({
								url: "../borrowController/booksinborrowlog?borrow_log_id=" + tableReturnList[0][index].id.id
										+"&type=1",
								type: "GET",
								data: "",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
									console.log(data);
									var contentHtml = "";
									
									for (var i = 0; i < data.length; i++){
										tdTableMoreTmp = tdTableMore;
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#id", i);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_id", data[i].id.id.split("-")[0]);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_name", data[i].name);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_category", data[i].id.id.split("-")[1]);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_edition", data[i].timePrint);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_yearmanufacture", data[i].ymanufacture);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#book_manufacturer", data[i].manufacturer);
										tdTableMoreTmp = 
											tdTableMoreTmp.replace("#img", data[i].cover_image);
										contentHtml += tdTableMoreTmp;
									}
									
									$(".moreContainer table").html(thTableMore + contentHtml);
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							//
						});
					});
				}
				//
//			});
			//The end of make report clicked
		});
	});
	//The end of info button clicked
	
	//Broken level
	$(".brokenBtn").click(function() {
		$(".brokenContainer").addClass("appear");
		$(".black").addClass("blackAppear");
		$("nav").removeClass("navAppear");
		$(".toggle").removeClass("toggleMove");
	});
	
	$("#brokenLevelAdd").keydown(function() {
		$(".addBrokenContainer label.brokenLevelAdd").removeClass("emptyBrokenLevel");
		$(".addBrokenContainer label.brokenLevelAdd").removeClass("invalidBrokenLevel");
		$(".addBrokenContainer label.brokenLevelAdd").removeClass("amountBrokenLevel");
		if ($("#brokenFineAdd").val().trim().length == 0)
			$("#brokenFineAdd").val("");
	});
	
	$("#brokenFineAdd").keydown(function() {
		$(".addBrokenContainer label.brokenFineAdd").removeClass("emptyBrokenLevel");
		$(".addBrokenContainer label.brokenFineAdd").removeClass("invalidBrokenLevel");
		$(".addBrokenContainer label.brokenFineAdd").removeClass("amountBrokenLevel");
		if ($("#brokenFineAdd").val().trim().length == 0)
			$("#brokenFineAdd").val("");
	});
	
	$(".addBrokenBtn").click(function() {
		if ($("#brokenLevelAdd").val().trim().length > 0) {
			if (!isNaN($("#brokenLevelAdd").val())) {
				if (parseInt($("#brokenLevelAdd").val()) > 0 && parseInt($("#brokenLevelAdd").val()) <= 100) {
					if ($("#brokenFineAdd").val().trim().length > 0) {
						if (!isNaN($("#brokenFineAdd").val())) {
							if (parseInt($("#brokenFineAdd").val()) > 0 && parseInt($("#brokenFineAdd").val()) <= 100) {
								$.ajax({
									url: "../staffController/broken?broken_level=" + $("#brokenLevelAdd").val().trim() +
										"&broken_fine=" + $("#brokenFineAdd").val().trim(),
										type: "POST",
										data: "",
										contentType: "application/json;charset=utf-8",
										success: function(data) {
											$("button.addBrokenBtn").addClass("addSuccessfull");
											setTimeout(function(){location.href="../staffController/staffs";},1000);
										},
										async: false,
										error: function (e) {
											console.log(e);
										}
								});
							} else {
								$(".addBrokenContainer label.brokenFineAdd").addClass("amountBrokenLevel");
							}
						} else {
							$(".addBrokenContainer label.brokenFineAdd").addClass("invalidBrokenLevel");
						}
					} else {
						$(".addBrokenContainer label.brokenFineAdd").addClass("emptyBrokenLevel");
					}
				} else {
					$(".addBrokenContainer label.brokenLevelAdd").addClass("amountBrokenLevel");
				}
			} else {
				$(".addBrokenContainer label.brokenLevelAdd").addClass("invalidBrokenLevel");
			}
		} else {
			$(".addBrokenContainer label.brokenLevelAdd").addClass("emptyBrokenLevel");
		}
	});
	var check;
	$(".removeBrokenBtn").click(function() {
		check = confirm("Do you really want to delete this one?");
		if (check) {
			$.ajax({
				url: "../staffController/broken?broken_id=" + $(this).val(),
					type: "DELETE",
					data: "",
					contentType: "application/json;charset=utf-8",
					success: function(data) {
						$("button.addBrokenBtn").addClass("removeSuccessfull");
						setTimeout(function(){location.href="../reportController/reports";},1000);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
			});
		}
	});
	
	$(".modifyBrokenBtn").click(function() {
		check = confirm("Do you really want to modify this one?");
		if (check) {
			$.ajax({
				url: "../staffController/ubroken?broken_id=" + $(this).val() + 
				"&broken_level=" + $("#brokenLevel"+$(this).val()).val().trim() +
				"&broken_fine=" + $("#brokenFine" + $(this).val()).val().trim(),
					type: "POST",
					data: "",
					contentType: "application/json;charset=utf-8",
					success: function(data) {
						$("button.addBrokenBtn").addClass("updateSuccessfull");
						setTimeout(function(){location.href="../reportController/reports";},1000);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
			});
		}
	});
	
	$(".undoBtn").click(function() {
		var borken_id = $(this).val();
		$.ajax({
			url: "../staffController/broken?broken_id=" + $(this).val() ,
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function(data) {
					console.log(data);
					$("#brokenLevel"+ borken_id).val(data.percentage);
					$("#brokenFine" + borken_id).val(data.fine);
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
		});
	});
	//The end of broken level
});