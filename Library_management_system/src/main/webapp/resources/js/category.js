window.addEventListener("DOMContentLoaded", function() {
	// Pagination
	setTimeout(function() {
		var biggestHeight = 0;
		$(".books").each(function() {
			if ($(this).height() > biggestHeight) {
				biggestHeight = $(this).height() + $("div.pages").height();
			}
		});
		$(".books_container").height(biggestHeight + $("div.pages").height());
	}, 0);

	var tables = $(".content table");
	tables[0].classList.add("slide_active");
	for (let i = 1; i < tables.length; i++) {
		tables[i].classList.add("moveRight");
	}
	//
	
	// Slide show
	// Next button clicked
	$("span.next").click(function() {
		if ($(".books.slide_active").next().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))+1].classList.add("page_active");
			$(".books.slide_active").addClass("moveLeft");
			$(".books.slide_active.moveLeft").next().addClass("slide_active");
			$(".books.slide_active.moveLeft").next().removeClass("moveRight");
			$(".books.slide_active.moveLeft").removeClass("slide_active");
			
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		}
	});
	//
	// Previous button clicked
	$("span.previous").click(function() {
		if ($(".books.slide_active").prev().prop("tagName") == "TABLE") {
			pages[tables.index($(".books.slide_active"))].classList.remove("page_active");
			pages[tables.index($(".books.slide_active"))-1].classList.add("page_active");
			$(".books.slide_active").addClass("moveRight");
			$(".books.slide_active.moveRight").prev().addClass("slide_active");
			$(".books.slide_active.moveRight").prev().removeClass("moveLeft");
			$(".books.slide_active.moveRight").removeClass("slide_active");
			
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		}
	});
	//
	
	//Page number clicked
	var pages = $(".pages span");
	pages[0].classList.add("page_active");
	pages.each(function() {
		$(this).click(function() {
			var tmp = $(this);
			if (parseInt($(this).text()) <= parseInt($(".pages .page_active").text())){
				$(pages.get().reverse()).each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.add("moveRight");
				});
			} else {
				pages.each(function() {
					console.log($(this));
					if ($(this).is(tmp)) return false;
					$(this).removeClass("page_active");
					tables[parseInt($(this).text())-1].classList.remove("slide_active");
					tables[parseInt($(this).text())-1].classList.remove("moveLeft");
					tables[parseInt($(this).text())-1].classList.remove("moveRight");
					tables[parseInt($(this).text())-1].classList.add("moveLeft");
				});
			} 
			
			$(this).addClass("page_active");
			tables[parseInt($(this).text())-1].classList.add("slide_active");
			tables[$(this).text()-1].classList.remove("moveLeft");
			tables[$(this).text()-1].classList.remove("moveRight");
			if ($(".content .books_container").html() != "")
				$(".content .books_container").height($(".content .books.slide_active").height()+2*$(".content div.pages").height());
			else
				$(".searchContainer .books_container").height($(".searchContainer .books.slide_active").height()+2*$(".searchContainer div.pages").height());
		});
	});
	//
	//
	
	//
	var indexTmp;
	
	$("form input").each(function(index) {
		$(this).keyup(function() {
			if ($(this).val().trim().length > 0 && index == indexTmp)
				$(".notify").text("");
		});
	});
	var t = 0;
	var error = false;
	$("form").submit(function(e) {
		if (t == 0) {
			if ($(".btnSub").text() == "Add") {
				console.log(1);
				if ($($("form input")[0]).val().trim().length == 0) {
					$(".notify").text("Category ID cannot be empty!");
					$("form input")[0].focus();
					indexTmp = 0;
					e.preventDefault();
					return;
				} else {
					$.ajax({
						url: "./category?category_id=" +$($("form input")[0]).val().trim(),
						type: "GET",
						data: "",
						contentType: "application/json;charset=utf-8",
						success: function(data) {
							console.log(data);
							if (data.id != null) {
								indexTmp = 0;
								error = true;
							}
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					if (error) {
						$(".notify").text("Category ID already existed!");
						$("form input")[0].focus();
						e.preventDefault();
						return;
					} else {
						if ($($("form input")[1]).val().trim().length == 0) {
							$(".notify").text("Category name cannot be empty!");
							$("form input")[1].focus();
							indexTmp = 1;
							e.preventDefault();
							return;
						} else {
							t = 1;
							$("form").submit();
						}
					}
				}
			} else {
				console.log(2);
				if ($($("form input")[1]).val().trim().length == 0) {
					$(".notify").text("Staff name cannot be empty!");
					$("form input")[1].focus();
					indexTmp = 1;
					e.preventDefault();
					return;
				}
				t = 1;
				$("form").submit();
			}
		}
	});
	//
	
	// Add button clicked
	$(".button_add_container .button_add").click(function() {
		$(".add_form").addClass("appear");
		$(".black").addClass("blackAppear");
		
		//Change action of form input
		$(".add_form form").attr("action", "./category");
		//
	});
	$(".black").click(function() {
		if ($(".add_form form .id_container input").attr("disabled") == "disabled"){
			$(".add_form form input").removeAttr("disabled");
			
			//Change type of button to button instead of adding
			$(".add_form form button:first-child").text("Add");
			$(".add_form form button:first-child").attr("type", "submit");
			$(".add_form form button:first-child").val("");
			//
			
			//Open warning window
			$(".add_form form h2 span").text("Add a new category").css("color", "");
			//
		}
		
		//Set all input tags and select tags
		$(".add_form form input").val("");
		//
		
		$(".add_form").removeClass("appear");
		$(this).removeClass("blackAppear");
	});
	//
	
	//Close add form button clicked
	$(".add_form form h2 i").click(function() {
		if ($(".add_form form .id_container input").attr("disabled") == "disabled"){
			$(".add_form form input").removeAttr("disabled");
			
			//Change type of button to button instead of adding
			$(".add_form form button:first-child").text("Add");
			$(".add_form form button:first-child").attr("type", "submit");
			$(".add_form form button:first-child").val("");
			//
			
			//Open warning window
			$(".add_form form h2 span").text("Add a new category").css("color", "");
			//
		}
		
		//Set all input tags and select tags
		$(".add_form form input").val("");
		//
		
		$(".add_form").removeClass("appear");
		$(".black").removeClass("blackAppear");
	});
	//
	
	//Delete category
	var deleteBtns = $(".books tr td button:first-child");
	deleteBtns.each(function() {
		$(this).click(function() {
			var deleteBtn = $(this);
			$.ajax({
				url: "../categoryController/category?category_id=" + $(this).val(),
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function (data) {
					//Change type of button to button instead of adding
					$(".add_form form button:first-child").text("Remove?");
					$(".add_form form button:first-child").attr("type", "button");
					$(".add_form form button:first-child").val(data.id.id);
					$(".add_form form button:first-child").data("table", $(deleteBtn).data("table"));
					console.log($(deleteBtn).data("table"));
					console.log($(".add_form form button:first-child").data("table"));
					//
					
					//Disable input tag in add form
					$(".add_form form input").attr("disabled", "disabled");
//					console.log(data);
					//
					
					//Set the values of the book will be deleted
					$(".add_form form .id_container input").val(data.id.id);
					$(".add_form form .name_container input").val(data.name);
					//
					
					//Open warning window
					$(".add_form form h2 span").text("Waring").css("color", "red");
					$(".add_form").addClass("appear");
					$(".black").addClass("blackAppear");
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		});
	});
	//
	
	//Remove? or Save? button clicked
	$(".add_form form button:button").click(function() {
		var remove;
		if ($(this).text() == "Remove?"){
			remove = $(this);
			//Remove this record in the database
			$.ajax({
				url: "./category?category_id=" + $(this).val(),
				type: "DELETE",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function(data) {
					if (data == 1){
						//Remove this record in the table
//						console.log(".books tbody tr#" + $(this).val());
//						console.log($(this).data("table"));
						$(".books tbody")[remove.data("table")].removeChild($(".books tbody tr#" + remove.val())[0]);
						//
						alert("The category removed successfull!!!");
					}
					else
						alert("Get some troubles during removing!!!\nPlease, try later!!!");
					
					$(".add_form").removeClass("appear");
					$(".black").removeClass("blackAppear");
					
					$(".add_form form input").removeAttr("disabled");
					
					//Open warning window
					$(".add_form form h2 span").text("Add a new category").css("color", "");
					//
					
					//Set all input tags and select tags
					$(".add_form form input").val("");
					//
					
					setTimeout(function(){
						//Change type of button to button instead of adding
						$(".add_form form button:first-child").text("Add");
						$(".add_form form button:first-child").attr("type", "submit");
						$(".add_form form button:first-child").val("");
						//
					}, 500)
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
			//
		}else {
			
		}
	});
	//
	
	//Edit button clicked
	var editBtns = $(".books tr td button:last-child");
	editBtns.each(function() {
		$(this).click(function(){
			var editBtn = $(this);
			$.ajax({
				url: "./category?category_id=" + $(this).val(),
				type: "GET",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function (data) {
					//Change type of button to button instead of adding
					$(".add_form form button:first-child").text("Save?");
					$(".add_form form button:first-child").val(data.id.id);
					$(".add_form form button:first-child").data("table", $(editBtn).data("table"));
					//
					
					//Disable input tag in add form
					$(".add_form form .id_container input").attr("disabled", "disabled");
					//
					
					//Set the values of the book will be deleted
					$(".add_form form .id_container input").val(data.id.id);
					$(".add_form form .name_container input").val(data.name);
					//
					
					//Open warning window
					$(".add_form form h2 span").text("Modify the book").css("color", "");
					$(".add_form").addClass("appear");
					$(".black").addClass("blackAppear");
					//
					
					//Change action of form input
					$(".add_form form").attr("action", "./ucategory");
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		});
	});
	//
	
	//
	if ($(".addSuccessful")[0] != undefined) {
		$(".addSuccessful").addClass("warningAppear");
		$(".black").addClass("blackAppear");
		$.ajax({
			url : "../categoryController/resetstatus",
			type : "GET",
			data : "",
			contentType : "application/json",
			success : function(data) {
				console.log("Reset status successfully");
			}
		});
	}
	//
});