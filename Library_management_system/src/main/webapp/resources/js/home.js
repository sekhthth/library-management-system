window.addEventListener("DOMContentLoaded", function () {
	var student =
		"						<div class=\"studentResult\" id=\"#divId\">\r\n" +
		"							<div class=\"studentResultLeft\">\r\n" +
		"								<h3 class=\"studentResultLeftId\">#id</h3>\r\n" +
		"								<h3 class=\"studentResultLeftName\">#name</h3>\r\n" +
		"								<h3 class=\"studentResultLeftDepartment\">#department</h3>\r\n" +
		"								<h3 class=\"studentResultLeftEmail\">#email</h3>\r\n" +
		"								<h3 class=\"studentResultLeftPhone\">#phone</h3>\r\n" +
		"							</div>\r\n" +
		"							<div class=\"studentResultRight\">\r\n" +
		"								<img alt=\"\"\r\n" +
		"									src='/Library_management_system#img'>\r\n" +
		"							</div>\r\n" +
		"						</div>\r\n";
	var book = "<div class=\"bookResult\" id=\"#divId\">\r\n" +
		"							<div class=\"bookResultLeft\">\r\n" +
		"								<h3 class=\"bookResultLeftId\">#id</h3>\r\n" +
		"								<h3 class=\"bookResultLeftName\">#name</h3>\r\n" +
		"								<h3 class=\"bookResultTimePrint\">#timeprint</h3>\r\n" +
		"								<h3 class=\"bookResultYManufacture\">#ymanufacuture</h3>\r\n" +
		"								<h3 class=\"bookResultManufacturer\">#manufacturer</h3>\r\n" +
		"								<h3 class=\"bookResultCategory\">#category</h3>\r\n" +
		"							</div>\r\n" +
		"							<div class=\"bookResultRight\">\r\n" +
		"								<img alt=\"\"\r\n" +
		"									src=\"/Library_management_system#img\">\r\n" +
		"							</div>\r\n" +
		"						</div>";
	var bookReturn = "<div class=\"bookResult\">\r\n" + 
		"						<div class=\"bookResultLeft\">\r\n" + 
		"							<h3 class=\"bookResultLeftId\">#id</h3>\r\n" +
		"							<h3 class=\"bookResultLeftName\">#name</h3>\r\n" +
		"							<h3 class=\"bookResultTimePrint\">#timeprint</h3>\r\n" +
		"							<h3 class=\"bookResultYManufacture\">#ymanufacuture</h3>\r\n" +
		"							<h3 class=\"bookResultManufacturer\">#manufacturer</h3>\r\n" +
		"							<h3 class=\"bookResultCategory\">#category</h3>\r\n" +
		"						</div>\r\n" + 
		"						<div class=\"bookResultRight return\">\r\n" + 
		"							<img alt=\"\"\r\n" + 
		"								src=\"/Library_management_system#img\">\r\n" + 
		"							<div class=\"penalties\" id=\"#penalties_book_id\">\r\n" +
		"								<div>\r\n" + 
		"									<label for=\"penalty#order\">1. Late</label> <input name=\"penalty#order\"\r\n" + 
		"										value=\"1\" #pen_1 data-broken=\"\" type=\"radio\">\r\n" + 
		"								</div>\r\n" + 
		"								<div>\r\n" + 
		"									<label for=\"penalty#order\">2. Miss</label> <input name=\"penalty#order\" value=\"2\"\r\n" + 
		"										#pen_2 data-broken=\"\" type=\"radio\">\r\n" + 
		"								</div>\r\n" + 
		"								<div>\r\n" + 
		"									<label for=\"penalty#order\">3. Other</label> <input name=\"penalty#order\" value=\"3\"\r\n" + 
		"										#pen_3 data-broken=\"\" type=\"radio\">\r\n" + 
		"								</div>\r\n" + 
		"								<div>\r\n" + 
		"									<label for=\"penalty#order\">4. Normal</label> <input name=\"penalty#order\"\r\n" + 
		"										#pen_4 data-broken=\"\" value=\"4\" type=\"radio\">\r\n" + 
		"								</div>\r\n" + 
		"								<div>\r\n" + 
		"									<label for=\"penalty#order\">5. None</label> <input name=\"penalty#order\"\r\n" + 
		"										#pen_5 data-broken=\"\" value=\"5\" type=\"radio\">\r\n" + 
		"								</div>"
		"							</div>\r\n" + 
		"						</div>\r\n" + 
		"					</div>";
	var returnCard = "<div class=\"bookResult\">\r\n" + 
	"					<div class=\"bookResultLeft\">\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bookResultLeftId\">Borrow card id :</h3>\r\n" + 
	"							<h3>#id</h3>\r\n" + 
	"						</div>\r\n" +
	"						<div>\r\n" + 
	"							<h3 class=\"bookResultLeftName\">Borrower id :</h3>\r\n" + 
	"							<h3>#borrower</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bookResultLeftName\">Borrower name:</h3>\r\n" + 
	"							<h3>#borrower_id</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bokkResultTimePrint\">Borrow date :</h3>\r\n" + 
	"							<h3>#borrow</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bokkResultYManufacture\">Return date :</h3>\r\n" + 
	"							<h3>#return</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bokkResultManufacturer\">Status :</h3>\r\n" + 
	"							<h3>#status</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bokkResultCategory\">Staff :</h3>\r\n" + 
	"							<h3>#staff</h3>\r\n" + 
	"						</div>\r\n" + 
	"					</div>\r\n" + 
	"				</div>";
	var reservationCard = "<div class=\"bookResult\">\r\n" + 
	"					<div class=\"bookResultLeft\">\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bookResultLeftId\">Reservation card id :</h3>\r\n" + 
	"							<h3>#id</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bookResultLeftName\">Reservation person :</h3>\r\n" + 
	"							<h3>#reservation_person</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bokkResultTimePrint\">Reservation date :</h3>\r\n" + 
	"							<h3>#reservation_date</h3>\r\n" + 
	"						</div>\r\n" +
	"						<div>\r\n" + 
	"							<h3 class=\"bokkResultManufacturer\">Status :</h3>\r\n" + 
	"							<h3>#status</h3>\r\n" + 
	"						</div>\r\n" + 
	"						<div>\r\n" + 
	"							<h3 class=\"bokkResultCategory\">Staff :</h3>\r\n" + 
	"							<h3>#staff</h3>\r\n" + 
	"						</div>\r\n" + 
	"					</div>\r\n" + 
	"				</div>";
	//Variables for cards
	var borrowerContent = "<div>\r\n" + 
	"                <h4>Borrower id : </h4>\r\n" + 
	"                <h4>#borrower_id</h4>\r\n" + 
	"            </div>\r\n" + 
	"            <div>\r\n" + 
	"                <h4>Borrower name : </h4>\r\n" + 
	"                <h4>#borrower_name</h4>\r\n" + 
	"            </div>";
	var borrowerContentTmp;
	var borrow_logContent = "<div>\r\n" + 
	"                <h4>Borrow id : </h4>\r\n" + 
	"                <h4>#borrow_id</h4>\r\n" + 
	"            </div>\r\n" + 
	"            <div>\r\n" + 
	"                <h4>Borrow date : </h4>\r\n" + 
	"                <h4>#borrow_date</h4>\r\n" + 
	"            </div>\r\n" + 
	"            <div>\r\n" + 
	"                <h4>Return date : </h4>\r\n" + 
	"                <h4>#return_date</h4>\r\n" + 
	"            </div>";
	var borrow_logContentTmp;
	var staffContent = "<div>\r\n" + 
	"                <h4>Staff id : </h4>\r\n" + 
	"                <h4>#staff_id</h4>\r\n" + 
	"            </div>\r\n" + 
	"            <div>\r\n" + 
	"                <h4>Staff name : </h4>\r\n" + 
	"                <h4>#staff_name</h4>\r\n" + 
	"            </div>";
	var bookPunishmentCard = "<div class=\"penalty_record\">\r\n" + 
	"                <p><span>#order.</span> #book_id - #book_name</p>\r\n" + 
	"                <p><span>Borrow card id : </span>#borrow_card_id</p>\r\n" + 
	"                <p><span>Penalty : </span>#penalty</p>\r\n" + 
	"                <p><span>From : </span>#from</p>\r\n" + 
	"                <p><span>To : </span>#to</p>\r\n" + 
	"                <p><span>Status : </span>#status</p>\r\n" + 
	"            </div>";
	var bookFullCard = " <div class=\"penalty_record\">\r\n" + 
	"                <p><span>#order. </span>#book_id - #book_name</p>\r\n" + 
	"                <p><span>Borrow card id : </span>#borrow_card_id</p>\r\n" + 
	"                <p><span>From : </span>#from</p>\r\n" + 
	"                <p><span>To : </span>#to</p>\r\n" + 
	"                <p><span>Status : </span>#status</p>\r\n" + 
	"            </div>";
	var staffContentTmp;
	var booksContent = "<h2 class=\"small_title\">List book</h2>";
	var booksContentTmp;
	var bookContent = "<p>#book</p>";
	var bookContentTmp;
	//End variable for cards
	var studentTmp;
	var staffTmp;
	var maximum = 5;
	
	//$(".penalties input").each(function(){console.log($(this).prop("checked"));})
	
	//
	function getStaffTmp(){
		$.ajax({
			url: "../staffController/staff?staff_id=" + $(".staffInfo .studentResult .studentResultLeft .studentResultLeftId").text(),
			type: "GET",
			data: "",
			contentType: "application/json",
			success: function (data) {
				staffTmp = data;;
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
	}
	if ($(".staffInfo .studentResult .studentResultLeft .studentResultLeftId").text() != "")
		getStaffTmp();
	//
	
	//Student ID input tag
	var studentResultsContainer = false;
	var bookResultsContainer = false;
	document.querySelector("body").addEventListener("click", function () {
		var elementClicked = document.activeElement;
		if (elementClicked != document.querySelector(".student input") && !studentResultsContainer) {
			document.querySelector(".studentResultsContainer").classList.remove("studentResultsContainerAppear");
		}
		if (elementClicked != document.querySelector(".book input") && !bookResultsContainer) {
			if (document.querySelector(".bookResultsContainer") != null)
				document.querySelector(".bookResultsContainer").classList.remove("bookResultsContainerAppear");
		}
	});

	document.querySelector(".student input").addEventListener("focus", function () {
		if (document.querySelector(".studentId").value.toUpperCase().trim().length == 0)
			document.querySelector(".studentResultsContainer .studentResults").innerHTML = "";
		document.querySelector(".studentResultsContainer").classList.add("studentResultsContainerAppear");
	});
	document.querySelector(".student input").addEventListener("blur", function () {
		if (!studentResultsContainer)
			document.querySelector(".studentResultsContainer").classList.remove("studentResultsContainerAppear");
	});
	document.querySelector(".book input").addEventListener("focus", function () {
		document.querySelector(".bookResultsContainer").classList.add("bookResultsContainerAppear");
	});
	document.querySelector(".book input").addEventListener("focusout", function () {
		if (!bookResultsContainer)
			document.querySelector(".bookResultsContainer").classList.remove("bookResultsContainerAppear");
	});
	//
	//Results Container
	//Student
	document.querySelector(".studentResultsContainer").addEventListener("mouseenter", function () {
		studentResultsContainer = true;
	});
	document.querySelector(".studentResultsContainer").addEventListener("mouseleave", function () {
		studentResultsContainer = false;
	});
	//Book
	document.querySelector(".bookResultsContainer").addEventListener("mouseenter", function () {
		bookResultsContainer = true;
	});
	document.querySelector(".bookResultsContainer").addEventListener("mouseleave", function () {
		bookResultsContainer = false;
	});
	//
	
	$(".feature button:first-child").click(function() {
		if (!$(this).hasClass("featureBtnActive")) {
			$(".bookInfo").removeClass("returnFeature");
			$(".studentInfo").removeClass("returnFeature");
			$(".returnInfo").addClass("returnFeature");
			$(".takeInfo").addClass("returnFeature");
		}
		removeAllFeatureBtnActiveClass(1);
		reset();
	});
	
	$(".feature button:nth-child(2)").click(function() {
		if (!$(this).hasClass("featureBtnActive")) {
			$(".bookInfo").addClass("returnFeature");
			$(".studentInfo").addClass("returnFeature");
			$(".returnInfo").removeClass("returnFeature");
			$(".takeInfo").addClass("returnFeature");
		}
		removeAllFeatureBtnActiveClass(2);
		reset();
	});
	
	$(".feature button:nth-child(3)").click(function() {
		if (!$(this).hasClass("featureBtnActive")) {
			$(".bookInfo").addClass("returnFeature");
			$(".studentInfo").addClass("returnFeature");
			$(".returnInfo").addClass("returnFeature");
			$(".takeInfo").removeClass("returnFeature");
		}
		removeAllFeatureBtnActiveClass(4);
		reset();
	});
	
	$(".feature button:nth-child(4)").click(function() {
		var current_screen;
		$(".feature button").each(function(index) {
			if ($(this).hasClass("featureBtnActive"))
				current_screen = index+1;
		});
		switch(current_screen) {
		case 1 :
			$(".bookInfo").removeClass("returnFeature");
			$(".studentInfo").removeClass("returnFeature");
			$(".returnInfo").addClass("returnFeature");
			$(".takeInfo").addClass("returnFeature");
			break;
		case 2 :
			$(".bookInfo").addClass("returnFeature");
			$(".studentInfo").addClass("returnFeature");
			$(".returnInfo").removeClass("returnFeature");
			$(".takeInfo").addClass("returnFeature");
			break;
		case 3 :
			$(".bookInfo").addClass("returnFeature");
			$(".studentInfo").addClass("returnFeature");
			$(".returnInfo").addClass("returnFeature");
			$(".takeInfo").removeClass("returnFeature");
			current_screen++;
			break;
		};
		removeAllFeatureBtnActiveClass(current_screen);
		reset();
//		$(this).removeClass("featureBtnActive");
		console.log("screen " + current_screen);
	});
	
	
	function removeAllFeatureBtnActiveClass(order) {
		$(".feature button").each(function() {
			$(this).removeClass("featureBtnActive");
		});
		
		$(".generatingOrReturning button").each(function() {
			$(this).addClass("returnFeature");
		});
		
		$(".content input").attr("disabled", false);
		
		if (order != 0) {
			$(`.generatingOrReturning button:nth-child(${order})`).removeClass("returnFeature");
			if (order == 4)
				$(`.feature button:nth-child(${order-1})`).addClass("featureBtnActive");
			else
				$(`.feature button:nth-child(${order})`).addClass("featureBtnActive");
		}
	}
	//

	//Check student
	document.querySelector(".student input").addEventListener("keyup", function () {
		//Remove previous student results
		document.querySelector(".studentResultsContainer .studentResults").innerHTML = "";
		//
		if (document.querySelector(".studentId").value.toUpperCase().trim().length != 0) {
			var studentTemp;
			var content = "";
//			setTimeout(function(){},800);
			$.ajax({
				url: "./student?id=" + document.querySelector(".studentId").value.toUpperCase().trim(),
				type: "POST",
				data: "",
				contentType: "application/json;charset=utf-8",
				success: function (data) {
					console.log(data);
					for (var i = 0; i < data[0][0].length+data[0][1].length; i++) {
						studentTemp = student;
						if (i < data[0][0].length) {
							studentTemp = studentTemp.replace("#divId", data[0][0][i].id.id);
							studentTemp = studentTemp.replace("#id", data[0][0][i].id.id);
							studentTemp = studentTemp.replace("#name", data[0][0][i].name);
							studentTemp = studentTemp.replace("#email", data[0][0][i].email);
							studentTemp = studentTemp.replace("#phone", data[0][0][i].phone);
							studentTemp = studentTemp.replace("#img", data[0][0][i].avatar);
						} else {
							studentTemp = studentTemp.replace("#divId", data[0][1][i-data[0][0].length].id.id);
							studentTemp = studentTemp.replace("#id", data[0][1][i-data[0][0].length].id.id);
							studentTemp = studentTemp.replace("#name", data[0][1][i-data[0][0].length].name);
							studentTemp = studentTemp.replace("#email", data[0][1][i-data[0][0].length].email);
							studentTemp = studentTemp.replace("#phone", data[0][1][i-data[0][0].length].phone);
							studentTemp = studentTemp.replace("#img", data[0][1][i-data[0][0].length].avatar);
						}
						
						studentTemp = studentTemp.replace("#department", data[1][i]);
						content += studentTemp;
//						document.querySelector(".studentResultsContainer .studentResults").innerHTML = document.querySelector(".studentResultsContainer .studentResults").innerHTML + studentTemp;
					}
					
					document.querySelector(".studentResultsContainer .studentResults").innerHTML = content;

					//Student result click event
					for (var i = 0; i < document.querySelectorAll(".studentResult").length; i++) {
						document.querySelectorAll(".studentResult")[i].addEventListener("dblclick", function () {
							//Hind result container
							document.querySelector(".studentResultsContainer").classList.remove("studentResultsContainerAppear");
							//
							var studentTemp = student;
							$.ajax({
								url: "./student?id=" + this.id,
								type: "POST",
								data: "",
								dataType: 'json',
								contentType: "application/json; charset=utf-8",
								success: function (data) {
									var person;
									console.log(data);
									if (data[0][0].length > 0)
										person = data[0][0][0];
									else
										person = data[0][1][0];
									studentTmp = person;
									console.log(person);
									
									studentTemp = studentTemp.replace("#divId", person.id.id);
									studentTemp = studentTemp.replace("#id", person.id.id);
									studentTemp = studentTemp.replace("#name", person.name);
									studentTemp = studentTemp.replace("#department", data[1][0]);
									studentTemp = studentTemp.replace("#email", person.email);
									studentTemp = studentTemp.replace("#phone", person.phone);
									studentTemp = studentTemp.replace("#img", person.avatar);

//									if (document.querySelector(".feature button").textContent == "Borrow") {
										if (document.querySelector(".bookInfo .studentResult") != null)
											document.querySelector(".bookInfo").removeChild(document.querySelector(".bookInfo .studentResult"));
//										var innerHTML = document.querySelector(".bookInfo").innerHTML;
//										document.querySelector(".bookInfo").innerHTML = "";
//										document.querySelector(".bookInfo").innerHTML = studentTemp + innerHTML;
//									} else {
										document.querySelector(".studentBorrow").innerHTML = "";
										document.querySelector(".studentBorrow").innerHTML = studentTemp;
//									}
								}
							});
						});
					}
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		}
	});
	//

	//Check Book
	var books_borrow_list = [];
	document.querySelector(".book input").addEventListener("keyup", function () {
		console.log(1);
		//Remove previous book results
		document.querySelector(".bookResultsContainer .bookResults").innerHTML = "";
		//
//		if (document.querySelector(".feature button").textContent == "Return") {
			if (document.querySelector(".bookId").value.toUpperCase().trim().length != 0) {
				var bookTemp;
//				setTimeout(function(){},800);
				$.ajax({
					url: "./book?id=" + document.querySelector(".bookId").value.toUpperCase().trim(),
					type: "POST",
					data: "",
					dataType: 'json',
					contentType: "application/json; charset=utf-8",
					success: function (data) {
						for (var i = 0; i < data[0].length; i++) {
							bookTemp = book;

							bookTemp = bookTemp.replace("#divId", data[0][i].id.id);
							bookTemp = bookTemp.replace("#id", data[0][i].id.id);
							bookTemp = bookTemp.replace("#name", data[0][i].name);
							bookTemp = bookTemp.replace("#timeprint", data[0][i].timePrint);
							bookTemp = bookTemp.replace("#ymanufacuture", data[0][i].ymanufacture);
							bookTemp = bookTemp.replace("#manufacturer", data[0][i].manufacturer);
							bookTemp = bookTemp.replace("#category", data[1][i].name);
							bookTemp = bookTemp.replace("#borrow", data[0][i].is_borrowed == 1 ? "Borrowed" : "Available");
							bookTemp = bookTemp.replace("#img", data[0][i].cover_image);
							document.querySelector(".bookResultsContainer .bookResults").innerHTML = document.querySelector(".bookResultsContainer .bookResults").innerHTML + bookTemp;
						}

						//Book result click event
						for (var i = 0; i < document.querySelectorAll(".bookResult").length; i++) {
							document.querySelectorAll(".bookResult")[i].addEventListener("dblclick", function () {
								//Hind result container
								document.querySelector(".bookResultsContainer").classList.remove("bookResultsContainerAppear");
								//
								//Check if selected book was in list book or has not
								if (books_borrow_list.includes(this.id)) {
									alert("This book has existed in list book!!!");
								} else if (books_borrow_list.length >= 5){
									alert("You are only allowed borrow at most five books!!!");
								} else {
									//Add this book into an array
									books_borrow_list.push(this.id);
									//
									var bookTemp = book;
									$.ajax({
										url: "./book?id=" + this.id.split("-")[0],
										type: "POST",
										data: "",
										dataType: 'json',
										contentType: "application/json; charset=utf-8",
										success: function (data) {
											bookTemp = bookTemp.replace("#divId", data[0][0].id.id);
											bookTemp = bookTemp.replace("#id", data[0][0].id.id);
											bookTemp = bookTemp.replace("#name", data[0][0].name);
											bookTemp = bookTemp.replace("#timeprint", data[0][0].timePrint);
											bookTemp = bookTemp.replace("#ymanufacuture", data[0][0].ymanufacture);
											bookTemp = bookTemp.replace("#manufacturer", data[0][0].manufacturer);
											bookTemp = bookTemp.replace("#category", data[1][0].name);
//											bookTemp = bookTemp.replace("#borrow", data[0][0].is_borrowed == 1 ? "Borrowed" : "Available");
											bookTemp = bookTemp.replace("#img", data[0][0].cover_image);

											document.querySelector(".listBooks").innerHTML = document.querySelector(".listBooks").innerHTML + bookTemp;

											//Remove a book from list books (in borrow fearture)
											for (var i = 0; i < document.querySelectorAll(".listBooks .bookResult").length; i++) {
												document.querySelectorAll(".listBooks .bookResult")[i].addEventListener("dblclick", function () {
													books_borrow_list.splice(books_borrow_list.indexOf(this.id), 1);
													document.querySelector(".listBooks").removeChild(this);
													console.log(books_borrow_list);
												});
											}
											//
										},
										async: false,
										error: function (e) {
											console.log(e);
										}
									});
								}
//								console.log(books_borrow_list.length);
								//
							});
						}
						//
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
//		}
	});
	//
	
	//Checkout button clicked
	var books_out_of_available_list = [];
	var isReser = false;
	var tmp = [];
	$(".checkOutBtn").click(function() {
		//Check if borrower is selected or not
		if ($(".studentBorrow").children().length == 0){
			alert("Please, select the borrower!!!");
			document.querySelector(".studentId").focus();
			return;
		}
		//
		
		//Kiểm tra có đang bị phạt không
		var isPunished = false;
		$.ajax({
			url: "../penaltyController/ispunished?person_id=" + studentTmp.id.id,
			type: "GET",
			data: "",
			contentTpe: "application/json;charset=utf-8",
			success: function(data) {
				var htmlTmp = $(".penalties").html();
				$(".penalties").html("");
				$(".penalties").addClass("penalty");
				$(".penalties").html(htmlTmp);
				$(".punishmentCard .big_title").text("You are being punished");
				$(".punishmentCard .small_title").text("Penalties");
				if (data.length > 0) {
					console.log(data);
					var bookPunishmentCardTmp;
					var bookTmpPunish;
					for (var i = 0; i < data.length; i++) {
						bookPunishmentCardTmp = bookPunishmentCard;
						bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#borrow_card_id", data[i].borrow_log_id.id.id);
						bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#from", data[i].start);
						var start = new Date(data[i].start);
						start.setDate(start.getDate() + 30);
						start = start.getFullYear() + "-" + (start.getMonth()+1) + "-" + 
						start.getDate() + " " + start.toString().slice(16, 24);
						bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#to", start);
						$.ajax({
							url : "../bookController/book?book_id=" + data[i].book_id.id.id,
							type: "GET",
							data: "",
							contentType: "application/json; charset=utf-8",
							success: function(data) {
								bookTmpPunish = data;
								bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#order", `${i+1}`);
								bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#book_id", data.id.id);
								bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#book_name", data.name);
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
						switch(data[i].penalty) {
						case 1:
							bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#penalty", "You are not allowed to borrow any books for 30 days.");
							$.ajax({
								url: "../borrowController/borrowlogdetail?borrow_log_id=" + data[i].borrow_log_id.id.id +
									 "&book_id=" + data[i].book_id.id.id,
								type: "GET",
								data: "",
								contentType: "text/plain; charset=utf-8",
								success: function(data) {
									console.log(data);
									if (data.penalty == 1)
										bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#status", "Uncompleted");
									else
										bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#status", "Completed");
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							break;
						case 2:
							bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#penalty", "You are not allowed to borrow any books for 30 days and You have to pay money for the book you lost.");
							$.ajax({
								url: "../borrowController/borrowlogdetail?borrow_log_id=" + data[i].borrow_log_id.id.id +
									 "&book_id=" + data[i].book_id.id.id,
								type: "GET",
								data: "",
								contentType: "text/plain; charset=utf-8",
								success: function(data) {
									console.log(data);
									if (data.penalty == 2)
										bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#status", "Uncompleted");
									else
										bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#status", "Completed");
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							break;
						case 3:
							var percentage;
							$.ajax({
								url: "../homeController/otherpercentage?penalty_log_id=" + data[i].id,
								type: "GET",
								data: "",
								contentType: "text/plain; charset=utf-8",
								success: function(data) {
									percentage = data;
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#penalty", "You are not allowed to borrow any books for 30 days " +
																					"and You must pay money " +
																					"("+((percentage.fine/100)*bookTmpPunish.price) + 
																					"VND = " + percentage.fine + "% of " + 
																					bookTmpPunish.price + "VND.)");
							$.ajax({
								url: "../borrowController/borrowlogdetail?borrow_log_id=" + data[i].borrow_log_id.id.id + 
									 "&book_id=" + data[i].book_id.id.id,
								type: "GET",
								data: "",
								contentType: "text/plain; charset=utf-8",
								success: function(data) {
									console.log(data);
									if (data.penalty == 3)
										bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#status", "Uncompleted");
									else
										bookPunishmentCardTmp = bookPunishmentCardTmp.replace("#status", "Completed");
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							break;
						default:
							break;
						}
						$(".penalties").html($(".penalties").html()+bookPunishmentCardTmp);
					}
					
					$(".punishmentCard").addClass("reportContainerAppear");
					$(".black").addClass("blackAppear");
					
					isPunished = true;
					$(".feature button:nth-child(4)").click()
				}
				console.log(data)
					
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
		if (isPunished)
			return;
//		console.log("dung day");
//		return;
		//Kết thúc kiểm tra đang bị phạt
		
		//Kiểm tra mượn đủ 5 quyển chưa
		var list = listForIsFull(studentTmp.id.id);
		console.log(list);
		if (list.length == 5) {
			var htmlTmp = $(".penalties").html();
			$(".penalties").html("");
			$(".penalties").html(htmlTmp);
			$(".punishmentCard .big_title").text("You are only allowed to borrow at most 5 books.");
			$(".punishmentCard .small_title").text("Borrowed books");
			
			var bookFullCardTmp;
			for (var i = 0 ; i < list.length; i++) {
				bookFullCardTmp = bookFullCard;
				bookFullCardTmp = bookFullCardTmp.replace("#borrow_card_id", list[i].id.borrow_log_id);
				$.ajax({
					url : "../bookController/book?book_id=" + list[i].id.book_id,
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						bookFullCardTmp = bookFullCardTmp.replace("#order", `${i+1}`);
						bookFullCardTmp = bookFullCardTmp.replace("#book_id", data.id.id);
						bookFullCardTmp = bookFullCardTmp.replace("#book_name", data.name);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				
				$.ajax({
					url: "../borrowController/getborrowlog?borrow_log_id=" + list[i].id.borrow_log_id,
					type: "GET",
					data: "",
					contentType: "text/plain; charset=utf-8",
					success: function(data) {
						bookFullCardTmp = bookFullCardTmp.replace("#from", data.borrow_date);
						bookFullCardTmp = bookFullCardTmp.replace("#to", data.return_date);
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				
				if (list[i].penalty != 4)
					bookFullCardTmp = bookFullCardTmp.replace("#status", "Uncompleted");
				else
					bookFullCardTmp = bookFullCardTmp.replace("#status", "Completed");
				$(".penalties").html($(".penalties").html()+bookFullCardTmp);
			}
			
			$(".punishmentCard").addClass("reportContainerAppear");
			$(".black").addClass("blackAppear");
			$(".feature button:nth-child(4)").click();
			return;
		}
		
		//Kết thúc kiểm tra mượn đủ 5 quyển
		
		//Check if there is at least one book selected or not
		if (books_borrow_list.length == 0){
			alert("Please, select at least one book!!!");
			document.querySelector(".bookId").focus();
			return;
		}
		//
		
		//check những quyển ko bị out of xem nó có mượn trc đó chưa
//		console.log(books_borrow_list);
		var books_borrowed_before_list_string = "";
		var books_borrowed_before_list = [];
		if (books_borrow_list.length != 0){
			console.log(books_borrow_list);
			for (var i = 0; i < books_borrow_list.length; i++){
				$.ajax({
					url : "../borrowController/borrowed?book_id=" + books_borrow_list[i] + "&person_id=" + studentTmp.id.id,
					type: "GET",
					data: "",
					contentType: "text/plain; charset=utf-8",
					success: function(data) {
						if (data == 1){
							books_borrowed_before_list.push(books_borrow_list[i].trim());
							var book_name;
							$.ajax({
								url : "../bookController/book?book_id=" + books_borrow_list[i],
								type: "GET",
								data: "",
								contentType: "text/plain; charset=utf-8",
								success: function(data) {
									book_name = data.name;
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							if (i != books_borrow_list.length-1){
								books_borrowed_before_list_string += `${i}. ${book_name}\n`;
							}else {
								books_borrowed_before_list_string += `${i}. ${book_name}.\n`;
							}
							
							$(".listBooksContainer .listBooks")[0].removeChild($(`.listBooksContainer .bookResult#${books_borrow_list[i].trim()}`)[0]);
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
			if (books_borrowed_before_list.length != 0){
				//Delete the book that borrowed before
				for (var i = 0; i < books_borrowed_before_list.length; i++)
					books_borrow_list.splice($.inArray(books_borrowed_before_list[i], books_borrow_list),1);
				//
				console.log(books_borrowed_before_list_string);
				alert("There are several books that you borrowed without returning :\n" + books_borrowed_before_list_string + "So, these book will be removed from list.");
			}
			
			if(books_borrow_list.length == 0){
					reset();
					resetUI();
					alert("Currently, there is no book for borrowing.\nPlease, come back later!");
			} else {
				
				//For out of available list
				var books_out_of_available_list_string = "";
				for (var i = 0; i < books_borrow_list.length; i++){
					$.ajax({
						url :"../bookController/outof?book_id=" + books_borrow_list[i],
						type: "GET",
						data: "",
						contentType: "text/plain; charset=utf-8",
						success: function(data) {
							if (data == 1){
								books_out_of_available_list.push(books_borrow_list[i].trim());
								var book_name = `.bookResult#${books_borrow_list[i].trim()} .bookResultLeftName`;
								if (i != books_borrow_list.length-1){
									books_out_of_available_list_string += `${i}. ${$(book_name).length==1?$($(book_name)[0]).text():$($(book_name)[1]).text()}\n`;
								}else {
									books_out_of_available_list_string += `${i}. ${$(book_name).length==1?$($(book_name)[0]).text():$($(book_name)[1]).text()}.\n`;
								}
								
								$(".listBooksContainer .listBooks")[0].removeChild($(`.listBooksContainer .bookResult#${books_borrow_list[i].trim()}`)[0]);
							}
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
				}
				console.log(books_borrow_list);
				console.log(books_out_of_available_list);
				//Delete the book that are out of available list from borrow list
				for (var i = 0; i < books_out_of_available_list.length; i++)
					books_borrow_list.splice($.inArray(books_out_of_available_list[i], books_borrow_list),1);
				console.log(books_borrow_list);
				//
				if (books_out_of_available_list.length != 0) {
					isReser = confirm("There are several books that are out of the borrowable list :\n" + books_out_of_available_list_string + 
							"Do you want to reservate them?");
					
					//Kiểm tra những quyển chuẩn bị đặt trước có cuốn nào nó đã đặt chưa
					if (isReser){
						var books_reservated_before_list = [];
						var books_reservated_before_list_string = "";
						for (var i = 0; i < books_out_of_available_list.length; i++){
							$.ajax({
								url : "../reservateController/reservated?book_id=" + books_out_of_available_list[i] + "&person_id=" + studentTmp.id.id,
								type: "GET",
								data: "",
								contentType: "text/plain; charset=utf-8",
								success: function(data) {
									if (data == 1){
										books_reservated_before_list.push(books_out_of_available_list[i].trim());
										var book_name;
										$.ajax({
											url : "../bookController/book?book_id=" + books_out_of_available_list[i],
											type: "GET",
											data: "",
											contentType: "text/plain; charset=utf-8",
											success: function(data) {
												book_name = data.name;
											},
											async: false,
											error: function (e) {
												console.log(e);
											}
										});
										if (i != books_borrow_list.length-1){
											books_reservated_before_list_string += `${i}. ${book_name}\n`;
										}else {
											books_reservated_before_list_string += `${i}. ${book_name}.\n`;
										}
									}
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
						}
						console.log(books_reservated_before_list);
						
						if (books_reservated_before_list.length != 0){
							//Delete the book that reservated before
							for (var i = 0; i < books_reservated_before_list.length; i++)
								books_out_of_available_list.splice($.inArray(books_reservated_before_list[i], books_out_of_available_list),1);
							//
							
							alert("There are several books that you reservated before :\n" + books_reservated_before_list_string + ".\nSo these books will be remove from your order list.");
							
							if(books_out_of_available_list.length == 0){
								alert("Currently, your order list is empty!");
							} else {
								isReser = confirm("Do you want to reservate the left books?");
								if (isReser) {
									alert("You will move to reservation screen after complete borrowing!");
								}
							}
						}
						tmp = books_out_of_available_list;
					}else {
						//Xóa khỏi list
						books_out_of_available_list = [];
						//
					}
					//
				}
				//Kết thúc Kiểm tra out of
				
				if(books_borrow_list.length == 0){
					if (tmp.length > 0 ){
						alert("Currently, there is no book for borrowing.\nPlease, come back later!\nLet move to reservation screen.");
						$(".reservateBtn").css({"display":"inline-block"});
						$(".checkOutBtn").css({"display":"none"});
						$(".studentId").attr("disabled", "disabled");
						$(".bookId").attr("disabled", "disabled");
						$(".checkOutBtn").addClass("returnFeature");
						$(".reservateBtn").removeClass("returnFeature");
						addBookToReservationScreen(tmp);
					} else {
						reset();
						resetUI();
						alert("Currently, there is no book for borrowing.\nPlease, come back later!");
					}
				} else {
					//Kiểm tra những quyển cb được mươn xem có quyển nào đã được đặt chưa
					var books_reservated_before_list = [];
					var books_reservated_before_list_string = "";
					for (var i = 0; i < books_borrow_list.length; i++){
						$.ajax({
							url : "../reservateController/reservated?book_id=" + books_borrow_list[i] + "&person_id=" + studentTmp.id.id,
							type: "GET",
							data: "",
							contentType: "text/plain; charset=utf-8",
							success: function(data) {
								if (data == 1){
									books_reservated_before_list.push(books_borrow_list[i].trim());
									var book_name;
									$.ajax({
										url : "../bookController/book?book_id=" + books_borrow_list[i],
										type: "GET",
										data: "",
										contentType: "text/plain; charset=utf-8",
										success: function(data) {
											book_name = data.name;
										},
										async: false,
										error: function (e) {
											console.log(e);
										}
									});
									if (i != books_borrow_list.length-1){
										books_reservated_before_list_string += `${i}. ${book_name}\n`;
									}else {
										books_reservated_before_list_string += `${i}. ${book_name}.\n`;
									}
									
									$(".listBooksContainer .listBooks")[0].removeChild($(`.listBooksContainer .bookResult#${books_borrow_list[i].trim()}`)[0]);
								}
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
					}
					console.log(books_reservated_before_list);
					if (books_reservated_before_list.length != 0){
						//Delete the book that reservated before
						for (var i = 0; i < books_reservated_before_list.length; i++)
							books_borrow_list.splice($.inArray(books_reservated_before_list[i], books_borrow_list),1);
						//
						
						alert("There are several books that you reservated before :\n" + books_reservated_before_list_string + ".\nSo these books will be remove from your current list.");
					}//Kết thúc kiểm tra những quyển cb được mươn xem có quyển nào đã được đặt chưa
					if(books_borrow_list.length == 0){
						reset();
						resetUI();
						alert("Currently, there is no book for borrowing.\nPlease, come back later!");
					} else {
						console.log("cb, mượn");

						//Xử lý mượn
						var borrow_date = new Date();
						borrow_date = borrow_date.getFullYear() + "-" + (borrow_date.getMonth()+1) + "-" + 
						borrow_date.getDate() + " " + borrow_date.toString().slice(16, 24);
						var return_date = new Date(borrow_date);
						return_date.setHours(17,0,0,0);
						return_date.setDate(return_date.getDate() + 30);
						return_date = return_date.getFullYear() + "-" + (return_date.getMonth()+1) + "-" + 
						return_date.getDate() + " " + return_date.toString().slice(16, 24);
						var current_id;
//						console.log(borrow_date);
//						console.log(return_date);
						
						$.ajax({
							url : "../borrowController/currentid",
							type: "GET",
							data: "",
							contentType: "text/plain; charset=utf-8",
							success: function(data) {
								current_id = data;
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
//						return;
						
						//
						borrowerContentTmp = borrowerContent;
						borrowerContentTmp = borrowerContentTmp.replace("#borrower_id", studentTmp.id.id);
						borrowerContentTmp = borrowerContentTmp.replace("#borrower_name", studentTmp.name);
						
						borrow_logContentTmp = borrow_logContent;
						borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_id", current_id);
						borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_date", borrow_date);
						borrow_logContentTmp = borrow_logContentTmp.replace("#return_date", return_date);
						
						staffContentTmp = staffContent;
						staffContentTmp = staffContentTmp.replace("#staff_id", staffTmp.id.id);
						staffContentTmp = staffContentTmp.replace("#staff_name", staffTmp.name);
						
						booksContentTmp = booksContent;
						//
						
						//Kiểm tra đã mượn đủ 5 quyền
						var list = listForIsFull(studentTmp.id.id);
						if (maximum < list.length + books_borrow_list.length) {
							var htmlTmp = $(".penalties").html();
							$(".penalties").html("");
							$(".penalties").html(htmlTmp);
							$(".punishmentCard .big_title").text("You are only allowed to borrow at most 5 books.\nYou borrowed " + 
									list.length + " books.\nSo, you are allowed to borrow " + (maximum-list.length) + " more.");
							$(".punishmentCard .small_title").text("Borrowed books");
							
							var bookFullCardTmp;
							for (var i = 0 ; i < list.length; i++) {
								bookFullCardTmp = bookFullCard;
								bookFullCardTmp = bookFullCardTmp.replace("#borrow_card_id", list[i].id.borrow_log_id);
								$.ajax({
									url : "../bookController/book?book_id=" + list[i].id.book_id,
									type: "GET",
									data: "",
									contentType: "application/json; charset=utf-8",
									success: function(data) {
										bookFullCardTmp = bookFullCardTmp.replace("#order", `${i+1}`);
										bookFullCardTmp = bookFullCardTmp.replace("#book_id", data.id.id);
										bookFullCardTmp = bookFullCardTmp.replace("#book_name", data.name);
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
								
								$.ajax({
									url: "../borrowController/getborrowlog?borrow_log_id=" + list[i].id.borrow_log_id,
									type: "GET",
									data: "",
									contentType: "text/plain; charset=utf-8",
									success: function(data) {
										bookFullCardTmp = bookFullCardTmp.replace("#from", data.borrow_date);
										bookFullCardTmp = bookFullCardTmp.replace("#to", data.return_date);
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
								
								if (list[i].penalty != 4)
									bookFullCardTmp = bookFullCardTmp.replace("#status", "Uncompleted");
								else
									bookFullCardTmp = bookFullCardTmp.replace("#status", "Completed");
								$(".penalties").html($(".penalties").html()+bookFullCardTmp);
							}
							
							$(".punishmentCard").addClass("reportContainerAppear");
							$(".black").addClass("blackAppear");
							return;
						}
						console.log("here");
//						return;
						//Kết thuc kiểm tra đã mượn 5 quyển
						
						for (var i = 0; i < books_borrow_list.length; i++){
							$.ajax({
								url : "../borrowController/borrow?book_id=" + books_borrow_list[i] + "&person_id=" + studentTmp.id.id + 
										"&borrow_date=" + borrow_date + "&return_date=" + return_date + 
										"&staff_id=" + staffTmp.id.id + "&current_id=" + current_id,
								type: "POST",
								data: "",
								contentType: "text/plain; charset=utf-8",
								success: function(data) {
									
									$.ajax({
										url: "../bookController/book?book_id=" + `${books_borrow_list[i]}`,
										type: "GET",
										data: "",
										contentType: "application/json;charset=utf-8",
										success: function(data) {
											bookContentTmp = bookContent;
											bookContentTmp = bookContentTmp.replace("#book", `${i+1}. ` + data.id.id + " - " + data.name);
										
											booksContentTmp += bookContentTmp;
										},
										async: false,
										error: function (e) {
											console.log(e);
										}
									});
									
									if (`${i}` == `${books_borrow_list.length-1}`){
										if (tmp.length > 0 ){
											alert("You borrowed successfull!!!\nLet move to reservation screen!!!");
											
											$(".borrowCard .borrower").html(borrowerContentTmp);
											$(".borrowCard .borrow_log").html(borrow_logContentTmp);
											$(".borrowCard .staff").html(staffContentTmp);
											$(".borrowCard .books").html(booksContentTmp);
											
											$(".borrowCard").addClass("reportContainerAppear");
											$(".black").addClass("blackAppear");
											
											$(".reservateBtn").css({"display":"inline-block"});
											$(".checkOutBtn").css({"display":"none"});
											$(".studentId").attr("disabled", "disabled");
											$(".bookId").attr("disabled", "disabled");
											$(".checkOutBtn").addClass("returnFeature");
											$(".reservateBtn").removeClass("returnFeature");
											addBookToReservationScreen(tmp);
										} else {
											alert("You borrowed successfull!!!");
											
											$(".borrowCard .borrower").html(borrowerContentTmp);
											$(".borrowCard .borrow_log").html(borrow_logContentTmp);
											$(".borrowCard .staff").html(staffContentTmp);
											$(".borrowCard .books").html(booksContentTmp);
											
											$(".borrowCard").addClass("reportContainerAppear");
											$(".black").addClass("blackAppear");
											
											reset();
											resetUI();
										}
									}
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
						}
						//kêt thúc sử lý mượn
					}
					//Ket thuc muon
				}
			}
		} else {
//			if (tmp.length > 0 ){
//				alert("Currently, there is no book for borrowing.\nPlease, come back later!\nLet move to reservation screen.");
////				$(".reservateBtn").css({"display":"inline-block"});
////				$(".checkOutBtn").css({"display":"none"});
//				$(".studentId").attr("disabled", "disabled");
//				$(".bookId").attr("disabled", "disabled");
//				$(".checkOutBtn").addClass("returnFeature");
//				$(".reservateBtn").removeClass("returnFeature");
//				addBookToReservationScreen(tmp);
//			} else {
//				reset();
//				resetUI();
//				alert("Currently, there is no book for borrowing.\nPlease, come back later!");
//			}
			console.log(2)
		}
		//
			
	});
	//end checkOutBtn clicked
	
	//ReservateBtn clicked
	$(".reservateBtn").click(function() {
		var isChanged = false;
		$(".listBooksContainer .listBooks .bookResult").each(function(){
			if($(this).css("background-color") == "rgb(255, 0, 0)"){
				isChanged = true;
				return false;
			}
		});
		var confirmReser = false;
		if (isChanged) {
			confirmReser = confirm("There are several books that remove from order list.\nStill reservate?");
			if (confirmReser) {
				reservate(tmp);
			} else
				console.log("Có change nhưng chưa đặt");
		} else {
			confirmReser = confirm("Reservate without any change in order list.");
			if (confirmReser) {
				reservate(tmp);
			} else
				console.log("Không change nhưng chưa đặt");
		}
		console.log(tmp);
	});
	//
	
	
	//Reset all
	function reset(){
		books_borrow_list = [];
		books_out_of_available_list = [];
		books_out_of_available_list_string = "";
		
		$(".listBooks").html("");
		$(".studentBorrow").html("");
		if ($(".returnInfo .bookResult")[0] != undefined)
			$(".returnInfo .bookResult").remove();
	}
	//End reset all
	
	//Reservate
	function reservate(arr) {
		$(".listBooksContainer .listBooks .bookResult").each(function(){
			if($(this).css("background-color") == "rgb(255, 0, 0)"){
				arr.splice($.inArray($(this).attr("id"), arr),1);
//				console.log($(this));
			}
		});
		
		if (arr.length < 1){
			alert("Currently, there is no book for ordoring?");
			reset();
			resetUI();
		} else {
			var reservate_date = new Date();
			reservate_date = reservate_date.getFullYear() + "-" + (reservate_date.getMonth()+1) + "-" + 
			reservate_date.getDate() + " " + reservate_date.toString().slice(16, 24);
			var current_id;
			$.ajax({
				url : "../reservateController/currentid",
				type: "GET",
				data: "",
				contentType: "text/plain; charset=utf-8",
				success: function(data) {
					current_id = data;
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
			
			//
			borrowerContentTmp = borrowerContent;
			borrowerContentTmp = borrowerContentTmp.replace("Borrowwer", "Reservation person");
			borrowerContentTmp = borrowerContentTmp.replace("#Borrower", "Reservation person");
			borrowerContentTmp = borrowerContentTmp.replace("#borrower_id", studentTmp.id.id);
			borrowerContentTmp = borrowerContentTmp.replace("#borrower_name", studentTmp.name);
			
			borrow_logContentTmp = borrow_logContent;
			borrow_logContentTmp = borrow_logContentTmp.replace("Borrow", "Reservation");
			borrow_logContentTmp = borrow_logContentTmp.replace("Borrow", "Reservation");
			borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_id", current_id);
			borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_date", reservate_date);
//			borrow_logContentTmp = borrow_logContentTmp.replace("#return_date", return_date);
			
			staffContentTmp = staffContent;
			staffContentTmp = staffContentTmp.replace("#staff_id", staffTmp.id.id);
			staffContentTmp = staffContentTmp.replace("#staff_name", staffTmp.name);
			
			booksContentTmp = booksContent;
			//
			
			for(var i = 0; i < arr.length; i++){
				$.ajax({
					url : "../reservateController/reservate?book_id=" + arr[i] + "&person_id=" + studentTmp.id.id + 
					"&reservate_date=" + reservate_date + "&staff_id=" + staffTmp.id.id + 
					"&current_id=" + current_id,
					type: "POST",
					data: "",
					contentType: "text/plain; charset=utf-8",
					success: function(data) {
						console.log(`${i}`);
						
						$.ajax({
							url: "../bookController/book?book_id=" + `${arr[i]}`,
							type: "GET",
							data: "",
							contentType: "application/json;charset=utf-8",
							success: function(data) {
								bookContentTmp = bookContent;
								var due_date = "";
								$.ajax({
									url: "../reservateController/getduedate?book_id="+data.id.id+"&borrow_log_id=" + current_id,
									type: "GET",
									data: "",
									contentType: "text/plain;charset=utf-8",
									success: function(data) {
										due_date = data.due_date;
									},
									async: false,
									error: function (e) {
										console.log(e);
									}
								});
								
								bookContentTmp = bookContentTmp.replace("#book", `${i+1}. ` + data.id.id + " - " + data.name + " - Due date : " + due_date);
								booksContentTmp += bookContentTmp;
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
						
						if (`${i}` == `${arr.length-1}`){
							alert("Reserved successfull!!!");
							
							$(".reservateCard .borrower").html(borrowerContentTmp);
							$(".reservateCard .borrow_log").html(borrow_logContentTmp);
							$(".reservateCard .staff").html(staffContentTmp);
							$(".reservateCard .books").html(booksContentTmp);
							$(".reservateCard .borrow_log div:last-child").remove();
							
							$(".reservateCard").addClass("reportContainerAppear");
							$(".black").addClass("blackAppear");
							
							reset();
//							resetUI();
							$(".feature button:nth-child(4)").click();
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
		}
		console.log(tmp);
	}
	//
	//Return
	$(".returnInfo .search button").click(function() {
		returnFunc();
	});
	//End of return
	
	//Return
	var borrower_return;
	var staff_return;
	var borrow_log_return;
	var books_return_list;
	function returnFunc(){
		var borrow_log_id;
		var returnCardTmp = returnCard;
		var innerHTML;
		books_return_list = [];
		if ($(".returnInfo .book input").val().trim().length != 0){
			if (isNaN($(".returnInfo .book input").val().trim()) || parseInt($(".returnInfo .book input").val().trim()) < 0){
				alert("Borrow card id must be a positive number!");
				document.querySelector(".returnInfo .book input").focus();
			}else {
				borrow_log_id = $(".returnInfo .book input").val().trim();
				
				var isExist = false;
				//Kiểm tra có tồn tại không
				$.ajax({
					url: "../borrowController/getborrowlog?borrow_log_id=" + 
					borrow_log_id,
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						if (data == "") {
							alert("This record does not exist!!!");
							isExist = true;
							$(".feature button:nth-child(4)").click();
							return;
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				if (isExist) return;
				//Kết thúc kiểm tra có tồn tại không
				var brokenList = [];
				//Lấy thông tin thẻ mượn
				$.ajax({
					url : "../borrowController/borrowlog?borrow_log_id=" + borrow_log_id,
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						if (data[0].status != false){
							alert("This already returned!!!");
							reset();
						} else {
							//0.borrow_log, 1.borrower, 2.staff
							borrow_log_return = data[0];
							staff_return = data[2];
							borrower_return = data[1];
	//						console.log(data);
							if ($(".returnInfo .bookResult") != null)
								$(".returnInfo .bookResult").remove();
							returnCardTmp = returnCardTmp.replace("#id", data[0].id.id);
							returnCardTmp = returnCardTmp.replace("#borrower_id", data[1].id.id);
							returnCardTmp = returnCardTmp.replace("#borrower", data[1].name);
							returnCardTmp = returnCardTmp.replace("#borrow", data[0].borrow_date);
							returnCardTmp = returnCardTmp.replace("#return", data[0].return_date);
							returnCardTmp = returnCardTmp.replace("#status", data[0].status==false?"Uncompleted":"Completed");
							returnCardTmp = returnCardTmp.replace("#staff", data[2].name);
							
							$(".returnInfo").html($(".returnInfo").html() + returnCardTmp);
							$(".returnInfo .search button").click(function(){
								returnFunc();
							});
							
							console.log(borrow_log_return);
							console.log(borrower_return);
							console.log(staff_return);
							//Lấy những quyển sách thuộc thẻ này
							var book_returnTmp = bookReturn;
							$.ajax({
								url : "../borrowController/borrowlogdetails?borrow_log_id=" + borrow_log_return.id.id,
								type: "GET",
								data: "",
								contentType: "application/json; charset=utf-8",
								success: function(data) {
									books_return_list = data;
//									console.log(books_return_list);
									var tmp;
									document.querySelector(".listBooks").innerHTML = "";
									for (var i = 0; i < books_return_list.length; i++){
										for (var j = 0; j < 10; j++){
											book_returnTmp = book_returnTmp.replace("#order", i);
										}
										book_returnTmp = book_returnTmp.replace("#penalties_book_id", books_return_list[i].id.id);
										book_returnTmp = book_returnTmp.replace("#divId", books_return_list[i].id.id);
										book_returnTmp = book_returnTmp.replace("#id", books_return_list[i].id.id.substring(0, books_return_list[i].id.id.indexOf(",")));
										book_returnTmp = book_returnTmp.replace("#name", books_return_list[i].name);
										book_returnTmp = book_returnTmp.replace("#timeprint", books_return_list[i].timePrint);
										book_returnTmp = book_returnTmp.replace("#ymanufacuture", books_return_list[i].ymanufacture.substring(0, books_return_list[i].ymanufacture.indexOf(",")));
										book_returnTmp = book_returnTmp.replace("#manufacturer", books_return_list[i].manufacturer);
										book_returnTmp = book_returnTmp.replace("#category", books_return_list[i].ymanufacture.substring(books_return_list[i].ymanufacture.indexOf(",")+1, books_return_list[i].ymanufacture.indexOf("-")));
										book_returnTmp = book_returnTmp.replace("#img", books_return_list[i].cover_image);
										
										tmp = parseInt(books_return_list[i].ymanufacture.substring(books_return_list[i].ymanufacture.lastIndexOf("-")+1));
										console.log(books_return_list[i].ymanufacture.substring(books_return_list[i].ymanufacture.lastIndexOf("-")+1));
										if (tmp == 1){
											book_returnTmp = book_returnTmp.replace("#pen_1", "checked");
										} else if (tmp == 2) {
											book_returnTmp = book_returnTmp.replace("#pen_2", "checked");
										} else if (tmp == 3) {
											//borrow_log_return : borrow_log_id, person_id
											//book_id
											//Get percentage of this book
											console.log(books_return_list[i].id.id.substring(0, books_return_list[i].id.id.indexOf(",")));
											console.log(borrow_log_return.id.id);
											console.log(borrower_return.id.id);
											$.ajax({
												url: "../homeController/otherpercentagereturn?" +
														"borrow_log_id=" + borrow_log_return.id.id +
														"&person_id=" + borrower_return.id.id + 
														"&book_id=" + books_return_list[i].id.id.substring(0, books_return_list[i].id.id.indexOf(",")),
												type: "GET",
												data: "",
												contentType: "application/json;charset=utf8",
												success: function(data) {
													brokenList.push(data);
												},
												async: false,
												error: function (e) {
													console.log(e);
												}
											});
											book_returnTmp = book_returnTmp.replace("#pen_3", "checked");
										} else if (tmp == 4) {
											book_returnTmp = book_returnTmp.replace("#pen_4", "checked");
										} else if (tmp == 5){
											book_returnTmp = book_returnTmp.replace("#pen_5", "checked");
										}
										
										document.querySelector(".listBooks").innerHTML = document.querySelector(".listBooks").innerHTML + book_returnTmp;
										
										
										book_returnTmp = bookReturn;
									}
								},
								async: false,
								error: function (e) {
									console.log(e);
								}
							});
							//
//							console.log(books_return_list);
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				//
				
				console.log(brokenList);
				var count = 0;
				for (var i = 0; i < books_return_list.length; i++){
					$(`.bookResult:nth-child(${i+1}) .penalties input`).each(function() {
						if ($(this).prop("checked") == true) {
							if (parseInt($(this).val()) == 3) {
								$(this).data("broken", brokenList[count++].id);
							}
						}
					});
				}
				
				//Set broken level cho quyển đc chọn hư
				$(".otherContainer input").each(function(index) {
					$(this).click(function() {
						$($(".penalties div:nth-child(3) input")[parseInt($(".otherContainer button#percentageof").val())]).data("broken", $(this).val());
//						console.log($($(".penalties div:nth-child(3) input")[parseInt($(".otherContainer button#percentageof").val())]).data("broken"));
					});
				});
				//
				
				$(".penalties div:nth-child(3) input").each(function(index) {
					var index = index;
					$(this).click(function() {
						console.log(books_return_list[index]);
						$(".black").addClass("blackAppear");
						$(".otherContainer").addClass("reportContainerAppear");
						$(".otherContainer button#percentageof").val(index);
						if ($(this).data("broken") != "")
							$(".otherContainer input[value="+$(this).data("broken")+"]").click();
						else {
							$(".otherContainer input[value=1]").click();
							$($(".penalties div:nth-child(3) input")[parseInt($(".otherContainer button#percentageof").val())]).data("broken", 1);
						}
						
						$(".otherContainer label").each(function(){
							console.log($($(this).children()[0]));
							$($(this).children()[1]).text(books_return_list[index].price);
							$($(this).children()[2]).text((parseInt($($(this).children()[0]).text())/100)*books_return_list[index].price)
						});
					});
				});
				
				for (var i = 0; i < books_return_list.length; i++){
					$(`.bookResult:nth-child(${i+1}) .penalties input`).each(function() {
						if ($(this).prop("checked") == true && $(this).val() == 4) {
//							$(this).data("broken", books_return_list[i].ymanufacture.subtring(books_return_list[i].ymanufacture.lastIndexOf(",")+1));
							$(".bookResult:nth-child(" + (i+1) + ") .penalties input").attr("disabled", true);
						}
					});
				}
			}
		}else {
			alert("You have not input borrow card id!!!");
			document.querySelector(".returnInfo .book input").focus();
		}
		
	}
	//
	//checkInBtn clicked
	$(".checkInBtn").click(function() {
		if ($(".returnInfo .bookResult").length == 1){
			for (var i = 0; i < books_return_list.length; i++){
				$(`.bookResult:nth-child(${i+1}) .penalties input`).each(function() {
					if ($(this).prop("checked") == true) {
						if (books_return_list[`${i}`].id.id.indexOf(",") > 0)
//							books_return_list[`${i}`].id.id = books_return_list[`${i}`].id.id.substring(0, books_return_list[`${i}`].id.id.indexOf(","));
//							books_return_list[`${i}`].id.id += ("-"+ $(this).val());
						if (parseInt($(this).val()) == 3) {
							books_return_list[`${i}`].id.id += ("-"+ $(this).val()+","+$(this).data("broken"));
						}else {
							books_return_list[`${i}`].id.id += ("-"+ $(this).val()+","+0);
						}
					}
				});
			}
			console.log(books_return_list);
			//
			borrowerContentTmp = borrowerContent;
			borrowerContentTmp = borrowerContentTmp.replace("#borrower_id", borrower_return.id.id);
			borrowerContentTmp = borrowerContentTmp.replace("#borrower_name", borrower_return.name);
			
			borrow_logContentTmp = borrow_logContent;
			borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_id", borrow_log_return.id.id);
			borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_date", borrow_log_return.borrow_date);
			borrow_logContentTmp = borrow_logContentTmp.replace("#return_date", borrow_log_return.return_date);
			
			staffContentTmp = staffContent;
			staffContentTmp = staffContentTmp.replace("#staff_id", staff_return.id.id);
			staffContentTmp = staffContentTmp.replace("#staff_name", staff_return.name);
			
			booksContentTmp = booksContent;
			//
			
			var notifyReturn = "Borrow Card\nBorrow id : " + borrow_log_return.id.id + "\n" +
			"Borrower : " + borrower_return.name + "\n" +
			"Borrow date : " + borrow_log_return.borrow_date + "\n" +
			"Return date : " + borrow_log_return.return_date + "\n" +
			"Staff : " + staff_return.name + "\n" +
			"List book : \n";
			var end = 0;
			var current_date = new Date();
			current_date = current_date.getFullYear() + "-" + (current_date.getMonth()+1) + "-" + 
			current_date.getDate() + " " + current_date.toString().slice(16, 24);
			for (var i = 0; i < books_return_list.length; i++){
				if (i == books_return_list.length - 1)
					end = 1;
				console.log(books_return_list[i].id.id);
				$.ajax({
					url : "../returnController/return?data=" + books_return_list[i].id.id + "&borrow_log_id=" + 
						borrow_log_return.id.id + "&end=" + end + "&current_date=" +current_date+"&person_id=" + borrower_return.id.id,
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						console.log(data);
						notifyReturn += (`${i+1}. ` + data.text.replace("#", "\n") + "\n");
						
						bookContentTmp = bookContent;
						bookContentTmp = bookContentTmp.replace("#book", `${i+1}. ` + data.text.replace("#", "-"));
						booksContentTmp += bookContentTmp;
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
			//
			
			//Check xem trả đủ chưa để bật biến status lên
			$.ajax({
				url : "../returnController/checknumbook?borrow_log_id=" + borrow_log_return.id.id,
				type: "GET",
				data: "",
				contentType: "application/json; charset=utf-8",
				success: function(data) {
					notifyReturn += ("\nGeneral status : " + data.text + ".");
					alert(data.text);
					resetAllReturn();
					
					$(".status").text("General status : " + data.text);
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
			
			$(".returnCard .borrower").html(borrowerContentTmp);
			$(".returnCard .borrow_log").html(borrow_logContentTmp);
			$(".returnCard .staff").html(staffContentTmp);
			$(".returnCard .books").html(booksContentTmp);
			
			$(".returnCard").addClass("reportContainerAppear");
			$(".black").addClass("blackAppear");
			
			console.log(notifyReturn);
			//
		} else {
			alert("You have not input borrow card id!!!");
			document.querySelector(".returnInfo .book input").focus();
		}
	});
	//
	
	//Receive book
	var booksTaken = [];
	var booksNotTaken = [];
	var reservation_person;
	var reservate_log;
	var booksForTaking = [];
	var staff_take;
	var takeInfo = "";
	$(".takeBtn").click(function() {
		booksTaken = [];
		if ($(".listBooks").children().length > 0) {
			$(".listBooks .bookResult").each(function(index) {
				if ($(this).css("background-color") == "rgb(76, 209, 55)")
					booksTaken.push($(this).prop("id"));
				else
					booksNotTaken.push(booksForTaking[index].id.id);
			});
			console.log(booksTaken);
			console.log(booksNotTaken);
//			return;
			if (booksTaken.length > 0){
				for (var i = 0; i < booksTaken.length; i++) {
					$.ajax({
						url : "../reservateController/take?reservate_log_id=" + reservate_log[0].id.id + "&book_id=" + booksTaken[i],
						type: "POST",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							reset();
							console.log("Take done");
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
				}
				
				borrowFunc(booksTaken, reservation_person, staff_take);
			} else {
				alert("You still have not choosen books in order to take!!!");
				return;
			}
			var end = 0;
			if (booksNotTaken.length > 0){
				for (var i = 0; i < booksNotTaken.length; i++) {
					if (i == booksNotTaken.length-1) end = 1;
					$.ajax({
						url : "../reservateController/nottake?reservate_log_id=" + reservate_log[0].id.id + 
								"&book_id=" + booksNotTaken[i] + 
								"&end=" +end,
						type: "POST",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							reset();
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
				}
			} else {
//				alert("You still have not choosen books in order to take!!!");
//				return;
			}
		} else {
			alert("You have not searched reservation card id!!!");
			document.querySelector(".takeInfo input").focus();
		}
	});
	$(".takeInfo .search button").click(function() {
		$(".studentBorrow").html("");
		$(".listBooks").html("");
		if ($(".takeInfo input").val().trim().length != 0) {
			if (isNaN($(".takeInfo input").val().trim()) || parseInt($(".takeInfo input").val().trim()) < 0) {
				alert("You must enter a positive number!!!");
				document.querySelector(".takeInfo input").focus();
			} else {
				
				var isExist = true;
				//Kiểm tra có tồn tại không
				$.ajax({
					url: "../reservateController/reservatelog?reservate_log_id=" + 
					parseInt($(".takeInfo input").val().trim()),
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						if (data[0] == null) {
							alert("This record does not exist!!!");
							isExist = false;
							$(".feature button:nth-child(4)").click();
							return;
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				if (!isExist) return;
				//Kết thúc kiểm tra có tồn tại không
				
				var isTaken = false;
				//Kiểm tra lấy xong chưa
				$.ajax({
					url: "../reservateController/taken?reservate_log_id=" + 
					parseInt($(".takeInfo input").val().trim()),
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						if (data.length == 1) {
							alert("This one already completed!!!");
							isTaken = true;
							return;
						}
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
				if (isTaken) return;
				//Kết thúc kiểm tra lấy xong chưa
				
				var bookTmp = book;
				var reservateCardTmp = reservationCard;
				var urlTmp = "";
				$.ajax({
					url: "../reservateController/reservatelog?reservate_log_id=" + 
					parseInt($(".takeInfo input").val().trim()),
					type: "GET",
					data: "",
					contentType: "application/json; charset=utf-8",
					success: function(data) {
						reservate_log = data;
						console.log(reservate_log);
						if (reservate_log[1].id.person_id.substring(0, 2) == "LE")
							urlTmp = "../lecturerController/lecturer?id=" + reservate_log[1].id.person_id;
						else
							urlTmp = "../studentController/student?id=" + reservate_log[1].id.person_id;
						staff_take = reservate_log[2];
						$.ajax({
							url: urlTmp,
							type: "GET",
							data: "",
							contentType: "application/json; charset=utf-8",
							success: function(data) {
								reservation_person = data;
								console.log(reservation_person);
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
						
						$.ajax({
							url: "../reservateController/bookfortaking?reservate_log_id=" + reservate_log[0].id.id,
							type: "GET",
							data: "",
							contentType: "application/json; charset=utf-8",
							success: function(data) {
								booksForTaking = data;
								console.log(booksForTaking);
//								if (booksForTaking.length == 0) {
////									$(".takeBook .takeBtn").addClass("returnFeature");
////									$(".takeBook .resetBtn").removeClass("returnFeature");
//									alert("This one already is taken!!!");
//								} else {
									reservateCardTmp = reservateCardTmp.replace("#id", reservate_log[0].id.id);
									reservateCardTmp = reservateCardTmp.replace("#reservation_person", reservation_person.name);
									reservateCardTmp = reservateCardTmp.replace("#reservation_date", reservate_log[0].reservate_date);
									reservateCardTmp = reservateCardTmp.replace("#status", booksForTaking.length>0?"Uncompleted":"Completed");
									reservateCardTmp = reservateCardTmp.replace("#staff", reservate_log[2].name);
									$(".studentBorrow").html($(".studentBorrow").html() + reservateCardTmp);
									
									if (booksForTaking.length > 0) {
										for (var i = 0; i < booksForTaking.length; i++){
											bookTmp = bookTmp.replace("#divId", booksForTaking[i].id.id);
											bookTmp = bookTmp.replace("#id", booksForTaking[i].id.id);
											bookTmp = bookTmp.replace("#name", booksForTaking[i].name);
											bookTmp = bookTmp.replace("#timeprint", booksForTaking[i].timePrint);
											bookTmp = bookTmp.replace("#ymanufacuture", booksForTaking[i].ymanufacture.substring(0, booksForTaking[i].ymanufacture.indexOf("-")));
											bookTmp = bookTmp.replace("#manufacturer", booksForTaking[i].manufacturer);
											bookTmp = bookTmp.replace("#manufacturer", booksForTaking[i].manufacturer);
											bookTmp = bookTmp.replace("#img", booksForTaking[i].cover_image);
											bookTmp = bookTmp.replace("#category", booksForTaking[i].ymanufacture.substring(booksForTaking[i].ymanufacture.indexOf("-")+1));
											
											$(".listBooks").html($(".listBooks").html() + bookTmp);
											bookTmp = book;
										}
									}
									
									$(".listBooks .bookResult").each(function(){
										$(this).dblclick(function() {
											$(this).toggleClass("enableBookTake");
										});
									});
									
									if ($(".listBooks .bookResult").length == 0) {
										alert("Currently, the book(s) that you reserved are not available now!!!");
										$(".feature button:nth-child(4)").click();
									}
//								}
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
						
						
					},
					async: false,
					error: function (e) {
						console.log(e);
					}
				});
			}
		} else {
			alert("You must enter the reservation card id first!!!");
			document.querySelector(".takeInfo input").focus();
		}
	});
	//
	
	function resetUI(){
		//Delete borrower
		$(".studentBorrow").html("");
		//
		
		//
		$(".reservateBtn").css({"display":"none"});
		//
		
		//
		$(".checkOutBtn").css({"display":"inline-block"});
		//
		
		//Reset student id input tag and book id input to empty
		$(".student input").val("")
		$(".book input").val("")
		$(".student input").prop("disabled", false);
		$(".book input").prop("disabled", false);
		//
		
		//Reset list book to empty
		$(".listBooks").html("")
		//
		
		$(".bookResultsContainer .bookResults").html("");
		$(".studentResultsContainer .studentResults").html("");
	}
	//
	
	//Function thêm sách cho việc đặt trước
	function addBookToReservationScreen(list){
		//Reset list book to empty
		$(".listBooks").html("")
		//
		for (var i = 0 ; i < list.length; i++){
			var bookTemp = book;
			$.ajax({
				url: "./book?id=" + list[i],
				type: "POST",
				data: "",
				dataType: 'json',
				contentType: "application/json; charset=utf-8",
				success: function (data) {
					bookTemp = bookTemp.replace("#divId", data[0][0].id.id);
					bookTemp = bookTemp.replace("#id", data[0][0].id.id);
					bookTemp = bookTemp.replace("#name", data[0][0].name);
					bookTemp = bookTemp.replace("#timeprint", data[0][0].timePrint);
					bookTemp = bookTemp.replace("#ymanufacuture", data[0][0].ymanufacture);
					bookTemp = bookTemp.replace("#manufacturer", data[0][0].manufacturer);
					bookTemp = bookTemp.replace("#category", data[1][0].name);
					bookTemp = bookTemp.replace("#borrow", data[0][0].is_borrowed == 1 ? "Borrowed" : "Available");
					bookTemp = bookTemp.replace("#img", data[0][0].cover_image);
	
					document.querySelector(".listBooks").innerHTML = document.querySelector(".listBooks").innerHTML + bookTemp;
	
					//Remove a book from list books (in borrow fearture)
					for (var i = 0; i < document.querySelectorAll(".listBooks .bookResult").length; i++) {
						document.querySelectorAll(".listBooks .bookResult")[i].addEventListener("dblclick", function () {
							this.classList.toggle("enableBook");
						});
					}
					//
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		}
	}
	//
	
	//Reset UI Return feature
	function resetAllReturn(){
		$(".returnInfo .bookResult").remove();
		
		$(".listBooksContainer .listBooks .bookResult").remove();
	}
	//
	
	//Nhắc trả sách trước 1 tuần
	function remindReturn() {
		$.ajax({
			url : "../borrowController/remindreturn",
			type: "POST",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
				console.log("Remind done");
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
	}
	remindReturn()
	//
	
	//Kiểm tra những record đặt trước đã đến hạn mà chưa nhận hay chưa và huy nếu chưa nhận
	function notTakenYet() {
		$.ajax({
			url : "../reservateController/nottaken",
			type: "POST",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function (data) {
				console.log("Bao het han xong");
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
	}
	notTakenYet();
	//
	
	//Mượn function
	function borrowFunc(list, studentTmp, staffTmp) {
		//Xử lý mượn
		var borrow_date = new Date();
		borrow_date = borrow_date.getFullYear() + "-" + (borrow_date.getMonth()+1) + "-" + 
		borrow_date.getDate() + " " + borrow_date.toString().slice(16, 24);
		var return_date = new Date(borrow_date);
		return_date.setHours(17,0,0,0);
		return_date.setDate(return_date.getDate() + 30);
		return_date = return_date.getFullYear() + "-" + (return_date.getMonth()+1) + "-" + 
		return_date.getDate() + " " + return_date.toString().slice(16, 24);
		var current_id;
		console.log(borrow_date);
		console.log(return_date);
		$.ajax({
			url : "../borrowController/currentid",
			type: "GET",
			data: "",
			contentType: "text/plain; charset=utf-8",
			success: function(data) {
				current_id = data;
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
		
		//
		borrowerContentTmp = borrowerContent;
		borrowerContentTmp = borrowerContentTmp.replace("#borrower_id", studentTmp.id.id);
		borrowerContentTmp = borrowerContentTmp.replace("#borrower_name", studentTmp.name);
		
		borrow_logContentTmp = borrow_logContent;
		borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_id", current_id);
		borrow_logContentTmp = borrow_logContentTmp.replace("#borrow_date", borrow_date);
		borrow_logContentTmp = borrow_logContentTmp.replace("#return_date", return_date);
		
		staffContentTmp = staffContent;
		staffContentTmp = staffContentTmp.replace("#staff_id", staffTmp.id.id);
		staffContentTmp = staffContentTmp.replace("#staff_name", staffTmp.name);
		
		booksContentTmp = booksContent;
		//
		
		for (var i = 0; i < list.length; i++){
			$.ajax({
				url : "../borrowController/borrow?book_id=" + list[i] + "&person_id=" + studentTmp.id.id + 
						"&borrow_date=" + borrow_date + "&return_date=" + return_date + 
						"&staff_id=" + staffTmp.id.id + "&current_id=" + current_id,
				type: "POST",
				data: "",
				contentType: "text/plain; charset=utf-8",
				success: function(data) {
					$.ajax({
						url: "../bookController/book?book_id=" + `${list[i]}`,
						type: "GET",
						data: "",
						contentType: "application/json;charset=utf-8",
						success: function(data) {
							bookContentTmp = bookContent;
							bookContentTmp = bookContentTmp.replace("#book", `${i+1}. ` + data.id.id + " - " + data.name);
						
							booksContentTmp += bookContentTmp;
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
				},
				async: false,
				error: function (e) {
					console.log(e);
				}
			});
		}
		
		$(".borrowCard .borrower").html(borrowerContentTmp);
		$(".borrowCard .borrow_log").html(borrow_logContentTmp);
		$(".borrowCard .staff").html(staffContentTmp);
		$(".borrowCard .books").html(booksContentTmp);
		
		$(".borrowCard").addClass("reportContainerAppear");
		$(".black").addClass("blackAppear");
		//kêt thúc sử lý mượn
		console.log("xong");
	}
	//
	
	//Make report area
	var thTableBorrowAndReturn = "<tr>\r\n" + 
	"                    <th>No.</th>\r\n" + 
	"                    <th>Borrower id</th>\r\n" + 
	"                    <th>Borrower name</th>\r\n" + 
	"                    <th>Borrow date</th>\r\n" + 
	"                    <th>Return date</th>\r\n" + 
	"                    <th id=\"removed\">Action</th>\r\n" + 
	"                </tr>";
	var thTableReservate = "<tr>\r\n" + 
	"                    <th>No.</th>\r\n" + 
	"                    <th>Reservation person id</th>\r\n" + 
	"                    <th>Reservation person name</th>\r\n" + 
	"                    <th>Reservation date</th>\r\n" + 
	"                    <th id=\"removed\">Action</th>\r\n" + 
	"                </tr>";
	var tdTableBorrowAndReturn = "<tr>\r\n" + 
	"                    <td>#borrow_id</td>\r\n" + 
	"                    <td>#borrower_id</td>\r\n" + 
	"                    <td>#borrower_name</td>\r\n" + 
	"                    <td>#borrow_date</td>\r\n" + 
	"                    <td>#return_date</td>\r\n" + 
	"                    <td id=\"removed\"><button id=\"#borrow_id\">More</button></td>\r\n" + 
	"                </tr>";
	var tdTableBorrowAndReturnTmp;
	var tdTableReservate = "<tr>\r\n" + 
	"                    <td>#reservation_id</td>\r\n" + 
	"                    <td>#reservation_person_id</td>\r\n" + 
	"                    <td>#reservation_person_name</td>\r\n" + 
	"                    <td>#reservation_date</td>\r\n" + 
	"                    <td id=\"removed\"><button id=\"#reservation_id\">More</button></td>\r\n" + 
	"                </tr>";
	var tdTableReservateTmp;
	var thTableMore = "<tr>\r\n" + 
	"                <th>No.</th>\r\n" + 
	"                <th>Book id</th>\r\n" + 
	"                <th>Book name</th>\r\n" + 
	"                <th>Category</th>\r\n" + 
	"                <th>Edition</th>\r\n" + 
	"                <th>Year manufacture</th>\r\n" + 
	"                <th>Manufacturer</th>\r\n" + 
	"                <th>Cover image</th>\r\n" + 
	"            </tr>";
	var tdTableMore = "<tr>\r\n" + 
	"                <td>#id</td>\r\n" + 
	"                <td>#book_id</td>\r\n" + 
	"                <td>#book_name</td>\r\n" + 
	"                <td>#book_category</td>\r\n" + 
	"                <td>#book_edition</td>\r\n" + 
	"                <td>#book_yearmanufacture</td>\r\n" + 
	"                <td>#book_manufacturer</td>\r\n" + 
	"                <td><img alt=\"\" src='/Library_management_system#img'></td>\r\n" + 
	"            </tr>";
	var tdTableMoreTmp;
	
	var tableBorrow;
	var tableReservate;
	var tableReturn;
	var tableBorrowList;
	var tableReservateList;
	var tableReturnList;
	var report_date_report;
	//Make report clicked
	$(".makeReportBtn").click(function() {
		$(".reportContainer").addClass("reportContainerAppear");
		$("nav").removeClass("navAppear");
		$(".toggle").removeClass("toggleMove");
		
		tableBorrow = $(".borrow table");
		tableReservate = $(".reservate table");
		tableReturn = $(".return table");
//		console.log(tableBorrow);
//		console.log(tableReservate);
//		console.log(tableReturn);
		
		//Get current date
		var current_date = new Date();
		current_date = current_date.getFullYear() + "-" + (current_date.getMonth()+1) + "-" + 
		current_date.getDate() + " " + current_date.toString().slice(16, 24);
		report_date_report = current_date;
		
		$(".report_date span").text(current_date);
		//The end of get current date
		
		//Add record into borrow table
		$.ajax({
			url: "../borrowController/recordswithoutreturn?current_date=" + current_date + "&status=0",
			type: "GET",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function(data) {
				//data is list of borrow records without return
				tableBorrowList = data;
//				console.log(tableBorrowList);
				
				var contentHtml = "";
				var borrower;
				var urlTmp;
				for (var i = 0; i < tableBorrowList[0].length; i++){
					
					if (tableBorrowList[1][i].id.person_id.substring(0, 2) == "LE")
						urlTmp = "../lecturerController/lecturer?id=" + tableBorrowList[1][i].id.person_id;
					else
						urlTmp = "../studentController/student?id=" + tableBorrowList[1][i].id.person_id;
					
					$.ajax({
						url: urlTmp,
						type: "GET",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							borrower = data;
//							console.log(borrower);
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					
					tdTableBorrowAndReturnTmp = tdTableBorrowAndReturn;
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrow_id", tableBorrowList[0][i].id.id);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrower_id", tableBorrowList[1][i].id.person_id);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrower_name", borrower.name);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrow_date", tableBorrowList[0][i].borrow_date);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#return_date", tableBorrowList[0][i].return_date);
					contentHtml += tdTableBorrowAndReturnTmp;
				}
				
				$(".borrow table").html(thTableBorrowAndReturn + contentHtml);
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
		//
		
		//Add record into reservate table
		$.ajax({
			url: "../reservateController/recordswithoutreturn?current_date=" + current_date,
			type: "GET",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function(data) {
				//data is list of reservation records
				tableReservateList = data;
//				console.log(tableReservateList);
				
				var contentHtml = "";
				var borrower;
				var urlTmp;
				for (var i = 0; i < tableReservateList[0].length; i++){
					
					if (tableReservateList[1][i].id.person_id.substring(0, 2) == "LE")
						urlTmp = "../lecturerController/lecturer?id=" + tableReservateList[1][i].id.person_id;
					else
						urlTmp = "../studentController/student?id=" + tableReservateList[1][i].id.person_id;
					
					$.ajax({
						url: urlTmp,
						type: "GET",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							borrower = data;
//							console.log(borrower);
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					
					tdTableReservateTmp = tdTableReservate;
					tdTableReservateTmp = 
						tdTableReservateTmp.replace("#reservation_id", tableReservateList[0][i].id.id);
					tdTableReservateTmp = 
						tdTableReservateTmp.replace("#reservation_person_id", tableReservateList[1][i].id.person_id);
					tdTableReservateTmp = 
						tdTableReservateTmp.replace("#reservation_person_name", borrower.name);
					tdTableReservateTmp = 
						tdTableReservateTmp.replace("#reservation_date", tableReservateList[0][i].reservate_date);
					contentHtml += tdTableReservateTmp;
				}
				
				$(".reservate table").html(thTableReservate + contentHtml);
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
		//
		
		//Add record into return table
		$.ajax({
			url: "../borrowController/recordswithoutreturn?current_date=" + current_date + "&status=1",
			type: "GET",
			data: "",
			contentType: "application/json; charset=utf-8",
			success: function(data) {
				//data is list of borrow records within return
				tableReturnList = data;
//				console.log(tableReturnList);
				
				var contentHtml = "";
				var borrower;
				var urlTmp;
				for (var i = 0; i < tableReturnList[0].length; i++){
					
					if (tableReturnList[1][i].id.person_id.substring(0, 2) == "LE")
						urlTmp = "../lecturerController/lecturer?id=" + tableReturnList[1][i].id.person_id;
					else
						urlTmp = "../studentController/student?id=" + tableReturnList[1][i].id.person_id;
					
					$.ajax({
						url: urlTmp,
						type: "GET",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							borrower = data;
//							console.log(borrower);
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					
					tdTableBorrowAndReturnTmp = tdTableBorrowAndReturn;
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrow_id", tableReturnList[0][i].id.id);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrower_id", tableReturnList[1][i].id.person_id);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrower_name", borrower.name);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#borrow_date", tableReturnList[0][i].borrow_date);
					tdTableBorrowAndReturnTmp = 
						tdTableBorrowAndReturnTmp.replace("#return_date", tableReturnList[0][i].return_date);
					contentHtml += tdTableBorrowAndReturnTmp;
				}
				
				$(".return table").html(thTableBorrowAndReturn + contentHtml);
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
		//
		
		//Get more button in table borrow
		if ($(".borrow table button").length > 0) {
//			console.log($(".borrow table button"));
			$(".borrow table button").each(function(index) {
				$(this).click(function() {
					console.log("Button more : "+index);
					$(".black2").addClass("black2Appear");
					$(".moreContainer").addClass("reportContainerAppear");
					
					//Get books in this borrow record
					$.ajax({
						url: "../borrowController/booksinborrowlog?borrow_log_id=" + tableBorrowList[0][index].id.id
						+"&type=0",
						type: "GET",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							console.log(data);
							var contentHtml = "";
							
							for (var i = 0; i < data.length; i++){
								tdTableMoreTmp = tdTableMore;
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#id", i);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_id", data[i].id.id.split("-")[0]);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_name", data[i].name);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_category", data[i].id.id.split("-")[1]);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_edition", data[i].timePrint);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_yearmanufacture", data[i].ymanufacture);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_manufacturer", data[i].manufacturer);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#img", data[i].cover_image);
								contentHtml += tdTableMoreTmp;
							}
							
							$(".moreContainer table").html(thTableMore + contentHtml);
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					//
				});
			});
		}
		//
		
		//Get more button in table reservate
		if ($(".reservate table button").length > 0) {
//			console.log($(".reservate table button"));
			$(".reservate table button").each(function(index) {
				$(this).click(function() {
					console.log("Button more : "+index);
					$(".black2").addClass("black2Appear");
					$(".moreContainer").addClass("reportContainerAppear");
					
					//Get books in this borrow record
					$.ajax({
						url: "../reservateController/booksinresevatelog?reservate_log_id=" + tableReservateList[0][index].id.id,
						type: "GET",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							console.log(data);
							var contentHtml = "";
							
							for (var i = 0; i < data.length; i++){
								tdTableMoreTmp = tdTableMore;
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#id", i);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_id", data[i].id.id.split("-")[0]);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_name", data[i].name);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_category", data[i].id.id.split("-")[1]);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_edition", data[i].timePrint);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_yearmanufacture", data[i].ymanufacture);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_manufacturer", data[i].manufacturer);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#img", data[i].cover_image);
								contentHtml += tdTableMoreTmp;
							}
							
							$(".moreContainer table").html(thTableMore + contentHtml);
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					//
				});
			});
		}
		//
		
		//Get more button in table return
		if ($(".return table button").length > 0) {
//			console.log($(".return table button"));
			$(".return table button").each(function(index) {
				$(this).click(function() {
					console.log("Button more : "+index);
					$(".black2").addClass("black2Appear");
					$(".moreContainer").addClass("reportContainerAppear");
					
					//Get books in this borrow record
					$.ajax({
						url: "../borrowController/booksinborrowlog?borrow_log_id=" + tableReturnList[0][index].id.id
								+"&type=1",
						type: "GET",
						data: "",
						contentType: "application/json; charset=utf-8",
						success: function(data) {
							console.log(data);
							var contentHtml = "";
							
							for (var i = 0; i < data.length; i++){
								tdTableMoreTmp = tdTableMore;
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#id", i);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_id", data[i].id.id.split("-")[0]);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_name", data[i].name);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_category", data[i].id.id.split("-")[1]);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_edition", data[i].timePrint);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_yearmanufacture", data[i].ymanufacture);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#book_manufacturer", data[i].manufacturer);
								tdTableMoreTmp = 
									tdTableMoreTmp.replace("#img", data[i].cover_image);
								contentHtml += tdTableMoreTmp;
							}
							
							$(".moreContainer table").html(thTableMore + contentHtml);
						},
						async: false,
						error: function (e) {
							console.log(e);
						}
					});
					//
				});
			});
		}
		//
	});
	//The end of make report clicked
	
	//Send report to manager clicked
	var reportMail = "<!DOCTYPE html>\r\n" + 
	"<html lang=\"en\">\r\n" + 
	"\r\n" + 
	"<head>\r\n" + 
	"    <meta charset=\"UTF-8\">\r\n" + 
	"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n" + 
	"    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\r\n" + 
	"    <title>Document</title>\r\n" + 
	"    <style>\r\n" + 
	"       #style_content\r\n" + 
	"    </style>\r\n" + 
	"</head>\r\n" + 
	"\r\n" + 
	"<body>\r\n" + 
	"    <div class=\"reportContainer\">\r\n" + 
	"        <h2 class=\"big_title\">Report</h2>\r\n" + 
	"        <h3 class=\"small_title\">Staff name : <span>#staff_name</span></h3>\r\n" + 
	"        <h3 class=\"small_title\">Staff id : <span>#staff_id</span></h3>\r\n" + 
	"        <h4 class=\"report_date\">Report date : <span>#report_date</span></h4>\r\n" + 
	"        <div class=\"borrow\">\r\n" + 
	"            #borrow_content\r\n" + 
	"        </div>\r\n" + 
	"        <div class=\"reservate\">\r\n" + 
	"            #reservate_content\r\n" + 
	"        </div>\r\n" + 
	"        <div class=\"return\">\r\n" + 
	"            #return_content\r\n" + 
	"        </div>\r\n" + 
	"        <a href=\"http://localhost:8080/Library_management_system/loginController/login\">LOGIN FOR MORE DETAILS</a>\r\n" + 
	"    </div>\r\n" + 
	"</body>\r\n" + 
	"\r\n" + 
	"</html>"
	var reportMailTmp;
	$(".sendReport").click(function() {
		var borrow_content = $(".borrow").html();
		var reservate_content = $(".reservate").html();
		var return_content = $(".return").html();
		
		while(borrow_content.indexOf("<th id=\"removed\">Action</th>") > 0 || borrow_content.indexOf("<td id=\"removed\"><button id=\"#borrow_id\">More</button></td>") > 0) {
			console.log(borrow_content.indexOf("<th id=\"removed\">Action</th>"));
			borrow_content = borrow_content.replace("<th id=\"removed\">Action</th>", "");
			borrow_content = borrow_content.replace("<td id=\"removed\"><button id=\"#borrow_id\">More</button></td>", "");
		}
		
		while(reservate_content.indexOf("<th id=\"removed\">Action</th>") > 0 || reservate_content.indexOf("<td id=\"removed\"><button id=\"#reservation_id\">More</button></td>") > 0) {
			console.log(reservate_content.indexOf("<th id=\"removed\">Action</th>"));
			reservate_content = reservate_content.replace("<th id=\"removed\">Action</th>", "");
			reservate_content = reservate_content.replace("<td id=\"removed\"><button id=\"#reservation_id\">More</button></td>", "");
		}
		
		while(return_content.indexOf("<th id=\"removed\">Action</th>") > 0 || return_content.indexOf("<td id=\"removed\"><button id=\"#borrow_id\">More</button></td>") > 0) {
			console.log(return_content.indexOf("<th id=\"removed\">Action</th>"));
			return_content = return_content.replace("<th id=\"removed\">Action</th>", "");
			return_content = return_content.replace("<td id=\"removed\"><button id=\"#borrow_id\">More</button></td>", "");
		}
		
		console.log(borrow_content);
		console.log(reservate_content);
		console.log(return_content);
		reportMailTmp = reportMail;
		reportMailTmp = reportMailTmp.replace("#report_date", report_date_report);
		reportMailTmp = reportMailTmp.replace("#staff_name", staffTmp.name);
		reportMailTmp = reportMailTmp.replace("#staff_id", staffTmp.id.id);
		reportMailTmp = reportMailTmp.replace("#borrow_content", borrow_content);
		reportMailTmp = reportMailTmp.replace("#reservate_content", reservate_content);
		reportMailTmp = reportMailTmp.replace("#return_content", return_content);
//		reportMailTmp = reportMailTmp.replace(/\s+/g, "");
		reportMailTmp = encodeURIComponent(reportMailTmp);
//		reportMailTmp = reportMailTmp.substring(0, 5000);
		console.log(reportMailTmp.length);
		var strs = reportMailTmp.match(/.{1,5000}/g);
		console.log(strs.length);
		//Send report
		$.ajax({
			url: "./sendreport?content=" + reportMailTmp+"&staff_id=" + staffTmp.id.id + "&report_date="+report_date_report,
			type: "POST",
			data: "",
			contentType: "application/json;charset=utf-8",
			success: function(data) {
				alert("Report sent to manager!!!");
				$(".reportContainer").removeClass("reportContainerAppear");
				$(".black").removeClass("blackAppear");
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
		//
	});
	//The end of send report to manager clicked
	
	//The end of make report area
	
	function listForIsFull(person_id) {
		//Kiểm tra đã mượn đủ 5 quyển chưa
		var list = [];
		$.ajax({
			url: "./full?person_id=" + person_id,
			type: "GET",
			data: "",
			contentTpe: "application/json;charset=utf-8",
			success: function(data) {
				if (data.length > 0) list = data;
			},
			async: false,
			error: function (e) {
				console.log(e);
			}
		});
		return list;
		//Kết thúc kiểm tra mượn đủ 5 quyển
	}
	
	//Preview image
	var input = document.querySelector('input[type=file]');

	input.addEventListener('change', updateImageDisplay);
	
	function updateImageDisplay() {
//		if (indexTmp == 4) $(".notify").text("");
		  var curFiles = input.files;
		  if(curFiles.length === 0) {
		  } else {
		    for(var i = 0; i < curFiles.length; i++) {
		      if(validFileType(curFiles[i])) {
		        var image = $(".staffProfile form .previewImage img");
		        console.log(window.URL.createObjectURL(curFiles[i]));
		        image.attr("src",window.URL.createObjectURL(curFiles[i]));
		      } else {
		      }
		    }
		  }
		}
	
	var fileTypes = [
		  'image/jpeg',
		  'image/pjpeg',
		  'image/png'
		]

		function validFileType(file) {
		  for(var i = 0; i < fileTypes.length; i++) {
		    if(file.type === fileTypes[i]) {
		      return true;
		    }
		  }

		  return false;
		}
	
	function returnFileSize(number) {
		  if(number < 1024) {
		    return number + 'bytes';
		  } else if(number >= 1024 && number < 1048576) {
		    return (number/1024).toFixed(1) + 'KB';
		  } else if(number >= 1048576) {
		    return (number/1048576).toFixed(1) + 'MB';
		  }
		}
	//The end review image
	
	//Set Staff avatar
	$(".profileBtn").click(function() {
		$(".staffProfile form .previewImage img").prop("src", "/Library_management_system" + $(".staffProfile input#avatarHidden").val());
		$(".staffProfile").addClass("reportContainerAppear");
		$(".black").addClass("blackAppear");
		document.querySelector("nav").classList.remove("navAppear");
		document.querySelector(".toggle").classList.remove("toggleMove");
	});
	var mailPattern = /^[0-9a-zA-Z]+@[0-9a-zA-Z]{4,7}(\.[a-z]{2,5}){1,3}$/;
	var indexTmp;
	$(".staffProfile form input").each(function(index) {
		$(this).keyup(function() {
			if ($(this).val().trim().length > 0 && index == indexTmp){
				$(".notify").text("");
				error = false;
			}
		});
	});
	var t = 0;
	var error = false;
	$(".staffProfile form").submit(function(e) {
		if (t == 0){
			if ($($(".staffProfile form input")[1]).val().trim().length == 0) {
				$(".notify").text("Staff name cannot be empty!");
				$("form input")[1].focus();
				indexTmp = 1;
				e.preventDefault();
				return;
			} else {
				if ($($(".staffProfile form input")[2]).val().trim().length == 0) {
					$(".notify").text("Staff email cannot be empty!");
					$(".staffProfile form input")[2].focus();
					indexTmp = 2;
					e.preventDefault();
					return;
				} else {
					if (!mailPattern.test($($(".staffProfile form input")[2]).val().trim())) {
						$(".notify").text("Staff email is invalid!");
						$(".staffProfile form input")[2].focus();
						indexTmp = 2;
						e.preventDefault();
						return;
					} else {
						$.ajax({
							url: "../staffController/staffcheckemail?email=" +$($(".staffProfile form input")[2]).val().trim(),
							type: "GET",
							data: "",
							contentType: "text/plain;charset=utf-8",
							success: function(data) {
								if (data == 1) {
									$.ajax({
										url: "../staffController/staff?staff_id=" +$($(".staffProfile form input")[0]).val().trim(),
										type: "GET",
										data: "",
										contentType: "application/json;charset=utf-8",
										success: function(data) {
											if (data.email == $($(".staffProfile form input")[2]).val().trim()) {
												error = false;
											} else {
												error = true;
												indexTmp = 2;
											}
										},
										async: false,
										error: function (e) {
											console.log(e);
										}
									});
								}
							},
							async: false,
							error: function (e) {
								console.log(e);
							}
						});
						if (error) {
							$(".notify").text("This email is already used!!!");
							$(".staffProfile form input")[2].focus();
							e.preventDefault();
							return;;
						} else {
							if ($($(".staffProfile form input")[3]).val().trim().length == 0) {
								$(".notify").text("Staff password cannot be empty!");
								$(".staffProfile form input")[3].focus();
								indexTmp = 3;
								e.preventDefault();
								return;
							} else {
									if ($($(".staffProfile form input")[5]).val().length == 0) {
	//								$(".notify").text("Staff avatar must be interted!");
									indexTmp = 4;
									var con = confirm("Do you want to keep the current avatar?");
									if (con) {
										t = 1;
										$(".notify").text("Profile update!!!");
										setTimeout(function(){$(".staffProfile form").submit();},1500);
									}else {
										e.preventDefault();
									}
								}
							}
						}
					}
				}
			}
		}
	});
	console.log(staffTmp);
	//Staff button reset
	$(".nameResetBtn").click(function() {
		$($(".name_container input")[0]).val(staffTmp.name);
		$(".notify").text("");
	});
	$(".emailResetBtn").click(function() {
		$($(".email_container input")[0]).val(staffTmp.email);
		$(".notify").text("");
	});
	$(".passwordResetBtn").click(function() {
		$($(".password_container input")[0]).val(staffTmp.password);
		$(".notify").text("");
	});
	$(".imgResetBtn").click(function() {
		$(".staffProfile input[type=file]").val("");
		$($(".staffProfile .previewImage img")[0]).prop("src", "/Library_management_system" + staffTmp.avatar);
		$(".notify").text("");
	});
	var saw = false;
	$(".seePassBtn").click(function() {
		if (!saw) {
			$($(".password_container input")[0]).prop("type", "text");
			saw = true;
		} else {
			$($(".password_container input")[0]).prop("type", "password");
			saw = false;
		}
		$(".seePassBtn").toggleClass("fa-eye-slash");
		$(".seePassBtn").toggleClass("fa-eye");
	});
	//The end staff button reset
	//The end set staff avatar
});